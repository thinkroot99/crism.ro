% Cum să reporniți și să închideți un sistem
% ThinkRoot99

Un sistem GNU/Linux oferă mai multe moduri diferite de a reporni sau de a opri un calculator. Un sistem normal ar trebui să răspundă la orice comandă, în timp ce un calculator degradat poate avea nevoie să fie repornit/oprit forțat.

Rețineți că majoritatea acestor comenzi vor necesita un anumit nivel de privilegii, fie ca superutilizator/administrator, fie ca sudo, pentru a fi executate cu succes, deoarece repornirea unui sistem este o sarcină administrativă și pentru un sistem multi-utilizator sau un server care rulează procese critice, repornirea sau oprirea este o activitate perturbatoare.

Acest articol acoperă câteva astfel de metode care vă pot ajuta cu activitatea de repornire/oprire.

# 1. Metoda GUI

Dacă utilizați un mediu de lucru GUI, cum ar fi GNOME, KDE Plasma, MATE, Xfce etc., veți găsi un buton de pornire în meniul sistemului care oferă acțiuni precum deconectare, repornire, hibernare sau oprire.

Opțiunile disponibile pot diferi ușor, în funcție de distribuția, mediul de lucru și versiunea sistemului de operare utilizat.

Dar cu siguranță veți găsi setări legate de alimentare în meniurile disponibile pentru a schimba starea de alimentare a sistemului vostru.

De exemplu, în distribuția Ubuntu MATE, opțiunea de închidere se poate găsi în meniul de alimentare din partea dreaptă sus a ecranului.

> ![Ubuntu MATE shutdown GUI](img/Ubuntu-MATE-shutdown-GUI.jpg)

Odată ce selectați opțiunea **Oprire**, vi se va afișa un dialog cu mai multe opțiuni precum **Suspendare**, **Repornire**, **Anulare** și **Închidere**. Puteți fie să continuați cu repornirea sau oprirea făcând clic pe butonul din dreapta sau să anulați și să reveniți la mediul de lucru.

> ![Ubuntu MATE shutdown](img/Ubuntu-MATE-shutdown.jpg)

Interfața grafică și opțiunile oferite pot diferi ușor în funcție de distribuția și mediul de lucru pe care îl utilizați, dar fiți sigur că există o opțiune pentru a schimba starea de alimentare a sistemului. Și cu GUI, nu sunt mai mult de câteva clicuri în meniul de alimentare.

# 2. Comanda systemctl

Pe un calculator care rulează GNU/Linux cu [systemd](https://en.wikipedia.org/wiki/Systemd), puteți utiliza comanda `systemctl` pentru a reporni mașina.

    $ sudo systemctl start reboot.target

Sau puteți utiliza pur și simplu `systemctl` cu opțiunea necesară, cum ar fi pentru repornire:

    $ sudo systemctl reboot

Pentru a iniția o închidere, comanda este astfel:

    $ sudo systemctl shutdown

Pentru a opri sistemul, utilizați:

    $ sudo systemctl halt

# 3. Comanda shutdown

Comanda `shutdown` poate fi folosită pentru a închide, precum și pentru a reporni un sistem GNU/Linux. Pentru a reporni imediat un sistem, utilizați comanda:

    $ sudo shutdown -r now

Sau pentru a închide un sistem fără a aștepta, utilizați indicatorul `-h` sau `-P`.

    $ sudo shutdown -P now
    $ sudo shutdown -h now

Putem adăuga o întârziere la comanda `shutdown` pentru a reporni/închide sistemul după un interval specificat. Acest lucru poate fi realizat astfel:

    $ sudo shutdown -r 10

Pentru a închide fără a cere hardware-ului să se oprească, puteți utiliza indicatorul `-H`.

    $ sudo shutdown -H now

Dacă pur și simplu rulați comanda `shutdown simplu`, închiderea va fi programată după un minut. Pentru a opri sistemul la o anumită oră, utilizați sintaxa:

    $ sudo shutdown 22:30

În multe cazuri, în timp ce un sistem multi-utilizator se oprește, acesta va difuza un mesaj către consola utilizatorilor conectați. Dacă doriți să adăugați un mesaj personalizat în această difuzare, puteți specifica mesajul împreună cu comanda shutdown ca:

    $ sudo shutdown 23:00 'Powering off system for maintenance...save your work'

Pentru a anula o oprire programată, puteți utiliza indicatorul `-c`.

    $ sudo shutdown -c

Pe unele sisteme, comanda `shutdown` rulează comanda `systemctl` în fundal pentru a obține operația de repornire/închidere dorită.

## 4. Comanda de repornire

Pentru a reporni un sistem, puteți utiliza și comanda `reboot` (poate fi necesar să o utilizați cu sudo).

    $ sudo reboot

Pentru a închide sistemul cu comanda de repornire, utilizați indicatorul `-p`.

    $ sudo reboot -p

Pentru a forța o repornire (pentru sistemele care nu răspund la comanda normală), puteți încerca indicatorul `-f`.

    $ sudo reboot -f

# 5. Comanda halt

Pentru a opri un sistem, puteți utiliza pur și simplu comanda `halt`.

    $ sudo halt

# 6. Comanda poweroff

Pentru a opri un sistem, aveți optiunea de a utiliza comanda `poweroff`.

    $ sudo poweroff

# 7. Comanda init

Pentru sistemele care nu rulează `systemd`, comanda `init` (va funcționa și `telinit`) oferă optiuni pentru a schimba nivelul de rulare al sistemului. Pentru a reporni un sistem folosind `init`, folosiți comanda:

    $ sudo init 6

În mod similar pentru a opri un sistem cu comanda `init`, se folosește:

    $ sudo init 0

Apelarea unui anumit nivel de rulare folosind `init` face ca procesul de inițializare al sistemului să execute o serie de scripturi de pornire într-o anumită ordine pentru a se asigura că sistemul atinge nivelul de rulare dorit.

Nivelul de rulare 6 este definit pentru repornire în timp ce nivelul de rulare 0 este definit pentru oprirea sistemului.

# 8. Butonul de pornire

Pentru GNU/Linux care rulează pe un calculator sau laptop, apăsarea implicită a butonului de pornire are acțiunea fie de a pune sisteul în stare de repaus, fie de a-l opri prin trimiterea semnalului de oprire.

Această opțiune poate fi ajustată din setările de alimentare al sistemului. Deși în mod ideal ar trebui să fie folosită ca ultimă opțiune, numai daca comenzile normale nu funcționează.

# 9. Combinația Alt+SysRq

Pentru un sistem înghețat, care nu răspunde, este greu să îl aduceți la o stare de repornire sau oprire.

Când nu aveți altă opțiune și ca ultimă soluție, puteți încerca să apăsați `Alt+PrintScreen+o_secvență_de_taste` pentru a reporni instantaneu sistemul. Este posibil să nu fie o opțiune preferată sau recomandată, dar poate fi totuși folosită ca ultimă soluție.

Amintiți-vă întodeauna că există posibilitatea pierderii și coruperii datelor folosind această metodă, așa cum o va avea orice altă opțiune de repornire sau oprire forțată.

Asigurați-vă cele mai bune practici de sistem pentru copii de rezervă și redundață și aveți grijă când utilizați această opțiune.

Pentru ca această opțiune să funcționeze, ar trebui să fie activată în nucleul Linux. Puteți verifica acest lucru utilizând comanda de mai jos:

    cat /proc/sys/kernel/sysrq

Afișarea cifrei `1` indică că este activată complet, în timp ce un număr mai mare indică că este parțial activată cu unele funcții. Pentru a-l activa în mod explicit, puteți utiliza:

    $ sudo echo "1" > /proc/sys/kernel/sysrq

Pentru a reporni un sistem folosind această metodă, țineți apăsat `Alt+SysRq (PrintScreen)` și apoi apăsați în secvență următoarele taste după un interval de câteva secunde între fiecare tastă:

    R E I S U B

sau pentru a opri apăsați `O` în loc de `B` la sfârșit:

    R E I S U O

Iată ce înseamnă tastele:

- **R:** comutați tastatura din modul brut în modul XLATE.
- **E:** trimit semnal SIGTERM către toate procesle, cu excepția init.
- **I:** trimite semnal SIGKILL la toate procesele, cu excepția init.
- **S:** sincronizează toate sistemele de fișiere montate.
- **U:** remontați toate sistemele de fișiere montate ca doar pentru citire.

În cele din urmă, avem fie B, fie O, care înseamnă:

- **B:** reporniți imediat sistemul, fără a demonta partițiile sau fără efectuarea sincronizări.
- **O:** închideți imediat sistemul, fără a demonta partițiile sau fără efectuarea sincronizări.

Aici sunt diferite moduri care vă permite să reporniți sau să închideți un sistem GNU/Linux. Sunt opțiuni, de la interfețe grafice simple până la comenzi de terminal relativ complexe care vă pot face viața mai ușoară sau chiar vă permit să automatzați lucruri.

Pentru sistemele care au înghețat dintr-un motiv sau altul, am discutat modalități forțate de a obține o stare de repornire sau oprire, care poate să nu fie modalitatea ideală, dar trebuie utilizată în rare ocazii.
