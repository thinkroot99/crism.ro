# Am implementat Feed RSS

De câteva zile îmi tot stă capul să implementez un Feed RSS pentru jurnal, nu pentru tot sit-ul. Îmi place această opțiune pentru jurnale pentru că pot fi mai ușor de urmărit.

În această după-masă m-am apucat de lucru și ca de obicei am apelat la AI (ChatGPT). Lui ChatGPT i-am cerut să îmi facă un script care să automatizeze totul, dar nu reușea să facă scriptul pe placul meu chiar dacă i-am dat instrucțiunile clare. ChatGPT are unele momente când nu vrea să funcționeze bine indiferent de cum îi ceri sau oferi instrucțiunile - acest lucru se întâmplă la varianta gratuită, la varianta cu abonament nu știu cum se comportă.

După câteva ore bune de trimis instrucțiuni lui ChatGPT, de testat script-uri, am renunțat până la urmă și am creat fișierul manual - am scris tot codul manual și l-am urcat prin FTP în contul de găzduire a sit-ului.

Acum a început a doua mare problemă.

Am apelat la [validatorul W3C](https://validator.w3.org/feed/) pentru a valida codul fișierului `feed.xml` și spre surpriza mea, validatorul mi-a raportat 3 sau 4 probleme. Exemplele din documentația W3C erau la fel ca și codul scris de mine, deci nu aveam probleme în cod.

După încă câteva verificări am început să verific Feed-ul RSS de la mai multe jurnale inclusiv de la [jurnalul W3C](https://www.w3.org/blog/) și surpriza cea mare este că nu am găsit nici un Feed RSS care să fie valid.

În acest caz am construit un fișier `.xml` cât mai simplu, totul făcut manual fără ajutorul AI-ului. După ce am terminat fișierul l-am testat într-un client RSS, și anume în [The Old Reader](https://theoldreader.com/) unde a funcționat cum trebuie. Am mai testat feed-ul și în programul `Newsboat`, unde a mers dar a dat ceva eroare la deschiderea navigatorului. Am mai testat feed-ul și în programul meu făcut cu ajutorul AI-ului, `ThinkRSS`, unde a mers fără probleme.

Treaba pe jumătate făcută pentru că nu am reușit să automatizez totul și fișierul `feed.xml` trebuie să îl editez manual de fiecare dată când public un articol. Dar nu este un deranj așa mare.


Pentru cei care doresc să se aboneze pot să folosească legătura [https://crism.ro/feed.xml](https://crism.ro/feed.xml), acest feed se găsește și în meniu.
