% Resetare parolă de root în Ubuntu - metoda 1
% ThinkRoot99

În acest articol, vă voi arăta cum se poate reseta parola de root în Ubuntu din meniul de boot (GRUB).

# Pasul 1: Reporniți sistemul de operare și încărcați meniul GRUB

Primul pas este să porniți calculatorul și să continuați prin apăsarea tastei `Shift` până când apare meniul GRUB, așa cum este în imaginea de mai jos.

> ![GNU GRUB](img/gnu-grub.png)

# Pasul 2: Deschiderea editorului de comenzii

Acum, selectați sistemul de operare pentru pornire, care este Ubuntu în cazul nostru și apăsați tasta `e` pentru a edita unele comenzi. Procedând astfel, se încarcă terminalul de comandă în modul root și prin apăsarea tastei `e` vă permite să aveți un ecran de editare, așa cum se arată în imaginea de mai jos.

> ![Edit Command in GRUB](img/edit-command-in-grub.png)

# Pasul 3: Editarea comenzii

După ce ați intrat în modul de editare a comenzilor, mergeți în jos până la ultima linie de comandă unde găsiți o comandă care începe cu `linux` și schimbați clauza acestei linii care este `ro quiet splash $vt_handoff` la `rw init=/bin/bash`, așa cum este și în imaginile de mai jos.

**Înainte**

    ro quiet splash $vt_handoff

> ![Edit Command GRUB - before](img/edit-command-grub-before.png)

**După**

    rw init=/bin/bash

> ![Edit Command GRUB - after](img/edit-command-grub-after.png)

# Pasul 4: Salvarea modificărilor

După ce ați editat linia pentru încărcarea modulului GRUB, apăsați tasta `F10` sau `Ctrl+x` pentru a salva și a porni sistemul de operare. După repornire, va apărea un ecran pentru linia de comandă în modul root.

> ![Command Line Root](img/command-line-root.png)

# Pasul 5: Confirmarea drepturilor de acces

În ecranul liniei de comandă trebuie să tastați comanda de mai jos pentru confirmarea privilegiilor de citire și scriere.

    # mount | grep -w /

> ![Confirmation of The Read and Write](img/confirmation-of-the-read-and-write.png)

# Pasul 6: Setarea noii parole

După confirmarea drepturilor de acces la citire și scriere, tastați comanda `passwd` și introduceți sau setați noua parolă de root.

    # passwd

> ![Set The New Password](img/set-the-new-password.png)

Puteți vedea că parola a fost actualizată cu succes.

# Pasul 7: Repornirea sistemului de operare

După actualizarea cu succes a parolei root, ultimul pas este să reporniți sistemul tastând comanda de mai jos:

    # exec /sbin/init

> **Notă:**
>
> Aceste modificări au fost efectuate pe Ubuntu 20.04. Nu garantez că acest tutorial mai este valabil și pentru ultima versiune de Ubuntu. Efectuați aceste modificări pe ultima versiune a Ubuntu pe propriile voastre riscuri.
>
> Acest tutorial se poate aplica și pe alte distribuții GNU/Linux.
