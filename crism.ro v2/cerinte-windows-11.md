% Cum să ocoliți cerințele de instalare pentru Windows 11
% ThinkRoot99

Lansarea lui Windows 11 a decurs într-un ritm fulminant. Noua versiune include multe shimbări, ceea ce înseamnă că vor exista probleme de compatibilitate cu care nu va-ți confruntat anterior în Windows 10.

Pentru Windows 11, calculatorul trebui să îndeplinească anumite cerințe stricte, ceea ce a provocat multă confuzie și frustrare în rândul utilizatorilor. Din fericire, există mai multe modalități de a ocoli cerințele de instalare și de a instala Windows 11 pe calculatoarele incompatibile, așa că nu totul este pierdut.

Haideți să trecem la treabă pentru a vedea cum putem elimina verificările de compatibilitate pentru Windows 11.

# Care sunt cerințele de instalare pentru Windows 11?

Este esențial să înțelegeți că specificațiile minime necesare pentru instalarea noii versiuni a sistemului de operare sunt diferite de cele recomandate. Iată ce este absolut necesar:

- Procesor Intel, AMD sau Qualcomm cu două nuclee pe 64 de biți care funcționează la 1 GHz sau mai rapid.
- Cel puțin 4 GB de memorie RAM.
- Cel puțin 64 GB de spațiu de stocare.
- UEFI Secure Boot acceptat și activat.
- Un modul de platformă de încredere (Trusted Platforme Module – TPM), versiunea 2.0.
- Suport DirectX 12 sau o versiune ulterioară, utilizând driverele WDDM 2.0.
- Afișaj de înaltă definiție (720p), monitor de 9″ sau mare, 8 biți pe canal de culoare.

Verificările stricte de compatibilitate ale Windows 11 sunt cele mai restrictive în ceea ce privește cerințele penru procesor, iar hardware-ul mai vechi are puține șanse să le treacă.

# Ce se întâmplă dacă instalați Windows 11 pe un hardware neacceptat?

Atunci când încercați să instalați Windows 11 pe un calculator incompatibil, veți primi un avertisment cu mențiunea „*Calculatorul dvs. nu va mai beneficia de asistență și nu va avea dreptul să primească actualizări*”.

Din fericire, Microsoft permite utilizatorilor să ocolească cerințele minime de instalare, permițându-le să folosească sistemul de operare fără probleme. Microsoft însuși a publicat o metodă de a face acest lucru, care implică modificarea **Editorului de registru** (Registry Editor).

Pur și simplu, instalarea Windows prin ocolirea cerințelor minime de instalare nu va afecta în nici un fel sistemul, deci este complet sigură! Puteți proceda cu una dintre metodele enumerare mai jos pentru a începe instalarea.

# 1. Utilizați intrumentul Windows11Upgrade Tool

Cel mai simplu este să ocoliți cerințele de instalare a Windows 11 cu ajutorul instrumentului Windows11Upgrade, care oferă opțiunea unei actualizări pe loc, precum și a unei instalări curate.

Iată cum îl puteți instala și utiliza:

1. Vizitați pagina oficială de pe GitHub a instumentului [Windows11Upgrade](https://github.com/coofcookie/Windows11Upgrade/releases/).
2. Faceți clic pe fișierul numit `Windows11Upgrade_EN.zip` și așteptați ca descărcarea să se finalizeze.
3. Fișierul descărcat trebui să fie în formatul `ZIP`. Navigați la locația de descărcare și faceți clic dreapta pe fișier.
4. Selectați `Extract All` din meniul contextual.

> ![Extract file](img/Extract-file.png)

5. După aceea, accesați dosarul extras, faceți clic dreapta pe fișierul **Windows11Upgrade** și selectați `Run as administrator` din meniul contextual.
6. Acum trebui să apară o fereastră pop-up cu două opțiuni. Faceți clic pe `Select Windows 11 ISO file` (Selectați fișierul ISO Windows 11) dacă aveți deja un fișier ISO. Dacă nu avevți un fișier ISO, faceți clic pe opțiunea `Download Windows 11 ISO file` (Descărcați fișierul ISO Windows 11).

> ![Windows11Upgrade pop-up window option](img/Windows11Upgrade-pop-up-window-option.png)

7. Localizați și selectați fișierul ISO.
8. Acum ar trebui să vă ofere trei opțiuni: **Upgrade**, **Data Only** și **Clean Install**.

> ![Windows 11 installation options](img/Windows-11-installation-options.png)

9. Alegeți opțiunea preferată și apoi faceți clic pe **Install System**.

Apoi, urmați instrucțiunile de pe ecran pentru a instala Windows 11.

# 2. Ocolirea cerințelor TPM 2.0 și Secure Boot

Trusted Module Platform (TPM) protejează datele împotriva pirateriei informatice și a altor amenințări la nivel hardware. Pe de altă parte, Secure Boot la nuvel UEFI împiedică pornirea oricărui sistem de operare care nu este autorizat de utilizator.

Mai jos am enumerat două modalități de a ocoli cerințele TPM 2.0 și Secure Boot. Procedați cu cea care vi se potrivește cel mai bine.

## 1. Utilizați Rufus

În această metodă, vom folosi un program terță numit Rufus pentru a ocoli cerințele TPM 2.0 și Secure Boot.

Iată ce trebui să faceți:

1. Dacă nu aveți deja fișierul ISO, iată ce trebui să faceți: acesați [pagina de descărcare a Windows 11](https://www.microsoft.com/en-us/software-download/windows11).
2. În secțiunea **Download Windows 11 Disk Image (ISO)**, selectați Windows 11 și apăsați pe **Download**.

> ![Download Windows 11 ISO](img/Download-Windows-11-ISO.png)

3. Confirmați alegerea limbii pentru a continua.
4. Faceți clic pe legătura de descărcare pe 64 de biți pentru a descărca fișierul ISO.
5. După aceea, descărcați Rufus – ultima versiune (versiunea 3.20 la scrierea acestui articol).
6. Pentru a rula Rufus, faceți **dublu clic** pe fișierul `exe`.
7. Apoi conectați unitatea USB la calculator.
8. Faceți clic pe **Select** pentru a selecta fișierul `ISO` pe care l-ați descărcat.
9. În fereastra de dialog, extindeți meniul derulant de la `Image option` și selectați opțiunea `Extended Windows 11 Installation (No TPM/no SEcure Boot/8GB- RAM)`. Alegând această opțiune, veți sări peste cerințele de Secure Boot și TPM 2.0 ale Windows 11, precum și peste cerința minimă de 8 GB de RAM.

> ![Rufus - No TMP option selected](img/Rufus-No-TMP-option-selected.png)

10. În cele din urmă, pentru a crea suportul de instalare faceți clic pe **Start**.

## 2. Utilizați Media Creation Tool

În cea de-a două metodă, vom folosi Media Creation Tool pentru a ocoli cerințele TPM 2.0 și Secure Boot. Rețineți, totuși, că această metodă va funcționa numai pentru **partițiile formatate** în sistemul de fișiere `NTFS`.

Dacă utilizați sistemul de fișiere `FAT32`, treceți la următoarea metodă de mai jos. În caz contrar, urmați pașii menționați mai jos pentru a continua:

1. Începeți a crea o unitate USB bootabilă pentru Windows 10 utilizând Media Creation Tool.
2. După finalizarea procesului, deschideți unitatea USB cu Windows 10 și ștergeți fișierele `install.esd` sau `install.wim` (aceste fișiere se găsesc în directorul x86/x64 > sources de pe stick). Este posibil să găsiți doar unul dintre aceste fișiere.
3. După ce ați eliminat fișierul, montați fișierul ISO cu Windows 11.
4. Acum mergeți din nou în acelați director și copiați fișierul `install.wim`.
5. Acum puteți lipi fișierul install.wim pe care l-ați copiat anterior pe unitatea USB cu Windows 10.
6. În etapa finală, porniți Windows-ul modificat de pe unitatea USB.

## 3. Modificarea registrului Windows (Windows Registry)

Puteți ocoli verificarea TPM 2.0 și a modelului de procesor utilizând această metodă. Cel puțin, calculatorul ar trebui să aibă TPM 1.2 pentru a continua.

De asemenea, este important să rețineți că Windows Registry este un utilitar de nivel avansat, motiv pentru care vă recomand să creați o copie de rezervă a Registrului înainte de a continua.

Crearea unei copii de rezervă vă va ajuta să restaurați versiunea curentă a Registrului Windows în cazul în care ceva nu merge bine.

Iată ce trebui să faceți:

1. Deschideți editorul de registru apăsând cobinația de taste `Win+R, tastați `regedit` și apăsați `Enter`.
2. În editorul de registru, mergeți la `HKEY_LOCAL_MACHINE > SYSTEM > Setup > MoSetup` în panoul din stânga al editorului de registru.
3. Faceți clic stânga pe `MoSetup` și selectați `New > DWORD (32-bit) Value`. Dați-i numele `AllowUpgradesWithUnsupportedTPMOrCPU`.
4. Faceți dublu clic pe funcția nou creată, introduceți cifra `1` în câmpul `Value Data` și apăsați pe `OK`.

> ![Windows 11 Unsupported Machine - Value Data](img/Windows-11-Unsupported-Machine-Value-Data.png)

5. În cele din urmă, închideți editorul de registru și reporniți calculatorul.

Calculatorul vostru este acum pregătit pentru Windows 11, chiar dacă nu îndeplinește cerințele minime.

Cu toate acestea, dacă intenționați să utilizați Windows 11 pentru o perioadă lungă de timp, ar putea merita să plătiți pentru un nou calculator.

Sper ca una dintre metodele enumerate mai sus a fost de folos. Este de remarcat faptul că Microsoft a acordat o mare atenție dezvoltării de noi caracteristici de securitate pentru Windows 11, care depind în mare parte de puterea de procesare a celor mai recente procesoare.

Dacă ați ajuns până aici și credeți că Windows 10 este mai potrivit pentru dvs. până când veți trece la un hardware compatibil, trebuie să știți că îl puteți folosi până în 2025.
