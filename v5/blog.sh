#!/bin/bash
set -eu

MARKDOWN="pandoc"

# Funcție pentru a extrage titlul din fișierul Markdown
get_title() {
    sed -n '/^# /{s/# //p; q}' "$1"
}

# Funcție pentru a genera pagina HTML de index
index_html() {
    local index_file="$1"

    # Printăm header-ul
    local title=$(get_title "$index_file")
    sed "s/{{TITLE}}/$(printf '%s\n' "$title" | sed 's/[\/&]/\\&/g')/" header.html

    # Textul introductiv
    $MARKDOWN -f markdown -t html "$index_file"

    # Lista de postări
    echo "<ul>"

    # Citim fiecare linie din fișierul index și extragem informațiile necesare
    while IFS=$'\t' read -r f title created updated; do
        if [[ "$f" != *".md" ]]; then
            continue  # Saltă liniile care nu corespund formatului așteptat
        fi
        local link=$(echo "$f" | sed 's|.*/\(.*\)\.md|\1.html|')
        echo "<li><a href=\"$link\">$title</a></li>"
    done < "$index_file"

    echo "</ul>"
}

# Funcție pentru a scrie pagina HTML individuală
write_page() {
    local filename="$1"
    local dir=$(dirname "$filename")
    local target="sit/$(basename "$filename" .md).html"

    created=$(date -r "$filename" "+%d %m %Y")
    updated=$(date -r "$filename" "+%d %m %Y")

    local title=$(get_title "$filename")

    $MARKDOWN -f markdown -t html "$filename" | \
        sed "$ a <small>Scris la $created / actualizat la $updated.</small>" | \
        cat header.html - | \
        sed "s|{{TITLE}}|$title|" \
        > "$target"
}

# Verificăm dacă directorul build există și îl ștergem înainte de a-l recrea
if [ -d "sit" ]; then
    rm -fr sit
fi
mkdir sit

# Generăm paginile HTML individuale pentru articolele din directorul posts și pages
for f in articole/*.md pagini/*.md; do
    [ -e "$f" ] || continue
    write_page "$f"
done

# Generăm pagina HTML de index
index_html index.md > sit/index.html

# Copiem fișierele statice (excluzând fișierele Markdown)
if [ -d "static" ]; then
    find static -type f ! -name '*.md' -exec cp {} sit \;
fi
