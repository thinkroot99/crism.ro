# Blogroll

Toată lumea știe ce este un blogroll, cel puțin bănuiesc că toată lumea știe ce este un blogroll. Dacă nu știți, definiția este mai jos:

> Un blogroll este o listă de legături spre alte bloguri sau sit-uri pe care un autor le recomandă sau le urmărește și le include pe propriul lui sit sau blog. Aceasta poate fi o modalitate de a promova alte surse de conținut și de a conecta cititorii cu alte resurse relevante sau de interes pentru subiectele abordate în blogul respectiv.

După cum puteți vedea am și eu un [blogroll](/liste.html) pe acest sit. Unde am salvat câteva sit-uri și mai multe bloguri. Dar vreau să schimb acest format pentru că am văzut un alt format mai interesant pe un sit mic și interesant.

Formatul pe care vreau să îl adopt este: salvarea/postarea de legături spre articolele interesante pe care le-am citit pe diverse sit-uri și bloguri. Și astfel nu voi mai salva legătura spre tot sit-ul/blogul. Dar încă mă gândesc dacă să pun aceste salvări pe categorii în funcție de data când am citit articolul sau să fac doar o lista lungă.

Sigur este mai util salvarea legăturile pe categorii și categoria este data când am citit articolul sau în loc de dată, să pun doar luna. Și mai jos am trecut câteva exemple.

## Listă lungă neordonată

- legătură articol 1
- legatură articol 2
- legătură articol 3

Și așa mai departe. Nu cred că este o varianta așa de bună.

## Listă cu categorie: Data

**03 iunie 2024**

- legătură articol 1
- legătură artciol 2
- legătură articol 3

**02 iunie 2024**

- legătură articol 1

**01 iunie 2024**

- legătură articol 1
- legătură artciol 2

Și așa mai departe. Este o variantă bunicică.

## Listă cu Categorie: Lună

**iunie 2024**

- legatură articol 1
- legătură articol 2
- legătură articol 3

**mai 2024**

- legătură articol 1
- legătură artciol 2
- legătură artciol 3

Și așa mai departe. Pare cea mai bună variantă.