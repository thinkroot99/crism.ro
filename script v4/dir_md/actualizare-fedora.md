# Configurare comandă actualizare pentru Fedora

Pentru actualizarea distribuției Fedora sau pentru orice altă distribuție îmi place să folosesc terminalul și utilizarea comenzii de actualizare pentru că se face mult mai repede decât în mediul grafic. Dar nu îmi place să folosesc o comandă prea lungă, care oricum este destul de lungă pentru orice distribuție Linux.

De exemplu comenzile de actualizare ale Fedora, Ubuntu și Arch Linux sunt:

    [Fedora] sudo dnf upgrade
    
    [Ubuntu] sudo apt update și urmat de sudo dnf upgrade
    
    [Arch Linux] sudo pacman -Syu

Din punctul meu de vedere aceste comenzi sunt prea lungi și mi-ar plăcea să văd o comanda de genul: `update` șși atât, nimic mai mult. Din păcate nu se poate face acest lucru doar dacă facem personal undele modificări prin sistem.

Comanda de actualizare a Fedora pe care o folosesc este mult mai lungă decât cea de mai sus, și aceasta este:

`flatpak update && sudo dnf upgrade --refresh`

Și pentru a nu mai scrie o comandă așa de lungă trebuie să creez un `alias` în fișierul `.bachrc` de fiecare dată când reinstalez/instalez ditsribuția. Astfel am ajuns să fac un script cu ajutorul AI-ului ChatGPT pentru că nu mă pricep să fac astfel de scripturi.

## Configurarea alias-ului și a permisiunilor sudo în Fedora

În pași care urmează voi descrie cum se adaugă un *alias* pentru actualizarea sistemului folosind `flatpak` și `dnf` și cum se configurează `sudo` pentru a nu mai cere parola la rularea comenzilor de actualizare.

Din punct de vedere al securități știu că nu este bine pentru a rula comanda `sudo` ffără a mai cere parola, dar cum laptopul nu părăsește casa nu prea îmi fac griji din acest punct de vedere.

În acest articol, voi descrie cum am adăugat un alias pentru actualizarea sistemului folosind `flatpak` și `dnf` și cum am configurat `sudo` pentru a nu cere parola la rularea comenzilor de actualizare. Dacă vă folosiți de acest articol/tutorial, puteți sări pasul cu configurarea `sudo`.

### Pasul 1: Adăugarea alias-ului în `.bashrc`

Pentru a adăuga un alias pentru actualizarea sistemului am modifica fișierul `~/.bashrc`.

1. Am deschis fișierul `~/.bashrc` într-un editor de text. De exemplu, cu `nano`:

    nano ~/.bashrc

În loc de nano puteți folosi orice editor de text preferat.

2. Am adăugat următoarea linie la sfârșitul fișierului:

    alias update='flatpak update --noninteractive && sudo dnf upgrade --refresh -y'

3. Am salvat și închis editorul (`Ctrl+O` pentru a salva în `nano`, apoi `Ctrl+X` pentru a ieși).

4. Am reîncarcă fișierul `~/.bashrc` pentru a aplica modificările:

    source ~/.bashrc

Și asta este tot pentru configurarea unui `alias` în fișierul `.bashrc`.

### Pasul 2: Configurarea `sudo` pentru a nu cere parola

Pentru a evita solicitarea parolei atunci când folosesc `sudo` pentru a actualiza sistemul, a trebuit să modific fișierul `sudoers`.

1. Am deschis fișierul `sudoers` folosind comanda `visudo`:

    sudo visudo

2. Am adăugat următoarea linie la sfârșitul fișierului pentru a îmi permite să ruleze `dnf` fără parolă. Trebuie înlocuit `username` cu numele tău de utilizator:

    username ALL=(ALL) NOPASSWD: /usr/bin/dnf

3. Am salvat și închis editorul `visudo`.

Am terminat și cu configurarea lui `sudo` pentru a nu mai cere parola. Și pe urmă am trecut la ChatGPT și i-am cerut să facă scriptul de mai jos pentru a face toate modificările de mai sus automat ca pe viitor să nu mai fiu nevoit să fac toate aceste lucruri manual. Este mult mai ușor cu rularea unui script.

### Pasul 3: Script pentru automatizarea configurărilor

Pentru a automatiza procesul de adăugare a alias-ului și configurarea `sudoers`, i-am cerut lui ChatGPT să creeze un script simplu.

În rândurile care urmează nu este discuția mea cu ChatGPT. Sunt pași pentru crearea scriptului și tot ce trebuie să faceți este să urmați ce scrie mai jos și să copiați codul scriptului la voi în fișier.

#### Crearea script-ului `setup_update.sh`

Creează un fișier numit `setup_update.sh` și adaugă următorul conținut:

    #!/bin/bash
    
    # Numele utilizatorului curent
    USER=$(whoami)
    
    # Adaugă alias-ul în ~/.bashrc dacă nu există deja
    if ! grep -q "alias update='flatpak update --noninteractive && sudo dnf upgrade --refresh -y'" ~/.bashrc; then
        echo "alias update='flatpak update --noninteractive && sudo dnf upgrade --refresh -y'" >> ~/.bashrc
        echo "Alias-ul 'update' a fost adăugat în ~/.bashrc"
    else
        echo "Alias-ul 'update' există deja în ~/.bashrc"
    fi
    
    # Creează o regulă sudoers specifică utilizatorului curent
    SUDOERS_RULE="$USER ALL=(ALL) NOPASSWD: /usr/bin/dnf"
    
    # Verifică dacă regula există deja
    if sudo grep -q "^$SUDOERS_RULE" /etc/sudoers; then
        echo "Regula pentru 'dnf' fără parolă există deja în sudoers"
    else
        # Adaugă regula direct în sudoers folosind visudo
        echo "$SUDOERS_RULE" | sudo EDITOR='tee -a' visudo
        if [ $? -eq 0 ]; then
            echo "Regula pentru 'dnf' fără parolă a fost adăugată"
        else
            echo "Eroare la adăugarea regulii în sudoers. Modificările nu au fost aplicate."
       fi
    fi
    
    # Reîncarcă ~/.bashrc
    source ~/.bashrc
    echo "~/.bashrc a fost reîncărcat"

Sau puteți să descărcați scriptul de pe GitLab, de [aici](https://gitlab.com/fedoratools/Setup-Update). Pe GitLab se găsesc și instrucțiuni de descărcare și folosire.

Acum urmează instrucțiunile de utilizare pentru cei care nu știu să folosească un script în Linux. Urmați pași de mai jos.

#### Instrucțiuni pentru rulare:

1. Creează sau actualizează fișierul `setup_update.sh` cu conținutul de mai sus.

2. Asigură-te că scriptul este executabil. Pentru asta trebuie executată comanda următoare - terminal-ul trebuie deschis acolo unde este scriptul, altfel nu va merge.

    chmod +x setup_update.sh

3. Pentru a rula scriptul se folosește comanda de mai jos. La un moment dat vi se cere și parola - tastați parola și scriptul va rula automat.

    ./setup_update.sh

## Concluzie

Dacă ai urmat pași de mai sus și ai făcut totul corect nu o sa fie probleme și pentru viitoarea actualizare a sistemului din terminal, comanda de actualizare este `update` fără nici o parolă.