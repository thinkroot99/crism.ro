% De ce scurtătura Alt+F2 este importantă
% ThinkRoot99

Știți că există o comandă rapidă de la tastatură în GNU/Linux care oferă acces rapid la toate programele și la câteva comenzi utile? Este ca o linie de comandă portabilă și probabil că este deja încorporată în distribuția GNU/Linux pe care o utilizați. Doar apăsați `Alt` și `F2` în același timp și veți descoperi despre ce vorbesc.

Majoritatea utilizatorilor GNU/Linux de lungă durată ar putea fie deja familiarizați cu comenzile rapide de la tastatură, dar noii convertiți la GNU/Linux ar putea să nu fi descoperit încă această comandă rapidă simplă.

# Ce este atât de special la această comandă rapidă?

Combinația de taste `Alt+F2` este indispensabilă pentru utilizatorii GNU/Linux. Indiferent dacă doriți să închideți forțat o aplicație blocată, să lansați rapid programul preferat sau să efectuați orice altă sarcină la care vă puteți gândi, `Alt+F2` este acolo pentru a vă ușura sarcina.

Această comandă rapidă de la tastatură funcționează implicit pe toate distribuțiile care folosesc mediile de lucru GNOME, KDE Plasma sau Xfce.

# Utilizări practice ale comenzii rapide Alt+F2

Când apăsați `Alt` și `F2` pe tastatură, veți vedea o fereastră simplă.

> ![Krunner Shortcut](img/krunner-shortcut.png)

Ar putea arăta usor diferit în funcție de distribuția pe care o folosiți. Un lucru este însă sigur: începeți să introduceți numele unui program și veți vedea rezultatele rapid.

> ![Krunner Brave Shortcut](img/krunner-brave-shortcut.png)

Utilizarea acestei comenzi rapide de la tastatură pentru a lansa programe necesită să cunoașteți numele comenzii lor, dar de obicei este destul de evident, deoarece majoritatea comenzilor sunt o variație sau o potrivire exactă a numelui aplicației lor. Doar introduceți numele programului, fără majuscule în majoritatea cazurilor.

După cum se vede în imaginea de mai sus, o funcție de completare automată la îndemână vă va ajuta cu o listă de programe legate de literele pe care le-ați tastat deja. Acest lucru poate face ca tastarea anumitor comenzi să fie foarte rapidă; tastați brave, de exemplu, și vor apărea toate versiunile instalate ale Brave.

> ![Krunner Install Software Shortcut](img/krunner-install-software-shortcut.png)

Puteți lansa orice program în acest fel; trebuie doar să știi comanda pentru acel program specific. Pe lângă deschiderea programelor, puteți efectua calcule matematice, puteți instala aplicații din centrul de programe, puteți căuta fișiere și directoare și multe altele, în funcție de distribuția pe care o folosiți.

> ![Krunner kcalc Shortcut](img/krunner-kcalc-shortcut.png)

Utilizați această comandă rapidă pentru a lansa programe pentru o perioadă și vă veți da rapid seama cât de rapid este în comparație cu găsirea și deschiderea manuală a programului din meniul de aplicații. În afară de lansarea programelor, puteți folosi chiar și această comandă rapidă pentru alte sarcini.

# Câteva comenzii minunate de încercat

Da, poți face ceva mai mult cu `Alt+F2`. De exemplu, dacă doriți să opriți un anumit program, încercați să tastați `xkill`. Această comandă vă va oferi un cursor pe care îl puteți utiliza pentru a forță închiderea oricăriui program făcând clic pe el, ceea ce este perfect atunci când o bucată de program sa blocat.

> ![Linux xkill command](img/linux-xkill.png)

Alternativ, puteți încerca comanda `killall`. Tastați `killall` urmat de numele programului pe care doriți să îl opriți și apăsați `Enter`. De exemplu, pentru a opri managerul de fișiere Dolphin, tastați `killall dolphin`, iar sistemul va închide forțat programul.

# Bucurați-vă de o experiență GNU/Linux mai bună

`Alt+F2` este o comandă rapidă excelentă pentru GNU/Linux care ar trebui implementată și pe Windows sau macOS.

Când doriți să lansați un program rapid, această comandă de la tastatură ofera rezultate.
