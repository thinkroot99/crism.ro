% Desktop Environment vs. Window Manager
% ThinkRoot99

Este posibil ca fiecare utilizator nou de GNU/Linux să se confrunte cu întrebarea care este diferența dintre un `desktop environment` și un `window manager` la un moment dat în procesul de învățare.

În acest articol, voi încerca să răspund la această întrebare din punct de vedere al unui nou utilizator de GNU/Linux.

#  Diferența dintre Desktop Environment și Window Manager

Este posibil ca majoritatea dintre voi să fi văzut acești doi termeni când descărcați o distribuție GNU/Linux. Mediul de lucru (Desktop Environment) este un termen foarte comun în universul GNU/Linux, în timp ce managerul de ferestre (Window Manager) este cunoscut printre utilizatori GNU/Linux mai avansați.

## Ce este un mediu de lucru (desktop environment)?

Orice mediu de lucru este o colecție de programe care ne permit să interacționăm cu sistemul nostru de operare într-o varietate de moduri. Un mediu de lucru include interfața grafică care este utilizată pentru a accesa meniul, aplicațiile deschise, aspectul sistemului și aplicațiile care vin cu acesta, inclusiv un manager de ferestre.

Există un număr mare de medii de lucru pentru GNU/Linux. Fiecare mediu de lucru are propria sa abordare pentru rezolvarea problemelor în îndeplinirea sarcinilor de zi cu zi.

Unele dintre cele mai populare sunt:

- [GNOME](https://www.gnome.org/)
- [Plasma](https://kde.org/plasma-desktop/)
- [Xfce](https://xfce.org/)
- [LXQt](https://lxqt-project.org/)
- [Cinnamon](https://projects.linuxmint.com/cinnamon/)

> ![GNOME](img/GNOME-Shell-41-with-GNOME-Web.png){ width=45% }
> ![Plasma](img/KDE-Plasma-5.23.png){ width=45% }
> ![Xfce](img/XFCE-4.14-on-Debian-11.png){ width=45% }
> ![LXQt](img/LXQt-1.0.0.png){ width=45% }
> ![Cinnamon](img/Cinnamon-4.4.8-on-Linux-Mint-19.3.png){ width=45% }

Când un mediu de lucru este eliminat dintr-un sistem de operare, sistemul rămâne cu o interfață de linie de comandă pentru a interacționa cu sistemul.

Înseamnă că un mediu de lucru nu este un instrument necesar pentru rularea unui sistem de operare, ci mai degrabă un set excelent de instrumente pentru utilizarea grafică a sistemului și pentru a-l face mai utilizabil pentru publicul vizat.

Cu toate acestea, în timp ce mediile de lucru au o serie de avantaje, pot exista și unele dezavantaje în utilizarea lor. De exemplu, mediul de lucru pe care îl utilizați în prezent poate veni preinstalat cu sute de aplicații pe care nu le veți folosi niciodată.

Majoritatea acestor aplicații consumă din resursele calculatorului și pot face calculatorul să încetinescă. Cea mai bună parte a programului open source este că oricine își poate implementa propriile idei și poate rezolva probleme, iar această problemă a fost rezolvată în mod semnificativ.

Comunitatea GNU/Linux a dezvoltat mai multe medii de lucru atunci când vine vorba de construirea unui sistem de operare ușor și rapid.

## Ce este un manager de ferestre (window manager)?

Un manager de ferestre, după cum sugerează numele, este pur și simplu un manager de ferestre. Este responsabil de poziționarea, dimensionarea și gestionarea ferestrelor sistemului de operare.

Există tipuri de gestionare de ferestre: `placare (tiling)` și `stivuire (stacking)`. Un manager de ferestre cu placare așează ferestrele unul în jurul celuilalt pentru a evita pierderea spațiului, în timp ce un manager de ferestre cu stivuire așează ferestrele una peste alta sau le mută oriunde pe ecran.

Cele mai populare managere de ferestre sunt:

- [i3wm](https://i3wm.org/)
- [dwm](https://dwm.suckless.org/)
- [awesomewm](https://awesomewm.org/)
- [IceWM](https://ice-wm.org/)
- [Openbox](http://openbox.org/)
- [Sway](https://swaywm.org/)

> ![i3wm](img/i3-with-vim-and-terminals-open.png){ width=45% }
> ![dwm](img/dwm-5.7.2-showing-urxvt-GIMP-and-Chromium.png){ width=45% }
> ![awesomewm](img/awesome-with-a-number-of-terminals-open.png){ width=45% }
> ![IceWM](img/JWM-on-Puppy-Linux.png){ width=45% }
> ![Openbox](img/Basic-Openbox-X-session.png){ width=45% }
> ![Sway](img/sway-wm.png){ width=45% }

S-ar putea să întrebați ce face un manager de ferestre diferit de un mediu de lucru. Fiecare mediu de lucru are un manager de ferestre; nu se poate altfel. Un manager de ferestre este doar o bucată de program, dar un mediu de lucru poate conține sute de alte programe.

Fiecare mediu de lucru are propriul manager de ferestre. De exemplu, GNOME folosește `mutter` ca manager de ferestre implicit, Cinnamon folosește `muffin`, iar Plasma folosește `KWin`.

# Care este mai bun, mediul de lucru sau managerul de ferestre?

Este determinat de nevoile utilizatorului. Un mediu de lucru oferă utilizatorilor un mediu care nu necesită configurare sau personalizare. Chiar și după aceea, dacă utilizatorul vrea să personalizeze mediul de lucru o poate face pentru că majoritatea mediile de lucru sunt foarte personalizabile.

Managerul de ferestre, pe de altă parte, este pentru utilizatorii care doresc să personalizeze totul de la zero sau să-și construiască propria aromă de GNU/Linux.

Un mediu de lucru vine preinstalat cu un număr mare de aplicații care necesită mai multe resurse, în timp ce un manager de ferestre poate fi instalat pe un sistem cu doar 100 MB RAM sau chiar mai puțin.
