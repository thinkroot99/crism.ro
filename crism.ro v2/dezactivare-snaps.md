% Dezactivare Snaps în Ubuntu 20.04
% ThinkRoot99

Primul lucru pe care îl fac după actualizarea sistemului, este să dezactivez Snaps și să dezinstalez toate aplicațiile snap.

Nu spun că această tehnologie nu este bună, din contră este foarte bună. Dar nu îmi place deocamdată să folosesc aplicații dezvoltate în snap.

Nu îmi plac aplicațiile snap pentru că nu se integrează cu tema sistemului și în plus mai pornesc și destul de greu.

# Eliminare snap-uri

După fiecare instalare pe curat, Ubuntu instalează câteva aplicații snap. Pentru a vedea lista cu aceste aplicații folosiți următoarea comandă:

    $ snap list
    Name               Version           Rev   Tracking         Publisher   Notes
    core               16-2.44.1         8935  latest/stable    canonical✓  core
    core18             20200311          1705  latest/stable    canonical✓  base
    gnome-3-34-1804    0+git.2c86692     24    latest/stable/…  canonical✓  -
    gtk-common-themes  0.1-30-gd41a42a   1502  latest/stable/…  canonical✓  -
    snap-store         20200415.e028804  394   latest/stable/…  canonical✓  -

Pentru a elimina aceste aplicații, trebuie să rulați comanda: `sudo snap remove (nume pachet)`.

Rulați următoarele comenzii pentru a îndepărta toate snap-urile:

    $ sudo snap remove snap-store
    $ sudo snap remove gtk-common-themes
    $ sudo snap remove gnome-3-34-1804
    $ sudo snap remove core18

Dacă vă întrebați de ce nu am eliminat și **snap-core**, deocamdată nu poate fi eliminat, dar o să se poată elimina după următori pași.

# Demontare snap core

**snap-core** poate fi eliminat doar după ce se face demontarea lui. Și demontarea se poate face doar pe baza ID-ului real al directorului core al sistemului.

ID-ul de la directorul core se poate afla rulând comanda `df`. Demontarea directorului core se face rulând comanda:

    $ sudo unmount /snap/core/xxxx

# Îndepărtarea și curățarea pachetului snapd

Pentru a elimina pachetul `snapd` și toate serviciile sale trebuie să rulați comanda:

    $ sudo apt purge snapd

# Îndepărtarea directoarelor snap

Ultimul pas pe care trebuie să îl mai faceți este să eliminați direcoarele snap rămase. Este posibil să nu aveți nici unul dintre aceste directoare după pasul 3.

Totuși este bine să verificați rulând comenzile:

    $ rm -rf ~/snap
    $ sudo rm -rf /snap
    $ sudo rm -rf /var/snap
    $ sudo rm -rf /var/lib/snapd

Acum vă puteți bucura de un Ubuntu fără snap.

> **Notă:**
>
> Aceste modificări au fost efectuate pe Ubuntu 20.04. Nu garantez că acest tutorial mai este valabil și pentru ultima versiune de Ubuntu. Efectuați aceste modificări pe ultima versiune a Ubuntu pe propriile voastre riscuri.
