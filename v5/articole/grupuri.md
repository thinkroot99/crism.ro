# Am adăugat grupuri la utilizator


Am instalat Arch Linux pe curat și trebuie să fac aproape totul de la zero.

Unele lucruri le fac așa de rar încât nu le ține minte sau nu le țin minte bine. Așa cum este crearea grupurilor și adăugarea lor la utilizator.

După ce am terminat cu instalarea Arch-ului, contul de utilizator aveam doar două grupuri adăugate: `whell` și `numele_utilizatorului`.

Așa că am creat câteva grupuri noi și acestea sunt: `adm`, `audio`, `input`, `lp`, `video`, `plugdev`, `netdev`, `docker`, `scanner`, `optical`, `storage` și `cdrom`. Pentru a crea grupurile am folosit comanda:

```bash
ex: sudo groupadd nume_grup

sudo grouppadd cdrom
```

După care am adăugat grupurile la propriul utilizator pentru a putea avea acces la diverse funcții.

```bash
sudo usermod -aG adm,dialout,cdrom,plugdev,audio,video,lp,netdev,input,docker,scanner,optical,storage nume_utilizator
```

Pe urmă am repornit laptopul pentru a pune în funcție setările făcute.
