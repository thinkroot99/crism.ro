% Ce trebuie să știți despre Arch Linux înainte de al instala
% ThinkRoot99

Arch Linux este, fără îndoială, una dintre cele mai bune distribuții pentru utilizatorii. Dar există câteva lucruri pe care ar trebuie să le știți despre Arch înainte de a-l instala.

Când instalați majoritatea distribuțiilor GNU/Linux, pur și simplu descărcați un ISO, creați un mediu de pornire și începeți procesul de instalare.

Dar lucrurile stau puțin diferit cu Arch Linux. Dacă mergeți direct la partea de instalare fără a afla mai întâi despre distribuție, veți fi uimiți de complexitatea procesului.

Pentru a vă asigura că prima impresia despre Arch Linux nu include cuvintele „*sofisticat*”, „*confuz*” sau „*nu este pentru mine*”, iată o scurtă listă de lucruri cu care ar trebui să vă familiarizați înainte de a vă scufunda cu capul în lumea liberă și vastă a Arch Linux.

# Arch Linux este o distribuție cu lansare continuă

Dacă veniți de la o distribuție GNU/Linux stabilă precum Ubuntu sau Fedora, atunci veți găsi distribuția Arch destul de fascinantă. Spre deosebire de alte distribuții stabile, dezvoltatorii Arch nu lansează LTS sau versiuni beta pentru public. În schimb, există un singur Arch Linux – unul care funcționează și funcționează foarte bine.

După cum scrie în subtitlu, Arch este o distribuție cu lansare continuă (rolling releases), ceea ce înseamnă că pachetele sunt disponibile pentru a fi actualizate de îndată ce dezvoltatorii lor lansează o nouă versiune. Acest lucru asigură că sistemul vostru este întotdeauna actualizat cu cele mai recente programe, spre deosebire de alte distribuții care oferă doar pachete stabile și testate utilizatorului.

Dar acest lucru vine și cu un dezavantaj. Cele mai recente pachete nu sunt întotdeauna încercate și testate, ceea ce înseamnă că câteva defecte s-ar putea strecura și vă pot face sistemul inutilizabil. Dar acesta este un cost pe care trebuie să-l plătiți pentru obținerea celor mai recente versiuni de programe imediat după lansarea lor.

# Arch Linux are o instalare bazată pe linia de comandă

Aproape fiecare distribuție își propune să transforme GNU/Linux într-un sistem ușor de utilizat pentru publicul larg. Dar Arch își face treaba cu ideologia sa centrată pe utilizator.

În loc să ofere un program de instalare grafic elegant, Arch vă lasă cu o interfață de linie de comandă plictisitoare (dar puternică) după pornire. Trebuie să faceți totul manual ca sistemul de operare să se instaleze și asta include partiționarea spațiului de stocare, instalarea pachetelor de bază, configurarea utilizatorilor și instalarea unui mediu de lucru (desktop environment sau windows manager).

Pentru unii utilizatorii, aceasta poate fi o atenționare, pentru alții este o șansă de a învăța conceptele GNU/Linux chiar de la început. Veți învăța cum să lucrați cu mediile de stocare, să editați fișierele de configurare, să instalați modulul de boot-are și alte pachetele; și asta ne duce la următorul punct.

# Va trebui să instalați totul manual pe Arch Linux

Procesul de instalare a fost făcut deja; va trebui să instalați manual totul pe Arch. Acesta include nucleul Linux, pachetul de firmware, mediile de lucru (desktop) și pachetele suplimentare de care aveți nevoie pentru sistemul vostru.

Alte distribuții GNU/Linux configurează adesea majoritatea lucrurilor pentru voi cu programele lor de lucru ușor de utilizat, inclusiv un mediu de lucru, configurarea utilizatorilor și instalarea nucleului. Configurarea manuală a componentelor menționate nu este un dezavantaj pentru utilizarea Arch Linux, chiar dacă sună astfel.

Prin abordarea DIY (do it yourself), Arch transferă complet controlul utilizatorilor, oferindu-le posibilitatea de a instala doar pachetele pe care le doresc. Acest lucru minimizează bloatware-ul și vă permite să construiți un sistem pe care îl puteți numi cu mândrie că este făcut de voi.

# Arch Linux este cel mai apropiat de minimalism

Minimalismul este în tendință de ceva timp, fie că este vorba de designe, viață sau calculatoare. Arch este una dintre puținele distribuții GNU/Linux, dacă nu singura, care vă permite să configurați un sistem de operare personalizat care se potrivește gusturilor voastre.

Puteți alege fie să vă umpleți sistemul cu aplicații, fie să îl păstrați cât mai curat, alegerea este a voastră. După cum am menționat mai sus, alte distribuții sunt adesea livrate cu bloatware care nu au aplicații practice pentru utilizatorul final.

Pe de altă parte, o instalare minimă a Arch vă va lăsa cu un sistem GNU/Linux de bază și nimic altceva în plus, nici măcar un mediu de lucru. Puteți alege ce mediu de lucru să instalați – dacă doriți unul. Vă puteți descurca chiar și fără un mediu de lucru folosind shell-ul implicit din Arch dacă vă simțiți confortabil cu linia de comandă.

# Arch Linux este diferit de Ubuntu sau Fedora

Distribuții precum Ubuntu sau Fedora lansează frecvent noi versiuni ale sistemului de operare în timp. Aceste noi versiuni includ programe actualizate, noi completări ale distribuției și îmbunătățiri ale funcțiilor existente. Arch nu funcționează așa.

Când cineva menționează Ubuntu, puteți întreba ce versiune și de obicei va răspunde cu un număr de versiune sau un nume de cod. Pe de altă parte, când auziți Arch Linux, nu trebuie să vă întrebați despre ce versiune este vorba, deoarece nu există nici o versiune de Arch Linux.

În schimb, există un singur Arch Linux, care este actualizat în mod regulat pe măsură ce dezvoltatorii își lansează actualizările de programe. O nouă versiune a unui pachet este lansată, o puteți instala a doua zi. A fost lansat un nou nucleu Linux? Îl puteți testa imediat.

# Va trebui să reparați singur ceea ce ați stricat

Când actualizați în mod regulat pachetele de pe sistemul vostru la cea mai recentă versiune, veți întâmpina o oarecare instabilitate de când în când. Deși de cele mai multe ori problema se rezolvă rapid (mulțumită actualizărilor rapide), uneori este posibil să trebuiască să remediați singur problema.

Dacă stricați ceva în sistemul vostru, în majoritatea situațiilor, puteți remedia problema anulând ceea ce tocmai ați făcut. Dar unele defecte ar putea depăși cunoștințele voastre și este posibil să aveți nevoie de ajutor pentru a le remedia. Aici intervine Arch Wiki.

# Arch Linux are o documentație bine întreținută

Spre deosebire de Ubuntu sau Fedora, care au de obicei o perioadă de suport care durează ani de zile, nu obțineți un suport similar în Arch Linux. În schimb, ceea ce aveți este o colecție mare de documentație bine întreținută, cunoscută sub numele de [Arch Wiki](https://wiki.archlinux.org/). Ori de câte ori te trezești în fața unui ecran plin de defecte, caută Arch Wiki. Nu știi de ce sunetul nu funcționează pe sistem? Informațiile oferite de motorul de căutare duc de obicei spre Arch Wiki.

Desigur, vor exista momente în care nu veți găsi ajutor în documentația oficială și în astfel de situații neașteptate, vă puteți posta problema pe [forumul Arch Linux](https://bbs.archlinux.org/). Comunitatea este de ajutor și veți găsi sute de utilizatori gata să vă ajute, dar numai dacă vă publicați în mod corespunzător problema.

<ins>Un cuvânt de precauție:</ins> asigurați-vă că ați răsfoit paginile de pe Arch Wiki înainte de a căuta ajutor pe forum, cu excepția cazului în care doriți să fiți troll-at. Acest lucru se datorează în primul rând pentru ca utilizatorilor Arch nu le place să ofere cu lingura fiecare detaliu începătorilor, mai ales când pot găsi cu ușurință soluția pe Wiki.

# AUR are aproape toate programele de care aveți nevoie

Pe lângă depozitul oficial, de unde puteți descărca și instala majoritatea pachetelor, aveți și AUR (Arch User Repository). AUR este un depozit condus de comunitate în care oricine poate încărca și găzdui programe dezvoltate de ei. Dacă nu puteți găsi un anumit pachet în depozitele oficiale, există șanse mari să fie în AUR.

Dar acest acces ușor la programe vine și cu un cost. Majoritatea pachetelor de pe AUR nu sunt testate corect și există o șansă mare de vă face sistemul inutil.

Pacman, managerul implicit de pachete pentru distribuțiile bazate pe Arch, poate descărca pachete numai din depozitele oficiale. pentru a descărca din AUR, aveți nevoie de un AUR Helper – yay sau paru de exemplu.

# De ce ar trebui să instalați Arch Linux

Există multe motive pentru a trece la Arch Linux: disponibilitatea celui mai recent program, control complet asupra sistemului și capacitatea de a personaliza GNU/Linux după gustul vostru.

În afară de asta, poți spune „*BTW, I use Arch*” pe forumurile de pe internet, care este unul dintre cele mai bune motive pentru a instala Arch Linux.

Nu vă place complexitatea asociată cu Arch Linux? Nu este nici o problemă. Testați puterea Arch instalând mai întâi o distribuție ușor de folosit bazată pe Arch, și dacă vă place, puteți oricând face trecerea la Arch Linux.
