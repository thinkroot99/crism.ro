% Web1 vs Web2 vs Web3: Care este diferența?
% ThinkRoot99

Probabil că ați văzut o mulțime de zvonuri despre „*Web3*”. Deși este posibil să înțelegeți sau nu cum arată Web3, este posibil să aveți o întrebare suplimentară: Ce au fost Web2 și Web1? Pare o întrebare simplă, dar este mai mult decât atât.

Există trei idei mari, câte una pentru fiecare dintre aceste generații ulterioare ale web-ului. Acestea sunt „*Citiți/Read*”, „*Scrieți/Write*” și „*Propietatea/Own*”.

# Ce a fost Web1?

De dragul acestui articol, să trecem peste [preistoria internetului](https://www.britannica.com/story/who-invented-the-internet). Prima comunicare pe internet a fost trimisă fără succes la sfârșitul anilor `60. Agențiile guvernamentale și universitățile au petrecut următorii douăzeci și ceva de ani pentru a-l dezvolta.

Primul domeniu a fost cumpărat în 1985, așa că putem începe cronologia Web1 de aici, chiar dacă a fost cu cinci ani înainte ca „*World Wide Web*” (nu același lucru cu „*internetul*”) să apară. În următorii paisprezece ani (aproximativ), internetul s-a axat pe lectură.

Uitați de faptul că calculatoarele nu erau omniprezente, așa cum sunt acum. Chiar și în cazul persoanelor care aveau calculatoare și care le puteau folosi destul de bine, nu oricine putea posta online într-un mod semnificativ. Pentru majoritatea oamenilor, internetul era un loc de căutare de informații, nu de publicare de informații.

Repere ale Web1:

- Dificil de creat conținut
- Dificil de descoperit conținut
- Dificultatea de a monetiza conținutul

# Ce este Web2?

Începând cu sfârșitul anilor 1990, instrumentele și serviciile online au început să faciliteze generarea și postarea de către oamenii obișnuiți a propriului conținut online în moduri care să poată fi descoperite. De la site-urile persoanle la site-urile de bloguri, care au dus în cele din urmă la „*microblogging*” și, în cele din urmă, la începutul nebuniei social media. Aceasta este epoca „*cititului/scrierii*” a internetului.

Social media este importantă pentru înțelegerea Web2, dar și pentru înțelegerea multor promisiuni ale Web3. Coloana vertebrală a social media este ideea că te conectezi la un site pentru a vedea conținut generat de utilizatori, fără ca aceștia să primească vreo recompensă. În schimb, recompensa revine site-ului care a făcut ca acel conținut să poată fi descoperit.

Același lucru se poate spune și despre lucruri precum motoarele de căutare și chiar despre navigatoarele (browsere) web. Cea mai mare parte a monetizării revine celor care găsesc conținutul pentru dumneavoastră, nu celor care l-au creat. Mai mult, site-urile și serviciile care sunt disponibile gratuit (inclusiv rețelele de socializare) își fac majoritatea banilor din colectarea și exploatarea datelor utilizatorilor.

În ultimi ani, am văzut, de asemenea, platforme și servicii care permit utilizatorilor să recompenseze sau să dea bacșișuri altor utilizatori și să ofere utilizatorilor un control mai mare asupra datelor colectate – primele semne ale unora dintre elementele majore ale Web3. Tendința generală de trecere de la Web2 la Web3 este de a avea mai mult control asupra acestui conținut.

Aspecte importante ale Web:

- Ușor de creat conținut
- Ușor de descoperit conținut
- Monetizarea este posibilă, dar favorizează companiile în detrimentul creatorilor

# Ce este Web3?

Web3 este ideea că instrumentele care se dezvoltă și sunt puse în aplicare chiar acum ne vor permite să ne controlăm și, eventual, să ne rentabilizăm prezența online în moduri care nu sunt posibile în prezent sau care nu sunt realizate în prezent. Aceasta este era „*citire/scriere/proprietate*” a internetului și, pentru mulți oameni, înseamnă blockchain.

Valoarea din [blockchain nu înseamnă doar speculații pe piața criptomonedelor](https://builtin.com/blockchain). Această tehnologie este un mijloc sigur de stocare a informațiilor, precum și de verificare a cui le deține. Acum aduceți criptomonedele, în special ca modalitate de plată directă între persoane fizice, și aveți baza unei revoluții pe internet.

Există implicații uriașe pentru economia creatorilor, dar și pentru gestionarea datelor personale. Viitoarele servicii online ar putea permite utilizatorilor să furnizeze diferite tipuri de informații în schimbul unor avantaje și plăți diferite. Acest lucru se întâmplă deja pe platforme precum Brave, navigatorul cu blockchin care recompensează utilizatorii pentru vizualizarea de reclame.

Utilizatorii pot, de asemenea, să se implice în modul în care organizațiile sunt conduse mai ușor prin intermediul „*organizațiilor autonome descentralizate*”. Acestea pot fi atât de complicate pe cât doriți să le faceți, dar gândiți-vă la ele ca la niște cooperative pe internet: utilizatorii cumpără calitatea de membru și, în schimb, ajută la luarea deciziilor și culeg roadele.

Aspecte importante ale Web3:

- Ușor de creat conținut
- Ușor de descoperit conținut
- Crearea de conținut și implicarea sunt monetizate direct

# Web3 este încă în dezvoltare

Tendința generală de la Web1 la Web2 a fost aceea de accesibilitate. Conținutul a devenit mai ușor de găsit și apoi mai ușor de realizat. Web2 a pus mai mult control în mâinile creatorilor, dar companile mari continuă să culeagă cele mai mari recompense – Meta, Alphabet, TikTok și așa mai departe, toate fac bani pe seama creatorilor pe care îi ajută.

Tranziția de la Web2 la Web3 va pune mai multă putere în mâinile creatorilor, permițându-le acestora să-și monetizeze conținutul direct, fără a da o felie unei platforme. Sună minunat în practică. De ce să nu eliminați intermediarul și să păstrați profiturile pentru voi? Cu toate acestea, realitatea este că încercarea de a monetiza totul și de a vinde totul direct consumatorilor va avea alte consecințe ne intenționate și, ca atare, dezvoltarea Web3 va dura mult timp.
