% Cum se șterg aplicatiile din Chrome
% ThinkRoot99

Atunci când se instalează Google Chrome acesta vine și cu câteva aplicații preinstalate, pentru că acesta permite să se instaleze aplicații.
Aplicațiile preinstalate care vin o dată cu Chrome sunt: Google Drive, YouTube, Gmail, Docs, Sheets și Slides.

Aceste aplicații îmi apăreau în sistem atunci când făceam clic pe un fisier și mergeam la `Open with...`. Cum nu îmi place să am aplicații pe care nu le folosesc am vrut să le dezinstalez.

Când am deschis Google Chrome, nu îmi apărea nicăieri un buton care să mă ducă la aceste aplicații (poate din cauză personalizării). În mod normal trebuia să fie un buton pe pagina implicita a navigatorului.

Am uitat că aceste aplicații se poat dezinstala și prin altă metodă, destul de simplă.

În bara de adrese se tastează `chrome://apps` - această adresă va deschide meniul de aplicații instalate în Chrome.

Aici se face clic dreapta pe aplicația pe care doriți să o dezinstalați și selectați `Uninstall`, după care dați clic pe `Remove`.

Destul de simplu atunci când ști ce ai de făcut, dar complicat când nu ști sau uiți, ca mine :)
