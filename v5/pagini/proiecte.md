# Proiecte

## Proiecte active

* [netmic.ro](http://www.netmic.ro): Promovarea conceptului de small web în România.
* [Sit Mic](http://sit.netmic.ro): Se oferă subdomeniu și găzduire gratuită pentru sit-uri small web.
* [/ Linux](https://www.rootlinux.ro): Știri despre Linux și Open Source.
* [PasteBin](https://pastebin.crism.ro/): Serviciu de pastebin open source.
* [Forum](https://forum.crism.ro/): Forum despre despre diverse subiecte.

## Experimente cu AI

* [Pass](https://pass.crism.ro/): Generator de parole.
* [Conversie zile/săptămâni](https://conversie.crism.ro/): Conversie zile în săptămâni și invers.
* [Info Călătorie](https://g.crism.ro/): Informații despre cum se poate călători de la Aeroportul Memmingen la localitatea Geislingen an der Steige și invers.
* [Calculator](https://calc.crism.ro): Un calculator online simplu.
* [Calendar](https://calendar.crism.ro): Calendar online.

## Proiecte vechi

* [Xubuntu România remix (2012)](https://web.archive.org/web/20121017041731/http://crismblog.ro/xubuntu-romania-remix/). Distribuția Xubuntu Linux personalizată pentru România.
* [gnulinux.ro(2015-2018)](https://web.archive.org/web/20151005110929/http://www.gnulinux.ro/), Știri și tutoriale despre Linux și Open Source.
* [gentoo-land.org (2013-2015)](https://web.archive.org/web/20131021094208/http://gentoo-land.org/), Comunitate pentru utilizatorii de Gentoo și derivate.
