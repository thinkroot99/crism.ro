% Un internet mic 

De când am început să citesc sit-uri mici și simple, am dat peste comunitați ca smolweb.org, 250kb.club, 512kb.club și 1mb.club. Așa că am ajuns să modific sit-ul care a fost pe acest domeniu, dar nu a fost de ajuns și astfel am creat proiectul netmic.ro.

netmic.ro este un proiect care promovează conceptul „smol web” în România și prin acest proiect vreau să aduc la cunoștința tuturor care dețin un sit sau blog că internetul nu înseamnă doar CMS-uri, rețele de socializare, etc. Toate aceste lucruri care încarcă sit-urile. Internetul este și un sit mic static, dar în același timp arată la fel de bine ca un sit construit pe un CMS. Interentul nu înseamnă doar rețele de socializare și toate ideeile trebuie postate pe aceste rețele - aici se pierde toată informația bună.

Cel mai bine este să deți propriul sit sau blog unde să postezi toate ideile tale că sunt bune sau nu nu, pentru cineva tot sunt bune și astfel informația nu se pierde ca pe rețelele de socializare.

Din păcate internetul din România este plin de sit-uri construite pe CMS, este plin de sit-uri și blog-uri încărcate. Aceste sit-uri se pot construit/creea și doar în HTMl/CSS mai ales că la un sit nu trebuie modificat în fiecare zi, aceste se modifica o dată la câteva luni. Și un blog se poate construi în HTMl/CSS chiar dacă se public în fiecare zi - cu ajutorul automatizări tot este simplu.

Proiectul netmic.ro este un proiect finalizat, să zic așa, și nu mai are nevoie de îmbunătățiri. Dar personal vreau să fac acest proiect ceva mai mare pentru România, vreau să combin smolweb.org, 512kb.club și găzduire gratuită pentru blog-uri mici până într-o anumită mărime.

Binențeles că aceste planuri de dezvoltare durează pentru că nu se poate implementa peste noapte, dar începutul este promițător, zic eu :) În primul rând conceptul „smol web” trebuie să ajunga să fie cunoscut în România, după care urmează restul pașilor.

Dacă vreți să aflați ce este și ce înseamna „smol web” vizitați sit-ul [netmic.ro](https://netmic.ro)
