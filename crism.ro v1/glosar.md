% Glosar
% ThinkRoot99

Lista cuvintelor în engleză pe care nu le folosesc, dar folosesc trducerea lor în românește.

Pe prima coloană se află cuvântul în engleză și pe a doua coloană se află traducerea în românește.


| Cuvând în engleză   | Traducerea în românește |
| -----------------   | ----------------------- |
| Site                | Sit                     |
| Browser             | Navigator               |
| Software            | Program                 |
| Driver              | Pilot                   |
| Rolling Release     | Lansare continuă        |
| Patch               | Petic                   |
| Upstream            | Amonte                  |
| Downstream          | Aval                    |
| Bug                 | Defect                  |
| Kernel              | Nucleu                  |
| Desktop Environment | Mediu de lucru          |
| Window Manager      | Manager de ferestre     |
| Tiling              | Placare                 |
| Stacking            | Stivuire                |
| Flavour/s           | Aromă/Arome             |
