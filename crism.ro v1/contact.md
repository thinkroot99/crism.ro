% Contact
% ThinkRoot99

# Contact

Mă puteți contacta prin e-mail la <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#115;&#111;&#99;&#105;&#97;&#108;&#64;&#99;&#114;&#105;&#115;&#109;&#46;&#114;&#111;" class="email">&#115;&#111;&#99;&#105;&#97;&#108;&#64;&#99;&#114;&#105;&#115;&#109;&#46;&#114;&#111;</a>. Deocamdată nu am o cheie PGP, dar va exista în viitor.

Mă mai puteți contact pe [Mastodon](https://linux.social/@thinkroot99), [X](https://www.x.com/@thinkroot99) sau pe [Threads](https://www.threads.net/@thinkroot99). Dar și pe una dintre platformele video: [YouTube/YouTube Shorts](https://www.youtube.com/@thinkroot99), [TikTok](https://www.tiktok.com/@thinkroot99) și [Instagram Reels](https://www.instagram.com/thinkroot99/).

Toate conturile și sit-urile mele le pe puteți vedea/afla accesând legătura de la [linktr.ee](https://linktr.ee/thinkroot99).