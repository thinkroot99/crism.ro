% Folder sau Directory?
% ThinkRoot99

Dacă utilizați calculatoare cu Windows, este posibil sâ utilizați termenul `folder` (dosar). Dar când treceți la GNU/Linux, veți descoperi că dosarele sunt adesea numite `direcotry` (director).

Acest lucru îi poate deruta pe unii utilizatori de GNU/Linux. Ar trebui să-l numiți dosar sau director?

Puteți să le denumiți și dosare, și directoare - cum doriți pentru că nu există nici o diferență.

Dar dacă vă întrebați de ce un dosar se numește director în GNU/Linux, iată mai jos câteva explicații.

# De un dosar se numește director în GNU/Linux?

Înainte de a explica asta, să ne amintim ce este un dosar și ce este un director în lumea reală.

Un dosar poate fi folosit pentru a păstra mai multe fișiere (sau alte elemente) în el. Un director poate fi folosit pentru a menține un index al articolelor, astfel încât sa puteți găsi articolele mult mai ușor acolo unde se află.

> ![Dosar sau Direcetor](img/folder-directory.webp)

Acum, să revenim la director. Termenul a existat chiar înainte de existența GNU/Linux-ului. Vine din era UNIX și GNU/Linux moștenește o mulțime de lucruri de la UNIX șî acesta este doar unul dintre acele multe lucruri.

Un director nu păstrează cu adevărat fișiere în el. Directorul este un `fișier special` care știe unde este stocat (conținutul) unui fișier în memoria de stocare (prin [inode](https://linuxhandbook.com/inode-linux/)).

Astfel are sens de ce se numește director. Un director păstrează indexul articolelor, nu neapărat articolele în sine. Directoarele din GNU/Linux și UNIX nu păstrează fișierele în interiorul lor. Directoarele au doar informații despre locația fișierelor.

Când sistemele de operare au început să folosească elemente grafice, unii termeni au fost modificații în consecință și directorul/dosarul a fost unul dintre ele.

# Ar trebui să-l numiti dosar sau director?

Asta depinde în întregime de voi. Puteți folosi oricare dintre termeni. Cu toate acestea, dacă înveți linia de comandă din GNU/Linux sau o folosești des, folosirea termenului director ar putea fi puțin mai utilă.

Există comenzi în GNU/Linux precum `mkdir` ,`rmdir` etc. Termenul `dir` oferă un indiciu că aceste comenzi au ceva de-a face cu directoarele. În mod similar, multe comenzi GNU/Linux și scripturi bash vor folosi indicatorul `-d` pentru directoare și `-f` pentru fișiere.
Chiar și proprietățile fișierelor din terminal disting între fișiere și dosare (direcetoare) punând litera **d** în fața directoarelor.

Luați acest exemplu în care am un fișier numit „*unele*” și un dosar/director numit „*ceva*”. Observați cum diferitele comenzi GNU/Linux fac diferența între un fișier și un director cu `dir` sau `d`.

> ![File directory - commands diference](img/file-directory-commands-difference.png)

Toate acestea fac ca folosirea termenului „*director*” va fi benefică în timp ce se folosește GNU/Linux. Ar fi mai ușor pentru subconștientul vostru dacă asociați termenii `dir` si `d` cu directorul.

Încă o dată, depinde în întregime de voi dacă doriți să-l numiți dosar (folder) sau director (directory). Oamenii ar înțelege la ce vă referiți.
