% Ce este Arch Linux
% ThinkRoot99

Arch Linux ese o distribuție GNU/Linux de uz general pentru arhitectura x86-64, dezvoltată în mod independent, care se străduiește să ofere cele mai recente versiuni stabile ale majorității programelor, urmând un model de lansare continuă.

# Istorie

Judd Vinet, un programator Canadian, a început să dezvolte Arch Linux la începutul anului 2001, ceea ce face ca în prezent această distribuție să se numere printre cele mai importante care modelează lumea GNU/Linux din prezent.

Prima sa versiune oficială, `Arch Linux 0.1`, a fost lansat pe data de 11 martie 2002.

Inspirat de simplitatea elegantă a Slackware, BSD, PLD Linux și CRUX, dar dezamăgit de lipsa lor de gestionare a pachetelor la acea vreme, Vinet și-a construit distribuția pe principii similare cu cele ale distribuțiilor menționate.

Mergând mai departe, a scris și un instrument de gestionare a pachetelor numit `pacman` pentru a gestiona automat instalarea, eliminarea și actualizarea pachetelor pe Arch Linux.

La sfârșitul anului 2007, Vinet s-a retras din activitatea de dezvoltator al Arch Linux. El a transfert fără probleme ștafeta către programatorul American Aaron Griffin, care a coordonat distribuția până în 2020, când s-a retras, iar Levente Polyak, un dezvoltator GNU/Linux de origine maghiară, stabilit în Germania, a preluat conducerea Arch Linux.

Una dintre cele mai importante schimbări legate de distribuție a avut loc între 2012 și 2013, când sistemul `init System V`, utilizat în mod tradițional, a fost [înlocuit](https://archlinux.org/news/systemd-is-now-the-default-on-new-installations/) cu `systemd`.

Câțiva ani mai târziu, la 25 ianuarie 2017, dezvoltatorii Arch Linux au [anunțat](https://www.archlinux.org/news/phasing-out-i686-support/) că suportul pentru arhitectura i686 va fi retras treptat din cauza popularității în scădere în rândul dezvoltatorilor și al comunității.

# Caracteristici distinctive

Arch Linux este una dintre cele mai cunoscute distribuții GNU/Linux, care cu siguranță nu va câștiga niciun premiu pentru ușurința de utilizare.

Cu toate acestea, în același timp, pune toată libertatea și alegerea în mâinile utilizatorilor experimentați de GNU/Linux pentru un control suprem asupra modului în care funcționează și arată sistemele lor.

De-a lungul anilor, Arch Linux a câștigat o mulțime de fani care îl adoră, transformându-se într-un cult. S-a ajuns chiar până în punctul în care distribuția și-a câștigat fraza **`BTW, I Use Arch`**, folosită pentru a face mișto de acel tip de persoană care se simte superior pentru că folosește o distribuție GNU/Linux mai dificilă.

## Arch Linux este o distribuția originală

Arch Linux se numără printre puținele distribuții originale, deoarece nu s-a bazat pe nicio distribuție sau sistem de operare existent.

Spre deosebire de multe alte distribuții GNU/Linux care se bazează pe ceva anterior lor, cum ar fi:

- Ubuntu care este bazat pe Debian,
- Linux Mint este bazat pe Ubuntu,
- Manjaro este bazat pe Arch Linux,
- Rocky Linux și AlmaLinux sunt bazate pe RHEL.

Arch Linux a fost construit de la zero pentru a fi simplu, ușor și flexibil.

## Independent, pragmatic și bazat pe comunitate

Arch Linux nu depinde de nicio altă organizație, instituție, corporație sau tip de afacere. În schimb, este un proiect condus de voluntari, sponorizat, dezvoltat și întreținut de o comunitate de entuziaști ai GNU/Linux.

În același timp, Arch Linux poate fi definit ca o distribuție GNU/Linux mai degrabă pragmatică decât ideologică.

Spre deosebire de alte distribuții care aderă strict la modelul Open Source, pachetele, driverele, firmware-ul și bibliotecile Arch nu se limitează doar la utilizarea de programe libere.

Pachetele disponibile în depozitele Arch Linux oferă programe gratuite și open source pentru cei care preferă acest lucru. În același timp distribuția oferă și pachete de programe proprietărești pentru cei care prioritizează funcționalitatea în detrimetrul ideologiei.

## Minimalist

Arch Linux oferă o experiență GNU/Linux autentică, deoarece instalarea lui Arch implicită este un sistem de bază minimalist, configurat de către utilizator doar pentru a adăuga ceea ce este necesar în mod intenționat.

Faptul că utilizatorul decide cum ar trebui să arate sistemul său și pachetele pe care ar trebui să le aibă instalate face ca acesta să fie curat și să nu aibă aplicații inutile care consumă din memoria RAM și CPU.

Cu alte cuvinte, se poate instala doar ceea ce are nevoie utilizatorul pentru utilizarea zilnică a calculatorului. Totuși, acest lucru are un preț: instalarea sistemului de operare va dura ceva timp, dar la sfărșitul zilei, nimeni nu va ști mai bine decât tine cum funcționează calculatorul.

## Simplu și modern

Principala filozofie din spatele lui Arch este **KISS** (Keep It Simple, Stupid), ceea ce înseamnă că se străduiește să fie cât mai minimalist posibil, oferind în accelași timp un sistem complet și utilizabil.

Distribuția încearcă să aibă modificări minime specifice distribuției, probleme minime cu actualizările, opțiuni de proiectare pragmatice în detrimetrul celor ideologice, ușurința de utilizare și un nivel minim de bloatware.

Arch definește simplitatea ca fiind fără adăugiri sau modificări inutile.

Aceasta înseamnă că distribuie programele așa cum au fost lăsate de dezvoltatorii inițiali, cu un minim de modificări specifice distribuției: sunt evitate peticile respinse în amonte, iar peticile Arch din aval constau aproape în întregime în remedieri de defecte care sunt depășite de următoarea versiune a proiectului.

Arch urmează un model de lansare continuă, ceea ce înseamnă că sistemul de operare este actualizat în permanență.

Nu există versiuni majore, versiuni noi ale sistemului sau nu este nvoie de instalări noi. Imaginile ISO de instalare lunare publicate de echipa Arch sunt pur și simplu instantanee actualizate ale principalelor componente ale sistemului.

Pentru a beneficia de cele mai recente programe, este nevoie doar de o actualziare regulată a sistemului. Cu alte cuvinte, se instalează o dată, iar în următoarele luni și anii trebuie doar actualizat periodic sistemul pentru a-l menține la zi și sigur.

Modelul de lansare continuă urmat de Arch vine cu un avantaj substanțial - pachetele de programe sunt disponibile pentru utilizatori imediat ce sunt publicate. Acest lucru le asigură acestora accesul la cele mai recente caracteristici și corecții de defecte.

Pe scurt, lucrați întotdeauna cu și aveți la dispoziție cele mai recente versiuni de programe pentru instalare.

## Arch este extrem de personalizabil și educativ

Arch vă permite să vă construiți sistemele de la zero, permițând utilizatorilor să personalizeze fiecare aspect al sistemului, de la nucleu la mediul de lucru.

Dar acest lucru are un cost, ceea ce face ca Arch Linux să se adreseze în primul rând utilizatorilor de GNU/Linux mai avansați și să fie mai puțin atractiv pentru cei care încep să utilizeze GNU/Linux.

Arch Linux este o distribuție axată pe linia de comandă, nu veți găsi instrumentele utile de interfață grafică pentru gestionarea sistemului disponibile în multe alte distribuții axate către utilizator, cum ar fi:

- Manjaro
- Ubuntu,
- Linux Mint
- Zorin OS

De exemplu, Arch nu vine cu un program de instalare grafic, iar procesul de instalare se face prin intermediul unui terminal. Acest lucru nu poate fi foarte încurajator pentru noii utilizatori de GNU/Linux.

# Managerul de pachete

Fiind o distribuție axată pe terminal, Arch nu are un manager de pachete grafic (GUI). În schimb, Arch folosește un manager de pachet pentru linia de comandă, `pacman`. Acesta se ocupă de instalarea, eliminarea și actualizarea pachetelor de programe. Pacman combină un format simplu de pachete binare cu un sistem de compilare ușor de utilizat.

La fel ca APT pentru Debian și Ubuntu sau DNF pentru Fedora și RHEL, instalarea pachetelor în Arch este realizată cu ajutorul lui `pacman`, care este scris în `limbajul de programare C` și folosește `formatul bsdtar` pentru împachetare.

Pacman menține sistemul la zi prin sincronizarea listelor de pachete cu serverul principal. Acest model server/client permite utilizatorilor să descarce/instaleze pachete printr-o simplă comandă, cu toate dependențele necesare.

Pacman este unul dintre cele mai rapide, dacă nu chiar cel mai rapid, manager de pachete din lumea GNU/Linux.

Arch pune la dispoziția utilizatorilor săi și `Arch Build System (ABS)`, o colecție de scripturi și fișiere de configurare pentru construirea și împachetarea programelor din codul sursă în pachete de instalare `.pkg.tar.xz` pe care Pacman le poate gestiona.

Prin urmare, ABS-ul permite utilizatorilor să personalizeze și să construiască pachete în funcție de nevoile lor specifice, în loc să se bazeze pe binare precompilate. Acest lucru oferă o mai mare flexibilitate și control asupra programului instalat pe un sistem Arch Linux.

# Managerii GUI pentru Pacman

Nu ar trebui să fiți îngrijorați de faptul că Arch oferă doar un manager de pachete pentru linia de comandă. Există și interfețe grafice pentru pacman.

Dezvoltate de persoane terțe, acestea oferă o oportunitate excelentă pentru noii utilizatori de Arch pentru a-și gestiona programele de sistem într-un mod convenabil.

![Pamac, a frontend for pacman](img/pamac-frontend-for-pacman.png)

De exemplu, unul dintre acestea este `Pamac` - o interfață grafică GTK3 pentru Pacman cu suport suplimentar pentru Alpm, AUR, Appstream, Flatpak și Snap, construit de un inginer din echipa de dezvoltare a Manjaro.

Pamac este unul dintre numeroasele opțiuni. Alte alternative pentru Pacman cu o interfață grafică excelentă sunt Octopi și tkPacman.

Nu uitați că niciunul nu vine instalat în mod implicit în Arch Linux, așa că va trebui să îl instalați manual din AUR.

# AUR (Arch User Repository)

Pe Arch Linux, depozitele oficiale de programe sunt: **Core**, **Extra** și **Community**. Acestea includ pachete compilate, gata să fie instalate pe sistemul Arch prin pacman.

De exemplu, peste 11.000 de pachete se află în depozitele oficiale ale Arch. Cu toate acestea, există multe alte programe disponibile pe GNU/Linux.

[Arch User Repository](https://aur.archlinux.org/) (AUR) este un depozit condus de comunitate pentru utilizatorii de Arch care găzduiește unele pachete în afara bazei de date oficiale. Este o comoară de programe care conține aproximativ 55.000 de pachete și unul dintre principalele motive care atrag utilizatorii către Arch.

În spiritul programelor Open Source, AUR urmează un concept interesant. Este vorba de o serie de descrieri a pachetelor, realizate de utilizatori, cunoscute sub numele de `PKGBUILD`, care automatizează procesul de descărcare, configurare, compilare și instalare a programelor din codul sursă și instalarea acestora prin pacman.

Cu alte cuvinte, orice utilizator poate adăuga un nou program la AUR și poate deveni cel care asigură mentenanța acestuia sau poate adopta un pachet „*orfan*” fără un administrator actual.

Prin urmare, și față de multitudinea de susținători ai distribuției, este greu să găsești programe disponibile pentru GNU/Linux și, în același timp, care să nu fie deja prezente ca fiind disponibile pentru instalare pe Arch prin intermediul depozitului AUR.

Cu toate acestea, este esențial să rețineți că managerul de pachete Pacman nu acceptă pachetele AUR. Prin urmare, trebuie să folosiți așa-numitele ajutoare AUR, cum ar fi `Yay` sau `Paru` - deoarece instalarea unui pachet AUR fără un astfel de ajutor poate părea dificilă.

# Cea mai bine documentată distribuție

[Arch Wiki](https://wiki.archlinux.org/) este o comoară de informații - documentația cuprinzătoare a Arch Linux este un Wiki al comunității. Este în beneficiul Arch și al celorlalți utilizatori de distribuții GNU/Linux, deoarece îndrumările și soluțiile sunt relevante și practice în afara ecosistemului Arch.

Astfel, chiar dacă alte distribuții împachetează lucrurile diferit de Arch, Arch Wiki poate oferi indicații corecte.

Veți obține tot ceea ce doriți să știți în ceea ce privește instalarea și întreținerea fiecărei componente și a fiecărui detaliu al unui sistem GNU/Linux adecvat.

Cu alte cuvinte, această documentație poate fi o referință pentru administrarea generală a GNU/Linux. Așadar, dacă sunteți nou în Arch, dar aveți experiență cu alte distribuții GNU/Linux sigur ați dat de Arch Wiki prin căutările pe Google sau DuckDuckGO de cel puțin câteva ori.

# Distribuții bazate pe Arch Linux

Fiind extrem de popular în cercurile GNU/Linux, Arch se bucură de o largă popularitate sub forma mai multor alte distribuții care îl folosesc ca bază pentru versiunilor lor. Unele dintre acestea sunt:

- ArcoLinux
- BlackArch Linux
- CachyOS
- EndeavoursOS
- Garuda Linux
- Manjaro Linux
- RebornOS
- XeroLinux

# De final

Arch Linux este un sistem de operare simplu și personalizabil care se adresează utilizatorilor avansați cărora le place să își construiască sistemele de la zero.

Acesta oferă o abordare minimalistă a instalării și întreținerii programelor, ceea ce necesită o bună înțelegere a interfețelor din linia de comandă și a administrării sistemului.

Deși pot exista alegeri mai bune pentru începători decât Arch Linux, acesta poate fi un instrument puternic pentru cei care apreciază flexibilitatea și controlul asupra experienței lor informatice.
