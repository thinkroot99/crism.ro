#!/bin/bash

# Directorul sursă pentru fișierele Markdown
SOURCE_DIR="dir_md"
# Directorul destinație pentru fișierele HTML
DEST_DIR="dir_html"

# Verifică dacă pandoc este instalat
if ! command -v pandoc &> /dev/null
then
    echo "Pandoc nu este instalat. Instalează-l și încearcă din nou."
    exit 1
fi

# Verifică dacă template2.html există
if [ ! -f template2.html ]; then
    echo "Template-ul HTML (template2.html) nu a fost găsit."
    exit 1
fi

# Crează un director pentru fișierele HTML convertite dacă nu există
mkdir -p "$DEST_DIR"

# Convertirea fișierelor .md în .html
for md_file in "$SOURCE_DIR"/*.md; do
    if [ -f "$md_file" ]; then
        html_file="$DEST_DIR/$(basename "$md_file" .md).html"
        
        # Extrage titlul din fișierul Markdown (prima linie care începe cu #)
        title=$(grep '^# ' "$md_file" | head -n 1 | sed 's/^# //')
        # Dacă titlul nu este găsit, setează un titlu implicit
        if [ -z "$title" ]; then
            title="Untitled"
        fi

        # Extrage data creării fișierului
        date=$(stat -c %y "$md_file" | cut -d' ' -f1)

        # Elimină prima linie (titlul) și convertește restul fișierului în HTML
        content=$(sed '1{/^# /d}' "$md_file" | pandoc)

        # Adaugă atributul target="_blank" la toate link-urile
        content=$(echo "$content" | sed 's/<a href/<a target="_blank" href/g')

        # Citește template-ul și înlocuiește placeholder-ele folosind printf
        {
            while IFS= read -r line; do
                line=${line//'{{ title }}'/$title}
                line=${line//'{{ date }}'/$date}
                line=${line//'{{ content }}'/$content}
                printf "%s\n" "$line"
            done < template2.html
        } > "$html_file"
        
        echo "Fișierul $md_file a fost convertit la $html_file"
    fi
done

# Funcție pentru a genera feed-ul Atom XML
atom_xml() {
    local uri=$(sed -rn '\|atom.xml| s|.*href="([^"]*)".*|\1| p' header.html)
    local host=$(echo "$uri" | sed -r 's|.*//([^/]+).*|\1|')

    cat <<EOF
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <title>$(get_title index.md)</title>
    <link href="$uri" rel="self" />
    <updated>$(date --iso-8601=seconds)</updated>
    <author>
        <name>Author Name</name>
    </author>
    <id>tag:$host,$(date --iso-8601):default-atom-feed</id>
EOF

    for f in posts/*.md pages/*.md; do
        if [ ! -e "$f" ]; then
            continue
        fi

        title=$(get_title "$f")
        created=$(date -r "$f" "+%Y-%m-%d %H:%M")
        updated=$(date -r "$f" "+%Y-%m-%d %H:%M")
        day=$(echo "$created" | sed 's/T.*//')

        cat <<EOF
    <entry>
        <title>$title</title>
        <link href="$(echo "$f" | sed -E 's|(posts|pages)/(.*)\.md|\1.html|')"/>
        <id>tag:$host,$day:$f</id>
        <published>$created</published>
        <updated>$updated</updated>
    </entry>
EOF
    done

    echo '</feed>'
}

echo "Conversia a fost finalizată!"
