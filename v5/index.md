# Colțul de internet al lui ThinkRoot

**Salut și bine ai venit pe pagina mea de internet. Sper să găsești ceva interesant aici :)**

Sunt un simplu utilizator de Linux sau GNU/Linux - îmi plac programele open source și să scriu pe/în jurnal. Nu sunt dezvoltator, programator și nici nu mă pricep la orice altceva ce ține de IT.

Pur și simplu îmi place să folosesc Linux și să îl personalizez pe placul meu. Lucru care se poate afla citind articolele pe care la public pe acest sit.

Ca hardware folosesc un Lenovo Thinkpad T440p cu Arch Linux și cu interfața grafică i3.

**Abonați-vă la [feed-ul Atom](atom.xml) pentru a primi toate articolele noi de pe acest sit.**

------

Membru al cluburilor: [512kb.club](https://512kb.club/), [250kb.club](https://250kb.club/crism-ro) și [darktheme.club](https://darktheme.club/).
