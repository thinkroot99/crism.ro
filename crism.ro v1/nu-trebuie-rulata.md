% Comanda care nu trebuie rulată niciodată
% ThinkRoot99

Probabil ați văzut peste tot, în toate grupurile, forumurile și comunitățile GNU/Linux, chiar și în viața reală pe tricouri. Dacă sunteți începător și vă întrebați ce face comanda `sudo rm -rf /` și de ce nu ar trebui rulată, cititi mai jos.

Pe scurt, `sudo rm -rf /` este comanda care șterge tot ce se află în directorul rădăcină, ceea ce practic distruge întreg sistemul. Voi explica mai jos comanda în detaliu.

**Dacă nu știi ce înseamnă sau ce face, nu ar trebui să rulezi comanda.**

# Ce face comanda „*rm*”?

Comanda `rm` este folosită pentru a șterge fișiere și directoare în sisteme similiare Unix, inclusiv GNU/Linux. Și înseamnă `remove`.

Comanda `sudo` înseamnă că executați acea comandă ca super utilizator sau administrator. Dacă sunteți autentificat ca administrator, nu mai trebuie să utilizați `sudo`. Nu trebuie să utilizați `sudo` pentru comanda `rm`, dar este posibil să aveți nevoie de ea, în funcție de ce privilegii/drepturi are utilizatorul.

Dacă ați avea un fișier numai pentru citire numit *note.txt* și doriți să-l ștergeți, ați folosi comanda:

    # sudo rm note.txt

După rularea coomenzii `rm`, veți primi întrebarea dacă sunteți sigur că doriți să eliminați fișierul.

    rm: remove write-protected regular file 'note.txt'?

Pentru a elimina fișierul, trebuie să răspundeți cu `y`, `Y` sau `yes`.

# Ce înseamnă „*-rf*”?

Pentru a evita solicitarea pe care o veți primi cu privire dacă doriți să eliminați fișierul, puteți utiliza indicatorul `–force` sau `-f`.

Deci, dacă rulați comanda:

    $ sudo rm -f note.txt

Fișierul va fi șters imediat după rularea comenzii și nu va trebuie să confirmați.

Indicatorul `-r (–recursive)` este folosit pentru a șterge recursiv tot ce se află într-un director (dosar). Cea mai obișnuită utilizare este ștergerea unui director împreună cu tot ce se afă în acel director.

Deci, de exemplu, dacă aveți un director *test* în `/var/www/html/test/` cu tot cu fișiere și trebuie să îl eliminati/șterge trebuie să rulați următoarea comandă:

    $ sudo rm -r /var/www/html/test/

Puteți combina mai mulți indicatori cu comanda `rm`. Și să nu mai primiți solicitări pentru confirmarea ștergeri, puteți adăuga indicatorul `-f` așa cum este explicat mai sus.

   $  sudo rm -rf /var/www/html/test/

După cum puteți vedea din exemple, această comandă este destul de utilă, iar utilizatorii GNU/Linux o rulează în mod regulat. Partea periculoasă va fi explicată în rândurile următoare.

# Ce face comanda „*sudo rm -rf /*”?

Bara oblică `/` însemnă că comanda va șterge directorul rădăcină care este echivalentul *partiției C* din Windows. Acesta este directorul care conține toate celelalte directoare ale sistemului vostru, ceea ce înseamnă că va șterge toate directoarele din sistem.

Trolii dau adesea această comanda începătorilor cu intenție rea. Din fericire, majoritatea distribuțiilor GNU/Linux au un sistem de siguranță pentru acest caz, așă că dacă ați încercat să rulați:

    $ sudo rm -rf /

Probabil vi se va solicita:

    rm: it is dangerous to operate recursively on '/'
    rm: use --no-preserve-root to override this failsafe

Din fericire pentru troli, prompterul vă spune ce trebuie să faceți pentru a rula comanda și a lăsa-o să-și facă magia diabloică. Deci, dacă ai rula:

    $ sudo rm -rf / --no-preserve-root

Veți șterge întreg sistemul GNU/Linux și nu mai există cale de întoarcere. Trolii s-au adaptat la asta și au început să dea această comandă pentru a evita atenționarea care vă spune că această comandă este periculoasă.

În concluzie, dacă sunteți începător, nu rulați nici una dintre comenzile explicate în această secțiune. Puteți citi mai multe despre comanda `rm` și alți parametri [aici](https://www.ibm.com/docs/en/aix/7.2?topic=r-rm-command) sau rulând comanda `man rm`.
