% Tastele rapide pentru meniul de Boot / setări BIOS
% ThinkRoot99

Puteți accesa meniul de Boot sau setările BIOS folosind taste speciale.

Mai jos este o listă de mărci de PC-uri cu tastele de acces rapid corespunzătoare.

| PRODUCĂTOR | TIP | MODELE | MENIU BOOT | BOOT ONCE	| TASTĂ BIOS | SCHIMBARE PRIORITATE | 
| ----- | ----- | ----- | ----- | ----- | ----- | ----- |
| ACER | - | - | ESC, F12, F9 | - | Del, F2 | - |
| ACER | netbook | AspireOne, Aspire, Timeline | F12 | - | F2 | - |
|ACER | netbook | Aspire v3, v5, v7 | F12 | The „F12 Boot Menu” must be enabled in BIOS. It is disabled by default. | F2 | - |
| APPLE | - | After 2006 | Option | - | - | - |
| ASUS | desktop | - | F8 | - | F9 | - |
| ASUS | laptop | - | ESC | - | F9 | - |
| ASUS | laptop | R503C | F8 | - | Del | - |
| ASUS | netbook | Eee PC 1025c | ESC | - | F2 | Boot Tab, Boot Device Priority, 1st Boot Device, Removable Device, F10 |
| COMPAQ | - | Presario | ESC, F9 | - | F10 | BIOS „Advanced Tab”, Boot Order |
| DELL | desktop | Dimension, Inspiron, Latitude | F12 | Select „USB Flash Drive” | F2 | - |
