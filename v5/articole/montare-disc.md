# Cum se montează un disc în Linux

Atunci când ai mai multe HDD/SSD-uri în calculator sau ai montat un disc în calculator, vrei ca sistemul de operare să vadă acel disc automat.

Acest lucru se poate face și cu ajutorul programelor grafice, dar mie îmi place să fac această treabă din terminal și exemplele de mai jos sunt pentru terminal.

## Verificare disc

Pentru început trebuie să verificați dacă discul este văzut și recunoscut de sistem. Pentru acest lucru trebuie să rulați comanda următoare:

```bash
lsblk
```

sau comanda

```bash
sudo lsblk
```

După rularea comenzi de mai sus în terminal trebuie să apară ceva de genul:

```bash
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
sda      8:0    0 111.8G  0 disk 
├─sda1   8:1    0   512M  0 part /boot
└─sda2   8:2    0 111.3G  0 part /home
                                 /var/cache/pacman/pkg
                                 /var/log
                                 /.snapshots
                                 /
sdb      8:16   0 953.9G  0 disk 
├─sdb1   8:17   0   600M  0 part 
├─sdb2   8:18   0     1G  0 part 
└─sdb3   8:19   0 952.3G  0 part /mnt
zram0  254:0    0     4G  0 disk [SWAP]
```

## Aflarea UUID-ului

De exemplu, în cazul meu SSD-ul care trebuie montat este `sdb`. Acum că va-ți asigurat că sistemul recunoaște discul trebuie să aflați UUID-ul partiției.

Este nevoie de UUID pentru ca pe viitor sistemul să monteze partiția automat și să aveți acces la ea.

> UUID (Universally Unique Identifier) este un identificator standardizat, utilizat pentru a identifica în mod unic informații în sistemele informatice. Este folosit în multe contexte, inclusiv pentru a identifica partiții și dispozitive în sistemele de fișiere.

În cazul meu trebuie să motez partiția `sdb3`. Deci trebuie să aflu UUID-ul pentru partitia `sdb3` și nu pentru tot disk-ul care este `sdb`. Așa că rulez comanda următoare:

```bash
sudo blkid /dev/sdb3
```

În cazul vostru poate fi `sdb1` sau `sdc2` etc., trebuie să fiți atenți la acest detaliu pentru că este foarte important. Rezultatul comenzi trebuie să fie ceva de genul:

```bash
sudo blkid /dev/sdb3
[sudo] password for thinkroot: 
/dev/sdb3: LABEL="fedora" UUID="21e96c1f-32da-4e00-a3cf-02744e3887bd" UUID_SUB="4574c746-82d4-4814-8ca7-4cbfe0f99046" BLOCK_SIZE="4096" TYPE="btrfs" PARTUUID="df914fec-2eb3-47f6-a200-44af7aba0d89"
```

După cum se poate vedea, sunt două UUID-uri, dar pe voi vă interesează doar primul `UUID="21e96c1f-32da-4e00-a3cf-02744e3887bd"`. Notați undeva acest UUID sau mai deschideți un terminal.

## Editarea fișierul fstab

Trebuie să editați fișierul `fstab` pentru a trece informațiile necesare de care are nevoie sistemul pentru a monta automat discul sau partiția la pornirea calculatorului, astfel va trebuie să montați discul manual de fiecare dată.

Pentru început este bine să faceți o copie de rezervă a fișierului `fstab`, în caz că greșiți ceva să puteți să îl restaurați. Folosiți comanda următoare pentru a face un o copie de rezervă:

```bash
sudo cp /etc/fstab /etc/fstab.bak
```

Editarea fișierului se face cu comanda de mai jos folosind un editor de text. În acest exemplu am folosit nano, voi puteți folosi orice editor de text doriți.

```bash
sudo nano /etc/fstab
```

După ce ați deschis fișierul, mergeți jos de tot și introduceți următoarele:

```bash
# /dev/sdb3
UUID="21e96c1f-32da-4e00-a3cf-02744e3887bd" /mnt    btrfs   defaults    0   2
```

Fișierul trebuie să arate asemănător cu imaginea de mai jos:

![](/img/fstab.webp)

După ce ați trecut toate informațiile în fișier, salvați fișierul și să reporniți sistemul.

Fișierul se salvează cu ajutorul comenzilor următoare dacă este deschis cu nano: `Ctrl+o` pentru a salva fișierul și `Ctrl+x` pentru a închide fișierul.

### Explicația fiecărei componente:



- **# /dev/sdb3**: Este o linie de comentariu. Comentariile încep cu caracterul **#** și sunt utilizate pentru a adăugă informații utile. În acest caz comentariul se referă la partiția */dev/sdb3*.

- **UUID="21e96c1f-32da-4e00-a3cf-02744e3887bd"**: UUID-ul este un identificator unic pentru partiția specificată mai sus. Folosind UUID în loc de numele dispozitivului (în cazul meu */dev/sdb3*) se asigură că referința rămâne constantă, chiar dacă ordinea dispozitivelor se schimbă. De exemplu, partiția se poate schimba din */dev/sdb3* în */dev/sdc1*.

- **/mnt**: Acesta este directorul unde se va monta partiția.

- **btrfs**: Acesta specifică tipul sistemului de fișiere al partiției. În acest exemplu, partiția folosește sistemul de fișiere **btrfs**. În cazul vostru poate să fie și `ext4` sau `ntfs` în cazul partiției de Windows.

- **defaults**:  Acestea sunt opțiunile de montare. `defaults` înseamnă că vor fi utilizate opțiunile implicite de montare (citire/scriere, montare automată, execuție de binare, etc.).

- **0**: Acest câmp este utilizat de programul `dump` pentru a decide dacă să facă backup la partiție. **0** înseamnă că `dump` nu va face backup la această partiție.

- **2**:  Acest câmp specifică ordinea în care sistemele de fișiere vor fi verificate la pornire de către `fsck`. **2** indică că această partiție va fi verificată după toate celelalte partiți marcate cu 0 sau 1 din fișierul `/etc/fstab`.

## De final

Dacă ați urmat pași exact și ați fost atenți la detalii atunci nu veți avea nici o problemă și la repornirea calculatorului, sistemul de operare va monta automat partiția. Tot ce mai rămâne de făcut este să mergeți la directorul `/mnt` pentru a vedea și accesa partiția sau disul.

Puteți verifica în terminal daca partiția este montată corect folosind comanda `ls /mnt` și rezultatul este asemănător cu:


```bash
ls /mnt
home  root
```

