% Instalarea Jekyll pe Arch Linux
% ThinkRoot99

[Jekyll](https://jekyllrb.com/) este un generator de sit-uri statice cu sursă deschisă pe care îl folosește foarte multă lume.

Este o combinație excelentă pentru un sit dacă Jekyll este combinat cu o găzduire gratuită cum este cea de la [GitHub](https://github.com/).

Instrucțiunile de instalare pentru Arch Linux se găsesc pe sit-ul oficial al Jekyll, dar configurarea este puțin problematică.

Urmați pași de mai jos pentru o instalare și o configurare fără probleme pe Arch Linux.

1. Instalați [cerințele generale](https://jekyllrb.com/docs/installation/other-linux/#archlinux) pentru Jekyll folosind terminalul:

       $ sudo pacman -S ruby base-devel

2. Jekyll are nevoie de [gems](https://learn.cloudcannon.com/jekyll/gemfiles-and-the-bundler/). Dar pentru început trebuie să adăugați variabilele de mediu la `.bashrc` pentru a configura calea de instalare a gems.

       $ echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc
       $ echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc
       $ echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
       $ source ~/.bashrc

3. Acum se poate instala bundler, care la rândul lui instalează gems-urile.

       $ gem install jekyll bundler

4. Cu toate acestea, în timp ce `bundler` se instalează, o să apară un mesaj de avertizare:

       WARNING: Tou dont't have /home/thinkroot99/.local/share/gem/ruby/2.7.0/bin in your PATH, gem executables will not run.

Dacă încercați acum să creați un sit nou cu comanda următoare:

    $ jekyll new sit

apare mesajul: bash: `jekyll: command not found`

5. Pentru a corecta cele de mai sus, rulați comenzile:

       $ export PATH="/home/thinkroot99/.local/share/gem/ruby/2.7.0/bin:$PATH"
       $ PATH="$PATH:$(ruby -e 'puts Gem.user_dir')/bin"

Evident, trebuie să modificați calea din prima comandă pentru a se potrivi cu configurația dumneavostră.

Acum rulați comanda pentru a crea noul sit.

    $ jekyll new sit

Noul director va fi instalat și poate fi găsit în directorul **/home**.

> ![Jekyll Folder](img/jekyll-folder.png)

După ce ați creat directorul `sit`, în directorul **Home**, puteți construi sit-ul și verifica dacă funcționează totul cum trebuie.

În terminal, mergeți în directorul `sit` și apoi rulați comanda de pornire a sitului pe localhost.

    $ cd sit
    $ bundle exec jekyll serve

Deschideți navigatorul preferat și instroduceți următoarea legătură în bara de adrese: `http://localhost:4000`.

Ar trebui să puteți acesa sit-ul Jekyll.

Dacă nu merge adresa `http://localhost:4000`, accesați adresa care apare în terminal la rularea ultimei comenzi, `bundle exec jekyll serve`.

> ![Jekyll Site](img/jekyll-site.png)
