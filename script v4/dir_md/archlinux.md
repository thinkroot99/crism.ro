# Instalare Arch Linux

## Instalare cu archinstall

După ce am pus imaginea .iso a Arch Linux pe o memorie USB, am băgat acest USB în laptopul `T420s`, am pornit laptopul și am apăsat tasta `F12` pentru a intra în meniul de pornire, de un am ales să pornească Arch Linux de pe memoria USB.

După ce s-a pornit Arch Linux, am pornit scriptul de instalare `archinstall`. Mai departe am făcut următoarele setări:

- Archinstall language >  English
- Mirrors > Mirror region > Romania > Back
- Locales > Keyboard layout > ro-std
   - Locale language > en_US
   - Locale engcoding > UTF-8
   - Back
- Disk configuration > Use a best-effort default partition layout > am ales HDD-ul > btrfs > yes > Use Compresion
- Disk encryptions >  nu am făcut nimic
- Bootloader > GRUB
- Swap > True
- Hostname > am lăsat implicit
- Root password > am setat parola
- User account > Add a user > am adăugat numele de utilizator și parola > yes > Confirm and exit
- Profile > Type > Xorg > 
   -  Graphics driver > All open source
   - Back
- Audio > Pulseaudio
- Kernels > Linux
- Adition packages > git, base-devel
- Network configuration > Use NetworkManager (necesary to configure internet graphically in GNOME and KDE)
- Timezone > Europe/Buchares
- Automatic time sync (NTP) > am lăsat setările implicite
- Optional repositories > multilib

După ce am terminat cu toate setările din scriptul de instalare, archinstall, am trecut la instalarea sistemului selectând butonul `Install`. 
După ce pe ecran a apărut un rezumat la setările făcută am apăsat tasta `enter` și am așteptat să se instaleze Arch Linux.

În funcție de performanțele calculatorului sau a laptopului, și a viteziei internetului instalarea durează mai mult sau mai puțin, dar oricum merge destul de repede. Contează foarte mult viteza la internet.

După ce s-a terminat instalare, am apăsat pe `no` pentru a nu face `chroot`. Și astfel am ieșit din scriptul de instalare `archinstall`. Am rulat comanda de repornire `reboot`, am scos memoria USB și am așteptat să pornească Arch Linux pe laptop.

## Configurare Arch Install

Acum că sa pornit Arch pe laptop, m-am autentificat cu contul de utilizator și am rulat comanda de actualziare a sistemului.

```bash
sudo pacman -Syu
```

După ce m-am asigurat că sistemul este la zi, am trecut la instalarea unei configurați gata făcută.
Am ales [LARBS](https://larbs.xyz/), o configuratie creată de [Luke Smith](https://lukesmith.xyz/), rulând comenzile de mai jos:

```bash
curl -LO larbs.xyz/larbs.sh
sudo sh larbs.sh
```
La rularea scriptului `larbs.sh` apare pe ecran o interfața grafică clasică  de instalare (retro/veche - ca la Debian :)) și mai departe am urmat instrucțiunile de pe ecran.

La primul pas am apăsat pe `ok` după ce am citit ce va face scriptul.

> Welcome to Luke's Auto-Rice Bootstrapping Script!
> 
> This script will automatically install a fully-featured Linux desktop, whitch I use as my maine machine.

Dupa ce am apsat butonul `ok` a mai apărut o fereastră unde aveam de ales să merg mai departe sau să renunț. Bineînțeles că am mers mai departe.

> Be sure the computer you are using has current pacman update and refreshed Arch keyrings.
> <All ready!!>                   <Return...>

La al treilea pas a trebuie sa adaug un nume de utilizator și o parolă. După care a venit pasul patru cu o altă fereastră.

> The user `thinkroot` already exists on this system. LARBS can install for a user aleardy existing, but it will OVERWRITE any conflicting settings/dotfiles on the user account.
> 
> LARBS will NOT overwrite your user files, documents, videos, etc., so don't worry about that, but only click <CONTINUE> if you don't mind your settings being overwritten.

Și aici am apăsat pe butonul `CONTINUE` și pe butonul `Let's go!` unde m-a anunțat că restul instalării este total automat.

Și aici în funcție de viteza internetului și performanțele calculatorului sau a laptopului, insalarea durează mai mult sau mai putin.

## Final

Am așteptat un pic cât scriptul `LARBS` și-a făcut treaba și la final a trebuit să mai apăs un buton `ok`. Am rulat comanda de repornire `reboot` ca să fiu sigur că toate setările se aplică cum trebuie.

După repornire, am autentificat și s-a pornit automat DWM.

![Arch Linux cu DWM by LARBS](/img/larbs.webp)