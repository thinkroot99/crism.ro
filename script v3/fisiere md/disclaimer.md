% Declinare de răspundere

**Acesta este sit-ul meu personal. Opiniile exprimate aici sunt ale mele și nu ale altei persoane sau companii.**

Gândurile și părerile mele se mai schimba din când în când; Consider asta o consețință necesară a unei minți deschise.

Acest sit și secțiunea de blog are scopul de a oferi un punct în timp a diferitelor gânduri care îmi circulă prin cap. Ca atare, orice gânduri și opinii exprimate în postările mele anterioare pot să nu fie aceleași, sau chir similare, cu cele pe care le pot avea astăzi.

# Postări pe blog

În majoritatea cazurilor scriu postări tehnice în secțiunea dedicată blogului. Toate descriu un proces care a funcționat pentru mine la momentul scrierii.

Postările mele tehnice nu sunt, în general, singura modalitate de a atince un obiectiv, s-ar putea să nu fie chiar modalitatea corectă de aface acest lucru. Nu sunt altceva decât un document care descrie ceva ce a funcționat pentru mine sau am depășit o problemă cu care mă confruntam.

Toate postările mele presupun un anumit nivel de abilitate tehnică. Dacă nu te simți confortabil în linia de comandă a unui sistem de operare, probabil ca postările mele nu sunt pentru tine.

> Atenție:
> Nu pot fi tras la răspundere pentru nicio daună adusă sistemelor, datelor sau orice altceva ca urmare a postărilor pe care le creez la secțiunea dedicată blogului.
> Procesele pe care le descriu în postările mele sunt furnizate așa cum sunt, fără nicio garanție.

**Aceast avertisment poate fi modificat în orice moment, fără nici o notificare **