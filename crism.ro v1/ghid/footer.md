<footer>
    [Acasă](ghid-pentru-linux.html) &bull; [crism.ro](/index.html)

    2023, 2024 - ThinkRoot99, este titularul drepturilor de autor asupra acestei lucrări.
    
    Textul și imaginile din pagină sunt libere ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ro)).
    Oricine este liber să folosească, să copieze, să modifice și să (re)distribue aceaste informații în condițiile licențelor de mai sus.
    Sit creat cu [pandoc](https://pandoc.org/).
</footer>
