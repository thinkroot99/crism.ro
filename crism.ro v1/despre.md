% Despre
% ThinkRoot99

Acest sit este în întregime static și scris folosind codul `markdown`, folosesc `pandoc` și un `Makefile` pentru a-l genera. Codul și textul sunt scrie folosind aplicațiile Vim, [Apostrophe](https://flathub.org/ro/apps/org.gnome.gitlab.somas.Apostrophe) sau [Marker](https://flathub.org/ro/apps/com.github.fabiocolacio.marker).

Un exemplu de sursă al acestui sit și fișierul Makefile le puteți găsi [aici](https://crism.ro/proiecte/HTML/pandoc-site/).

# Activitate online: Comunități/Proiecte

- 2012 - 2015: Am fost membru voluntar în cadrul [Fundației Ceata](https://wiki.ceata.org/index.php/Utilizator:Crismblog) unde am promovat programele cu licență liberă.
- 2012 - 2015: Am făcut parte din echipa [Rogentos Linux Group](http://rogentos.ro/). În această echipă am avut rolul de a testa fiecare versiune nouă a distribuției Kogaion.
- 2012: Am făcut parte din echipa [Linux Mint România](https://web.archive.org/web/20210815061143/http://www.linuxmint.ro/) unde am avut funcția de `Moderator` pe forumul comunității.
- 2013: Am participat la a doua întâlnire la nivel național a [Mozilla România](https://wiki.mozilla.org/Romania).
- 2014 - 2018: Am lansat și întreținut sit-ul de știri și tutoriale despre GNU/Linux, gnulinux.ro. După ce am renunțat la domeniu în 2018, acesta a fost cumpărat de altcineva.
- 2021 - 2023: Pe data de 26 ianuarie 2021 am lansat Proiectul „Programe Libere”. În 2023 am renunțat la proiect și am vândut domeniul programelibere.ro.
- 2021 - prezent: Pe data de 24 august 2022 am lansat un [forum](https://forum.crism.ro/) și un serviciu [pastebin](https://pastebin.crism.ro/).
- 2022 - prezent: În noiembrie 2022 am lansat al doilea sit de știri și tutoriale despre GNU/Linux, [rootlinux.ro](https://rootlinux.ro/).
- 2023:  Pe data de 23 martie 2023, am lansat o extensie a sit-ul principal, hardxroot.guru. La sfârșitul anului 2023 am mutat toate articolele pe sit-ul principal (rootlinux.ro) și am renunțat la domeniu.
- 2023 - prezent: Am intrat în echipa Comunitatea Linux România unde am funcția de `Staff` pe [server-ul de Discord](https://discord.gg/GK6HGk634J) și funcția de `Moderator` pe [forumul comunități](https://comunitatealinux.ro/).
- 19 martie 2024 - Am lansat un Generator de parole online care poate fi accesat la adresa: [https://pass.crism.ro](https://pass.crism.ro/).
- 22 martie 2024 - Am lansat un site web simplu și util pentru conversia rapidă între săptămâni și zile. Aceste poate fi accesat la adresa: [https://www.conversie.crism.ro](https://www.conversie.crism.ro/).

# Activitate online: Rețele de socializare

Am avut cont pe multe rețele de socializare și am rămas doar cu câteva, mai exact cu 6 unde sunt activ. Acestea sunt:

- [YouTube](https://www.youtube.com/@thinkroot99): majoritatea clip-urilor sunt despre Linux și Open Source. Public și clipuri scurte care nu au legătură cu tehnologia.
- [TikTok](https://www.tiktok.com/@thinkroot99): public clipuri cu diverse subiecte.
- [Instagram](https://www.instagram.com/thinkroot99/): public mai mult clipuri scurte sau Reels-uri cum le spune :).
- [Mastodon](https://mstdn.ro/@crismblog), [X](https://www.x.com/@thinkroot99) și [Threads](https://www.threads.net/@thinkroot99) - public diverse informații despre Linux, Open Source, articole de pe propriul blog, informații despre magazin etc.

# Activitate online: Financiar

Pe lângă toată activitatea de mai sus și pe lângă serviciu mai încerc să fac și un ban în plus.

- Instalez sisteme de operare: Mă ocup de această activitate de câțiva ani și instalez în majoritatea cazurilor doar Windows, iar Linux instalez foarte rar.
- 2019 - 2023: Am făcut un magazin online pe o platformă internațională pentru a vinde căni, tricouri, hanorace, etc. Magazinul poate fi găsit la adresa: [https://draco-1.creator-spring.com](https://draco-1.creator-spring.com/). În 2023 am renunțat la acest magazin și această platformă.
- 2023 - 2023: Am făcut un magazin online pe o platformă Românească pentru a vinde doar căni personalizate. Magazinul poate fi găsit la adresa: [https://printado.ro/thinkroot.html](https://printado.ro/thinkroot.html). Tot în 2023 am renunțat la acest magazin și această platformă.
- 2023 - prezent: Am făcut un profil pe [kit.co](https://kit.co/ThinkRoot99) unde promovez cea ce folosesc, legăturile spre produse sunt legături afiliate - dacă dați clic pe ele, aceste legături produc bani.
- 2023 - prezent: Începând cu februarie 2023 am lansat noul magazin online pe o platformă mai avansată și i-am pus un domeniu. Magazinul poate fi accesat la adresa: [https://shop.crism.ro](https://shop.crism.ro)