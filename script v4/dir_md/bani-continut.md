# Ce este mai important: bani sau conținutul de calitate?

Când cineva se apucă de făcut/creat conținut indiferent că este text sau video, o face pentru un anumit scop. Acest scop sunt bani sau să ofere o informație utilă oamenilor (pe scurt conținut de calitate).

Părerea mea personală este că toată lumea este interesată de bani, indiferent că se dorește crearea de conținut doar pentru a face bani, sau că se dorește a se oferi o informație utilă către oameni indiferent de ce sume va câștiga persoana în cauză - creatorul de conținut.

Dar nu cred că exista cineva pe care să nu îl intereseze bani atunci când se apucă de creat conținut. Nu o să dau exemplu pe nimeni, în afară de mine.

De când m-am apucat de bloging, la modul amator - că nu am făcut bloging la nivel profesionist niciodată. Ma interesat bani aproape tot timpul, acum sunt în perioada când nu mă interesează bani așa de mult și în tot acest timp am strâns din reclamele oferite de google pe blog o sumă pe care nici măcar nu o pot scoate, așa mare este.

Am verificat Google AdSense și se pare că suma s-a înmulțit și acum dețin frumoasa sumă de 9.49 € - o sumă impresionantă care s-a strânsă în câțiva ani buni (nu 2, 3 ani) - îmi vine să rând singur :).

Din ce țin minte, am pus reclame pe blog sau sit de două ori - o dată pe un blog (nu mai știu ce blog a fost) și a două oară pe sit-ul/blogul [rootlinux.ro](https://rootlinux.ro). Și în rest nu am mai pus reclame pe nimic, nici pe acest sit nu există reclame, în afară de un buton pentru donație.

Și proiectul rootlinux.ro o să sufere o transformare în curând și nu cred că o să mai existe reclame pe el - doar un buton de donație o să mai existe. De ce sunt sigur de acest lucru? - pentru că am început transformările proiectului. Nu mai există plugin de SEO, poate o să dispară și reclamele (mă mai gândesc) și se va schimba structura blog-ului.

În concluzie, cea ce vreau să spun este că toată lumea este interesată de bani, diferența este că sunt persoane care fac conținut de calitate în primul rând și pe locul doi pun bani. Că există reclame, că exista un buton de donație - ceva tot există pentru a face bani.

Cred că m-am exprimat un pic greșit în rândurile de mai sus. Cred că sunt persoane pe care nu le interesează să facă bani din crearea de conținut, dar cel puțin eu încă nu am văzut acest lucru sau această persoană.