% De ce am renunțat la Arch Linux
% ThinkRoot99

Arch Linux nu este un sistem de operare sau o distribuție Linux rea, din contră este un sistem foarte bun - dar trebuie să fi un utilizator de Linux avansat, să cunoști terminalul și anumite comenzi pentru a folosi acest sistem de operare.

Am renunțat la el pentru că m-am săturat să configurez la el foarte mult timp. Pentru a face Arch Linux pe placul meu și pentru nevoile mele îmi luat cam două zile, dacă nu chiar trei zile.

M-am săturat de acest lucru. Am nevoie de un sistem de operare funcțional cu tot ce îmi trebuie. Bineînțeles că așa ceva nu există, dar sunt câteva distribuții Linux care se apropie de ceea ce am nevoie.

Aceste sisteme de operare trebuie să fie funcționale după instalare, să nu fiu nevoit să le lucrez la ele două, trei zile. Programele să se instaleze ușor, personalizarea să se facă ușor.

Pe scurt, totul să fie la un clic pe de mouse.

Și să nu uit, să ofere aproape tot ce este nou în tehnologie - astfel de sisteme de operare sunt puține. Printre ele sunt Fedora, openSUSE, Arch Linux, Gentoo (pot spune), deci multe nu sunt.

Distribuția pe care o folosesc în prezent este Fedora - aduce tot ce este nou în tehnologie, programele sunt la ultimele versiune, este ușor de folosit, foarte puțin de lucru la distribuție etc.

Nu mai folosesc nici i3 sau Sway. Fedora o folosesc cu GNOME pentru că îmi place grafica mai mult decât interfețele simple oferite de i3 sau Sway plus că la GNOME nu mai trebuie să fac nimic, așa cum făceam la i3 sau Sway.

Deci, cum am scris mai sus, am nevoie ca totul să fie cât mai simplu și ușor de folosit. M-am săturat să fac un sistem de operare funcțional între două, trei zile sau chiar mai mult.