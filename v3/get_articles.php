<?php
// Verificați dacă parametrii au fost transmiși corect
if (isset($_GET['start']) && isset($_GET['count'])) {
    $start = intval($_GET['start']);
    $count = intval($_GET['count']);

    // Directorul în care se află fișierele HTML
    $htmlDirectory = "note/";

    // Lista fișierelor din director
    $files = glob($htmlDirectory . "*.html");

    // Numărul total de fișiere
    $totalFiles = count($files);

    // Limităm numărul de fișiere în funcție de parametrii de start și numărul de postări pe pagină
    $limitedFiles = array_slice($files, $start, $count);
    
    // Încărcăm conținutul fiecărui fișier și îl returnăm ca răspuns
    $html = '';
    foreach ($limitedFiles as $file) {
        // Adăugați conținutul fișierului HTML
        $html .= file_get_contents($file);
    
        // Adăugați un div gol pentru spațiu vizual între postări
        $html .= '<div style="height: 20px;"></div>';
    }
    
    // Verificăm dacă mai sunt fișiere disponibile
    $morePostsAvailable = $start + $count < $totalFiles;

    // Returnăm conținutul HTML și un indicator pentru disponibilitatea de mai multe postări
    echo json_encode(array("html" => $html, "morePostsAvailable" => $morePostsAvailable));
} else {
    // Returnați o eroare sau un mesaj dacă parametrii lipsesc
    echo json_encode(array("error" => "Parametrii lipsesc."));
}
?>
