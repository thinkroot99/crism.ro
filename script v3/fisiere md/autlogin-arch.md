% Instalarea și Configurarea LightDM cu Autologin în Arch Linux

LightDM este un manager de autentificare (display manager) ușor și rapid pentru Linux.

Nu este îndeajuns să se instaleze doar LightDM, mai este necesar și instalarea unui Greeter. Greeter este interfața grafică a managerului de autentificare.

Acest Greeter permite utilizatorului să selecteze o sesiune (Xfce, GNOME, Plasma, etc.) plus alte câteva opțiuni.

## 1. Instalarea LightDM

Pentru început trebuie instalat LightDM și un greeter. Iar comanda de instalare este:

`sudo pacman -S lightdm lightdm-gtk-greeter`

## 2. Activarea LightDM

Comanda următoare activează serviciul pentru ca acesta să pornească la automat.

`sudo systemctl enable lightdm`

## 3. Configurarea LightDM pentru autologin

Pentru ca autentificarea să se facă automat este nevoie să se editeze fișierul `lightdm.conf`.

Se rulează comanda următoare pentru edita fișierul cu editorul de text vim. Voi puteți înlocui vim cu editorul de text preferat.

`sudo vim /etc/lightdm/lightdm.conf`

Caută secțiunea `[Seat:*]` și modifica următoarele linii:

```ini
[Seat:*]
autologin-user=USERNAME
autologin-session=i3
```

Înlocuiește `USERNAME` cu numele tău de utilizator și `i3` cu numele mediului de lucru pe care îl folosești.

## 4. Configurarea sesiuni pentru i3

Această parte este doar pentru utilizatori care au sau folosesc managerul de ferestre **i3**.

Asigură-te că ai instalat i3. Dacă nu, instalează-l cu comanda următoare:

`sudo pacman -S i3`

Trebuie să se creeze fișierul de configurare pentru sesiunea i3, dacă nu există deja.

`sudo vim /usr/share/xsessions/i3.desktop`

Conținutul fișierului `i3.desktop` ar trebui să arate astfel:

```ini
[Desktop Entry]
Name=i3
Comment=improved dynamic tiling window manager
Exec=i3
Icon=i3
Type=Application
```

## 5. Crearea Grupului `autologin`

Verifică dacă grupul `autologin` există deja. Dacă nu, creează-l:

`sudo groupadd autologin`

Adaugă utilizatorul în grupul `autologin`:

`sudo gpasswd -a USERNAME autologin`

Înlocuiește `USERNAME` cu numele utilizatorului tău.

## 6. Verificarea setărilor

Poți verifica dacă utilizatorul a fost adăugat corect în grupul `autologin` folosind:

`groups USERNAME`

sau:

`id USERNAME`

Aceste comenzi ar trebui să listeze grupurile din care face parte utilizatorul, inclusiv `autologin`.

Dacă totul este făcut urmând pași de mai sus, tot ce mai rămâne de făcut este să se repornească sistemul.

## Concluzie

Acum, Arch linux ar trebui să logheze automat în contul utilizatorului specificat și să lanseze managerul de ferestre i3.

