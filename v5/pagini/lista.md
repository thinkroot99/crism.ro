# Lista listelor

Sunt abonat la multe sit-uri și jurnale despre tehnologie și open source. Dar mai jos este o listă de sit-uri și jurnale personale și nu tehnice.

- [The Batuhan's Blog](https://www.yyilmaz.com.tr/) - [[Feed RSS]]( https://www.yyilmaz.com.tr/feed.xml)
- [Robert Birming](https://birming.com/) - [[Feed RSS]](https://birming.com/feed/)
- [Kev Quirk](https://kevquirk.com/) - [[Feed RSS]](https://kevquirk.com/feed)
- [~bt](https://btxx.org/) - [[Feed RSS]](https://btxx.org/index.rss)
- [Cory Dransfeldt](https://coryd.dev/) - [[Feed RSS]](https://feedpress.me/coryd) / [[Feed Json]](https://feedpress.me/coryd.json)
- [Daniel Wayne Armstrong](https://www.dwarmstrong.org) - [[Feed RSS]](https://www.dwarmstrong.org/feed.xml)
- [David Heinemeier Hansson](https://world.hey.com/dhh) - [[Feed Atom]](https://world.hey.com/dhh/feed.atom)
- [Juha-Matti Santala](https://hamatti.org/) - [[Feed Atom]](https://hamatti.org/feed/feed.xml)
- [Hey, I'm Marc.](https://atthis.link/) - [[Feed Atom]](https://atthis.link/rss.xml)
- [Drew DeVault's blog](https://drewdevault.com/) - [[Feed Atom]](https://drewdevault.com/blog/index.xml)
- [Robb Knight](https://rknight.me/) - [[Feed RSS]](https://rknight.me/subscribe/posts/rss.xml) / [[Feed Atom]](https://rknight.me/subscribe/posts/atom.xml) / [[Feed Json]](https://rknight.me/subscribe/posts/feed.json)
- [Cosmin's Website](https://cosmin.hume.ro/) - [[Feed Atom]](https://cosmin.hume.ro/post/atom.xml)