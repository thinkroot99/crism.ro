% Spotify pentru terminal
% ThinkRoot99

Ascult multă muzică prin Spotify și a vrut să rulez un client în terminal pentru a nu mai folosi interfața grafică - motivul pentru acest lucru este pentru a nu mai consuma din resursele laptopului.

Aplicațiile grafice consumă cele mai multe resurse ale unui calculator, iar aplicațiile pentru terminal nu sunt consumatoare mari de resurse.

După câteva căutări am găsit aplicația **[spotify-tui](https://github.com/Rigellute/spotify-tui)** care este o aplicație pentru terminal și scrisă în limbajul Rust.

**spotify-tui** este o aplicație simplă și ușor de configurat. Iar pentru a funcționa, aplicația are nevoie de câteva setări făcute.

> ![spotify-tui](img/spotify-tui.png)

3 sfaturi de luat în considerare:

- aveți nevoie de un cont premium la Spotify, altfel aplicația spotify-tui nu se poate conecta la serviciul de streaming.
- numele calculatorului nu trebuie să conțină spațiu. Exemplu: Da: „thinkroot99@archlinux”, Nu: „thinkroot@arch linux”.
- trebuie să aveți aplicația oficială instalată și deschisă. Altfel nu puteți controla Spotify din terminal.

# Cum se instalează spotify-tui

Pentru a instala spotify-tui m-am folosit de comanda `homebrew`. Dar mai multe detalii despre instalare găsiți pe [GitHub](https://github.com/Rigellute/spotify-tui#installation).

    [instalare]
    $ brew install Rigellute/tap/spotify-tui
    [rulare]
    $ spt

După instalare trebuie repornit calculatorul, cel puțin eu așa am fost nevoit să fac - până atunci nu am putut folosi spotify-tui.

# Ce setări trebuie efectuate

Pentru a funcționa spotify-tui, acesta trebuie conectat la API-ul Spotify. Urmați pași de mai jos pentru conectare la API.

- Accesați pagina pentru [dezvoltatori](https://developer.spotify.com/dashboard/applications).
- Faceți clic pe `Creat an App`.
- Introduceți un nume, o descriere și bifați căsuțele, după care dați clic pe `Create`.
- Dați clic pe `Edit settings` și la `Redirecet URIs` introduceți legătura: `http://localhost:8888/callback` - acum salvați.
- Deschideți un terminal și rulați aplicația spotify-tui cu comanda `spt`.
- Introduceți codul de client (Client ID) și codul secret (Client Secret).
- O să fiți redirecționați către o pagină Spotify unde vi se va solicita permisiunile.

După ce ați acordat permisiunile, veți fi redirecționați către `localhost`. Dacă totul merge bine, adresa URL va fi analizată automat.

În cazul în care server-ul local eșuează din anumite motive, veți fi redirecționat către o pagină albă care arată mesajul: `Connection Refused`. În acest caz copiați adresa URL și lipiți-o în terminal.

Către o pagină albă am fost și eu redirecționat după efectuarea tuturor pașilor, dar fără nici un mesaj.

Am rezolvat problema prin repornirea calculatorului și pe urmă totul a funcționat cum trebuie.
