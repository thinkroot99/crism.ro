% Un blog cât mai simplu

De când am dat de multe sit-uri și blog-uri construite doar în HTML și CSS, comunități și cluburi create în jurul acestor sit-uri și blog-uri cât mai simple posibil, am creat și eu acest sit sau blog în acest fel - mai ales că de mult timp îmi doream așa ceva.

Am început cu șabloane luate de la altcineva, după un timp am modificat acele șabloane. Pe urmă am construit tot sit-ul de la zero cu ajutorul AI-ului (îmi place mult să mă joc cu aceste AI-uri). Dar parcă tot este mare, încărcat acest sit/blog și nu îmi place acest lucur.

Nu mă înțelegeți greșit, îmi place cum arată și unde am ajuns cu sit-ul (îl consider sit pentru că blog-ul este doar o parte din el), dar simt că este prea încărcat în continuare - parcă am nevoie de ceva mai simplu.

Aproape în fiecare zi construiesc tot felul de șabloane cât mai simple pentru un sit cât mai minimalist. Dar nu lucrez doar la sit și șabloane HTML și CSS, lucrez și la automatizare, pentru că crearea unui sit HTML și CSS este foarte simplu - cu câteva lini de cod s-a creat sit-ul și l-am urcat pe internet.

Problema este că vreau ca totul să fie automatizat. Conținutul pentru blog vreau să îl scriu în cod markdown (cea ce fac și în prezent), după ce salvez fișierul markdown vreau să rulez un singur script pentru a converti fișierele markdown (.md) în fișiere HTML (.html) și cu ajutorul unor scripturi JavaScript și ce mai este nevoie restsul să se facă automat - integrarea legăturile în fișierul index.html, urcarea fișierelor prin fpt la contul de căzduire a sit-ului, etc.

Practit tot ce trebuie să fac este să rulez un script în terminal și restul să se facă automat. Acest lucru se face cel mai greu pentru că nu mă pricep la programare și fac totul cu ajutorul AI-ului. Codul HTMl și CSS îl pot scrie eu, dar dacă tot apelez la AI pentru restul codurilor și scripturilor, apelez la AI și pentru două, trei lini de HTML și CSS :



