% Ce este Fedora? Tot ce trebui să știți
% ThinkRoot99

Alături de Arch și Debian, Fedora este una dintre cele „*trei mari*” distribuții GNU/Linux. Aceasta își are originea în Red Hat Linux, distribuția originală bazată pe RPM.

Fedora este cunoscută pentru tehnologiile sale de ultimă generație, pentru cele mai recente programe și pentru actualizările frecvente. Este, de asemenea, una dintre puținele distribuții importante care adoptă [GNOME](https://www.gnome.org/) curat.

În ultima vreme, distribuția a devenit mai prietenoasă cu utilizatorul, cu un ecran de bun venit, suport pentru Flatpak și posibilitatea de a activa depozitele terțe.

Informații de bază și specificații
Iată o defalcare a informațiilor de bază despre Fedora și a specificațiilor moderne relevante:

| Categorie                    | Informații              |
| ---------                    | ----------              |
| Dată lansare                 | 2003                    |
| Bază                         | Original                | 
| Mediu de lucur               | GNOME                   |
| Audio                        | PipeWire                |
| Server de afișare            | Wayland                 |
| Sistem de fișiere            | Btrfs                   |
| Sistem init                  | systemd                 |
| Format pachete               | RPM*, Flatpak, AppImage |
| Ciclu de lansare             | Șase luni               |
| Dată lansare ultima versiune | 18/04/2023              |
| Ultima versiune              | 38                      |

Fedora are o serie de setări implicite notabile de care ar trebui să țină cont cei care doresc să schimbe distribuțiile moderne, inclusiv `PipeWire`, `Wayland` și `Btrfs`.

# Istoria Fedora

Istoria Fedora este indisolubil legată de istoria Red Hat. Inițial, distribuția a fost cunoscută sub numele de „*Fedora Linux*”, apoi „*Fedora Core*”, pentru ca în cele din urmă să fie numită doar Fedora.

Fedora Linux a fost un depozit al unei terțe părți pentru Red Hat Linux, în timp ce Fedora Core a fost o versiune gratuită, întreținută de comunitate, a [Red Hat Enterprise Linux](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux). În prezent, Fedora se află în amonte la Red Hat Enterprise Linux și servește ca o bună previzualizare a ceea ce va urma în versiunea Workstation plătită.

Deși este un proiect comunitar, Fedora este în mod evident finanțat de [Red Hat](https://www.redhat.com/), care acum este deținută de [IBM](https://www.ibm.com/).

# Caracteristici notabile

Cu o istorie atât de lungă, realizările sunt nenumărate. Din fericire, această distribuție are o mulțime de avantaje în momentul de față, așa că nu există niciun motiv să ne întoarcem în istoria străveche pentru a găsi cele mai importante realizări.

## 1. Vine cu GNOME stoc

Una dintre cele mai mari atracții ale Fedora este utilizarea mediulu de lucru GNOME, în mare parte standard.

Ai încredere că, având în vedere că GNOME este dintre cele „*două mari*” medii de lucru GNU/Linux și că atât de multe distribuții îl folosesc, GNOME stoc ar fi ceva obișnuit, dar nu este. În prezent, majoritatea distribuțiilor care livrează GNOME includ multe modificări care încearcă să facă GNOME să se conformeze convețiilor GUI din anii `90.

Fedora nu face acest lucru. În schimb, oferă unul dintre cele mai curate și mai actualizate exemple de GNOME în afara instantaneului de dezvoltare a sistemului de operare [GNOME OS](https://os.gnome.org/).

## 2. Ușor de utilizat

În timp ce multe distribuții fac tot posibilul pentru a ajuta noii utilizatori cu instalatoare grafice și ecrane de bun venit, nimeni nu se aștepa ca Fedora, un stâlp al open source, să se alăture. Dar a făcut-o. Fedora acceptă Flatpak din start, pe lângă fișierele RPM.

În plus, acum puteți activa depozitele de la terți în timpul configurării. La instalare, sunteți întâmpinat cu un ecran de bun venit liniar și util, care explică elementele de bază ale interfeței de utilizare, gesturile și comenzile rapide.

Considerată în mod tradițional ca fiind o distribuție pentru utilizatorii de GNU/Linux (utilizatorii avansați), Fedora modernă este aproape de a obține recomandări din partea publicului larg, iar unii oameni o numesc chiar noul Ubuntu.

## 3. Oferă programe de ultimă generație

O dată la șase luni se lansează o versiune nouă, fără versiuni cu suport pe termen lung (LTS), astfel încât veți primi întotdeauna cele mai recente actualizări, iar cea mai recentă versiune este întodeauna ediția principală.

Dincolo de actualizările frecvente, Fedora își învinge majoritatea rivalilor în ceea ce privește furnizarea implicită de programe de ultimă generație, open source. A fost prima distribuție majoră care a trecut de la ext4 la `Btrfs`, de la X11 la `Wayland` și de la PulseAudio la `PipeWire`.

## 4. Fedora este de încredere

Deși nu vezi prea des o distribuție cunoscută pentru că este de ultimă generație și fiabilă, nu vezi prea des nici distribuții sponsorizate de IBM.

Atunci când Fedora introduce schimbări fundamentale, ca în cazul exemplelor de mai sus, este un semn bun că acele tehnologii sunt în sfârșit pregătite pentru prima oară. Apoi, începeți să observați că alte distribuții încep să o urmeze încet.

Dacă vreți să fiți în avangardă, există versiuni de dezvoltare ale Fedora, cum ar fi Rawhide, despre care nu am scris nimic în acest articol.

# Ediții de Fedora

Fedora oferă trei ediții normale și două „*ediții emergente*” oficiale. Cu toate acestea, doar două dintre cele cinci sunt destinate utilizării pe calculatoare. Voi sări peste variantele pentru servere și cele axate pe IoT.

## 1. Workstation

> ![Fedora Workstation 36 with GNOME 42](img/Fedora-Workstation-36.png)

Workstation este ediția emblematică a proiectului. Aceasta dispune de mediu de lucru GNOME și suportă Flatpak gata de utilizare.

> [Descărcați](https://getfedora.org/en/workstation/download/)

## 2. Silverblue

Ediția emergentă Fedora Silverblue este o variantă imuabilă a Fedora Workstation. Diferența majoră constă în faptul că utilizatorii vor întâmpina problabil probleme la instalarea RPM-urilor, deoarece Flatpak este formatul nativ de pachete al Silverblue.

> [Descărcați](https://silverblue.fedoraproject.org/download)

## 3. Kinoite

Kinoite este o ediție viitoare a Fedora care nu este încă listată pe pagina de start. Kinoite este pur și simplu o alternativă la Silverblue cu mediul de lucru Plasma (KDE).

> [Descărcați](https://kinoite.fedoraproject.org/download/)

# Fedora Spins

La fel ca multe alte distribuții, Fedora oferă o varietate de descărcări alternative, cu o gamă variată de medii de lucru. Fedora numește aceste variante „*Spins*”.

## 1. KDE Plasma Desktop

> [Fedora KDE Plasma Desktop](img/Fedora-KDE-Plasma.png)

KDE Plasma Spin lasă cele mai multe dintre valorile implicite KDE intacte, schimbând doar imaginea de fundal și pictograma de la meniu și permițând dubla apăsare pentru deschidere/lansare.

> [Descărcați](https://spins.fedoraproject.org/en/kde/)

## 2. Xfce Desktop

> ![Fedora Xfce Desktop](img/Fedora-Xfce-Desktop.png)

Xfce Spin folosește o interfață tradițională de tip BSD/macOS. Arată destul de bine pentru un mediu de lucru ușor.

> [Descărcați](https://spins.fedoraproject.org/xfce/)

## 3. LXQt Desktop

> ![Fedora LXQt Desktop](img/Fedora-LXQt-Desktop.png)

Fedora nu se oprește la cele „trei mari” medii de lucru, ci oferă și LXQt. Această alternativă la LXDE, bazată pe Qt, oferă o experiență simplă, asemănătoare cu cea din Windows XP.

> [Descărcați](https://spins.fedoraproject.org/en/lxqt/)

## 4. MATE-Compiz Desktop

> ![Fedora MATE-Compiz Desktop](img/Fedora-MATE-Compiz-Desktop.png)

Blocat în timp, MATE-Compiz Spin este perfect pentru cei care tânjesc după zilele de glorie ale lui Ubuntu GNOME 2 și ale efectelor de birou strălucitoare.

> [Descărcați](https://spins.fedoraproject.org/en/mate-compiz/)

## 5. Cinnamon Desktop

> ![Fedora Cinnamon Desktop](img/Fedora-Cinnaomn-Desktop.png)

În mod surprinzător, Fedora oferă o versiune cu Cinnamon, mediul de lucru intern al Linux Mint.

Această iterație a lui Cinnamon nu are marca Fedora, o culoare de accent albastru, o bară de activități subțire și este lipsită în mod special de XApps de la Linux Mint. În ciuda acestor schimbări, este reconfortant să vezi Cinnamon utilizat diferit față de Linux Mint.

În afară de SOAS, acesta este singurul Spin care nu vine cu tapetul implicit al Fedora.

> [Descărcați](https://spins.fedoraproject.org/en/cinnamon/)

## 6. LXDE Desktop

> ![Fedora LXDE Desktop](img/Fedora-LXDE-Desktop.png)

Pentru cei care preferă versiunea originală a LXQt bazată pe GTK, Fedora vă oferă și această variantă. LXDE este un alt mediu de lucru ușor, modelat după versiunile mai vechi de Windows.

> [Descărcați](https://spins.fedoraproject.org/en/lxde/)

## 7. SOAS (Sugar on a Stick)

> ![Fedora SOAS](img/Fedora-SOAS.png)

După ce a epuizat toate (aproape) mediile de lucru de care ați auzit, Fedora continuă să impresioneze cu SOAS Spin. Poate îl cunoașteți mai bine sub numele de Sugar on a Stick, care, după cum îi spune și numele, este Sugar pe un stick USB bootabil.

Mediul de lucru de învățare timpurie a devenit cunoscut pe scară largă ca sistem de operare pentru proiectul OPLC (One Laptop per Child).

> [Descărcați](https://spins.fedoraproject.org/en/soas/)

## 8. i3 Tilign WM

> ![Fedora i3 Tiling Window Manager](img/Fedora-i3-Tiling-Window-Manager.png)

Da, Fedora are chiar și un Spin cu Window Manager de tip Tiling, așa că acum și tu poți posta pe subreddit-ul [r/unixporn](https://www.reddit.com/r/unixporn/). Lăsând gluma la o parte, i3 este unul dintre cele mai populare WM-uri de tip tiling și un punct de plecare perfect pentru a intra în lumea WM-urilor de tip tiling.

Această clasă de managere de ferestre oferă o eficiență superioară a ecranului, costuri de sistem mai mici și o viteză sporită de interacțiune cu utilizatorul prin intermediul comenzilor rapide de la tastatură.

> [Descărcați](https://spins.fedoraproject.org/en/i3/)

# Pentru cine este Fedora?

Nu numai că Fedora este o distribuție originală, dar a devenit și foarte ușor de utilizat în ultimii ani. Aceasta este o combinație rară, deoarece Arch Linux nu este deloc ușor de utilizat, iar Debian a adăugat un program de instalare grafică în urmă cu câțiva ani.

Dacă sunteți un fan GNOME, Fedora este singura distribuție majoră care oferă o versiune actualizată a GNOME vanilla. Utilizatorii de macOS și persoanele mai tinere care au crescut cu dispozitive mobile pot aprecia, de asemenea, GNOME. Iar utilizatorii de laptopuri se vor îndrăgosti de gesturile touchpad-ului pentru controlul spațiului de lucru.

Fedora este o alegere de top în rândul calculatoarelor GNU/Linux, iar utilizatorii noi de GNU/Linux și chiar jucătorii pot fi convinși să aleagă această distribuție.
