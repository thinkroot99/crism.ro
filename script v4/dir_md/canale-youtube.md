# Abonamentele mele la canale de YouTube

Mai jos este o listă cu canalele de YouTube pe care le urmăresc în prezent. Toate canalele sunt ordonate în ordine alfabetică.

Sunt multe canale în lista de mai jos la care sunt abonat. Dar din păcate multe nu au mai postat un clip de foarte mult timp.

Știu că era mai frumos dacă această listă era pusă pe categorii. ~~Promit că o să aranjez lista pe categorii, așa că mai aruncați câte un ochi pe aici~~ :) Am aranjat lista într-un tabel frumos pe trei coloane.

| Coloana 1                                | Coloana 2                                | Coloana 3                                |
|------------------------------------------|------------------------------------------|------------------------------------------|
| [10leej](https://www.youtube.com/@10leej) | [Ambro's](https://www.youtube.com/@ambros470) | [Brodie Robertson](https://www.youtube.com/@BrodieRobertson) |
| [A1RM4X](https://www.youtube.com/@A1RM4X) | [Andrea Borman](https://www.youtube.com/@AndreaBorman) | [BugsWriter](https://www.youtube.com/@bugswriter_) |
| [Adrian tutu](https://www.youtube.com/@AdrianTutuOficial) | [Arkengheist 2.0](https://www.youtube.com/@Arkengheist20) | [Bullet](https://www.youtube.com/@BulletAbravia) |
| [Akamai Developer](https://www.youtube.com/@AkamaiDeveloper) | [Awesome Open Soource](https://www.youtube.com/@AwesomeOpenSource) | [Cajun Koi Academy](https://www.youtube.com/@CajunKoiAcademy) |
| [Alex Bratu](https://www.youtube.com/@AlexBratu90) | [Blake Hensley](https://www.youtube.com/@blakehensley) | [Cassidy James Blaede](https://www.youtube.com/@CassidyJames) |
| [All Things Secured](https://www.youtube.com/@AllThingsSecured) | [Chris Titus Tech](https://www.youtube.com/@ChrisTitusTech) | [CipFlixTV](https://www.youtube.com/@CipFlixTV) |
| [Cat and Andrew](https://www.youtube.com/@ComputersAndTechAndAndrew) | [Cristian Poe](https://www.youtube.com/@CristianPoe) | [Cristian Florea](https://www.youtube.com/@cristianflorea) |
| [Cezar Machidon](https://www.youtube.com/@CezarOutdoors) | [Cipri Costea](https://www.youtube.com/@cipri29) | [Daniel TheArtist](https://www.youtube.com/@DanielTheArtist) |
| [DASGeek](https://www.youtube.com/@dasgeek) | [DCTekkie](https://www.youtube.com/@DCTekkie) | [Destination Linux](https://www.youtube.com/@DestinationLinux) |
| [Diego Sandoval](https://www.youtube.com/@diego_sandoval) | [Distro Domain](https://www.youtube.com/@distrodomain) | [DistroTube](https://www.youtube.com/@DistroTube) |
| [Doduț](https://www.youtube.com/@DodutOfficial) | [Dreams of Autonomy](https://www.youtube.com/@dreamsofautonomy) | [eBuzz Central](https://www.youtube.com/@eBuzzCentral) |
| [EF - Linux Made Simple](https://www.youtube.com/@eflinux) | [Electronics Repair School](https://www.youtube.com/@electronicsrepairschool) | [Eric Murphy](https://www.youtube.com/@EricMurphyxyz) |
| [ETA PRIME](https://www.youtube.com/@ETAPRIME) | [EZ Tutorials](https://www.youtube.com/@EZTutorialsOnline) | [ForrestKnight](https://www.youtube.com/@fknight) |
| [Gadget Boutique](https://www.youtube.com/@GadgetBoutiqueRo) | [Geek Outdoors](https://www.youtube.com/@GeekoutdoorsBrand) | [George Buhnici](https://www.youtube.com/@gbuhnici) |
| [Hallden](https://www.youtube.com/@Hallden_) | [InfinitelyGalactic](https://www.youtube.com/@InfinitelyGalactic) | [IONEȘTII](https://www.youtube.com/c/Ione%C8%99tii) |
| [It's FOSS - Linux Portal](https://www.youtube.com/@Itsfoss) | [Jake@Linux](https://www.youtube.com/@JakeLinux) | [Jamie Marsland](https://www.youtube.com/@jamiewp) |
| [Jeff Geerling](https://www.youtube.com/@JeffGeerling) | [Jorge Castro](https://www.youtube.com/@JorgeCastro) | [Joshua Strobl](https://www.youtube.com/@JoshuaStrobl) |
| [Just Perfection](https://www.youtube.com/@jperfection) | [jvscholz](https://www.youtube.com/@jvscholz) | [Learn Linux TV](https://www.youtube.com/@LearnLinuxTV) |
| [libreofficehelp](https://www.youtube.com/@libreofficehelp) | [Linux Lodge](https://www.youtube.com/@linuxlodge) | [Linux Out Loud](https://www.youtube.com/@LinuxOutLoud) |
| [Linux Saloon](https://www.youtube.com/@linuxsaloon) | [LinuxNext](https://www.youtube.com/@linuxnext) | [LinuxScoop](https://www.youtube.com/@linuxscoop) |
| [Lorin](https://www.youtube.com/@lorinq) | [Luke Smith](https://www.youtube.com/@LukeSmithxyz) | [MagnatesMedia](https://www.youtube.com/@MagnatesMedia) |
| [Majii](https://www.youtube.com/@Majii_ro) | [Mariciu](https://www.youtube.com/@Mariciu) | [Mariciu TV](https://www.youtube.com/@MariciuTV) |
| [Marius Vasilescu](https://www.youtube.com/@MariusVasilescu90) | [memo2k1](https://www.youtube.com/@memo2k1) | [Michael Horn](https://www.youtube.com/@MichaelNROH) |
| [Michel MJD](https://www.youtube.com/@MichaelMJD) | [Michael Tunnell](https://www.youtube.com/@michael_tunnell) | [Mozilla Thunderbird](https://www.youtube.com/@thunderbirdproject) |
| [My Linux For Work](https://www.youtube.com/@mylinuxforwork) | [Nash Well Coding](https://www.youtube.com/@nashwellscoding) | [NETVN82](https://www.youtube.com/@NETVN82) |
| [NetworkChuck](https://www.youtube.com/@NetworkChuck) | [NH Soft](https://www.youtube.com/@NHSoft) | [Nir Lichtman](https://www.youtube.com/@nirlichtman) |
| [No Text To Speech](https://www.youtube.com/@NoTextToSpeech) | [nwradu](https://www.youtube.com/@nwradu) | [Old Computers Sucked](https://www.youtube.com/@old-computers-sucked) |
| [Ombladon20CMofficial](https://www.youtube.com/@Ombladon20CMofficial) | [Podcast Neflitrat](https://www.youtube.com/@PodcastNefiltrat) | [Raluca Simple Living](https://www.youtube.com/@ralucasimpleliving) |
| [RAV Tech](https://www.youtube.com/@RAVTechz) | [Recorder](https://www.youtube.com/@RecorderRomania) | [RevoLinux](https://www.youtube.com/@revolinuxxx) |
| [RMS](https://www.youtube.com/@RMS1) | [SavvyNik](https://www.youtube.com/@SavvyNik) | [Shobby](https://www.youtube.com/@Shobby) |
| [Sol Does Tech](https://www.youtube.com/@SolDoesTech) | [STACS TV](https://www.youtube.com/@STACSTV) | [Stephen's Tech Talks](https://www.youtube.com/@stephenstechtalks5377) |
| [SubcarpatiRO](https://www.youtube.com/@SubcarpatiRO2010) | [Sudo Show](https://www.youtube.com/@SudoShow) | [Sun Knudsen](https://www.youtube.com/@sunknudsen) |
| [T+1](https://www.youtube.com/@t_plus_1) | [Tech Girl Story](https://www.youtube.com/@techgirlstory) | [Tech Pills](https://www.youtube.com/@TechPillsNet) |
| [TechAltar](https://www.youtube.com/@TechAltar) | [TechHunt](https://www.youtube.com/@TechHut) | [Techlore](https://www.youtube.com/@techlore) |
| [The Linux Cast](https://www.youtube.com/@TheLinuxCast) | [The Linux Experiment](https://www.youtube.com/@TheLinuxEXP) | [The PC Security Channel](https://www.youtube.com/@pcsecuritychannel) |
| [Titus Tech Talk](https://www.youtube.com/@TitusTechTalk) | [TJ Free](https://www.youtube.com/@TJFREE) | [Tom MacDonald](https://www.youtube.com/@TomMacDonaldOfficial) |
| [Trag Date](https://www.youtube.com/@tragdate) | [typecraft](https://www.youtube.com/@typecraft_dev) | [Veronica Explains](https://www.youtube.com/@VeronicaExplains) |
| [Victoriano de Jesus](https://www.youtube.com/@victorianodejesus) | [Vlogtehnica](https://www.youtube.com/@VlogTehnica) | [WesleyAda](https://www.youtube.com/@WesleyAda) |
| [Wolgang's Channel](https://www.youtube.com/@WolfgangsChannel) | [x90nix](https://www.youtube.com/@x80nix) | [XeroLinux Official](https://www.youtube.com/@XeroLinux) |
| [Zaney](https://www.youtube.com/@ZaneyOG) | [{ I L H A M }](https://www.youtube.com/@ILHAMFoss) |                                          |