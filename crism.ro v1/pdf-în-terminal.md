% Convertirea unui PDF în terminal
% ThinkRoot99

Sunt cazuri când nu ai nevoie de un fișier PDF, dar ai nevoie de o imagine. Și în acel moment dețineți doar fișierul PDF.

Sunt multe aplicații sau servicii online care pot face acest lucru. Dar convertirea cât mai repede a unui PDF în imagine se face în linia de comandă.

**Pdftoppm** convertește fișierele **PDF** în format de imagine precum **PNG** și altele. Este o aplicație pentru linia de comandă care poate converti un întreg document PDF în fișiere de imagine separate.

Cu **pdftoppm** se poate specifica și rezoluția preferată a imagini, se poate scala și se poate decupa imaginile.

Pentru a utiliza aplicația în linia de comandă, trebuie să instalați `pdftoppm`, care face parte din pachetul `poppler` (poppler-utils și poppler-tools).

Instalați acest pachet folosind comanda:

    $ sudo pacman -S poppler

Mai jos sunt câteva exemple de cum puteți utiliza aplicația `pdftoppm` pentru a converti fișierele PDF în imagini.

# Convertirea documentului PDF în imagine

Sintaxa pentru conversia unui întreg PDF este următoarea:

    $ pdftoppm -<format imagine> <nume fișier pdf> <nume imagine>

În exemplul de mai jos, numele documentului este **Linux_pentru_Începători.pdf** și va fi convertit în format PNG, iar imaginile vor avea numele **Linux_pentru_Începători**.

    $ pdftoppm -png Linux_pentru_Începători.pdf Linux_pentru_Începători

Fiecare pagină a PDF-ului va fi convertiă în PNG ca Linux_pentru_Începători-1.png, Linux_pentru_Începători-2.png etc.

# Convertirea anumitor pagini

Sintaxa pentru specificarea conversiei este următoarea:

    $ pdftoppm -<format imagine> -f N -l N <nume fișier pdf> <nume imagine>

Indicatorul `-f N` specifică numărul primei pagini și indicatorul `-l N` specifică numărul ultimei pagini pentru convertit.

În exemplul de mia jos, se va converti paginile de la 10 la 15 din documentul Linux_pentru_Începători.pdf.

    $ pdftoppm -png -f 10 -l 15 Linux_pentru_Începători.pdf Linux_pentru_Începători

Rezultatul va fi: imaginile sunt denumite ca Linux_pentru_Începători-10.png, Linux_pentru_Începători-11.png etc.

# Convertirea primei pagini

Pentru a converti prima pagină, utilizați doar comanda de mai jos:

    $ pdftoppm -png -f 1 -l 1 Linux_pentru_Începători.pdf Linux_pentru_Începători

# Ajustarea calități DPI la conversie

**Pdftoppm** convertește paginile PDF în imagini cu un **DPI** de **150** în mod implicit. Pentru a ajusta DPI-ul, trebuie să utilizați indicatorul `-rx` care specifică rezoluția **X** și indicatorul `-ry` care specifică rezoluția **Y** în DPI.

În exemplul de mai jos, se va ajuta calitatea DPI a Linux_pentru_Începători.pdf la 300.

    $ pdftoppm -png -rx 300 -ry 300 Linux_pentru_Începători.pdf Linux_pentru_Începători

# De final

Pentru a vedea toate opțiunile disponibile și acceptate în **pdftoppm**, rulați comenzile:

    $ pdftoppm --help  
    $ man pdftoppm

