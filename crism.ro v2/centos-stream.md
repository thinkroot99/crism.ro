% CentOS Stream: Tot ce trebui să știți
% ThinkRoot99

Ce este CentOS Stream? În acest articol, voi prezenta tot ceea ce trebui să știți despre CentOS Stream.

Să începem cu o scurtă reîmprospătare a istoriei. După cum știți, CentOS, una dintre cele mai populare distribuții GNU/Linux, [nu va mai beneficia de suport](https://blog.centos.org/2020/12/future-is-centos-stream/) pentru CentOS 8, începând cu 31 decembrie 2021, în timp ce suportul pentru CentOS 7 se va încheia la 30 iunie 2024.

Echipa CentOS încurajează utilizatorii actuali de CentOS 8 să actualizeze distribuția la CentOS Stream, dar acest lucru s-ar putea să nu fie acceptat cu ușurință, iar mulți ar putea migra către o altă distribuție.

# De ce a fost creat CentOS Stream

Lucrând în CentOS Stream între Fedora și RHEL, dezvoltatorii din ecosistem vor lucra la o previzualizare continuă a ceea ce va veni în următoarea versiune RHEL. Acest lucru le va permite să facă modificări mult mai rapid decât o pot face în prezent.

În prezent, dezvoltarea RHEL se realizează cu mulți dintre partenerii din ecosistemul Red Hat care lucrează în spatele firewall-ului Red Hat.

CentOS Stream le permite celor de la Red Hat și comunității mai largi să facă o dezvoltare cât mai transparentă în ceea ce va deveni următoarea versiune a RHEL.

Începând cu RHEL 8, Red Hat s-a angajat să lanseze versiuni majore de RHEL la fiecare trei ani și versiuni minore la fiecare șase luni. Aderarea la această cadență mai rapidă și mai previzibilă înseamnă că Red Hat are nevoie de un mediu de dezvoltare intermediar la care oricine poate contribui. Acest mediu este CentOS Stream.

Atunci când Fedora era singurul proiect upstream al RHEL, majoritatea dezvoltatorilor erau limitați să contribuie doar la următoarea versiune majoră a RHEL. Cu CentOS Stream, toți dezvoltatorii vor putea contribui cu noi caracteristici și cu remedieri de defecte la versiunile minore ale RHEL.

# Ce este CentOS Stream?

CentOS Stream este o distribuție GNU/Linux cu lansare continuă care se află la jumătatea drumului dintre dezvoltarea în amonte a Fedora Linux și dezvoltarea în aval a Red Hat Enterprise Linux (RHEL).

Acesta așteaptă cu nerăbdare următoarea versiune minoră a RHEL. Acesta se situează între Fedora, care se concentrează pe integrarea proiectelor din amonte în sistemul de operare și Red Hat Enterprise Linux, care se concentrează mai mult pe răspunsul la nevoile clienților din sectorul întrepinderilor.

Cu CentOS Stream, Red Hat își desfășoară activitatea de dezvoltare în exterior. Acum își va asambla toate modificările în mod public. În primul rând, să vedem unde se situează această distribuție în ciclul de dezvoltare al Red Hat.

> ![CentOS Stream relationship to RHEL and Fedora](img/CentOS-Stream-relationship-to-RHEL-and-Fedora.png)

Reh Hat conduce dezvoltarea distribuției, iar aceasta vizează din nou următoarea versiune minoră a RHEL. Astfel, dezvoltatorii ecosistemului CentOS Stream primesc o previzualizare a ceea ce RHEL a planificat pentru următoarea versiune minoră și au un loc unde pot înregistra idei despre ceea ce cred ei că va urma.

Cu CentOS Stream între Fedora și RHEL, Red Hat poate lua decizii și le poate comunica mai devreme către întreg ecosistem.

De exemplu, să presupunem că RHEL se îndreaptă într-o anumită direcție pentru o versiune minoră. În acest caz, cei care află în public pot observa această schimbare, o pot comenta și chiar pot propune ei înșiși modificări mult mai devreme.

Astfel, dezvoltatorii știu ce urmează să se întâmple, iar acest spațiu comun de colaborare îi permit lui Red Hat să se antreneze mai întâi în amonte, introducând caracteristici în Fedora și dându-le o aromă de întrepindere în CentOS Stream.

> CentOS Stream este o platformă de dezvoltare în amonte pentru dezvoltatorii din ecosistem. Este un flux continu de conținut, cu actualizări de mai multe ori pe zi, care cuprinde cele mai noi și cele mai bune informații din baza de cod RHEL.
> 
> Este o perspectivă asupra modului în care va arăta următoarea versiune de RHEL, disponibilă unei comunități mult mai largi decât o versiune beta sau o versiune „*preview*”.
>
> Chris Wright, vicepreședinte și CTO la Red Hat

Pe scurt, utilizatorii [CentOS Stream](https://www.centos.org/centos-stream/) vor testa RHEL înaintea tuturor, dar nu vor primi actualizări de securitate până când nu vor fi rezolvate în RHEL.

# Ar trebui să se mai folosească CentOS?

Mulți oameni sunt confuzi cu privire la faptul că CentOS Stream este o cale bună pentru viitor pentru utilizatorii actuali de CentOS Linux. Cu toate acestea, milioane de utilizatori au folosit CentOS ca o distribuție stabilă pentru servere, mașini virtuale și aparatele lor.

Așadar, în acest moment, utilizatorii actuali de CentOS se confruntă cu o întrebare cheie: *Ar trebui să continuăm să folosim CentOS?*

<ins>Să vedem faptele</ins>: În loc să fie o clonă stabilă a RHEL, CentOS Stream este programat să fie un sistem de operare în continuă transformare, reprezentând munca pe care inginerii Red Hat o depun pentru a construi viitoarea versiune RHEL.

După cum știți, cel mai important avantaj al CentOS a fost stabilitatea sa. Fără aceasta, mulți nu au nici un motiv să continue să-l folosească. De obicei, utilizatorii nu doresc să se bazeze pe strategii pentru servere. Ei vor acesta să funcționeze.

Nimeni nu dorește să ruleze un mediu de misiune critică pe un sistem de operare netestat, iar CentOS Stream este, în cele din urmă, un canal de testare pentru RHEL. Prin urmare, nimeni nu își va pierde timpul folosind o distribuție de servere care nu poate fi considerată stabilă.

În plus este o distribuție cu lansare continuă (rolling release), ceea ce ar putea fi o problemă atunci când vorbim de servere. Prin urmare, răspunsul scurt este Nu.

# O scurtă încheiere

La fel ca CentOS, CentOS Stream poate fi descărcat, utilizat și distribuit în întregime gratuit, fără limitări în ceea ce privește numărul de instanțe pe care un utilizator individual sau o organizație le poate implementa.

Dar CentOS Stream este mult diferit de CentOS Linux tradițional.

Este o distribuție pe care membrii comunității o pot folosi pentru a profita de un API stabil pentru dezvoltarea și testare, beneficiind în acelați timp de unele actualizări în mod accelerat.

Probabil că IBM/Red Hat nu mai dorește ca întrepinderile să considere CentOS ca fiind o opțiune viabilă pentru servere.

Cu toate acestea, este posibil ca unii utilizatori să își migreze sistemele CentOS actuale către CentOS Stream, în special dacă le place să testeze cel mai noi caracteristici și nu se bazează pe faptul că au sisteme foarte previzibile.

Dar atunci când aveți de-a face cu un server, mai ales dacă este un server de producție de care compania dumneavoastră depinde pentru afaceri, probabil că ar trebui să luați în considerare migrarea la o altă distribuție GNU/Linux.
