# Autentificare automată cu LightDM în Arch Linux

LightDM este un manager de afișare (display manager) ușor și rapid pentru Linux.

Nu este deajuns să se instaleze doar LightDM, este necesar să se instaleze și un Greeter. Greeter este interfața grafică a managerului de autentificare.

Acest Greeter permite utilizatorului să selecteze o sesiune (Xfce, GNOME, Plasma, etc.) plus alte câteva opțiuni.

## 1. Instalare LightDM

Pentru început trebuie instalat LightDM și un greeter. Iar comanda de instalare este:

```bash
sudo pacman -S lightdm lightdm-gtk-greeter
```

## 2. Activare LightDM

Comanda următoare activează serviciul pentru ca acesta să pornească automat.

```bash
sudo systemctl enable lightdm
```

## 3. Configurare LightDM pentru autentificare

Pentru ca autentificarea să se facă automat este nevoie să se editeze fișierul `lightdm.conf`.

Se rulează comanda următoare pentru a se edita fișierul cu editorul de text vim. Voi puteți înlocui vim cu editorul de text preferat.

```bash
sudo vim /etc/lightdm/lightdm.conf
```

Se caută secțiunea `[Seat:*]` și se modifică următoarele linii:

```ini
[Seat:*]
#autologin-user=
#autologin-session=
```

La sfârșitul editări fișierului, liniile de mai sus trebuie să arate astfel:

```ini
[Seat:*]
autologin-user=USERNAME
autologin-session=i3
```

Înlocuiește `USERNAME` cu numele tău de utilizator și `i3` cu numele mediului de lucru pe care îl folosești.

## 4. Configurarea sesiuni i3

Această parte este doar pentru utilizatori care au sau folosesc managerul de ferestre **i3**.

Asigură-te că ai instalat i3. Dacă nu, instalează-l cu comanda următoare:

```bash
sudo pacman -S i3
```

Trebuie să se creeze fișierul de configurare pentru sesiunea i3, dacă nu există deja.

```bash
sudo vim /usr/share/xsessions/i3.desktop
```

Conținutul fișierului `i3.desktop` trebui să arate astfel:

```ini
[Desktop Entry]
Name=i3
Comment=improved dynamic tiling window manager
Exec=i3
Icon=i3
Type=Application
```

## 5. Crearea grupului `autologin`

Verifică dacă grupul `autologin` există deja. Dacă nu, creează-l:

```bash
sudo groupadd autologin
```

Adaugă utilizatorul în grupul `autologin`:

```bash
sudo gpasswd -a USERNAME autologin
```

Înlocuiește `USERNAME` cu numele utilizatorului tău.

## 6. Verificarea setărilor

Poți verifica dacă utilizatorul a fost adăugat corect în grupul `autologin` folosind:

```bash
groups USERNAME
```

sau:

```bash
id USERNAME
```

Aceste comenzi ar trebui să listeze grupurile din care face parte utilizatorul, inclusiv `autologin`.

Dacă totul este făcut urmând pași de mai sus, tot ce mai rămâne de făcut este să se repornească sistemul.

## Concluzie

Acum, Arch Linux ar trebui să se autentifice automat în contul utilizatorului specificat și să pornească managerul de ferestre i3.