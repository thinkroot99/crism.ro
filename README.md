# crism.ro

Acest depozit conține codul sursă pentru situl personal [crism.ro](https://crism.ro), unde îmi prezint activitatea online, proiectele personale și comunitățile din care fac parte.

## Situl crism.ro

Sit-ul este structurat în mai multe secțiuni:

### Despre

În această secțiune găsiți informații despre mine, precum și experiența și interesele mele profesionale.

Aici sunt enumerate și detaliate proiectele personale la care lucrez sau pe care le-am finalizat. Fiecare proiect este însoțit de legături către depozitul GitLab sau pagini.

### Jurnal (Blog)

În secțiunea de jurnal, public în principal articole despre tehnologie, Linux și alte subiecte relevante. Aceste articole sunt menite să împărtășesc experiențele și cunoștințele mele cu comunitatea online.

### Liste (Blogroll)

În acestă secțiune găsiți o listă de blog-uri și resurse online pe care le recomand și le urmăresc. 

## Script de Conversie și Template

În depozit se găsesc, de asemenea, și scriptul utilizat pentru conversia fișierelor Markdown în fișiere HTML necesare pentru sit și jurnal, precum și template-ul utilizat pentru construcția acestora. Pentru versiunea 4 a sit-ului folosesc scriptul `convert.sh`.