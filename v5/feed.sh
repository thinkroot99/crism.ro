#!/bin/bash
set -eu

# Funcție pentru a extrage titlul din fișierul Markdown
get_title() {
    sed -n '/^# /{s/# //p; q}' "$1"
}

# Funcție pentru a genera feed-ul Atom XML
generate_atom_feed() {
    local uri="http://crism.ro/atom.xml"  # Adaptează la URL-ul feed-ului tău
    local host=$(echo "$uri" | sed -r 's|.*//([^/]+).*|\1|')

    declare -A articles

    for f in articole/*.md; do
        if [ ! -e "$f" ]; then
            continue
        fi

        local title=$(get_title "$f")
        local created=$(date -r "$f" "+%Y-%m-%dT%H:%M:%S%:z")
        local updated=$(date -r "$f" "+%Y-%m-%dT%H:%M:%S%:z")
        local html_file="sit/$(basename "$f" .md).html"
        local link="http://crism.ro/$(basename "$f" .md).html"

        articles["$created"]=$(cat <<EOF
    <entry>
        <title>$title</title>
        <link href="$link"/>
        <id>tag:$host,$(date -d "$created" "+%Y-%m-%d"):$html_file</id>
        <published>$created</published>
        <updated>$updated</updated>
    </entry>
EOF
)
    done

    # Sortăm articolele după data de creare
    local sorted_dates=($(for date in "${!articles[@]}"; do echo $date; done | sort -r))

    cat <<EOF
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <title>Blogul Meu</title>  <!-- Titlul blogului tău -->
    <link href="$uri" rel="self" />
    <updated>$(date --iso-8601=seconds)</updated>
    <author>
        <name>Author Name</name>  <!-- Numele autorului -->
    </author>
    <id>tag:$host,$(date --iso-8601):default-atom-feed</id>
EOF

    for date in "${sorted_dates[@]}"; do
        echo "${articles[$date]}"
    done

    echo '</feed>'
}

# Verificăm dacă directorul sit există și îl creăm dacă nu există
if [ ! -d "sit" ]; then
    mkdir sit
fi

# Generăm feed-ul Atom XML
generate_atom_feed > sit/atom.xml
