# Nu mai avem târg online

În România nu se mai știe de negociere, spiritul acela din târg sau piață și aici mă refer la spațiul online și la sit-uri de genul lui OLX.

Pe OLX sunt foarte multe firme care își vând produse și nu acceptă negocieri, mai nou și utilizatori simpli de pe OLX și alte sit-uri de genul nu mai negociază nimic.

Plus că prețurile la majoritatea produselor sunt prețuri de magazin, nu sunt prețuri de piață sau târg. Prețuri mari și foarte mari sunt la produse și astfel ajungi să cumperi produse noi sau ajungi să cumperi din China de pe AliExpress.

OLX se transformă așa încet în eMAG, adică o platformă unde diverse firme își vând produsele și simpli comercianți care trebuiau să își vând produse ca la târg dispar de pe OLX.

Înțeleg că profitul este pe primul loc, dar la noi se exagerează foarte mult. Toți vânzători, indiferent că sunt magazine sau persoane fizice, vor să se îmbogățească peste noapte și din această cauză se exagerează cu prețurile.

Puțini oameni mai sunt cu capul pe umeri și cer un preț decent pe produsul lor, din păcate astfel de oameni sunt tot mai puțini în România.

Nu știu cum este în alte țări pe platforme asemănătoare cu OLX, dar îmi imaginez că este mult mai bine decât la noi.

Sper ca într-o zi să apară aceea platformă care păstrează spiritul târgului și nu acceptă ca firmele să își vândă marfa. Iar utilizatori platformei să pună prețuri decente la produse.

Când a apărut OLX (cred că a avut și alt nume, nu mai sunt sigur) era o platformă foarte OK. Totul era ca în târg - oameni negociau, produsele aveau prețuri bune și decente. Dar de când a fost cumpărat, OLX-ul s-a stricat.

