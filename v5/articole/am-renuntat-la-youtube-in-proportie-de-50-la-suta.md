# Am renunțat la YouTube în proporție de 50%

De mult timp am vrut să renunț la YouTube și dintr-un anume motiv pe care toată lumea îl știe nu am putut renunța. Există câteva platforme video interesante dar nu ajung la nivelul lui YouTube și nici majoritatea creatorilor de conținut video nu folosesc celelalte platforme.

Astăzi am dat peste o platformă care mă ajută în mare să scap de dependența de YouTube. Practic nu mai trebuie să folosesc adresa `youtube.com`. Această platformă se numește [Invidious](https://invidious.io/) și practic este o interfață open source pentru YouTube.

Sunt câteva [instanțe](https://docs.invidious.io/instances/#list-of-public-invidious-instances-sorted-from-oldest-to-newest) de Invidious și există chiar și o [instanță Românească](https://invidious.flokinet.to/) de Invidous.

Problemă rezolvată pe jumătate :)

Dar cum mă uit la canalele la care sunt abonat? Păi este simplu. Toate canalele de YouTube au un Feed RSS pe care îl adaug în programul [Newsboat](https://newsboat.org/) și astfel am rezolvat problema cu accesarea YouTube-ului în proporție de 90%.

De ce 90%? Pentru că mă folosesc de platforma YouTube, dar nu direct, nu mai accesez sit-ul `youtube.com`.

Evitarea YouTube-ului o fac în acest mod pe laptop. Pentru telefon folosesc [NewPipe](https://newpipe.net/) unde am importat și abonamentele - asta pe Android, pentru iOS deocamdată nu știu o aplicație asemănătoare cu `NewPipe`.

Pe Android am dezactivat YouTube pentru că nu pot șterge aplicația și am mai lăsat doar Youtube Music pentru că mai ascult în mașină muzică și YouTube Studio pentru că am nevoie de această aplicație în acele momente când filmez ceva cu telefon și trebuie să urc clipul pe canalul de YouTube.

Pe scurt în proporție de 50% am renunțat la YouTube - cel puțin așa văd eu problema aceasta. Mi-ar plăcea să nu mai folosesc YouTube de loc, dar din păcate majoritatea creatorilor de conținut nu vor să își urce clipurile și în altă parte.
