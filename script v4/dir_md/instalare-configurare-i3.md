# Am reinstalat Arch Linux cu i3 WM

Am instalat Arch Linxu folosind utilitarul `archinstall` aproximativ cum am documentat [aici](archlinux.html). După repornire m-am autentificat în `tty` și am trecut la instalarea managerului de ferestre i3.

## Actualizare sistem și instalare `paru`

Primul lucru pe care l-am făcut după autentificare a fost să actualizeze sistemul, chiar dacă am folosit ultima versiune a imagini `.iso` a Arch Linux, folosind comnda: `sudo pacman -Syu`.

După ce m-am asigurat ca sistemul este actualizat la zi, am trecut la instalarea unui **AUR Helper** și anume am instalat utilitarul `paru`, folosind comenzile urmatoare:

```bash
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```

Acum că am instalat utilitarul `paru`, am acces și la **AUR** (*Arch User Repository*) care este unde depozit de programe alternativ la cel oficial, unde programele sunt întreținute de utilizatorii de Arch Linux și nu de echipa care dezvoltă distribuția Arch.

## Instalare i3

Abia acum am trecut la instalarea lui **i3 WM**. Comanda pentru instalarea lui i3 este: `sudo pacman -S i3`. După ce am scris comanda și am apăsat tasta `enter`, mi-a apărut un mic anunț în care scria că sunt 4 aplicații în grupul i3 și ma pus să aleg dacă vreau să le instalez pe toate sau nu.

```bash
sudo pacman -S i3
[sudo] password for thinkroot:
:: There are 4 members in group i3:
:: Repository extra
   1) i3-wm 2) i3blocks 3) i3lock 4) i3status

Enter a selection (default=all):
```

Nu am nevoie de toate acele pachete, așa că am ales pachetele 1 și 4. După ce am dat enter mi-a apărut următorul mesaj și altă selecție de pachete.

```bash
rezolving dependencies...
:: There are 10 providers available for ttf-font:
:: Repositoru extra
   1) gnu-free-fonts 2) noto-fonts 3) ttf-bitstream-vera 4) ttf-croscore 5) ttf-dejavu 6) ttf-droid 7) ttf-ibm-plex 8) ttf-input 9) ttf-liberation 10) ttf-mona-sans

Enter a selection (default=1):
```

La acest pas am ales pachetul implicit - aici nu am putut alege mai multe fonturi. După acest pas instalarea managerului de ferestre și a dependențelor lui/sale s-a făcut fără probleme.

## Instalarea aplicațiilor necesare pentru i3

Pentru început am vrut să instalez `dmenu` care este un program pentru accesarea/pornirea programelor. M-am răzgândit și am instalat `Rofi`, pentru că oricum instalam și această aplicație - deci nu am nevoie de două aplicații. Rofi se instalează astfel:

```bash
sudo pacman -S rofi
```

Am repornit sistemul, m-am autentificat din nou și am instalat `xorg` pentru a putea rula *i3*, pentru a avea grafică. Pentru a instala xorg am rulat comanda urmatoare:

```bash
sudo pacman -S xorg
```

La rularea comenzi de mai sus mi-a apărut o listă de 48 de pachete xorg. Am ales să instalez 47 de pachete, ce nu am instalat a fost pachetul `xorg-xwayland` pentru că vreau ca sistemul să meargă exclusiv pe **X11**. Știu că Wayland este mai bun și mai nou, dar laptopul meu este desul de vechi încât să nu aibă nevoie de Wayland.

După rularea comenzi de mai sus, am de ales acum între pachetele `mand-db` din depozitul `core` și `mandoc` din depozitul `extra`. Bineînțeles că am ales prima variantă și instalarea pachetelor **xorg** merge mai departe.

Pentru a porni **i3** cu ajutorul comenzii `startx` a mai trebuit să instalez `xorg-init` și `xf86-video-intel`, am repornit sistemul pentru a fi sigur că se aplică toate setările necesare după instalarea pachetelor.

Dar mai trebuia să instalez câteva pachete pentru a merge grafica.

```bash
sudo pacman -S xterm xorg-xclock xorg-twm
```

Dar **i3** tot nu s-a pornit la rularea comenzii `startx`. A trebuit să creez fișierul `.xinitrc` și să trec linia `exec i3`. Am creat fișierul cu comanda `nvim .xinitrc`, am scris linia menționată mai devreme în fișier, am salvat și închis fișierul. După care am rulat comanda `startx` și în sfârșit a pornit managerul de ferestre **i3**.

Complicat cu Linux și Arch Linux :)

### Recapitulare

O mică recapitulare :)

Am instalat `i3wm`, `i3-status`, `xorg` și multe dependențe. Am creat și editat fișierul `.xinitrc`. Am mai instalat aplicația `rofi` de care nu mă pot folosi încă :). De fapt mă pot folosi dacă pornesc aplicația din terminal, dar am nevoie să o pornesc din tastatură. În final este bine că i3 pornește și mă pot ocupa mai departe de configurarea lui.

## Configurare i3 WM

Urmează configurarea lui **i3**. Va trebuie să editez fișierul `config` din directorul `~/.config/i3/`. În acest fișier voi face toate setările necesare ce țin de funcționarea lui i3, de personalizare, de pornirea programelor automat, de deschiderea anumitor programe în anumite spații de lucru.

Îmi place să lucrez, mai nou, cu neovim și pentru a deschide fișierul `config` am rulat comanda:

```bash
nvim .config/i3/config
```

### Modificare terminalului

Pentru început am căutat linia `bindsym $mod+Return exec i3-sensible-terminal` pentru a o modifica astfel `bindysm $mod+Return exec alacritty`. Asta înseamnă că am înlocuit terminalul atunci când apăs combinația de taste `Mod+Enter` și astfel deschid terminalul **Alacritty** în locul celui implicit.

> Tasta `mod` este tasta `super` sau `windows`.

Ca să se aplice noile modificări trebuie să repornesc i3 și asta o fac cu ajutorul combinației de taste `Mod+Shift+R`. Și de acum înainte se deschide **Alacritty** în locul terminalului implicit.

### Adăugarea lui rofi

Știind că am **rofi** instalat și mă aflu în fișierul de configurare, am modificat linia `bindsym $mod+d exec --no-startup-id dmenu_run` ca să arate astfel `bindsym $mod+Shift+d exec no-startup-id rofi -show drun`.

### Modificarea comenzilor rapide

Majoritatea comenzilor sunt în formatul `$mod+tastă`. Nu îmi place acest lucru și o să modific tot ce pot în formatul acesta: `$mod+Shift+tastă`, pentru ca prima formă să o pot folosi pentru pornirea programelor. Și în acest fel separ comenzile pentru i3 de comenzile pentru programe. Unde nu am putut pune tasta *shift*, am pus tasta *ctrl*.

### Mutarea barei i3status

Bara de status implicit este afișată în partea de jos a ecranului. Nu îmi place acolo și am mutat-o în partea de sus a ecranului adăugân linia: `position top`. Acum că am terminat cu configurarea tastelor am trecut la designe și am schimbat culorile și fontul.

```cod
bar {
	status_command i3status -c ~/.config/i3status/i3status.conf
	position top
}
```

### Schimbarea fontului

Am pus tema `gruvbox dark` și fontul l-am pus `inconsolata fonts`. Pe partea de fonturi am instalat pachetul `Nerd Fonts` folosind comanda:

```bash
sudo pacman -S nerd-fonts
```

În fișierul de configurare al i3 am căutat linia `font pangoo:monospace 8` și am înlocuit-o cu linia `font pango: inconsolata nerd 10`. Acum parca arata mai bine :)

### Adăugare culori

Culorile alese pentru tema Gruvbox Dark sunt:

```code
set $bg #282828
set $red #cc241d
set $green #98971a
set $yellow #d79921
set $blue #458588
set $purple #b16286
set $aqua #689d68
set $gray #a89984
set $darkgray #1d2021
set $lightgray #bdae93
```

```cod
# class                 border|backgr|text|indicator|child_border
client.focused          $lightgray $lightgray $bg $purple $darkgray
client.focused_inactive $darkgray $darkgray $lightgray $purple $darkgray
client.unfocused        $darkgray $darkgray $light
```

### Schimbarea numelor spatiilor de lucru

Spațiile de lucru în i3 WM sunt numerotate de la 1 la 0, în total sunt 10. Am modificat numele acestora, lângă cifră apare și un mic text care să identifice mai bine acel spațiu de lucru. De exemplu pentru primul spațiu de lucru numele era `1` și acum este `1: terminal`.

Înainte de modificare:

```code
set $ws1 "1:l"
set $ws2 "2:"
set $ws3 "3:"
set $ws4 "4:"
set $ws5 "5:"
set $ws6 "6:"
set $ws7 "7:"
set $ws8 "8:"
set $ws9 "9:"
set $ws10 "10:"
```

După modificare:

```code
set $ws1 "1: terminal"
set $ws2 "2: browser"
set $ws3 "3: e-mail"
set $ws4 "4: chat 1"
set $ws5 "5: chat 2"
set $ws6 "6: file"
set $ws7 "7: doc"
set $ws8 "8: edit"
set $ws9 "9: media"
set $ws10 "10: other"
```

### Adăugare temă pentru aplicații GUI

Între timp am mai instalat tema gruvbox dark și pentru aplicațiile gtk, am instalat un set de pictograme și o temă pentru cursor.

```bash
paru -S gruvbox-dark-gtk tela-circle-icon capitane-cursors lxappearence
```

Aplicația `lxappearance` este pentru a seta tema pentru aplicațiile gtk, pentru a aplica tema pentru pictograme și pentru a aplica tema pentru cursor.

### Deschiderea aplicățiilor în anumite spații de lucru

Mai departe am trecut la alocarea de spații de lucru pentru aplicații. În fișierul de configurare al i3 am adăugat linii asemanatoare cu:

```bash
for_window [class="qutebrowser"] move to workspace $ws2
```

Astfel navigatorul **qutebrowser** se va deschide numai în spațiul de lucru 2, numit `2: browser`.

Toate acestea le-am făcut în timp ce căutam informații despre cum să personalizez `i3status` :)

## Configurare i3status

Am ajuns și la configurarea barei de status, i3status. Primul lucru pe care l-am făcut a fost să creez directorul `i3status`.

```bash
mkdir ~/.config/i3status/
```

După care am copiat fișierul de configurare `i3status.conf`

```bas
cp /etc/i3status.conf ~/.config/i3status/i3status.config
```

Și am trecut la editarea fișierului `i3status.conf` cu ajutorul comenzii `nvim .config/i3status/i3status.conf` pentru a personaliza și bara de status.

```cod
# i3status configuration file.
# see "man i3status" for documentation.

# It is important that this file is edited as UTF-8.
# The following line should contain a sharp s:
# ß
# If the above line is not correctly displayed, fix your editor first!

general {
        colors = true
        interval = 5
}

order += "volume master"
#order += "ipv6"
order += "wireless _first_"
order += "ethernet _first_"
order += "battery all"
order += "disk /"
order += "disk /home"
#order += "load"
order += "memory"
order += "tztime local"

wireless _first_ {
        format_up = "W: (%quality at %essid)"
        format_down = "W: down"
}

ethernet _first_ {
        format_up = "E: up (%speed)"
        format_down = "E: down"
}

battery all {
        format = "%status %percentage %remaining"
}

disk "/" {
        format = "%avail"
}

disk "/home" {
	format = "%avail"
}

load {
        format = "%1min"
}

memory {
        format = "%used | %available"
        threshold_degraded = "1G"
        format_degraded = "MEMORY < %available"
}

tztime local {
        format = "%d-%m-%Y %H:%M:%S"
}

volume master {
    format = "%volume  "
    format_muted = " "
    device = "default"
    mixer = "Master"
    mixer_idx = 0
}
```

### Instalare aplicații

Am mai instalat awesome-terminal-fonts ttf-font-icons ttf-font-awesome, lf, qutebrowser, mpv, nsxiv, unzip, zip, ueberzug, neovim, picom, alacritty, zathura, zathura-pdf-poppler, nord fonts, dunst, udiskie și udisks2. Am înlocuit qutebrowser cu Thorium Browser.

```bash
paru -S awesome-terminal-fonts ttf-font-icons ttf-font-awesome, lf, qutebrowser, mpv, nsxiv, unzip, zip, ueberzug, neovim, picom, alacritty, zathura, zathura-pdf-poppler, nord fonts, dunst, udiskie și udisks2
```



## De final

Câteva zile nu am mai făcut nimic, doar am instalat câteva aplicații pe care le folosesc mai des. Nimic important nu am mai făcut. Dar urmează pentru că am de modificat fișierul de configurare al `picom`, am de modificat fișierul de configurare al `allacrity`, am de modificat fișierul de configurare al `lf` și mai am multe de făcut.
