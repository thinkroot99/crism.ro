# Despre și Contact

## Despre acest sit

Sit-ul este făcut doar în HTML și CSS. Folosesc un șablon HTML și un script ca să construiesc acest sit.

Aplicațiile pe care le folosesc pentru a scrie postările de pe acest sit sunt: [Neovim](https://neovim.io/) și [Typora](https://typora.io/). Articolele le scriu în markdown și cu ajutorul unui script bash le transform în fișiere html.

Scriptul este creat de [Karl Bartel](https://www.karl.berlin/) și modificat destul de mult încât nu mai seamănă cu original-ul.

Șablonul HTML este împrumutat tot de la Karl Bartel, dar acesta nu l-am mai modificat așa de mult.

## Contact

Contactați-mă prin <a href="mailto:social@crism.ro" rel="me">e-mail</a> sau la următoarele adrese: <a href="https://gitlab.com/thinkroot99" rel="me">GitLab</a>, <a href="https://linux.social/@thinkroot99" rel="me">Mastodon</a>.

## Licența

Textul și imaginile din sit sunt libere [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ro). [Codul sursă](https://gitlab.com/thinkroot99/crism.ro/-/tree/main/v5) al paginii și [programul de generare](https://gitlab.com/thinkroot99/crism.ro/-/blob/main/v5/blog.sh) sunt libere ([GNU AGPL](https://www.gnu.org/licenses/agpl.html)).

Oricine este liber să folosească, să copieze, să modifice și să (re)distribuie aceste informații în condițiile licențelor de mai sus.
