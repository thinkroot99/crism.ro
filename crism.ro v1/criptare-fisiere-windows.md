% Criptarea fișierelor și a directoarelor în Windows
% ThinkRoot99

Niciodată nu puteți fi suficient de sigur în lumea nesigură și în continuă schimbarea a securități cibernetice. Lucrurile se precipită exact atunci când credeți că nu trebui să vă descurcați cu nimic.

Nici măcar companiile de tehnologie nu sunt imune la orice; fie că este vorba de furia unui angajat activist care trece prin faza de denunțător sau chiar de o încălcare obișnuită a datelor de către terți, protejarea datelor voastre din toate părțile este super esențială.

Unul dintre straturile suplimentare de securitate a datelor este criptarea. Și pe Windows, criptarea fișierelor sau a directoarelor Windows le face inaccesibile utilizatorilor neautorizați din exterior.

Puteți efectua criptarea fișierelor sau a directoarelor Windows în mai multe moduri. Vom începe cu cea mai simplă metodă: utilizarea secțiunii Proprietăți (Properties) a fișierelor sau directoarelor.

# Cum să criptați fișierele sau directoarele prin intermediul Proprietăților (Properties)

Cel mai simplu mod de a vă cripta dosarul sau fișierul în Windows este să utilizați instrumentele încorporate în Windows.

Un astfel de instrument este secțiunea `Properties (Proprietăți)` a fiecărui fișier sau dosar, care găzduiește și funcția de criptare a fișierelor voastre.

- Faceți clic dreapta pe fișierul sau directorul pe care doriți să îl criptați, selectați `Properties` și apoi faceți clic pe `Advanced (Avansat)`.
- Apoi, selectați caseta de selectare `Encrypt contents (Criptare conținut)` pentru a securiza datele și faceți clic pe `OK` și, în final, faceți clic pe `Apply (Aplică)`.

> ![Advanced Attributes](img/Advanced-Attributes.png)

Dați clic din nou pe `OK`, iar aplicația vă va confirma dacă doriți să faceți aceste modificări doar la dosar sau și la subdirectoarele și fișierele din dosarul principal.

Alegeți una dintre opțiuni și faceți clic pe `OK`. Este posibil să apară o nouă casetă de dialog care să vă ceară privilegii de administrare imediat ce faceți acest lucru. Faceți clic pe `Continue (Continuare)` pentru a merge mai departe.

Asta este tot. Directorul sau fișierul va fi criptat de acum înainte.

# Cum să criptați fișierele și directoarele cu 7-Zip

7-Zip este în primul rând un program de comprimare a fișierelor care are ca scop reducerea dimensiunii fișierelor sau a dosarelor fără a pierde date, disponibil pentru utilizatorii de Windows și Linux.

Cu toate acestea, în afară de compirmarea fișierelor, ca și în cazul altor instrumente similare, aplicația vă permite să vă criptați datele. 7-Zip utilizează criptarea [AES-254](https://www.atpinc.com/blog/what-is-aes-256-encryption), primul și singurul cifru publicat și creat de NSA.

> ![7-Zip](img/7-Zip.png)

Pentru a începe, luați mai întâi programul de instalare [7-Zip](https://www.7-zip.org/download.html) din secțiune de descărcări. Instalați aplicația imediat ce descărcarea este finalizată și mergeți la fișierul sau dosarul pe care doriți să îl criptați.

După ce ajungeți la fișier sau director, faceți clic dreapta pe el și selectați `7-Zip > Add to archive (Adăugați la arhivă)`.

Din caseta de dialog `Add to Archive`, setați o parolă specifică în secțiune `Encryption (Criptare)`.

În afară de alegerea locației pentru fișierul ales după ce 7-Zip a terminat de comprimat, puteți să vă jucați cu alte setări, dacă doriți. În cele din urmă, după ce ați făcut toate setările, faceți clic pe `OK`.

> ![Add to Archive in 7-Zip](img/Add-to-Archive-in-7-Zip.png)

Fișierul sau dosarul va fi compirmat și protejat cu o parolă pe care vă puteți baza ulterior.

Mai târziu, când doriți să decomprimați fișierele, mergeți la fișierul sau dosarul respectiv, faceți din nou clic dreapta pe el și selectați `7-Zip > Extract Here (Extrageți aici)`.

Imediat ce faceți acest lucru, vi se va cere mai întâi o parolă pentru decriptare. Introduceți-o și faceți clic pe `OK`, iar datele vor fi decriptate.

> [Descărcați 7-Zip](https://www.7-zip.org/download.html)

# Criptarea fișierelor sau a directoarelor pe un calculator cu Windows

Chiar și cu câteva decenii în urmă, criptarea era un lucru vast și obscur, folosit doar de marile corporații sau de universitățile de cercetare. Bineînțeles, acum nu mai este cazul.

Pentru utilizatorii de Windows, criptarea fișierelor și a dosarelor este acum la fel de ușoară ca și crearea de noi dosare, și este chiar o chestiune obișnuită pentru cei obsedați de confidențialitate.

Am acoperit tot ceea ce este necesar pentru a vă menține securitate și confidențialitate din Windows la cel mai înalt nivel.
