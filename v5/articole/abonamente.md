# Cât plătesc pentru abonamente online

Nu m-am gândit de foarte mult timp la cât plătesc pentru abonamentele online de la diverse servicii. Dar m-a pus pe gânduri articolul lui [Marius Sescu](https://mariussescu.ro/costa-cam-mult-streaming/) în care scrie că abonamentele de acum nu mai sunt așa de ieftine față de cum erau în urmă cu 2, 3 ani.

Abonamentele online nu mai sunt așa ieftine față de acum 2, 3 ani pentru că nu mai poți împărți contul de Netflix cu alte persoane, pentru că nu îți mai poți face cont de YouTube Premium prin nu știu ce țară unde este mai ieftin, și exemplele pot continua.

Dar să trec la subiectul titlului pentru că de aceea sunteți aici :)

**Cât plătesc pentru diverse servicii online?**

Am abonament la Netflix, Disney+, Prime Video, SkyShowtime, MAX, YouTube, Google One, iCloud, Găzduire web și domenii. Cred că acestea sunt toate abonamentele pe care le am.

Dar asta nu este tot pentru că la YouTube Premium plătesc 2 abonamente, iar la Google One plătesc 4 abonamente, și uite așa se adună o sumă destul de mare.

| Serviciu        | Preț abonament                 |
| :-------------- | :----------------------------- |
| Netflix         | 11,99 euro (59,95 lei)         |
| Disney+         | 36,99 lei                      |
| Prime Video     | 13,99 lei                      |
| MAX             | 15 lei (prin Digi)             |
| YouTube         | 26 lei x 2                     |
| Google One      | 13.99 lei x 1 și 9.99 lei x 3  |
| iCloud          | 14.99 lei                      |
| Găzduire (Host) | 19,90 lei x 2 și 29,85 lei x 1 |

Acestea sunt toate abonamentele lunare plus abonamentul de la Digi (TV, Internet și Telefonie). Pe lângă acestea mai am 8 abonamente pe care le plătesc anual - aceste abonamente anuale sunt pentru domenii.

Sunt abonat la câteva servicii nu foarte multe, dar nici puține nu sunt. Nu știu dacă aș putea renunța la aceste servicii deocamdată, pe viitor sper să nu mai am nevoie de așa multe servicii online și să mai tai din ele. Dar asta rămâne de văzut :)

Nu este o suma tocmai mică pe care o plătesc lunar pentru serviciile de mai sus. Abonamentul de la Digi nu îl mai socotesc pentru că nu prea se încadrează la servicii online.

Totalul acestor servicii este: `306,53 de lei` lunar + domeniile o dată pe an care costă: `462,73 de lei`.

