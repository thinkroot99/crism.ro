% Tipuri de distribuții GNU/Linux explicate
% ThinkRoot99

Dacă ați auzit despre GNU/Linux, probabil că ați auzit termeni precum `Fork`, `Derivative` și `Flavor`. Se referă la diferite tipuri de distribuții GNU/Linux, așa că haideți să aflăm mai multe despre ele.

Acești termeni sunt folosiți pentru a distinge un tip de distribuție de alta și sunt foarte folositori. De fapt, vă ajută să faceți diferența între modul în care o anumită distribuție GNU/Linux va funcționa față de alta.

Dacă nu știți ce înseamnă acest termeni, nu vă faceți griji. În acest articol voi explica acești termeni, ce înseamnă și cum puteți folosi acești termeni pentru a vă restrânge opțiunile în alegerea celei mai bune distribuții GNU/Linux pentru tine.

Mai presus de toate, există doi termeni care sunt termeni principali ai ierarhie – distribuții originale și distribuții derivate.

# Distribuțiile originale

Realizarea unei distribuții GNU/Linux de la zero implică multă muncă. În general, distribuțiile originale sunt produsul unei comunității uriașe din jurul lor și al multor utilizatori.

Distribuția originală este un tip de distribuție GNU/Linux care nu se bazează direct pe nici o altă distribuție.

Deci, când vorbim despre distribuțiile originale, vorbim despre distribuțiile GNU/Linux precum [Debian](https://www.debian.org/), [Slackware](http://www.slackware.com/), [Arch Linux](https://archlinux.org/), [Red Hat](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux), [Gentoo](https://www.gentoo.org/), [SUSE](https://www.suse.com/products/desktop/) și [Void Linux](https://voidlinux.org/), deoarece nu se bazează pe nici o altă distribuție.

Ei preiau [nucleul Linux](https://www.kernel.org/), utilitarele [GNU](https://www.gnu.org/), programele necesare și le combină într-un sistem de operare instalabil.

> ![Tipuri de distribuții Linux - Originale](img/linux-types-original-distro.png)

Uneori pot avea unele asemănări, deoarece unele dintre ele sunt distribuții bazate pe `RPM`, altele sunt bazate pe `DEB`, dar acesta este un alt subiect pentru diferențierea managementului pachetelor.

# Derivate (Fork)

O distribuție derivată se bazează pe munca realizată în distribuția originală, dar are propria identitate, obiective și audiență și este creată de o entitate care este independentă de distribuția originală.

Derivatele modifică distribuțiile originale pentru a atinge obiectivele pe care și le-ai stabilit.

Cu alte cuvinte, ei fac ceva diferit sau adaugă ceva sau iau ceva care îl face mai potrivit pentru anumite cazuri de utilizare. Pe scurt, o distribuție derivată ia o copie și face modificări peste distribuția originală și apoi o distribuie ca sistem de operare propriu.

Deci, dacă aveți o nevoie specifică care este mai bine deservită de un derivat, s-ar putea să preferați și să o utilizați în loc de distribuția originală.

> ![Tipuri de distribuții Linux - Derivate](img/linux-types-derevative-distro.png)

În secțiunea de derivate avem multe sub-niveluri și uneori chiar sub-niveluri în interiorul acelor sub-niveluri. De exemplu, [Linux Mint](https://linuxmint.com/) se bazează pe [Ubuntu](https://ubuntu.com/), care la rândul său se bazează pe Debian. Prin urmare, Linux Mint este derivatul unui derivat.

Deoarece acesta este cu sursă deschisă, după cum puteți vedea dacă cineva nu este mulțumit de ceea ce face distribuția GNU/Linux preferată, poate să o bifurce și să meargă pe drumul său.

# Arome

Aroma este o distribuție bazată pe o altă distribuție GNU/Linux care a fost oficial aprobată de distribuția de bază pentru a face acest lucru. În general, distribuțiile arome sunt la fel ca cireașa peste distribuția părinte pentru a se potrivi nevoilor unei anumite secțiuni de utilizatori.

> ![Tipuri de distribuții Linux - Arome](img/linux-types-flavor-distro.png)

Aromele unei distribuții au aceleași pachete de bază ca și originalul, partajează aceleași depozite ca și originalul, dar are programe diferite la instalare sau este configurat diferit.

Practic la aceste arome diferă mediul de lucru (desktop-ul / interfața grafică). De exemplu, [Xubuntu](https://xubuntu.org/) este Ubuntu, dar cu mediul de lucru Xfce și [Kubuntu](https://kubuntu.org/) este doar Ubuntu cu mediul de lucru Plasma.

Dacă sunteți interesat, puteți arunca o privire la [Cronologia distribuției GNU/Linux](https://upload-wikimedia-org.translate.goog/wikipedia/commons/1/1b/Linux_Distribution_Timeline.svg) pentru a vedea cum sunt legate o mulțime de distribuții GNU/Linux.
