% Cum se remediază „Pacman is currently in use, please wait”
% ThinkRoot99

Pe celălalt laptop am avut Arch Linux cu GNOME. Și m-am hotărât într-o zi să pun Sway în locul lui GNOME, dar nu prin a face o instalare pe curat a Arch.

Așa că m-am apucat și am dezinstalat tot ce ține de GNOME, am dezinstalat toate pachetele de care nu mai aveam nevoi până am ajuns la aproximativ 300, 400 și ceva de pachete instalate în Arch.

Toate bune și frumoase până aici. Când dupa o repornire de sistem am vrut să actualizez sistemul cu comanda `paru` iar aceast nu se executa pentru că îmi apărea mesajul `Pacman is curently in use, please wait`. Am mai repornit laptopul încă o dată și am rulat comanda de actualizare `sudo pacman -Syu` și de aceasta dată a fost afișat mesajul menționat anterior.

**Soluția este:**

Mesajul este rezultatul unor sarcini neterminale ale paru sau pacman, ceea ce face acestea să fie blocate în sarcinile anterioare.

Pentru a remedia acest lucru, tot ce este de făcut este rularea comenzii următoare:

    $ rm /var/lib/pacman/db.lck

Acum, pacman sau paru/yay merg fără probleme.
