% 20 de exemple de bază ale comenzii „ls” în Linux
% ThinkRoot99

Listarea fișierelor este una dintre cele mai răspândite sarcini întreprinse de utilizatorii obișnuiți de Linux și de administratorii de sistem.

În Linux, comanda ls (prescurtare de la „***list***”) este utilizată pentru a lista sau afișa conținutul unui director.

Acesta poate fi directorul curent, fie orice alt director din sistem. Comanda afișează atât fișierele, cât și subdirectoarele și, în cele mai multe cazuri, face distincția între diferitele tipuri de fișiere folosind coduri de culoare.

Fără nicio opțiune în linia de comandă, comanda `ls` va lista pur și simplu tot conținutul directorului. Cu toate acestea, ea oferă o serie de opțiuni utile în linia de comandă pentru a manipula și a afișa rezultatul dorit.

În acest articol, vom discuta despre elementele de bază ale comenzii `ls`, cu toate opțiunile disponibile pentru diferitele comenzi pe care le oferă în Linux.

# Opțiunile comenzii ls în Linux

Comanda `ls` are următoarea sintaxă:

    $  ls [ options ] /path/to/directory

Secțiunea de `opțiuni (options)` reprezintă argumentele din linia de comandă care pot fi transmise pentru a manipula rezultatul comenzii.

În acest tutorial, vom aborda următoarele argumente ale comenzii `ls`.

| Opțiuni | Descriere |
|---------|-----------|
| ls -m | Listează conținutul directoarelor separate prin virgulă. |
| ls -Q | Afișează conținutul directorului închis între ghilimele. |
| ls -l | Afișează fișierele în format „listă lungă”. |
| ls -lh | Afișează dimensiune fișierului într-un format lizibil pentru utilizator. |
| ls -g | Omite coloana de proprietate a grupului. |
| ls -F | Adaugă o bară oblică la directoare. |
| ls -i | Afișează numărul de ordine al fișierelor și directoarelor. |
| ls -a | Afișează toate fișierele, inclusiv fișierele ascunse. |
| ls -*. | Filtrează fișierele în funcție de extensia acestora. |
| ls -la | Afișează toate fișierele și directoarele în format „lista lunga”. |
| ls -R | Afișează fișierele și directoarele în mod recursiv. |
| ls -r | Sortează fișierele în sens invers. |
| ls -X | Sortați fișierele în ordine alfabetică după extensia de fișier. |
| ls -tl | Afișarea fișierelor în funcție de data și ora creării acestora. |
| ls -n | Lista UID-urile și GID-urilor. |

## Listarea fișierelor și directoarelor în Linux

Executând comanda `ls` fără a trece nicio opțiune sau argument în linia de comandă, comanda `ls` listează pur și simplu conținutul directorului în ordine alfabetică.

Aici nu vom putea vizualiza detalii precum tipurile de fișiere, dimensiune, data și ora modificării, permisiunile și legăturile etc.

    $ ls

    root@linux:~$ ls
    1267361.jpg     Descărcări Documente Poze     Public   Video
    'Buget 2023.ods Desktop    Muzică    Proiecte Șabloane 
    root@linux:~$

## Lista lunga de fișiere

Indicatorul `-l` vă permite să afișați informații detaliate despre conținutul directorului într-un format de coloană care include dimensiunea, data și ora modificării, numele fișierului sau al directorului și proprietarul fișierului, precum și permisiunea acestuia.

    $ ls -l

    root@linux:~$ ls -l
    total 1424
    -rwxr-xr-x. 1 thinkroot thinkroot 1417105 oct 18 08:58  1267361.jpg
    -rwxr-xr-x. 1 thinkroot thinkroot   38123 nov 18 09:46 'Buget 2023.ods'
    drwxr-xr-x. 1 thinkroot thinkroot      26 nov 20 21:39  Descărcări
    drwxr-xr-x. 1 thinkroot thinkroot       0 nov 18 15:17  Desktop
    drwxr-xr-x. 1 thinkroot thinkroot      14 nov 18 17:11  Documente
    drwxr-xr-x. 1 thinkroot thinkroot     104 nov 18 17:12  Muzică
    drwxr-xr-x. 1 thinkroot thinkroot     162 nov 21 13:15  Poze
    drwxr-xr-x. 1 thinkroot thinkroot      94 nov 18 21:33  Proiecte
    drwxr-xr-x. 1 thinkroot thinkroot       0 nov 18 15:17  Public
    drwxr-xr-x. 1 thinkroot thinkroot     364 nov 18 21:43  Șabloane
    drwxr-xr-x. 1 thinkroot thinkroot       0 nov 18 15:17  Video
    root@linux:~$

Începând din extrema stângă, avem:

- Prima coloană - Permisiuni pentru fișiere/directoare
- Coloana a 2-a - Numărul de legaturi
- Coloana a 3-a - Numele proprietarului
- Coloana a 4-a - Numele grupului din care face parte fișierul
- Coloana a 5-a Dimensiunea fișierului în bytes.
- De la coloana 6 la coloana 8 - data ultimei modificări
- Coloana a 9-a - Numele fișierului/directorului

## Lista fișierelor și directoarelor ascunse

Fișierele ascunse sunt fișiere speciale care stochează setările utilizatorului și fișierele de configurare, care sunt utilizate de programele și serviciile care rulează pentru a citi și stoca informații.

De exemplu, fișierul `.bashrc` este un script care conține setările și configurările utilizatorului conectat în acel moment, printre care se numără aliasurile la comenzi, istoricul shell-ului, colorarea fontului terminalului etc.

Fișierul `.bash_logout` este executat atunci când vă deconectați din sesiunile bash. Este utilizat în principal în scopuri de curățare, adică pentru a efectua orice operațiune care trebuie efectuată după ce ieșiți din shell-ul bash.

Pentru a lista fișierele ascunse, utilizați indicatorul `-a`, așă cum se arată, care afișează atât fișierele, cât și directoarele ascunse.

    $ la -a

    root@linux:~$ ls -a
    .             .bash_profile    Descărcări Muzică   .steam       .var
    ..            .bashrc          Desktop    .pki     .steampath   Video
    1267361.jpg   'Buget 2023.ods' Documente  Poze     .steampid    .vscode-oss
    .bash_history .cache           .local     Proiecte Șabloane     .wget-hsts
    .bash_logout  .config          .mozilla   Public   .thunderbird .wine
    root@linux:~$

## Lista tuturor fișierelor în Linux

După cum ați observat, indicatorul `-a` nu listează dor fișierele ascunse, ci toate fișierele și directoarele. Pentru o vizualizare mai bună, puteți utiliza indicatorul `-la`

    # ls -la

    root@linux:~$ ls -la
    total 1452
    drwx------. 1 thinkroot thinkroot     470 dec  3 18:08  .
    drwxr-xr-x. 1 root      root           18 nov 18 15:17  ..
    -rwxr-xr-x. 1 thinkroot thinkroot 1417105 oct 18 08:58  1267361.jpg
    -rw-------. 1 thinkroot thinkroot    4066 dec  3 04:47  .bash_history
    -rw-r--r--. 1 thinkroot thinkroot      18 iul 19 03:00  .bash_logout
    -rw-r--r--. 1 thinkroot thinkroot     144 iul 19 03:00  .bash_profile
    -rw-r--r--. 1 thinkroot thinkroot     522 iul 19 03:00  .bashrc
    -rwxr-xr-x. 1 thinkroot thinkroot   38123 nov 18 09:46 'Buget 2023.ods'
    drwx------. 1 thinkroot thinkroot     830 dec  3 06:04  .cache
    drwxr-xr-x. 1 thinkroot thinkroot     892 nov 30 22:54  .config
    drwxr-xr-x. 1 thinkroot thinkroot     204 dec  3 05:13  Descărcări
    drwxr-xr-x. 1 thinkroot thinkroot       0 nov 18 15:17  Desktop
    drwxr-xr-x. 1 thinkroot thinkroot     158 dec  3 05:13  Documente
    drwx------. 1 thinkroot thinkroot      20 nov 18 15:17  .local
    drwxr-xr-x. 1 thinkroot thinkroot      48 nov 18 15:19  .mozilla
    drwxr-xr-x. 1 thinkroot thinkroot     104 nov 18 17:12  Muzică
    drwx------. 1 thinkroot thinkroot      10 nov 18 16:46  .pki
    drwxr-xr-x. 1 thinkroot thinkroot     432 dec  2 10:53  Poze
    drwxr-xr-x. 1 thinkroot thinkroot      94 nov 18 21:33  Proiecte
    drwxr-xr-x. 1 thinkroot thinkroot       0 nov 18 15:17  Public
    drwxr-xr-x. 1 thinkroot thinkroot     148 nov 28 09:29  .steam
    lrwxrwxrwx. 1 thinkroot thinkroot      34 nov 28 09:28  .steampath -> /home/thinkroot/.steam/sdk32/steam
    lrwxrwxrwx. 1 thinkroot thinkroot      32 nov 28 09:28  .steampid -> /home/thinkroot/.steam/steam.pid
    drwxr-xr-x. 1 thinkroot thinkroot     364 nov 18 21:43  Șabloane
    drwx------. 1 thinkroot thinkroot     128 nov 18 17:49  .thunderbird
    drwxr-xr-x. 1 thinkroot thinkroot       6 nov 18 15:39  .var
    drwxr-xr-x. 1 thinkroot thinkroot      10 dec  2 10:56  Video
    drwxr-xr-x. 1 thinkroot thinkroot      38 nov 18 20:47  .vscode-oss
    -rw-r--r--. 1 thinkroot thinkroot     180 nov 25 13:09  .wget-hsts
    drwxr-xr-x. 1 thinkroot thinkroot     126 nov 25 13:10  .wine
    root@linux:~$


https://www.tecmint.com/ls-command-in-linux/#3_List_Hidden_Files_and_Directories