% 5 motive pentru care GNU/Linux folosește atât de mult linia de comandă
% ThinkRoot99

Când începeți să utilizați GNU/Linux, veți descoperi că sistemul se bazează pe linia de comandă mult mai mult decât alte sisteme de operare, în ciuda prezenței a numeroase medii de lucru (desktop).

De ce asta?

Există o mulțime de motive pentru care linia de comandă este o parte esențială a ecosistemului GNU/Linux.

# 1. Când Unix a fost dezvoltat, nu a existat nicio interfață grafică

Deși GNU/Linux nu este Unix, deoarece nu are cod din sistem, comportamentul său se bazează pe acesta, inclusiv utilizarea liniei de comandă. Când Unix a fost dezvoltat la Bell Labs la sfârșitul anilor ’60 și începutul anilor ’70, nu exista nici o interfață grafică pentru utilizator.

Majoritatea oamenilor si-au trimis programele pe carduri perforate, în timp ce câțiva norocoși au reușit să interacționeze cu sistemul folosind un terminal, precum creatorii lui Unix: Dennis Ritchie și Ken Tompson.

Aceste terminale erau fie aparate teletype, fie terminale video, care erau doar un ecran și o tastatură. Ambele tipuri de terminale acceptau doar text, nu și grafică.

Interfețele din linia de comandă erau naturale pentru acest tip de terminal. Utilizarea terminalelor text a fost, de asemenea, un motiv major pentru care dezvoltatorii Unix au preferat nume scurte de comenzi, deoarece acestea erau mai rapide de tastat.

# 2. Instrumente de programare utilizând linia de comandă

Programatorii au fost cei mai convinși susținători ai GNU/Linux, deoarece are atât de multe instrumente pentru a-și îndeplini munca: interpretoare, compilatoare și depanatoare. Și toate aceste instrumente rulează în linia de comandă.

Toate aceste pot fi accesat și dintr-un [IDE](https://en.wikipedia.org/wiki/Integrated_development_environment) (integrated development environment) grafic care este un front-end către o linie de comanda undeva.

# 3. Linia de comandă este mai rapidă

Mulți utilizatori de GNU/Linux le place să suțină că linia de comandă este mai rapidă decât utilizarea unui GUI (graphical user interface). Programele din linia de comandă pornesc mai repde decât cele grafice, deoarece sunt mai mici, mai ușoare – nu au nevoie de resurse hardware mari.

Acesta este unul dintre motivele pentru care, atunci când GNU/Linux a debutat pentru prima dată pe calculatoare, distribuțiile s-au folosit implicit de mediul consolei. Calculatoarele mai puțin puternice ale epocii aveau adesea probleme să ruleze [X](https://en.wikipedia.org/wiki/X.Org_Server), cel puțin cu cantitățile mici de memorie RAM cu care erau echipate sistemele desktop.

Puteți vedea cât de mult se luptă un calculator i386 cu 4 MB de RAM de la începutul anilor ’90 să încarce un simplu manger de fereste X, iar atunci era o cantiate mare de meorie RAM.

> [Slackware Linux on a 386sx40](https://www.youtube.com/watch?v=5DBPuZHWEXc)

# 4. Linia de comandă funcționează peste tot, inclusiv pe servere

Un mare motiv pentru care linia de comandă a supraviețuit pe sistemele GNU/Linux este că funcționează aproape peste tot. Dacă lui X nu-i place placa voastră grafica, o problemă care era și mai frecventă la primele sisteme GNU/Linux, veți fi trimiși la consolă. Aceasta înseamnă că puteți reveni la linia de comandă atunci când este necesar.

Din acest motiv, este popular să instalați servere GNU/Linux numai cu interfața liniei de comandă. Acest lucru permite o utilizare mai eficientă a serverului. La urma urmei, nu este nevoie de o interfață grafică dacă oricum nimeni nu o va vedea.

Mulți administratori preferă să se conecteze de la distanță prin SSH pentru a-și gestiona serverele. Această sarcină mai mică permite servere-lor GNU/Linux să ruleze mai eficient decât servere-le Windows.

# 5. Programele din linia de comandă pot fi automatizate

Un mare avantaj al programelor pentru linia de comandă față de cele grafice este că programatori le pot automatiza.

Dacă lucrați cu programe grafice, cum ar fi managerii de fișiere, veți întâlni adesea operațiuni repetitive, cum ar fi redenumirea fișierelor. Dacă aveți o mulțime de fișiere, acest lucru poate deveni plictisitor cu un manager de fișiere GUI. Shell-ul vă permite să utilizați metacaracterele pentru a genera o listă de fișiere.

Dacă doriți să copiați toate fișierele text într-un director, ați folosi această comandă:

    cp *.txt /example

De asemenea, puteți utiliza limbaje de scripting pentru sarcini mai complicate. Timp de mulți ani, limbajul de scripting ales a fost shell-ul. Avantajul shell-ului este că puteți utiliza programele familiare pe care le-ați folosit în linia de comandă în scripturi.

În timp ce limbajele de scripting precum Perl sau Python folosesc biblioteci, este de asemenea, posibil să folosiți programe standard GNU/Linux dacă nu există o bibliotecă.

**Acum știți de ce GNU/Linux folosește atât de mult linia de comandă.**
