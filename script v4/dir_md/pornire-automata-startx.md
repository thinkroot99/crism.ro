# Pornirea automată a i3 în Arch Linux

De când am reinstalat Arch Linux nu am instalat nici un manager de afișare (display manager) pentru că pur și simplu nu am nevoie de așa ceva.
Autentificarea o fac manual în TTY pentru că nu am avut chef să rezolv această mică „*problemă*”, iar pornirea lui **i3** o făceam cu comanda `startx`.

Nu pot spune că m-am săturat să tot rulez comanda `startx`, dar astăzi nu am avut chef să mai tastez așa de mult la pornirea sistemului :)

Și am făcut următoarea setare pentru pornirea automată a i3-ului:

1. Am deschis fișierul `.bash_profile`

```bash
nvim .bash_profile
```

2. După ce s-a deschis fișierul am adăugat următoarele informații:

```cod
if [ -z "$DISPLAY" ] && [ "$XDG_VTNR" = 1 ]; then
  exec startx
fi
```

3. Am salvat fișierul, după care am repornit sistemul.

După autentificare i3 s-a pornit automat și nu a mai fost nevoie să tastez comanda `startx`.
