% Dezactivare Snaps în Ubuntu 21.10
% ThinkRoot99

Ubuntu 21.10 aduce și navigatorul Firefox ca aplicație implicită în formatul snap.

Tehnologia snap a evoluat destul de mult, majoritatea programelor pornesc mai repede. Din păcate sunt dezvoltatori care nu pun preț mare pe această tehnologie și împacheteză programele aiurea și acestea pornesc greu sau nu se integrează cu tema sistemului.

Am mai scris un articol despre dezactivarea snap-urilor pentru [Ubuntu 20.04](dezactivarea-snaps-in-ubuntu.html) și acest articol vine doar ca o actualizare.

# Eliminare snap-uri existente

Ubuntu instaleaza câteva aplicații snap și rulând comanda de mai jos veți putea vedea lista acestor aplicații.

    $ snap list
    Name               Version               Rev    Tracking         Publisher   Notes
    bare               1.0                   5      latest/stable    canonical✓  base
    core               16-2.52.1             11993  latest/stable    canonical✓  core
    core18             20211015              2246   latest/stable    canonical✓  base
    core20             20210928              1169   latest/stable    canonical✓  base
    firefox            92.0-3                619    latest/stable    mozilla*    -
    gnome-2-28-1804    3.28.0-19-g98f9e67…   161    latest/stable/…  canonical✓  -
    gnome-3-34-1804    0+git.3556cb3         72     latest/stable/…  canonical✓  -
    gnome-3-38-2004    0+git.6ba6040         76     latest/stable/…  canonical✓  -
    gtk-common-themes  0.1-59-g7bca6ae       1519   latest/stable/…  canonical✓  -
    snap-store         3.38.0-66-gbd5b8f7    394    latest/stable/…  canonical✓  -

Pentru a elimina aceste aplicații, trebuie să folosiți comanda: `sudo snap remove <nume pachet>`.

Rulați comenzile următoare pentru a elimina toate aplicațiile snap:

    $ sudo snap remove firefox bare
    $ sudo snap remove snap-store
    $ sudo snap remove gtk-common-themes
    $ sudo snap remove gnome-2-28-1804 gnome-3-34-1804 gnome-3-38-2004
    $ sudo snap remove core18 core20

Dacă vă întrebați de ce nu am eliminat și `snap-core`, deocamdată nu poate fi eliminat, dar o să se poată elimina după următori pași.

# Demontare snap core

`snap-core` poate fi eliminat doar după ce se face demontarea lui. Și demontarea se poate face doar pe baza ID-ului real al directorului core al sistemului.

În versiunea 21.10 nu am mai găsit nici un ID pentru `core`, dar este bine să verificați acest lucru pentru că este posibil ca la voi să apară acest ID.

    $ sudo umount /snap/core/xxxx

# Îndepărtarea și curățarea pachetului snap

Pentru a elimina pachetul `snapd` și toate serviciile sale, executați comanda:

    $ sudo apt purge snapd

# Îndepărtarea directoarelor snap

Ultimul pas pe care trebuie să îl mai faceți este să eliminați direcoarele snap rămase. Este posibil să nu aveți nici unul dintre aceste directoare după pasul 3.

Totuși este bine să verificați rulând comenzile:

    $ rm -rf ~/snap
    $ sudo rm -rf /snap
    $ sudo rm -rf /var/snap
    $ sudo rm -rf /var/lib/snapd

Acum vă puteți bucura de un Ubuntu fără snap.

> **Notă**
> Aceste modificări au fost efectuate pe Ubuntu 21.10. Nu garantez că acest tutorial mai este valabil și pentru ultima versiune de Ubuntu. Efectuați aceste modificări pe ultima versiune a Ubuntu pe propriile voastre riscuri.
