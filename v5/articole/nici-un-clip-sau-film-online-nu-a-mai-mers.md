# Nici un clip sau film online nu a mai mers

De ieri nici un clip sau film online nu a mai mers, nu se mai încărca indiferent de platformă/sit. Am încercat **YouTube**, **PeerTube**, **Netflix** și **Disney+**. **YouTube** și **PeerTube** le-am încercat pe navigatoarele **Thorium Browser**, **LibreWolf** și **Google Chrome** și nu a mers nici un clip video.

Astăzi am căutat pe internet probleme de acest gen la alți utilizatori și până la urmă am aflat care a fost problema.

Pe scurt problema a fost de la pachetul `wireplumber` care nu a mai funcționat bine după ultima actualizare și astfel a trebuie să instalez pachetul `pipewire-media-session`.

Și din câte se pare problema nu este nouă, este destul de veche -  a se vedea [aici](https://archlinux.org/news/undone-replacement-of-pipewire-media-session-with-wireplumber/).

Așa că am instalat pachetele următoare:

```bash
sudo pacman -Syu pipewire-media-session pipewire-alsa pipewire-jack pipewire-pulse
```

Dar problema nu s-a rezolvat așa ușor pentru că pachetul `pipewire-media-session` a intrat în conflict cu `wireplumber`, iar pachetul `pipewire-pulse` a intrat în conflict cu `pulseaudio`.

`wireplumber` l-am dezinstalat ușor, dar `pulseaudio` nu a mai mers așa ușor pentru că este folosit de aplicația `musikcube` - o aplicație audio de terminal.

Așa că am rulat comanda de mai jos pentru a dezinstalat cele trei pachete/aplicații:

```bash
sudo pacman -R wireplumber pulseaudio musikcube
```

După care am trecut la rularea comenzii de mai sus pentru a instalat pachetele: `pipewire-media-session`, `pipewire-alsa`, `pipewire-jack` și `pipewire-pulse`.

Și ultimul pas a fost să repornesc laptopul și astfel s-a rezolvat problema, și clipurile/filmele online merg bine acum.