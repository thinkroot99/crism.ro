# Video

Am un canal de YouTube unde mai public din când în când câte un clip video. Majoritatea au ca subiect Linux și Open Source.

Această pagină este pentru cei care nu știu de canalul meu de YouTube, dar vizitează acest sit. Clipurile merg și de aici fără a mai deschide pagina de youtube.com.

| YouTube                                                      | PeerTube                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <iframe width="760" height="315" src="https://www.youtube.com/embed/uXMb85c_b2A" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> | <iframe title="IT News: Microsoft, Huawei și Geely" width="560" height="315" src="https://makertube.net/videos/embed/6cb7e8fa-ee17-47a5-a5c5-ba3f4d7d129d" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe> |
| <iframe width="760" height="315" src="https://www.youtube.com/embed/RCA0oXogLBk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> |                                                              |
| <iframe width="760" height="315" src="https://www.youtube.com/embed/j_Lzzslj5OI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> |                                                              |
| <iframe width="760" height="315" src="https://www.youtube.com/embed/wLpELTd1gzs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> |                                                              |
| <iframe width="760" height="315" src="https://www.youtube.com/embed/zCCaih8Ya98" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> |                                                              |
| <iframe width="760" height="315" src="https://www.youtube.com/embed/otL3Iy8akWg&t" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |
|                                                              |                                                              |