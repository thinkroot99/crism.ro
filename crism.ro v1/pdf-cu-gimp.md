% Convertirea unui PDF cu GIMP
% ThinkRoot99

În acest articol vă voi explica cum să convertiți paginile unui document PDF în fișiere imagine (PNG, JPEG sau alte formate) folosind programul de editat imagini GIMP.

[GIMP](https://www.gimp.org/) este un **program de editare a imaginilor gratuit** cu sursă deschisă disponibil spre descărcare pentru GNU/Linux, Windows și macOS. Poate exporta pagini de documente PDF în diferite formate de imagine, inclusiv PNG, JPEG, TIFF, BMP și alte formate.

Aceste instrucțiuni explică modul de utilizare a **GIMP** pentru a converti un document **PDF** pentru cei care preferă să utilizeze o aplicație grafică. GIMP exportă pagini PDF una câte una, dar necesită un plugin pentru a exporta automat toate paginile.

# Instalare GIMP

În primul rând, dacă nu aveți GIMP, va trebui să îl instalați folosind exemplele următoare:

În distribuția Arch Linux, puteți instala GIMP folosind **flatpak** sau **pacman** așa cum se arată mai jos.

    [varianta flatpak]
    $ sudo pacman -S flatpak
    $ flapak install flathub org.gimp.GIMP
    
    sau
    
    [varianta pacman]
    $ sudo pacman install gimo

# Convertiți PDF în imagini

În primul rând, vom începe prin a converti una sau câteva pagini PDF în PNG. Acest lucru nu necesită adăugarea de plugin-uri în GIMP.

Faceți clic pe meniul `Fișier (File)`, selectați `Deschide (Open)` și alegeți fișierul PDF pe care doriți să îl convertiți.

Veți vedea o casetă de dialog `Import din PDF (Import from PDF)`. Setați opțiunea `Deschide paginile ca straturi (Open pages option as Layer)` și selectați `Importă (Import)`.

> ![Importă](img/import-pdf-in-gimp.png)

În dialogul straturilor (în partea stângă jos), derulați la pagina pe care doriți să o convertiți în imagine. Trageți pagina selectată cu cursor-ul în partea de sus, astfel încât să fie primul strat.

> ![Vizualizare PDF](img/pdf-view-in-gimp.png)

Apoi, faceți clic pe meniul `Fișier (File)` și selectați `Exportă ca (Export As)`. Acum puteți schimba extensia fișierului la formatul de imagine preferat editând câmpul de nume din partea de sus a casetei de export sau făcând clic pe `Selectează tipul de fișier (Select File (By Extension))` din partea de jos a casetei de export.

> ![Convertire PDF](img/convert-pdf-to-image-in-gimp.png)

De asemenea, puteți selecta unde doriți să salvați imaginea pe calculator în secțiunea `Salvează în dosar (Save in Folder)` din partea de sus a ferestrei de dialog. În cele din urmă, faceți clic pe butonul `Exportă (Export)` pentru a salva fișierul în formatul de imagine pe care l-ați ales.

GIMP va afișa o fereastră de dialog pentru a face modificări imaginilor, cum ar fi nivelul de compresie și calitatea imaginii.

> ![Nivel de compresie](img/image-compression-level-in-gimp.png)

> **Notă:**
> Acest articol este util și dacă folosiți GIMP în Windows sau macOS.
