% yay Aur Helper: Ce este și cum se instalează
% ThinkRoot99

**Yay** este o abreviere de la *Yet Another Yogurt*. Din punct de vedere tehnic, este un pachet **pacman** și un **AUR Helper** scris în limbajul de programare Go. Este cel mai popular ajutor pentru **Arch User Repository** (AUR) disponibil în prezent. Cu `yay`, puteți profita de cunoscutul depozit de pachete AUR și puteți compila și instala cu ușurință orice program.

În principiu, automatizează multe sarcini de gestionare a pachetelor, cum ar fi căutarea, rezolvă problema dependențelor din mers, compilează și construiește pachete și, desigur, publică propriile pachete pentru AUR.

Să aruncăm o privire la modul în care puteți instala `yay` în Arch Linux sau orice distribuție bazată pe Arch, cum ar fi Manjaro. Când instalați Arch Linux, puteți instala pachete cu ajutorul managerului de pachete `pacman` din trei depozite oficiale Arch. Dar `yay` nu este instalat implicit după o nouă instalare Arch Linux, de aceea trebiue să îl instalați manual pentru a profita de AUR.

# Instalare yay în Arch Linux

Acești pași necesită [pachetul de dezvoltare de bază](https://archlinux.org/packages/core/any/base-devel/) și pachetul `git` pentru compilare și intalare. Deschideți un terminal și rulați comenzile următoare:

    $ sudo pacman -Syu
    $ sudo pacman -S base-devel
    $ sudo pacman -S git

La prima comandă o să vă apară o listă de pachete și întrebarea dacă vreți să le instalați pe toate sau doar câteva. Apăsați pe tasta `enter` și se vor instala toate.

Pachetul `yay` are trei versiuni în dezpoitul AUR, după cum urmează:

- [yay](https://aur.archlinux.org/packages/yay) – versiunea stabilă.
- [yay-bin](https://aur.archlinux.org/packages/yay-bin) – versiunea precompilată.
- [yay-git](https://aur.archlinux.org/packages/yay-git) – versiunea de dezvoltare.

Pentru acest articol mă voi folosi de versiunea stabilă. Dacă nu vreți să așteptați până se compilează puteți instala și versiune `yay-bin` – este la fel de stabilă. Deschideți un terminal și rulați următoarele comenzi:

    $ git clone https://aur.archlinux.org/yay.git
    $ cd yay
    $ makepkg -si

Dacă vreți să folosiți o singură comandă, rulați comanda următoare:

    $ sudo pacman -S --needed git base-devel && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si

Dacă nu vreți să compilați pachetul (nu durează mult compilarea) puteți să instalați pachetul precompilat astfel:

    $ git clone https://aur.archlinux.org/yay-bin.git
    $ cd yay-bin
    $ makepkg -si

# Instalare yay în Manjaro sau altă distribuție

Dacă utilizați Manjaro Linux sau altă distribuție bazată pe Arch, de obicei pachetul `yay` este disponibil în depozitul acelei distribuții. Puteți instala cu ușurință folosind următoarele comenzi:

    $ sudo pacman -Syu
    $ sudo pacman -S yay

Acum să aruncă o privire la modul în care se poate instala orice pachet și, de asemenea, unele utilizări de bază.

# Cum se folosește

Pentru a instala orice aplicație sau pentru a căuta în depozitul AUR, se folosește comenzile următoare:

    # comandă instalare
    $ yay -S nume_pachet

    # comandă căutare
    $ yay -Ss nume_pachet

De exemplu pentru navigatorul Firefox se folosește astfel:

    # pentru instalare
    $ yay -S firefox

    #pentru căutare
    $ yay -Ss firefox

# Sfaturi pentru utilizarea

Nu se folosește doar la instalare și căutare, se poate face mult mai multe cu acest AUR Helper. Unele dintre exemple sunt mai jos.

Reîmprospătare și actualizare pachete de sistem

    $ yay -Syu

Utilizați versiunile de dezvoltare ale pachetelor și actualizați pachetele

    $ yay -Syu --devel --timeupdate

Eliminare orice pachet

    $ yay -Rns nume_pachet
    $ yay -Rns firefox

Obțineți informații rapide despre sistem

    $ yay -Ps

Pentru a obține mai multe informății despre ce poate face folosiți comanda:

    $ yay --help
