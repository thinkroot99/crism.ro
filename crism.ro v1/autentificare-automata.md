% Autentificare automată în Windows 10
% ThinkRoot99

Windows 10 vine cu mai multe opțiuni pentru a te conecta la contul de utilizator. Cu toate acestea, dacă nu dorești să tastezi de fiecare dată codul pin sau parola pentru autentificare, poți activa autentificare automată în Windows 10.

# Cum se activează conectarea automată

Există două moduri de a realiza acest lucru. Puteți dezactiva datele de autentificare direct din panoul `User Accounts` sau prin editarea registrului `Registry editor`.

## 1. Activați autentificarea automată din User Accounts

1. Apăsați tastele `Windows+R` pentru a deschide caseta `Executare / Run`.
2. în caseta de dialog `Run` scrieți `netplwiz` și faceți clic pe **OK**. Acesta va deschide fereastra `Conturi de utilizatori / User Accounts`.
3. Debifați caseta `Utilizatorii trebuie să introducă un nume de utilizatori și o parolă pentru a utiliza acest calculator / User must enter a user name and password to use this computer`.
4. Faceți clic pe `Aplică / Apply`.

> ![Conturi de utilizatori](img/user-accounts.png)

Când vi se solicită datele de identificare, introduceți numele de utilizator și parola pentru a dezactiva autentificarea bazată pe parolă. Este necesar să introduceți numele de utilizator și parola de două ori.

După care faceți clic pe **Ok** pentru a salva și ieșiți din fereastră.

La următoarea re/pornire, autentificarea se va face automat fără să vi se mai ceară un cod pin sau o parolă.

## 2. Activați autentificarea automată folosind Registry editor

Dacă prima metodă nu a funcționat și Windows 10 încă solicită o parolă pentru autentificare, încercați următorul „*hack*”.

Înainte de a efectua modificarea registrului, creați un punct de restaurare a sistemului. Vă va permite să restaurați sistemul dacă ceva nu merge bine.

### Editați fișierele din registru

1. Apăsați tastele `Windows+R` pentru a deschide caseta `Executare / Run`.
2. În caseta de dialog `Run` scrieți `regedit` și faceți clic pe **OK**. Acesta va deschide fereastra `Editorul de registru / Registry editor`.
![Editorul de registru](img/registry-editor.png)
3. În editorul de registru, mergeți la următoarea linie: `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon`
4. În panoul din dreapta, faceți dublu clic pe intrarea `DefaultUserName`.
5. Introduceți numele de utilizator și faceți clic pe **OK**.
6. În cazul în care intrarea **DefaultPassword** nu există, trebuie să o creați manual. Dacă vedeți intrarea **DefaultPassword**, treceți la pasul 9.
7. În meniul `Editare / Edit`, faceți clic pe `Nou / New` și selectați `String Value`.
8. Denumiți-l **`DefaultPassword`** și apăsați pe tasta Enter.
9. Faceți dublu clic pe `DefaultPassword`, introduceți parola și faceți clic pe **OK**.
10. În meniul `Editare / Edit`, faceți clic pe `Nou / New` și selectați `String Value`.
11. Denumiți-l `AutoAdminLogin` și apăsați tasta Enter.
12. Faceți dublu clic pe `AutoAdminLogin`. În caseta `Edit string`, tastați **1** în câmpul `Value` și faceți clic pe **OK**.
13. Ieșiți din editorul de registru.

Acum reporniți calculatorul și autentificarea se va face automat. 

Folosiți prima metodă pentru autentificare automată și dacă aceasta nu funcționează, folosiți cea de a două metodă.
