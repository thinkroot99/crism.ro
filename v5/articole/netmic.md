# Un internet mic 

De când am început să citesc sit-uri mici și simple, am dat peste comunități ca [smolweb.org](https://smolweb.org/), [250kb.club](https://250kb.club/), [512kb.club](https://512kb.club/) și [1mb.club](https://1mb.club/). Așa că am ajuns să modific acest sit, dar nu a fost de ajuns și astfel am creat proiectul [netmic.ro](https://netmic.ro).

**netmic.ro** este un proiect care promovează conceptul „*small web*” în România și prin acest proiect vreau să aduc la cunoștința tuturor care dețin un sit sau jurnal (blog) că internetul nu înseamnă doar CMS-uri (WordPress, Drupal, etc.), rețele de socializare, etc., și toate lucruri care încarcă un sit.

Internetul înseamnă și un sit mic static, care în același timp arată la fel de bine ca un sit construit pe un CMS. Internetul nu înseamnă doar rețele de socializare unde să se posteze tot conținutul - pe aceste rețele de socializarea toată informația bună se pierde.

Cel mai bine este să deții propriul sit sau jurnal unde să postezi toate ideile tale că sunt bune sau nu, pentru cineva tot sunt bune și astfel informația nu se pierde ca pe rețelele de socializare.

Din păcate internetul din România este plin de sit-uri construite pe CMS, este plin de sit-uri și jurnale încărcate. Aceste sit-uri se pot construit/crea și doar în HTML/CSS mai ales că la un sit nu trebuie modificate paginile în fiecare zi, aceste se creează o dată, iar o dată la câteva luni/ani se mai modifică.

Și un jurnal se poate construi în HTML/CSS chiar dacă se public articole în fiecare zi - cu ajutorul automatizări este la fel simplu ca și cum ai avea jurnalul pe un CMS.

Proiectul **netmic.ro** este un proiect finalizat, să zic așa, și nu mai are nevoie de îmbunătățiri. Dar personal vreau să fac acest proiect ceva mai mare pentru România, vreau să combin **smolweb.org**, **512kb.club** și găzduire gratuită pentru blog-uri mici până într-o anumită mărime.

Bineînțeles că aceste planuri de dezvoltare durează pentru că nu se poate implementa peste noapte, dar începutul este promițător, zic eu :) În primul rând conceptul „*small web*” trebuie să devină cunoscut în România, după care urmează restul pașilor.

Dacă vreți să aflați ce este și ce înseamna „*small web*” vizitați sit-ul [netmic.ro](https://netmic.ro).