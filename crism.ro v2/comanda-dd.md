% Clonați și restaurați cu comanda dd
% ThinkRoot99

Când vine timpul să ștergeți, să faceți o copie de rezervă sau să restaurați datele pe GNU/Linux, există mai multe aplicații pe care le puteți alege. **GParted** este una dintre cele mai populare aplicații. 

**GNOME Disk** este o alternativă mai modernă, care arată grozav în mediul de lucru GNOME. Dar indiferent de distribuția GNU/Linux pe care o utilizați, există o opțiune care funcționează întotdeauna – aceasta este `comanda dd`.

Puteți utiliza GNU/Linux fără a fi nevoie să vă scufundați vreodată în terminal. Dar odată ce adăugați anumite comenzi la repertoriul vostru, nu numai că veți economisi timp, dar veți învăța abilități pe care le puteți utiliza în orice versiune de GNU/Linux.

# De ce să folosiți dd?

Indiferent dacă sunteți pe un calculator personal sau server, `dd` pur și simplu funcționează. `dd` a început ca o comandă Unix, așa că, pe lângă GNU/Linux, acceptă și alte sisteme de operare asemănătoare Unix, cum ar fi FreeBSD sau macOS.

Cu comanda `dd` durează doar câteva secunde pentru a iniția o ștergere sau pentru a crea o imagine de disc. Odată ce știți ce să tastați, puteți deschide un terminal și puteți șterge o unitate în timpul ce faceți o căutare online.

Cu o mare putere vine o mare responsabilitate. Comanda de a clona o unitate nu este deloc diferită de cea de ștergere. Trebuie să acordați o atenție sporită la ceea ce introduceți atunci când lucrați cu comenzi care pot șterge fișiere.

Ești nervos? Bun. Chiar și odată ce sunteți expert în `dd`, vreți întotdeauna să tastați comenzile cu atenție. **O minte absentă poate, în cel mai rău caz să distrugă hardware-ul care duce la pierederea datelor**. Acum, să începem.

#Clonarea unei unități

Clonarea unei unități este o modalitate sigură de a face o copie de rezervă a datelor. Puteți face copii de rezervă ale datelor pe o memorie de stocare externă sau în cloud, dar acest lucru vă salvează doar fișierele.

Pentru a vă salva aplicațiile, trebuie să învățați cum să faceți copii de rezervă pentru fiecare aplicație individuală. Pentru unii, asta înseamnă exportarea unui anumit fișier. Pentru alții, asta înseamnă copierea unui director ascuns. Apoi sunt acele aplicații la care nu se pot face deloc o copie de rezervă.

`dd` rezolvă acest lucru prin creearea unei copii exacte a întregului hard disk sau a partiției. Când restaurați această copie, vă aduceți calculatorul înapoi exact cum era. Vă veți recupera fișierele, aplicațiile, istoricul de navigare și chiar setările sistemului. Absolut tot.

Pentru a vă clona unitatea, veți avea nevoie de o a doua unitate de stocare care are mai mult spațiu decât cea pe care o copiați. Probabil, acesta va fi o memorie de stocare externă sau o unitate flash mare.

Începeți prin a deschide terminalul. Veți avea nevoie de drepturi de administrator pentru a executa orice comanda cu `dd`. Puteți să tastați `su` pentru a vă conecta ca administrator sau să tastați `sudo` la începutul comenzii `dd`.

Când sunteți gata să copiați, introduceți comanda de mai jos. Rețineți că va șterge toate datele preexistente de pe a doua unitate, așa că asigurați-vă că faceți o copie de rezervă a tuturor datelor.

    $ dd if=/dev/sdX of=/dev/sdY
    ex:
    $ dd if=/dev/sda of=/dev/sdc

Acum, să înțelegem ce se întâmplă. `dd` este comanda.

- `if` este intrarea pentru locația pe care doriți să o copiați.
- `of` este ieșirea pentru locația pe care o înlocuiți cu copia voastră.
- `sdX` și `sdY` se referă la unitățile cu care interacționați. Unitățile primesc adesea un nume cum ar fi `/dev/sda`, `/dev/sdb` sau `/dev/sdc`.

Puteți afla numele folosind un editor de partiții sau, deoarece sunteți deja în terminal, puteți folosi comanda `lsblk`.

# Crearea unei imagini de disc

O altă metodă de a clona o unitate este să creați o imagine de disc pe care o puteți muta și restaura, așa cum ați proceda cu un stick USB bootabil.

Crearea fișierelor de imagine vă permit să salvați mai multe copii de siguranță într-o singură destinație, cum ar fi un hard disk portabil mare. Din nou, acest proces necesită o singură comandă.

    $ dd if=/dev/sdX of=calea/spre/imaginae-de-backup.img
    ex:
    $ dd if=/dev/sda of=/home/thinkroot99/Backup/backup01.img

Pentru a economisi spațiu, puteți solicita `dd` să vă comprime copia de rezervă.

    $ dd if=/dev/sdX | gzip -c > calea/spre/imaginea-de-backup.img.gz
    ex:
    $ dd if=/dev/sda | gzip -c > /home/thinkroot99/Backup/backup01.img.gz

Această comandă comprimă back-up într-un fișier *IMG.GZ*, unul dintre multele formate de compresia pe care GNU/Linux le poate gestiona.

# Restaurarea unei unități

La ce servesc backup-urile dacă nu le folosiți? Când sunteți gata să restaurați o imagine cu `dd`, aveți două opțiuni. Dacă ați folosit prima abordare, schimbați pur și simplu cele două destinații.

    $ dd if=/dev/sdY of=/dev/sdX

La restaurarea dintr-un fișier imagine, se aplică același concept.

    $ dd if=calea/spre/imaginae-de-backup.img of=/dev/sdX

Dacă fișierul imagine este comprimat, atunci lucrurile devin puțin diferite. Folosiți această comandă în schimb:

    $ gunzip -c calea/spre/imaginea-de-backup.img.gz | dd of=/dev/sdX

Pentru a clarifica, **gunzip** este `g unzip`, ca în opusul `g zip`. Această comandă vă decomprimă backup-ul. Apoi `dd` înlocuiește unitatea existentă cu această imagine.

# Indicatori de luat în considerare

Puteți modifica comanda prin lipirea unui indicator la șfârșit. În mod implicit, operația cu comanda `dd` poate dura ceva timp pentru a transfera date. Puteți accelera procesul prin creșterea dimensiunii blocului. Faceți acest lucru adăugând `bs=` la sfârșit.

    $ dd if=/dev/sdX of=/dev/sdY bs=64

Acest exemplu mărește dimensiunea implicită a blocului de la 512 bytes la 64 kilobytes.

`conv=noerror` îi spune lui `dd` să continue în ciuda oricăror defecte care apar. Comportamentul implicit este oprirea procesului, rezultând un fișier incomplet. Rețineți că ignorarea defectelor nu este întotdeauna sigură. Fișierul rezultat poate fi corupt și să nu funcționeze.

`conv=sync` adaugă blocuri de intrare cu zero ori de câte ori există defecte de citire. În acest fel, datele rămân sincronizate.

Puteți combina acești indicatori ca `conv=noerror,sync` dacă doriți. Nu există spațiu după virgulă.

# Mai multe despre dd

Numele lui `dd` se referă la o declarație din [IBM Job Control Language](https://en.wikipedia.org/wiki/Job_Control_Language#In-stream_input). Dacă nu înțelegeți ce se întâmplă acolo, nu este nici o problemă. Asta nu face comanda mai greu de utilizat.

Dacă aveți nevoie de mai multe informații despre `dd`, pagina de [wiki](https://en.wikipedia.org/wiki/Dd_(Unix)) oferă multe informații. Mai puteți accesa, de asemenea, pagina de [Wiki Arch](https://wiki.archlinux.org/title/disk_cloning#Using_dd).

Din nou, nu contează dacă folosești Arch sau nu, `dd` funcționează în același mod indiferent de distribuția pe care o folosiți.
