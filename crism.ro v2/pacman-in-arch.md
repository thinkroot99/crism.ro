% Cum se folosește pacman în Arch linux
% ThinkRoot99

**Pacman** este un [manager de pachete](https://wiki.archlinux.org/title/pacman) pentru Arch Linux și distribuțiile bazate pe Arch. Dacă ați folosit un sistem de operare bazat pe Debian precum Ubuntu sau Linux Mint, atunci acesta este similar cu comanda **apt**. Pacman conține fișierele comprimate ca format de pachet și menține o bază de date de pachete bazată pe text.

Pacman menține sistemul la zi prin sincronizarea listelor de pachete cu serverul principal și poate instala pachetele din depozitele oficiale sau din propriile pachete de compilare.

În acest articol, vom vedea cum se poate folosi pentru a gestiona programele în sistemele bazate pe Arch Linux. Acum să vedem cum putem folosi pacman.

# Instalarea pachetelor cu pacman

Când instalăm orice sistem de operare nou pe calculatorul nostru, prima sarcina pe care o facem este să instalăm programele necesare. Pentru a instala programele pe Arch Linux, utilizați comanda pacman cu indicatorul `-S` și menționați numele pachetului. Indicatorul `-S` îi spune lui pacman să se sincronizeze cu serverul.

    $ sudo pacman -S nume_pachet
    ex:
    $ sudo pacman -S firefox

Pentru a instala mai multe programe, după indicatorul `-S`, se trec numele programelor cu spațiu între ele.

    $ sudo pacman -S nume_pachet1 nume_pachet2 nume_pachet3
    ex:
    $ sudo pacman -S firefox brave google-chrome

Apoi pacman va afișa dimensiunea pachetelor pentru descărcare și dimensiunea pachetelor instalate, după care vă cere să continuați prin confirmare cu tasta `Y` și `Enter`.

Pacman clasifică pachetele instalate în două categorii:

- **Implicitly Installed:** pachetul care a fost instalat folosind indicatorul `-S` sau `-U`.
- **Dependencies:** pachetul este instalat deoarece este cerut de un alt pachet.

# Eliminarea pachetelor cu pacman

Când nu mai avem nevoie de un anume program, atunci ar trebui să scoatem pachetul din sistem. Pentru a elimina pachetul cu toate dependențele sale care nu sunt cerute de alte aplicații, utilizați următoarea comandă:

    $ sudo pacman -Rs nume_pachet

Pentru a elimina pachetul fără a elimina dependențele acestuia, utilizați următoarea comandă:

    $ sudo pacman -R nume_pachet

Pentru a elimina dependențele care nu mai sunt necesare, utilzați următoarea comandă:

    $ sudo pacman -Qtdq | sudo pacman -Rs -

# Actualizarea pachetelor cu pacman

În Arch Linux, putem actualiza întregul sistem printr-o singură comandă. Utilizați următoarea comandă pentru a actualiza sistemul:

    $ sudo pacman -Syu

Să înțelegem sensul comenzii, `S` îi transmite lui pacman să sinconizeze baza de date locală cu baza de date principală, `y` îi transmite lui pacman să actualizeze cache-ul local pe sistem și `u` îi transmite lui pacman să actualizeze pachetele.

# Căutarea pachetelor

Acum să vedem cum putem căuta un program în baza de date. Pentru a căuta interogarea în numele și descrierea pachetului în baza de date, utilizați următoarea comandă:

    $ sudo pacman -Ss interogarea1 interogarea2
    ex:
    $ sudo pacman -Ss firefox
    $ sudo pacman -Ss browser

Pentru a căuta pachete deja instalate pe sistem, utilizați următoarea comandă:

    $ sudo pacman -Qs nume_pachet

Pentru a căuta interogarea în baza de date locală, utilizați următoarea comandă:

    $ sudo pacman -F interogare

# Curățarea cache-ului

Când pacman descarcă pachete, acesta stochează pachetele în `/var/cache/pacman/pkg` și în timp ce dezinstalează pachetul nu se șterg aceste fișiere. Pacman folosește aceste fișiere pentru a face downgrade (trecerea la o versiune mai veche) sau pentru a instala pachetul. Dar poate ocupa ceva spațiu pentru a stoca aceste date, deci pentru a șterge cache-ul folosiți următoarea comandă:

    $ sudo pacman -Sc

Pentru a elimina toate pachetele stocate și catch-ul, utilizați următoarea comandă:

    $ sudo pacman -Scc

# Instalarea pachetelor locale

Se pot instala și alte pachete nu numai din depozitul principal al Arch Linux. Utilizați următoarea comandă pentru a instala pachetele:

**Pentru local:**

    $ sudo pacman -U calea_la_fișier.pkg.tar.xz

**Pentru pachetul la distanță:**

    $ sudo pacman -U https://www/exemplu.ro/repo/exemplu.pkg.tar.xz

# Depanare

Uneori instalând pachetele ne confruntăm cu unele probleme. Următoarele probleme apar de obicei:

- **Conflicting file error:** această problemă apare din cauza unor conflicte a pachetelor prezente în depozit. Pentru a rezolva această problemă, putem redenumi manual fișierul sau forțat folosind funcția de suprascriere cu următoarea comandă:

       $ sudo pacman -S --overwrite glob package

- **Invalid package:** această problemă poate apărea din cauza instalării parțiale a pachetelor. Se poate rezolva această problemă ștergând fișierele `.part` din `/var/cache/pacman/pkg/`.
- **Locking database:** această problemă poate apărea atunci când managerul de fișiere este întrerup în timpul actualizării bazei de date. Pentru a rezolva această problemă, șterge-ți fișierul `/var/lib/pacman/db.lock` și actualizați baza de date. Utilizați următoarea comandă pentru a șterge fișierul:

       $ sudo rm /var/lib/pacman/db.lock

