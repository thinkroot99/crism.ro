# Cum se instalează paru în Arch Linux

**Paru** este un **AUR Helper** dezvoltat de [Morganamilo](https://github.com/Morganamilo), care a fost și unul dintre dezvoltatorii principali al **yay**, care este cel mai popular ajutor AUR

În mare diferențele dintre `paru` și `yay` este că, primul este scris în limbajul Rust, iar cel de al doilea este scris în limbajul Go. La fel ca și cu `yay`, și cu `paru` puteți profita la maxim de depozitul AUR și puteți compila și instala cu ușurință orice program.

Are câteva caracteristici și funcții noi, în ciuda faptului că a fost dezvoltat de același co-dezvoltator yay. De la lansarea inițială, au existat multe funcții noi. Cele mai notabile fiind:

- Afișare PKGBUILD.
- Afișare comentariilor din AUR.
- Afișare știrilor despre upgrade.
- Suport local pentru repo și chroot.
- Suport pentru semnarea pachetelor.
- Evidențierea sintaxelor cu bat.
- Suport pentru doas cu Sudo = doas și SudoLoop = true

> !Notă
> `yay` nu este întrerupt din dezvoltare. Este dezvoltat și întreținut în mod activ de către autorul său principal și alți contribuitori. Deci nu este nevoie să treceți complet la `paru`. De asemenea, puteți folosi `paru` și `yay` unul lângă altul pentru o perioadă.

## Instalare paru în Arch Linux

Acești pași necesita [pachetul de dezvoltare de bază](https://archlinux.org/packages/core/any/base-devel/) și pachetul git pentru compilare și instalare. Deschideți un terminal și rulați comenzile:

    $ sudo pacman -Syu
    $ sudo pacman -S base-devel
    $ sudo pacman -S git

La prima comandă o să vă apară o listă de pachete și întrebarea dacă vreți să le instalați pe toate sau doar câteva. Apăsați pe tasta `enter` și se vor instala toate.

Pachetul `paru` are trei versiuni în depozitul AUR, după cum urmează.

- [paru](https://aur.archlinux.org/packages/paru) – versiunea stabilă.
- [paru-bin](https://aur.archlinux.org/packages/paru-bin) – versiunea precompilată.
- [paru-git](https://aur.archlinux.org/packages/paru-git) – versiunea de dezvoltare.

Pentru acest articol mă voi folosi de versiunea stabilă. Dacă nu vreți să așteptați până se compilează puteți instala și versiune `paru-bin` – este la fel de stabilă. Deschideți un terminal și rulați următoarele comenzi:

    $ git clone https://aur.archlinux.org/paru.git
    $ cd paru
    $ makepkg -si

Dacă doriți să folosiți o singura comandă, rulați comanda următoare:

    $ sudo pacman -S --needed git base-devel && git clone https://aur.archlinux.org/paru.git && cd paru && makepkg -si

Dacă nu vreți să compilați pachetul (nu durează mult compilarea) puteți să instalați pachetul precompilat astfel:

    $ git clone https://aur.archlinux.org/paru-bin.git
    $ cd paru-bin
    $ makepkg -si

## Instalare paru în Manjaro sau altă distribuție

Dacă utilizați Manjaro Linux sau altă distribuție bazată pe Arch, de obicei pachetul paru este disponibil în depozitul acelei distribuții. Puteți instala cu ușurință folosind următoarele comenzi:

    $ sudo pacman -Syu
    $ sudo pacman -S paru

Acum să aruncăm o privire la modul în care se poate instala orice pachet și, de asemenea, unele utilizări de bază.

## Cum se folosește

Pentru a instala orice aplicație sau pentru a căuta în depozitul AUR, se folosește comenzile următoare:

    # comandă instalare
    $ paru -S nume_pachet
    
    # comandă căutare
    $ paru -Ss nume_pachet

De exemplu pentru navigatorul Firefox se folosește astfel:

    # pentru instalare
    $ paru -S firefox
    
    #pentru căutare
    $ paru -Ss firefox

## Sfaturi pentru utilizarea

Nu se folosește doar la instalare și căutare, se poate face mult mai multe cu acest AUR Helper. Unele dintre exemple sunt mai jos.

Reîmprospătare și actualizare pachete de sistem

    $ paru -Syu

Căutați și instalați pachete în mod interactiv

    $ paru nume_pachet
    $ paru firefox

Pe ecranul terminalului va fi afișată o listă numerotată cu toate programele găsite. Pentru a instala programul dorit trebuie să tastați numărul corespunzător și să apăsați tasta `enter`.

Dezinstalare pachet

    $ paru -Rns nume_pachet
    $ paru -Rns firefox

Obțineți informații rapide despre sistem

    $ paru -Ps

Pentru a obține mai multe informații despre ce poate face folosiți comanda:

    $ paru --help
