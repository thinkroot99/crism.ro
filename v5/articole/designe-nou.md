# De ce un designe nou?

De când am trecut cu crism.ro la un sit static, acesta este al treilea designe.

Am trecut la un nou designe pentru că am vrut ceva al meu, până acum designe-ul sit-ului nu a fost al meu. Doar l-am împrumutat de la altcineva și eu am schimbat paleta de culori.

Acest șablon este creat de la zero cu ajutorul AI-ului. AI-ul (*ChatGPT și Copilot*) a creat codul HTML și CSS, dar eu am gândit structura (cum să arate), paleta de culori (care este [Nord Theme](https://www.nordtheme.com/)) și am dat instrucțiunile.

Puteam să creeze un astfel de sit pentru că știu ceva HTML și CSS, dar a fost mai distractiv cu AI-ul.

Articolele le scriu în continuare în markdown și aceste fișiere le convertesc în fișiere .html cu ajutorul unui script scris în Python tot de AI :).

Tot ce am făcut la acest sit, am făcut cu ajutorul AI-ului - ajutor dat de ChatGPT și Copilot.

VSCodium folosesc pentru a scrie codul HTML și CSS, iar pentru a scrie textul în markdown folosesc Apostrophe.

Codul sursă al sit-ului și al scriptului de conversie se găsesc pe GitHub.

![versiunea 1](/img/crism.ro-v1.webp) ![versiunea 2](/img/crism.ro-v2.webp) ![versiunea 3](/img/crism.ro-v3.webp)