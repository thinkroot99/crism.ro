# Colecția de laptopuri ThinkPad

În 2021 m-a convins cineva - un amic, să cumpăr un laptop din seria ThinkPad indiferent de model sau de an.

M-am interesat eu pe internet despre aceste modele de laptopuri și mi sau părut interesante. Așa că am ajuns pe OLX și căutam un model ieftin.

Am dat peste un **ThinkPad T420** și un **ThinkPad T43**. Modelul T420 l-am cumpărat cu _450 de lei_, iar T43 l-am cumpărat cu _60 de lei_.

Cred că am făcut o afacere bună :) Primele cumpărături au fost în decembrie 2022.

> Geo (HardTek) master of Arch Linux :)) Dacă vezi acest articol să ști că tu ești de vină :) Păcat că nu ma faci clipuri video.

## ThinkPad T43 și ~~ThinkPad T420~~

Acestea au fost primele ThinkPad-uri cumpărate și am fost mulțumit de ele. T420 nu îl mai am pentru că l-am donat unui copil de 13 ani dintr-un sat din apropiere pentru că știam că își dorește foarte mult un laptop ThinkPad.

> ZMax dacă vezi acest articol îți recomand să înveți Linux

T43 încă îl m-ai am și merge ca un avion :), adică sună foarte tare - probabil are nevoie de o curățare de praf și o nouă pastă termoconductoare pe procesor.

Configurația celor două modele este următoarea:

```code
IBM ThinkPad T43

Sistem de operare: antiX 22 32 bit
Procesor: Intel Pentium M 1.86 GHz
Grafică: Intel Mobile 915GM
Memorie RAM: 1 GB
Stocare: HDD Hitachi 20 GB
Data de lansare: 2005

Galerie foto: https://pixelfed.de/i/web/post/477107463313182746
```

și

    Lenovo ThinkPad T420
    
    Sistem de operare: Arch Linux 64 bit
    Procesor: Intel Core i5 2540M 2.60 GHz
    Grafică: Intel HD Graphics 3000
    Memorie RAM: 6 GB
    Stocare: SSD Hitachi 320 GB & HDD Western Digital 1 TB
    Data de lansare: 2011
    
    Galerie foto: https://pixelfed.de/i/web/post/477107941070393376

## ~~ThinkPad E532~~, ThinkPad S230u Twist și ThinkPad T440p

În anul următor, 2022, am cumpărat alte trei modele și anume **E531**, **S230u Twist** și **T440p**.

Primul model nu este un model performant, este un model mai slab. Al doilea model este unul stil tabletă - are ecranul rotativ și se poate transforma în tabletă.

Dar T440p este un model destul de bun, chiar dacă a fost fabricat în urmă cu 11 ani (atunci a început producția seriei).

Configurația hardware la acestea trei este:

    Lenovo ThinkPad E531
    
    Sistem de operare: Windows 10 Home 64 bit
    Procesor: Intel Core i3 3110M 2.40 GHz
    Grafică: Intel HD Graphics 4000 & Nvidia GeForce GT 740M
    Memorie RAM: 4 GB
    Stocare: HDD Western Digital 500 GB
    Data de lansare: 2012
    
    Galerie foto: https://pixelfed.de/i/web/post/477108272273752099

și

    Lenovo ThinkPad S230u Twist
    
    Sistem de operare: Fedora 37 64 bit
    Procesor: Intel Core i7-3517U 1.90GHz
    Grafică: Intel HD Graphics 4000
    Memorie RAM: 8 GB
    Stocare: HDD Hitachi 320 GB
    Data de lansare: 2012
    
    Galerie foto: https://pixelfed.de/i/web/post/478938930181021570

și

    Lenovo ThinkPad T440p
    
    Sistem de operare: Windows 10 Home 64 bit
    Procesor: Intel Core i5 4300M 2.60 GHz
    Grafică: Intel HD Graphics 4600
    Memorie RAM: 8 GB
    Stocare: HDD Hitachi 500 GB
    Data de lansare: 2013
    
    Galerie foto: https://pixelfed.de/i/web/post/477108531798728749

Anul trecut în decembrie când am donat T420, am donat și modelul E532.

Am donat aceste modele pentru că am vrut să dau toate laptopurile și să rămân doar cu două laptopuri.

## Noul, vechiul T440p

În toată această poveste de cumpărături, am ajuns să am două modele de **T440p** - unul cu procesor i5 și celălalt cu procesor i7. Dar povestea despre cum am ajuns să am două astfel de modele este destul de interesantă și comică :)

În 2022 sau 2023 am desfăcu laptopul T440p i5 pentru al curăța de praf și a pune pastă termoconductoare la procesor. Toate bune și frumoase cu desfacerea lui, curățarea și schimbarea pastei termoconductoare a decurs foarte bine.

L-am asamblat frumos la loc și surpriza a fost că nu mai pornea de loc. L-am desfăcut din nou și m-am uitat pe la toate firele și cablurile să văd dacă sunt puse bine - totul în regulă, dar laptopul nu mergea. Inițial am crezut că s-a produs scurtcircuit și s-a ars ceva important.

Și astfel am mai stat câteva luni cu vechiul T420 până când în 2023 am cumpărat alt model de T440p dar cu un procesor și configurație hardware mai bună.

Iar după alte câteva luni am aflat de ce nu mai mergea primul T440p - acesta nu a mers pentru că s-a stricat touchpad-ul, așa că l-am desfăcut din nou - am deconectat touchpad-ul și laptopul a pornit fără alte probleme.

Acesta are configurația următoare:


    Lenovo ThinkPad T440p
    
    Sistem de operare: Arch Linux și Fedora 40 64 bit
    Procesor: Intel Core i7 4800MQ 2.70 GHz
    Grafică: Intel HD Graphics 4600
    Memorie RAM: 16 GB
    Stocare: SSD Intel 240 GB
    Data de lansare: 2013

Plus că are tastatură iluminată și ecranul mult mai bun decât cel cu procesor i5.

## ThinkPad R61 și ThinkPad T420s

Anul acesta am continuat seria de cumpărături. Practic în fiecare an am cumpărat câte un model de laptop sau mai multe.

Anul acesta, 2024, am cumpărat modelul **R61** și **T420s**. Pentru că am vrut să am din nou un model de T420, am ajuns să cumpăr modelul cu **s** în coadă, care este un pic mai slab decât T420.

Din păcate laptopul ThinkPad R61 este parola din BIOS și nu mai am acces la sistemul de operare sau BIOS. A mers o zi sau două, de am apucat să schimb Windows-ul cu distribuția antiX 22.

Iar la T420s am schimbat Windows 7 cu Arch Linux, care în momentul scrieri acestui articol este doar o interfață neagră (TTY). Nu am instalat nici o interfață grafică sau program.

La aceste modele nu știu configurația, dar o să actualizez articolul după ce mă interesez de configurația hardware a lor.

## Listă laptopuri

| Model                     | Sistem de operare                   | Procesor                     | Grafică                           | Memorie RAM | Stocare                                   | Data de lansare | Anul cumpărării |
|---------------------------|-------------------------------------|------------------------------|-----------------------------------|--------------|-------------------------------------------|-----------------|-----------------|
| IBM ThinkPad T43          | antiX 22 32 bit                     | Intel Pentium M 1.86 GHz     | Intel Mobile 915GM                | 1 GB         | HDD Hitachi 20 GB                         | 2005            | 2021            |
| IBM ThinkPad R61       | antiX 22 32 bit             | Intel Core 2 Duo T8100 2.10 GHz | Nvidia Quadro NVS 140M        | 2 GB         | HDD 160 GB                                | 2008            | 2024            |
| Lenovo ThinkPad T420 (**donat în 2023**) | Arch Linux 64 bit                 | Intel Core i5 2540M 2.60 GHz | Intel HD Graphics 3000            | 6 GB         | SSD Hitachi 320 GB & HDD Western Digital 1 TB | 2011            | 2021            |
| Lenovo ThinkPad T420s     | Arch Linux 64 bit       | Intel Core i5 2520M 2.50 GHz | Intel HD Graphics 3000            | 4 GB         | SSD 160 GB                                | 2011            | 2024            |
| Lenovo ThinkPad E531 (**donat în 2023**)  | Windows 10 Home 64 bit              | Intel Core i3 3110M 2.40 GHz | Intel HD Graphics 4000 & Nvidia GeForce GT 740M | 4 GB         | HDD Western Digital 500 GB               | 2012            | 2022            |
| Lenovo ThinkPad S230u Twist | Fedora 37 64 bit                  | Intel Core i7-3517U 1.90 GHz | Intel HD Graphics 4000            | 8 GB         | HDD Hitachi 320 GB                        | 2012            | 2022            |
| Lenovo ThinkPad T440p     | Windows 10 Home 64 bit              | Intel Core i5 4300M 2.60 GHz | Intel HD Graphics 4600            | 8 GB         | HDD Hitachi 500 GB                        | 2013            | 2022            |
| Lenovo ThinkPad T440p     | Arch Linux și Fedora 40 64 bit      | Intel Core i7 4800MQ 2.70 GHz | Intel HD Graphics 4600           | 16 GB        | SSD Intel 240 GB                          | 2013            | 2023            |

## De final

Am o colecție de **6 laptopuri** din seria **ThinkPad** produse de **IBM** și **Lenovo**. Laptopuri care arată mai mult sau mai puțin bine pentru că nu toate au fost întreținute cum trebuie, din păcate.

Nu am cheltuit foarte mulți bani pe ele. Cel mai ieftin a fost 60 de lei și cel mai scump a fost 900 de lei.

Din păcate nu am avut timp să mă ocup de aceste laptopuri. Cum am scris mai sus nu toate au fost întreținute bine, unele mai trebuie curățate pe dinafară, pe dinăuntru toate trebuie curățate, la unele mai trebuie schimbate anumite piese sau trebuie îmbunătățite.

Sper doar ca într-o zi să mă apuc de ele. Când voi face acest lucru cu siguranță o să scriu despre cea ce fac la ele