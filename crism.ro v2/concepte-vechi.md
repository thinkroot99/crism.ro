% Concepte care sunt mai vechi decât crezi
% ThinkRoot99

Deși GNU/Linux este considerat de obicei un sistem de operare modern, unele dintre ideiile pe care le conține sunt mult mai vechi decât ați putea crede.

Mai jos vă prezint câteva aspecte ale GNU/Linux care au o istorie îndelungată.

# Dual-Booting (pornirea în sistem dublu)

Utilizarea GNU/Linux în dubla pornire a fost mult timp modalitatea standard de a partaja GNU/Linux cu diferite sisteme de operare, inclusiv Windows.

Conceput de sisteme de operare cu mai multe tipuri de pornire a fost prezent cam de când există calculatoarele.

Într-un context asemănător Unix, a fost de asemenea, populară pe sistemele PC Unix mai vechi, cum ar fi Xenix, pentru a rula atât Unix, cât și MS-DOS. Pornirea dublă este, de asemenea, mai veche decât GNU/Linux pe Amiga.

Commodore Amiga 3000UX a fost livrat cu Amiga Unix, o versiune personalizată a System V și sistemul de operare standard Amiga Workbech. Puteai selecta sistemul de operare în momentul pornirii folosind butoanele stânga sau dreapta ale mouse-ului.

> [Amiga 3000UX - dual booting Amix (Amiga Unix) and AmigaOS.](https://www.youtube.com/watch?v=oSg3A4KQ6uA)

# Suport pentru diferite arhitecturi

Deși Linus Torvalds însuși nu a prevăzut că nucleul Linux se va răspândi și pe alte arhitecturi dincolo de platforma Intel x86 atunic când a anunțat lansarea sa pe Usenet în 1991, probabil că ar fi trebuit să o facă, având în vedere precedentul istoric.

La fel ca Unix-ul original, Linux este scris în limbajaul C. C în sine este un limbaj portabil. Programele de tip C, atât timp cât nu fac nicio presupunere cu privire la mediul de bază, pot fi compilate pe orice calculator care are un compilator scris pentru el.

La fel ca multe sisteme de operare al vremii, Unix a fost scris inițial în assembler, dar Dennis Ritchie l-a rescris la începutul anilor ’70 folosind limbajul C, inventat de el.

Un efect secundar al acestui lucru a fost că sistemul de operare a fost separat de hardware, iar Unix a devenit un sistem de operare universal.

Acest lucru era neobișnuit la acea vreme, deoarece sistemele de operare erau legat de o anumită mașină. Acesta este unul dintre motivele pentru care Unix a avut un asemenea succes în mediul academic de informatică în anii ’70 și ’80.

# Conceptul de shell-uri diferite

Bourne Again shell (Bash) este un shell predefini pe sistemele GNU/Linux, dar îl puteți schimba cu ușurință cu oricare dintre shell-urile de autentificare.

Probabil că știți că aceasta a fost o caracteristică a sistemului Unix original, dar știați că această idee este mai veche decât Unix?

Proiectul Multics a fost pionierul ideii de shell-uri interschimbabile. Bell Labs a fost una dintre entitățile care au participat la proiectul de construire a unui sistem de operare pentru partajarea fiabilă a timpului de lucru.

Conceptul era de a construi o facilitate pentru „*calculul utilitar*”, pe care să o poți folosi la fel ca apa sau electricitate. Conceptul era similar cu cel al calculului în cloud din prezent.

Din nefericire, Multics a fost un Windows Vista al vremii: ambițios, dar prea complicat, cu întârzieri și cu un buget exagerat.

Bell Labs s-a retras, lăsând doi cercetători, Dennis Ritchie și Ken Thompson, să tânjească după un mediu de programare bun. Unul dintre primele lucruri pe care le-au implementat în sistemul lor Unix a fost shell-urile înlocuibile.

Așa cum Unix a separat sistemul de operare de hardware-ul de bază, shell-ul a separat interfața cu utilizatorul de sistemul de operare de sub el.

Acest tip de flexibilitate a făcut ca sistemele de tip Unix să fie îndrăgite de programatori și tehnicieni încă de atunci.

# Mediile asemănătoare lui WSL

Windows Subsystem for Linux (WSL) vă permite să rulați aplicații native de GNU/Linux pe Windows 10 sau 11. Dar știați că o ideii similară ar fi putut fi implementată de Digital Equipment Corporation în 1988?

DEC Dezvolta un sistem de operare numit MICA care urma să ruleze pe o nouă arhitectură de procesor numită PRISM. Acesta urma să se bazeze pe popularul sistem de operare pentru minicalculatoare al DEC, VMS, dar urma să aibă și o personnalitate Unix.

Acest proiect ambițios a fost conceput de Dave Cutler. În cele din urmă, DEC a anulat MICA, iar Cutler s-a mutat la Microsoft, unde a condus ceea ce a devenit în cele din urmă Windows NT.

Pe partea de VMS, a existat un program numit Eunice, care rula și programe Unix. La fel ca WSL-ul inițial, a funcționat, dar s-a remarcat prin probleme de performanță și compatibilitate în comparație cu Unix-ul nativ.

Când Windows NT a apărut în cele din urmă ân 1993, acesta avea un mediu POSIX, dar părea să fie acolo doar pentru ca Microsoft să poată spune că este compatibil cu POSIX și să poată participa la anumite contracte cu Guvernul Federal al Statelor Unite.

De asemenea, Microsoft avea să lanseze un mediu mai complet, Windows Service for Unix, și a apărut și proiectul open source Cygwin.

# Aspecte juridice

În anii 2000, procesul intentat împotriva IBM de către SCO a fost discutat pe larg în rândul susținătorilor GNU/Linux și a programelor open source. SCO susținea că GNU/Linux a încălcat drepturile sale asupra codului original Unix, pe care îl achiziționase.

Deși IBM și comunitatea GNU/Linux au avut în cele din urmă câștig de cauză, situația a avut un precedent și în epoca Unix. Unix System Laboratories (USL) al AT&T a revendicat drepturile de autor asupra codului Berkeley Software Distribution, ceea ce a pus un adevărat blocaj asupra acestuia din urmă la începutul anilor ’90.

Deși în cele din urmă s-a deovedit că doar câteva fișiere erau „*afectate*” și puteau fi rescrise cu ușurință pentru a permite distribuția open source, GNU/Linux a devenit preferat entuziaștilor în domeniul informaticii.

# Competiția dintre arome (inter-flavor)

Deși comunității GNU/Linux îi place să dezbată care distribuție este mai bună, acest lucru nu este o noutate în cultura Unix.

În anii ’80, marea dezbatere a fost între System V al AT&T și BSD. Acesta din urmă era mai popular în lumea academică, fiind dezvoltat la UC Berkeley. Era, de asemenea, o componentă majoră a Unix pe stațiile de lucru, precum cele de la Sun Microsystems.

Spre sfârșitul anilor 1980, lumea Unix a intrat în ceea ce se numește „*Războiul Unix*”. AT&T și Sun au început să lucreze împreună la o fuziune între BSD și System V, iar acest lucru a alarmat alte companii de calculatoare precum HP, DEC și IBM.

Ultimele companii au format Open Software Foundation, în timp ce Sunt și AT&T au format Unix Interational.

„*Războiul*” s-a încheiat în cele din urmă printr-o încetare a luptei. Ambele organizații au fuzionat, dar GNU/Linux avea să înlocuiască în cele din urmă Unix-ul proprietar în majoritatea aplicațiilor.

# „*Anul calculatorului de birou (Unix)*”

Distribuțiile GNU/Linux au fost cunoscute pentru interfețele de utilizare a calculatorului de birou, încercând să facă GNU/Linux acceptabil pentru utilizatorii fără cunoștințe tehnice.

Eforturile au, de asemenea, o istorie îndelungată, după cum se vede într-un episod din 1989 al emisiunii PBS, „*The Computer Chronicles*”.

Aici, vedem oferte de la Sun Microsystems, HP și chiar Apple. Apple a avut, de asemenea, un sistem de operare bazat pe Unix sub forma A/UX.

> [Computer Chronicles - 06x18 - UNIX (1989)](https://www.youtube.com/watch?v=lkyyAKTvmx0)

# Programe open source

Deși GNU/Linux a popularizat conceptul de programe cu sursă deschisă, aceasta este o altă ideie care există de mult timp. Este posibl să fie la fel de veche ca și calculatoarele în sine.

În timp ce proiectul GNU este creditat pentru că i-a conferit o etică explicită sub forma programelor libere, programele erau deja transmise în mod liber în cercurile academice.

Dezvoltatorii BSD și-au creat propria licență care permitea, de asemenea, distribuirea în mod gratuit.
