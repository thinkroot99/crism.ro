% Conectarea la un server FTP cu File Explorer în Windows 10
% ThinkRoot99

Tot timpul am folosit FileZilla pentru a mă conecta prin FTP la server-ul unde am sit-ul găzduit. Și într-o zi am fost curios să văd dacă se poate face acest lucru nativ din Windows - pentru că nu degeaba vine sistemul cu atâtea aplicații și funcții.

# Cum se poate acces un server FTP cu File Explorer

Managerul de fișier - cunoscut sub numele de File Explorer în Windows 10 și 11 și Windows Explorer în Windows 7 - permite conectarea la servere FTP.

Pentru conectarea la un server FTP se deschide managerul de fișiere cu un clic pe `Acest PC / This PC`. Se face clic dreapta în panoul din dreapta și se selectează `Adăugare locație în rețea / Add a network locations`.

> ![Adăugare locație în rețea](img/add-a-network-location.png)

Se accesează expertul care apare și se selectează `Se alege o locație particularizată din rețea / Choose a custom network location`.

> ![Se alege o locație particularizată în rețea](img/choose-a-custom-network-location.png)

În caseta de dialog `Specificați locația sit-ului web / Specify the location of your website`, introduceți adresa server-ului FTP sub forma: `ftp://server.ro`

De exemplu, server-ul FTP al Microsoft este ftp.microsoft.com, așa că trebuie să introduceți adresa ftp://ftp.microsoft.com ca să vă conectați la respectivul server.

> ![Specificați locația sit-ului web](img/specify-the-location-of-your-website.png)

Dacă nu știți numele de utilizator și parola, puteți să folosiți caseta de dialog `Conectați-vă anonim / Log on anonymously` și astfel vă puteți conecta la server. Dar acest lucru vă oferă acces limitat - în general puteți descărca fișiere disponibile public, dar nu puteți încărca fișiere, de exemplu.

Dacă dețineți numele de utilizator și parola, introducețile numai după ce ați bifat căsuța `Conectați-vă anonim / Log on anonymously`. Prima dată când vă conectați la server-ul FTP, vi se va solicita să introduceți parola.

> ![Conectați-vă anonim](img/log-on-anonymously.png){ width=45% }
> ![Conectați-vă](img/log-on.png){ width=45% }

Acum vi se cere să introduceți un nume pentru locația rețelei. Aici puteți introduce orice nume doriți - server-ul FTP va apărea cu acest nume, astfel încât să vă puteți aminti cu ușurință care este.

> ![Introduceți un nume pentru această locație de rețea ](img/type-a-name-for-this-network-location.png)

După ce ați terminat, server-ul FTP va apărea sub `Locații în rețea / Network locations` în panoul managerului de fișiere.

Descărcarea și încărcarea fișierelor se face prin copierea și lipirea acestora așa cum se face implicit în orice director din Windows.

>![Locații în rețea](img/network-locations.png)
