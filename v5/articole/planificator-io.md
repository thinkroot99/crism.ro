# Optimizarea SSD-urilor în Linux

Dacă tot m-am ocupat de [optimizarea energiei pentru baterie](baterie.html), am zis să trec și la optimizarea SSD-urilor pentru că de ce să nu fie și acestea gestionate corespunzător de sistem.

## Activarea fstrim

Așa că am trecut la treabă și pentru început am rulat comanda:

```bash
sudo systemctl enable fstrim.timer
```

Treaba comenzi de mai sus este să activeze **fstrim** care se ocupă de performanțele și sănătatea SSD-urilor, adică le prelungește viața de funcționare.

## Planificator I/O

După ce am terminat rapid cu fstrim, am trecut la setarea planificatorului I/O-ului (Input/Output Scheduler) și am rulat comanda următoare:

```bash
echo mq-deadline | sudo tee /sys/block/sda/queue/scheduler
```

SSD-ul în cazul meu este `sda`, la voi poate fi *sdb* sau *sdc* - asta în caz că aveți și un HDD în calculator/laptop.
De fapt am fost nevoit să rulez comanda de mai sus de două ori, pentru `sda` și pentru `sdb`.

## Editarea GRUB-ului

Am trecut mai departe la editarea fișierului `grub` care se găsește la `/etc/default/grub`.

```bash
sudo nano /etc/default/grub
```

De data aceasta am folosit editorul de text **nano** și după deschiderea fișierului am căutat linia `GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet"` și am modificat-o să arate astfel:

```bash
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet elevator=mq-deadline"
```

Am salvat fișierul folosind combinațiile de taste: `Ctrl+o` pentru salvarea și `Ctrl+x` pentru a ieșit din fișier - combinațiile de taste sunt mai ok ca la vim :)

Dar ca să se aplice modificarea am rulat și comanda `sudo grub-mkconfig -o /boot/grub/grub.cfg`.

## Modificare valoare swappiness

Ultimul lucru de care m-am ocupat a fost să modific valoarea la **swappiness**. Swappiness este un parametru în nucleul Linux care controlează cât de activ este sistemul de operare în utilizarea memoriei Swap.

Pentru acest lucru a trebuit să verific care este valoare swappiness-ului și acest lucru l-am făcut cu ajutorul comenzii: `cat /proc/sys/vm/swappiness`.

```bash
cat /proc/sys/vm/swappiness
───────┬──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
       │ File: /proc/sys/vm/swappiness
───────┼──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
   1   │ 60
───────┴──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
```

Valoare swappiness este `60` cea ce nu este bine și trebuie modificată la `10` rulând comanda: 

```bash
sudo nano /etc/sysctl.conf
```

După ce s-a deschis fișierul am adăugat linia `vm.swappiness=10` - nu vă speriați dacă fișierul este gol, este în regulă. Pe urmă am rulat comanda de mai jos pentru a se aplica setările.

```bash
sudo sysctl -p
```

## Editarea fișierul fstab

Și pe când am crezut că am scăpat cu setările pentru optimizarea SSD-urilor, a trebuie să mai fac un singur lucru și anume să editez fișierul `/etc/fstab` pentru a schimba opțiunea `realtime` cu opțiunea `noatime`.

Pentru a edita fișierul `fstab` a trebuit să rulez comanda `sudo nvim /etc/fstab`, am căutat opțiunea `realtime` și am schimbat-o cu `noatime`.

De exemplu una dintre liniile din fișierul `fstab` arată astfel:

    rw,relatime,ssd,discard=async,space_cache=v2,subvolid=257,subvol=/@home 0 0

și am modificat-o astfel:

    rw,noatime,ssd,discard=async,space_cache=v2,subvolid=257,subvol=/@home 0 0

Și așa am făcut cu toate liniile unde a apărut `realtime`.

## De final

După ce am terminat toate setările necesare pentru optimizarea și prelungirea duratei de viața a SSD-urilor, am repornit laptop-ul pentru a mă asigura că toate setările se aplică cum trebuie și pentru ca sistemul de operare să se folosească de setările de mai sus așa cum trebuie.
