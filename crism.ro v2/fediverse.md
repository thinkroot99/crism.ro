% Ce este Fediverse
% ThinkRoot99

În condițiile în care oamenii sunt din ce în ce mai conștienți de puterea pe care o mână de companii au acumulat-o prin natura centralizată a web-ului modern, mulți se îndreaptă acum către o alternativă descentralizată: Fediverse.

Vă sună cunoscut acest nume? Pentru majoritatea oamenilor, probabil că nu. Dar există șanse tot mai mari să fi interacționat cu el.

Fediverse ar putea fi modalitatea prin care vă puteți exercita controlul asupra datelor dumneavoasdtră și a vieții voastre online.

# Ce este Fediverse?

Fediverse este o rețea de servere interconectate utilizate pentru social media, blogging, găzduirea de fișiere și alte activități web moderne. Numele este o combinație din cuvintele *federation* și *universe*.

Luați în considerare modul în care un guvern poate fi centralizat, cu puterea concentrată într-o locație centrală, sau federat, cu puterea răspândită în mai multe state sau localități. Atunci când proiectăm sau folosim un serviciu web, ne confruntăm cu aceeași întrebare.

Majoritatea serviciilor web populare sunt centralizate. Companiile precum Twitter sau Netflix stochează toate datele pe propriile servere (sau pe serverele pe care le închiriază de la o altă companie, cum ar fi Amazon), având control exclusiv asupra acestor date, a celor care le pot accesa și a modului în care le pot accesa.

Fediverse este federat. Mastodon, de exemplu, este o alternativă la Twitter, unde vă puteți stoca contul pe orice număr de servere sau vă puteți găzdui propriul cont.

Indifernet de serverul pe care îl utilizați, toți utilizatorii Mastodon pot interacționa ca și cum ar fi conectați la același server.

> ![Mastodon](img/Mastodon.webp)

Abordarea federativă nu este, de fapt, o idee nouă. Este de fapt o parte esențială a ceea ce face ca internetul să fie internet.

# Cum funcționează Fediverse

Internetul în sine nu este un lucru centralizat. Este o rețea interconectată de mașini. Unele calculatoare stochează date, cum ar fi site-urile, iar alte calculatoare solicită accesul la aceste date, cum ar fi telefonul sau laptopul vostru.

Acest lucru se poate întâmpla prin diverse protocoale și standarde deschise care permit acestor mașini să se înțeleagă între ele. Deoarece aceste standarde sunt deschise și interoperabile, vă puteți conecta la un server de pe orice calculator și puteți vizualiza un site de pe orice navigator.

Dar aceste site-uri și servere tind să funcționeze ca niște silozuri individuale. Un site este găzduit pe acest server. Un alt site există pe un server separat. Gradul de interacțiune tinde să se oprească la crearea de hyperlink-uri între ele.

În Fediverse, o experiență precum PixelFed (o alternativă la Instagram) nu există ca un singur site. În schimb, PixelFed este de fapt un cod interoperabil implementat pe mai multe site-uri.

Un grup dedicat ficțiunii fanilor Pokemon poate găzdui un server cu propria instanță PixelFed, angajatorul tău poate configura una pentru angajați și clienți sau tu și prietenii tăi puteți conveni să găzduiți una împreună.

> ![PixelFed](img/PixelFed.webp)

Fiecare dintre aceste site-uri PixelFed sau *instanțe* arată și se simt în mare parte la fel și pot comunica între ele.

Acest lucru este foarte asemănător cu modul în care funcționează e-mail-ul. Adresele de e-mail au un nume de utilizator și o adresă de domeniu (cum ar fi @gmail.com sau @protonmail.com), astfel încât clientul de e-mail să știe la ce server să trimită e-mail-ul.

Cu această abordare, tu, ca utilizator final, ai o experiență care te face să te simți ca și cum te-ai conecta la Twitter, dar nu există o singură organizație care să controleze întregul ecosistem.

# Ce este în neregulă cu centralizarea?

Poate că internetul are o fundație deschisă și descentralizată, dar majoritatea site-urilor și serviciilor cu care mulți dintre noi interacționăm zilnic sunt grădini centralizate și îngrădite.

Discord are control total asupra modului în care folosim Discord și nu puteți trimite un mesaj de pe Discord pe WhatsApp, Telegram sau Signal, chiar dacă toate aceste aplicații au o funcție oarecum similară.

Folosirea unui site precum Medium este mai ușoară decât codificarea propriei pagini web în HTML, dar Medium poate utiliza și monetiza conținutul de pe platforma sa în orice mod dorește, atât timp cât respectă propriile condiții de utilizare.

Medium poate schimba acești termeni în orice moment, iar majoritatea utilizatorilor nu ar ști, deoarece oricum nu citesc niciodată termenii.

Pe un site tradițional, puteți face clic dreapta pe o imagine sau pe un videoclip și îl puteți descărca pe calculator. Site-uri precum YouTube și Hulu blochează această funcție încorporată în navigatorul vostru și o consideră piraterie.

Îți poți construi viața profesională în jurul audienței pe care ți-o dezvolți pe o platformă de socializare, apoi îți poți vedea afacerea cum dispare atunci când contul tău de socializare este blocat.

Cu toții auzim despre cazuri de profil înalt în care cineva a fost blocat pentru că a spus ceva incendiar, dar, de multe ori, oamenii își găsesc conturile blocate din motive banale sau din cauza unor probleme tehnice, fără prea multe căi de atac pentru a reveni la conturile lor.

Pe Facebook, uni utilizatori au recurs la cumpărarea de căști Oculus ca o soluție costisitoare pentru ca Facebook să le reseteze contul.

Toate aceste exemple arată ce se întâmplă atunci când serviciile sunt centralizate în mâinile unor entități corporative.

Nu numai că aceste companii au un cuvânt greu de spus în ceea ce privește modul de funcționare al internetului, dar ele limitează ceea ce poate face tehnologia ori de câte ori funcționalitatea completă le-ar putea pune în pericol profiturile.

# Cum se comportă descentralizarea?

> ![ActivityPub](img/ActivityPub.webp)

Natura deschisă și interoperabilă a Fediverse înseamnă că există modalități de utilizare a unui proiect Fediverse pe care nu le puteți utiliza în cazul unui proiect non-federat.

Un astfel de beneficiu este acela că puteți integra cu ușurință un proiect Fediverse cu un altul. De exemplu, puteți să comentați un videoclip pe Mastodon și să îl afișați ca un comentariu la același videoclip pe PeerTube.

Dacă doriți un spațiu singur online pentru a discuta cu persoane care împărtășesc același idei despre un subiect, puteți cultiva acel spațiu fără a fi nevoit să blocați persoanele antagoniste de pe întreaga platformă.

Pentru a reveni la exemplul nostru anterior, grupul Pokemon își poate stabili propriul cod de conduită personal, care va fi diferit de cel al angajatorului, care va fi diferit de cel dintre tine și prietenii tăi.

Poți interzice cuiva accesul la instanța personală PixelFed, dar nu poți împiedica pe cineva să folosească PixelFed în sine.

Dacă sunteți persoana care a fost blocată, nimic nu vă împiedică să vă creați propria instanță sau să vă alăturați unui alt server și să continuați să utilizați serviciul în altă parte.

Până la urmă, datele vă aparțin, cu atât mai mult cu cât sunteți proprietar al acestsora, cu cât sunteți autohton. Și nu numai atât, ci și codul.

Proiectele Fediverse sunt gratuite și cu sursă deschisă. Puteți să audiați programul și să aveți mai multă încredere că acesta nu face ceva dubios în fundal. Acest lucru reduce probabilitatea ca cineva să colecteze, să adune și să analizeze tot ceea ce spuneți și faceți.

# Proiecte Fediverse pe care le puteți folosi

Fediverse nu este încă un termen familiar, dar există o serie de site-uri și servicii mature la care vă puteți înscrie sau pe care le puteți utiliza astăzi.

Iată câteva dintre ele și serviciile pe care le puteți vedea ca alternative la acestea:

- [Diaspora*](https://diasporafoundation.org/) (Facebook)
- [Mastodon](https://joinmastodon.org/) (Twitter)
- [Matrix](https://matrix.org/) (Slack, Discord)
- [Nextcloud](https://nextcloud.com/) (Dropbox, Google Docs, Google Calendar și multe altele)
- [PeerTube](https://joinpeertube.org/) (YouTube)
- [PixelFed](https://pixelfed.org/) (Instagram)
- [WordPress](https://wordpress.org/) (Blogger, Squarespace)
- [Write Freely](https://writefreely.org/) (Medium)

Această listă este departe de a fi completă. Puteți găsi o listă de proiecte pe [the-federation.info](https://the-federation.info/) sau [fediverse.party](https://fediverse.party/). S-ar putea să constatați că lista este mai lungă decât vă așteptați.

# De final

WordPress este de departe cel mai popular și utilizat proiect Fediverse. Cei mai mulți dintre noi nu se gândesc la el ca la un Fediverse special, un lucru federat.

Este pur și simplu WordPress, un instrument gratuit de blogging sau un CMS util pentru construirea unui site. Dar natura federată a WordPress este o mare parte din motivul pentru care a devenit o parte atât de dominantă a web-ului.

Este un exemplu de cum se poate arăta succesul pentru Fediverse.

Mastodon și Matrix sunt probabil următoarele proiecte Fediverse cele mai cunoscute. Ambele sunt mature și competente, dar, ca platforme sociale, problema nu este dacă funcționează sau nu.

Problema este dacă persoanele cu care doriți să comunicați sunt prezente pe aceste rețele, iar aceasta este o provocare greu de depășit.

Aceasta este provocarea fundamentală cu care se confruntă Fediverse. Fediverse oferă o soluție tehnologică capabilă să rezolve multe dintre problemele actuale ale internetului, dar o vor accepta oamenii?
