% Lucruri de făcut dupa instalarea i3wm
% ThinkRoot99

Dacă apreciați minimalismul și căutați să vă accelerați fluxul de lucru, cu siguranță ar trebui să treceți la un manager de ferestre. Cu configurația și ajustările potrivite, un manager de ferestre vă poate crește exponențial productivitatea, adăugând în același timp o notă estetică calculatorului.

i3wm sau i3 Windows Manager este o alegere populară atât printre începători cât și printre veterani. Este ușor, extrem de personalizabil, puternic și se potrivește perfect pentru aproape oricine se aventurează în lumea GNU/Linux.

Acest articol abordează toate sarcinile necesare post-instalare de care ar trebui să vă ocupați după instalarea i3wm pentru a facilita migrarea dintr-un mediul de lucru tradițional.

# 1. Setare tastă principală

Când porniți într-o sesiune nouă de i3wm, vă va solicita să setați combinația implicită a tastelor pentru funcția `Super`. Este o combinație crucială pe care o veți folosi pentru totdeauna în i3wm sau orice manager de ferestre.

În mod implicit, i3wm vă oferă opțiunea de a alege tasta `Win` sau `Alt` ca `Super`. Este o practică acceptată să mapați prima opțiune ca `Super`, deși sunteți întodeauna liber să alegeți orice tastă doriți.

# 2. Setare imagine de fundal

Dacă ați instalat i3wm de la zero, sunt șanse mari să aveți o imagine neagră pe fundal. Deși nu este cea mai atrăgătoare la prima impresie, dar nu vă îngrijorați, pentru că este nevoie doar de câteva comenzi pentru a schimba imaginea de fundal.

Pentru a vă schimba imaginea de fundal în i3wm, trebuie sâ instalați un manager de imagini de fundal. Nitrogen este un manager de imagini de fundal popular care vă permite să gestionați și să setați imagini de fundal.

Instalați Nitrogen pe sistemul vostru utilizând comanda următoare.

    sudo pacman -S nitrogent

Pentru a seta o imagine de fundal, activați Nitrogen și navigați la directorul în care sunt stocate imaginile de fundal. Nitrogen ar trebui să preia automat imaginile din directorul selectat.

În plus, adăugați linia de mai jos la fișierul de configurare al i3wm pentru a porni automat Nitrogen și a restabili imaginea de fundal de fiecare dată când vă deconectați sau reîmprospătați i3wm.

    exec always nitrogen --restore

# 3. Instalați și configurați compozitorul Picom

> ![Picom în i3wm pe Arch Linux](img/i3-personalizat.png)

i3wm, în mod implicit, nu este livrat cu un compozitor. Deci, dacă doriți să adăugați blur, transparență sau orice alte efecte vizuale, după cum puteți vedea și în subreddit-ul [r/unixporn](https://www.reddit.com/r/unixporn/), va trebui să instalați un compozitor separat.

Compton obișnuia să fie compozitorul de bază pentru utilizatorii i3wm. Dar recent, picom, o furcă a Compton, a preluat controlul. Instalați compozitorul picom, configuraț-il pentru pornirea automată așă cum ați făcut cu Nitrogen și veți fi gata de plecare.

Utilizarea unui compozitor va corecta, de asemenea, orice probleme de afișare pe care ați fi avut în i3wm.

## Instalare picom

Instalarea picom este destul de simplă, iar procesul este identic cu instalarea oricărui pachet. Pentru a instala picom, porniți terminalul și rulați următoarea comandă, în funcție de distribuția pe care o rulati.

    $ sudo pacman -S picom

## Configurare picom

Pentru a rula automat picom de îndată ce vă conectați la sesiunea de manager de ferestre i3, adăugați o linie la fișierul de configurare i3wm.

Tastați următoarea linie oriunde în fișierul de configurare și apoi reîncărcați i3wm cu combinația de taste: `Super+Shift+R`.

    exec picom

# 4. Schimbați terminalul

Fișierul de configurare i3wm este configurat să pornească i3-sensible-terminal ca terminal implicit. Deși nu este rău să vă mulțumiti cu ceea ce obțineți implicit, pierdeți în mod serios opțiuni mai bune care oferă caracteristici extinse și personalizare. Alacritty, Terminator, Kitty sunt câteva terminale mult mai bune.

Pentru a schimba emulatorul implicit de terminal în i3wm, editați următoarea linie din fișierul de configurare și înlocuiti „*i3-sensible-terminal*” cu terminalul preferat. De exemplu, pentru a seta Alacritty ca terminal implicit, puteți modifica lina astfel:

    bindsym $mod+Return exec alacritty

# 5. Instalați dmenu

Este posibil să fi observat deja că, spre deosebire de mediile de lucru precum GNOME sau Xfce, la apăsarea tastei Super nu apare nici un meniu de aplicații.

Pentru a rula o aplicație în i3wm, trebuie fie să o lansați prin terminal, fie să utilizați un lansator de aplicații. dmenu este o alegere solidă pentru același lucru și adesea vine preinstalat cu distribuții care oferă o variantă i3wm. Este rapid, ușor și ușor de personalizat.

Puteți instala dmenu în Arch Linux, deschideți un terminal și rulați următoarea comandă:

    $ sudo pacman -S dmenu

Odată instalat, porniți dmenu cu combinația de taste `Win+D` și căutați aplicația pe care doriți să o porniți.

# 6. Instalați o bară de stare

O bară de stare este o componentă opțională care se află în partea de jos sau în partea de sus a ecranului. Aceasta urmărește și afișează informații despre sistem, cum ar fi utilizarea CPU, utilizarea RAM, viteza internetului etc.

Puteți configura bara de stare pentru a afișa valori despre orice componentă hardware sau software a sistemului vostru. Dacă sunteți conștient de resursele voastre și preferați să urmăriți cu atenție performanța, cu siguranță ar trebui să instalați o bară de stare.

Opțiunile populare sunt i3status, Waybar și Polybar.

# 7. Organizați spațiile de lucru
În mod implicit, spațiile de lucru sunt aranjate cu numere de la 1 la 10. Puteți comuta între spațiile de lucru folosind `Super+X`, unde „**X**” este orice număr de la 1 la 0. Deși este în regulă așa cum este, abordarea optimă ar fi înlocuirea numerelor cu nume corespunzătoare conținutului fiecărui spațiu de lucru.

De exemplu, înlocuiți 1, 2, 3 cu Web, Cod, Media etc. Ați înțeles ideea! Pentru a obține acest format, accesați fișierul de cofigurare i3wm și introduceți sau editați următoarele rânduri:

    set $ws1 "1"
    set $ws2 "2"
    set $ws3 "3"
    set $ws4 "4"
    
    și
    
    bindsym $mod+1 $ws1
    bindsym $mod+2 $ws2
    bindsym $mod+3 $ws3
    bindsym $mod+4 $ws4

la

    set $term "1: term"
    set $web "2: web"
    set $file_manager "3: files"
    
    și
    
    bindsym $mod+1 $term
    bindsym $mod+2 $web
    bindsym $mod+3 $file_manager

Editați restul după cum preferați și reîmprospătați cu `Super+Shift+R`. Schimbările ar trebui să fie în vigoare.

# 8. Personalizați combinațiile de taste

Navigarea în managerii de ferestre este foarte centrată pe tastatură. Pentru a utiliza întreg potențial al unui manager de ferestre, trebuie să vă familiarizați cu combinațiile de taste, cel puțin pentru funcțiile elementare, dacă nu toate.

Dacă combinațiile de taste implicite nu vă convin, nu ezitați să editați fieșierul de configurare al i3wm și să setați propriile taste personalizate. Pentru a afla mai multe despre combinațiile tastelor și cum să le personalizați, consultați [documentația oficial i3wm](https://i3wm.org/docs/userguide.html#keybindings).

# 9. Faceți copie de rezervă

Probabil că cea mai importantă sarcină pe care o neglijează adesea începătorii este să facă copii de rezervă ale fișierelor care conțin configurația și personalizarea aplicațiilor. Dotfiles este cuvântul folosit pentru fișierele de cofigurare.

Este numit astfel deoarece toate fișierele de configurare sunt în general stocate în directoare ascunse, iar în GNU/Linux, fiecare nume de director ascuns începe cu un punct. De aici numele „*dot-files*”.

Dacă sunteți începător în folosirea i3wm sau orice alt manager de ferestre, este obligat să întâlniți defecte în timp ce experimentați.

Pentru a vă asigura că puteți seta rapid i3wm-ul la o stare funcțională de fiecare dată când se defectează, trebuie să mențineți o copie de rezervă a fișierelor sale de configurare.

O modalitate ideală de a face copii de rezervă pentru fișierele doftiles este să le încărcați într-un depozit GitHub.

Asta este tot ce puteți face pentru început, bineînțeles că se pot face mult mai multe. După ce vă obișnuiti cu i3wm, puteți să explorați mult mai adânc managerul de ferestre.
