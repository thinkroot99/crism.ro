# De ce să ai un jurnal

Am citit recent postarea lui Kev Quirk în care a scris: [de ce să ai un sit personal](https://kevquirk.com/blog/why-have-a-personal-site-instead-of-social-media).

Motivele pentru care se merită să ai un sit sau jurnal personal sunt multe și diferă de ma om la om. În cazul meu motivele pentru care îmi place să am propriul meu sit sau jurnal sunt:

- îmi place să scriu despre diverse lucruri.
- îmi place să ofer o parte din cunoștințele mele.
- îmi place că pot controla tot sit-ul și conținutul.
- conținutul este al meu.
- conținutul nu se pierde așa de ușor ca pe o rețea socială.

## Îmi place să scriu

Acesta nu este primul meu jurnal. Am început prin 2005 sau 2006 să scriu în jurnale (blog-uri), am avut jurnal pe tot felul de platforme de la WordPress, Blogger la platforme Românești care între timp au dispărut. Am scris despre diverse subiecte general la subiecte de nișă.

Nu am avut un jurnal stabil, tot timpul am schimbat platformele și numele acestor jurnal. Într-un fel îmi place că am tot experimentat, mi-ar fi plăcut să fiu mai stabil pe această parte.

## Îmi place să ofer cunoștințele mele

Am avut blog-uri de nișă despre tehnologie, mai ales despre Linux și Open Source. Încă mai am un astfel de jurnal pe care scriu despre tehologie, în mare parte despre Linux și Open Source. Mai scriu și pe acest sit despre tehnologie.

Nu îmi place să țin pentru mine tot ce știu, mai ales că aproape tot ce știu am luat din articole și tutoriale gratuite de pe internet, dar am învățat și de la alți mai pricepuți ca mine.

## Îmi place că pot controla totul

Un cont de pe o anumită rețea de socializare nu se poate controla, practic acel cont nu este al tău - tu, eu doar administrăm acel cont și în cel mai bun caz îl ștergem și nu va mai putea fi accesat public, el rămâne activ undeva pe un server.

Informațiile publicat pe rețelele de socialziare nu sunt ale noastre - citiți termenele și politica rețelelor de socializare și veți afla acest lucru. Toată informația publicat pe aceste rețele devine proprietarea firmei care administrează rețeaua.

Și atunci de ce informație publicată de mine să nu fie a mea?

Pe un sit personal, informația este a mea, este a ta. Nu o poate luat nimeni, nu o poate șterge nimeni, nu o poate manipula nimeni. Eu decid ce fac cu această informație.

## Conținutul este al meu

Am scris un pic mai sus despre acest subiect. Deci pe scurt toată informație este a mea și eu o pot controla.

## Conținnutul nu se pierde

Caută o informație de acum 5 ani pe o rețea de socialziare și vezi dacă o mai găsești și cât de ușor o găsești. Caută informația cu ajutorul unui motor de căutare. Știu, aproape că nu se mai găsește informația.

Informația se pierde foarte ușor când este publicată pe o rețeea de socialziare.

Dar atunci când ai sit sau blog, informația publicată nu se mai pierd aproape de loc. Și peste 5 sau 10 ani un articol se găsește foarte ușor chiar și cu ajutorul unui motor de căutare.

## De final

Acestea sunt motivele mele pentru care prefer un sit sau bog personal pentru a îmi publicat cunoștințele și tot ceea ce am de spus/scris. Pe scurt informația este a mea și o pot controla așa cum vrea eu.
