#!/bin/bash

# Definirea căilor sursă și destinație
SOURCE_DIR="/home/thinkroot/Proiect/crism.ro"
DEST_DIR="/home/thinkroot/MyGit/crism.ro/v5"

# Fișiere și directoare de exclus (separate prin spațiu)
EXCLUDE_LIST=("upftp.py" ".htaccess")

# Verifică dacă directorul sursă există
if [ ! -d "$SOURCE_DIR" ]; then
    echo "Source directory does not exist: $SOURCE_DIR"
    exit 1
fi

# Verifică dacă directorul destinație există, dacă nu, îl creează
if [ ! -d "$DEST_DIR" ]; then
    mkdir -p "$DEST_DIR"
fi

# Funcție pentru a verifica dacă un element este în lista de excludere
function is_excluded {
    local item=$1
    for exclude in "${EXCLUDE_LIST[@]}"; do
        if [[ "$item" == "$exclude" ]]; then
            return 0
        fi
    done
    return 1
}

# Copiază toate fișierele și directoarele din sursă în destinație, cu suprascriere, excluzând anumite fișiere/directoare
for item in "$SOURCE_DIR"/*; do
    basename_item=$(basename "$item")
    if ! is_excluded "$basename_item"; then
        cp -r "$item" "$DEST_DIR"
    fi
done

echo "All items from $SOURCE_DIR have been copied to $DEST_DIR with overwrite, excluding specified items."
