% Ce este TTY
% ThinkRoot99

Sunt sigur că toți utilizatori de GNU/Linux au auzit de **TTY**. Dar, ce este? Vă este util ca utilizator de mediu de lucru? Ai nevoie de el? Și, ce poți face cu acest `tty`?

Rețineti că nu există un răspuns definitiv, dar se referă la modul în care dispozitivele de intrare/ieșire au interacționat în trecut. Deci, va trebui să cunoașteți un pic de istorie pentru a obține o imagine clară.

# Istoricul din spatele termenului „*TTY*”

Totul începe cu un teleimprimator în anii 1930. Teleimprimatoarele vă permit să trimiteți/primiți mesaje text prin cablu. A fost înlocuitor al comunicării în cod Morse, în care erau necesari doi operatori pentru a comunica eficient unul cu celălalt.

Un teleimprimator avea nevoie doar de un singur operator pentru a transmite cu ușurință un mesaj. Deși nu avea o tastatură modernă, sistemul său a fost mai târziu dezvoltat de Donald Murray în 1901 pentru a include o tastatură asemănătoare mașinii de scris.

Codul Murray a redus efortul operatorilor de a trimite mesaje. Și, acest lucru a făcut posibil ca un teleprinter să evolueze ca teletypewriter comercial în 1908. **TTY** este prescurtarea pentru teletypewriter.

> ![Un teletype utilizat la Sydney Uni în 1968](img/a-teletype-in-use-at-Sydney-Uni-in-1968.jpg)

Diferența dintre teletypewriter și o mașină de scris obișnuită a fost că teletypewriter-ul a fost atașat la un dispozitiv de comunicare pentru a trimite mesajul.

[Teletypewriter a făcut posibil ca oamenii să comunice mai rapid](https://en.wikipedia.org/wiki/Teletype_Corporation#/media/File:What-is-teletype.jpg) printr-un fir fără calculatoare.

Și astfel a apărut **TTY**.

# Conceptul (relativ) modern

Acum, vă întrebați cum a ajuns în calculatorul moder și în GNU/Linux?

Pentru început, când teletypewriter-ele au apărut pe piață, câțiva ani mai târziu au fost dezvoltate tranzitoare semiconductoare care au evoluat în microprocesor făcând posibil un calculator. Calculatoarele inițiale nu aveau conceptul de tastatură. Cardurile perforate erau metoda de introducere.

> ![Card perforat pentru caclulatoare](img/punch-card-program.jpg)

În timp ce calculatoarele evoluau, cardurile de intrare în loturi au fost în cele din urmă înlocuite de telemașine ca dispozitiv convenabil de intrare/ieșire.

> ![Calculator LGP-30 din 1956 cu un TTY atașat](img/LGP-30-computer-1956.jpg)

Odată cu progresele tehnologice, teletypewriter-ele au fost „*virtualizate*” folosind electronice. Deci, nu veți avea nevoie de un TTY fizic, mecanic, ci de un TTY virtual, electronic.

Calculatoarele anterioare nici măcar nu aveau ecrane video. Lucrurile erau tipărite pe hârtie în loc să fie afișate pe un ecran (care nu exista). Și, prin urmare, vedeți termenul „*printare*”, nu „*afișare*”. Ecranele video au fost adăugate la terminal mai târziu, pe măsură ce tehnologia a avansat.

Cu alte cuvinte, s-ar putea să fi auzit de ele ca terminale video. Sau le puteți numi terminale „*fizice*”. Apoi acestea au evoluat în terminale emulate prin programe, care au venit cu abilități și caracteristici îmbunătățite.

Acesta este ceea ce numiți „*emulator de terminal*”. De exemplu, GNOME Terminal sau Konsole sunt unele dintre cele mai bune emulatoare de terminal pe care le veți găsi pe GNU/Linux.

# Ce este TTY

Când vine vorba de GNU/Linux, **TTY** este un dispozitiv abstract în UNIX și GNU/Linux. Uneori se referă la un dispozitiv de intrare fizic, cum ar fi un port serial, iar uneori se referă la un TTY virtual unde permite utilizatorilor să interacționeze cu sistemul ([referință](https://unix.stackexchange.com/questions/4126/what-is-the-exact-difference-between-a-terminal-a-shell-a-tty-and-a-con)).

TTY este un subsistem în GNU/Linux și Unix care face posibilă gestionarea proceselor, editarea liniilor și gestionarea sesunilor la nivel de nucleu prin drivere TTY. În ceea ce privește programarea, trebuie să cercetăm mai adânc. Dar, având în vedere domeniul de aplicare al acestui articol, aceasta ar putea fi o definiție ușor de digerat.

Dacă sunteți curioși, puteți explora o resursă veche ([TTY Demystified](https://www.linusakesson.net/programming/tty/index.php)) care încearcă să clarifice TTY în sistemele GNU/Linux și Unix cu toate detaliile tehnice de care aveți nevoie.

De fapt, ori de câte ori lansați un emulator de terminal sau utilizați orice fel de shell în sistemul vostru, acesta interacționează cu TTY-uri virtuale care sunt cunoscute ca pseudo-TTY sau PTY. Puteți să tastați TTY în emulatorul vostru de terminal pentru a găsi PTY-ul asociat.

# Cum se accesează TTY

> ![tty6 în Linux](img/tty6-in-linux.png)

Este ușor să accesați TTY în GNU/Linux. De fapt, când nu avem nici o idee despre ce este, l-am accesat din greșeală și am intrat un pic în panică cu privire la ce să fac și cum să ies din el.

Puteți obține ecranul TTY utilizând următoarele comenzi rapide de la tastatură pe majoritatea distribuțiilor:

- **Ctrl+Alt+F1** – blocare ecran.
- **Ctrl+Alt+F2** – mediu de lucru (desktop).
- **Ctrl+Alt+F3** – tty3
- **Ctrl+Alt+F4** – tty4
- **Ctrl+Alt+F5** – tty5
- **Ctrl+Alt+F6** – tty6

Puteți accesa până la 6 TTU-uri în total. Cu toate acestea, primele două comenzi rapide indică ecranul de blocare al distribuției și mediul de lucru. Cu restul veți obține o interfață de linie de comandă.

> ![tty4 în Ubuntu](img/tty4-in-ubuntu.jpg)

# Când ați folosit TTY

TTY nu este doar o comoară tehnică. Este util chiar și pentru utilizatori ca mine care nu sunt dezvoltatori. Ar trebui să fie util în cazul în care mediul grafic îngheață (se blochează). În unele cazuri, reinstalarea mediului de lucru (desktop) din TTY ajută la rezolvarea problemei.

Sau puteți alege, de asemenea, să efectuați sarcini, cum ar fi actualizarea sistemului GNU/Linux și altele asemenea. În cel mai rău caz, puteți accesa TTY-ul pentru a reporni calculatorul dacă interfața grafică nu răspunde. Unii utilizatori preferă să efectueze transferuri mari de fișiere cu ajutorul TTY.

# TTY ca o comandă

> ![comanda tty în terminal](img/terminal-tty-command.png)

Când tastați TTY în emulatorul de terminal, acesta va imprima numele fișierului terminalului conectat la intrarea standard. așa cum este descris în pagina de manual (comanda: man tty).

Cu alte cuvinte, pentru a afla numărul TTY la care sunteți conectat, trebuie doar să introduceți `tty`. Și dacă există mai mulți utilizatori conectați la mașina GNU/Linux de la distanță, puteți utiliza comanda `who` pentru a verifica la ce sunt conectați alți utilizatori.
