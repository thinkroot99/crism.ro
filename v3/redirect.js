// Așteaptă ca pagina să se încarce complet
window.onload = function() {
    // Obține URL-ul curent
    var path = window.location.pathname;
    // Verifică dacă URL-ul nu se termină cu '/'
    if (!path.endsWith('/')) {
        // Adaugă extensia .html la URL
        var newPath = path + '.html';
        // Navighează către noul URL
        window.location.href = newPath;
    }
}
