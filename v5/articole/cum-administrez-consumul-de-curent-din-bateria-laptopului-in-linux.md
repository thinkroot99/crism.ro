# Cum administrez consumul de curent din bateria laptopului în Linux

De obicei nu instalam nici o aplicație, pentru consumul curentului atunci când laptop-ul nu est conectat la priză, în distribuția Linux pe care o foloseam.

Multe dintre aceste distribuții au implicit o astfel de aplicație pentru gestionarea energiei bateriei.

Astăzi am decis să instalez aplicația **TLP** pentru **gestionarea energiei bateriei** pentru că am o baterie destul de veche la ThinkPad T440p și am zis să o mai protejez cât de cât.

Pe Arch Linux am rulat comanda `paru -S tlp tlp-rdw` pentru a instala **TLP**, dar se poate folosi și comanda `sudo pacman -S tlp tlp-rdw`.

După ce s-a instalat aplicația TLP, a trebuit să editez fișierul `tlp.conf` care se găsește la `/etc/tlp.conf`.

```bash
sudo nvim /etc/tlp.conf
```

Voi puteți folosi orice editor de text doriți.

În acest fișier am căutat liniile următoare:

```cod
#CPU_BOOST_ON_AC=1
#CPU_BOOST_ON_BAT=0
```

Am anulat comentariile și liniile de mai sus trebuie să arate astfel:

```cod
CPU_BOOST_ON_AC=1
CPU_BOOST_ON_BAT=0
```

Am salvat fișierul tastând următoarele taste: `ESC` după care `:WQ` și `enter` - știu, este complicat - dar asta este frumusețea :)

Mai departe a trebuie să activez serviciul `tlp.service` rulând urătoarele comenzii:

```bash
systemctl enable tlp.service
systemctl start tlp.service
```

Și ca să fiu sigur că a pornit programul **TLP** și se ocupă de **gestionarea energiei bateriei** am mai rulat și comanda: `sudo tlp start`.

De acum înainte nu mai am grija bateriei pentru că totul este bine.
