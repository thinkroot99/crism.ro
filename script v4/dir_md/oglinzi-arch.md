# Oglinzi rapid pentru Arch Linux

Citeam diferse sit-uri mici, scrise doar în HTML, așa cum erau cu ceva ani în urmă. Și dintr-un sit în sit, dintr-un articol în articol, am dat peste un articol interesant pentru Arch Linux.

Cum să detectezi ce oglindă/depozit este cel mai rapid și la acele oglinzi să se conecteze sistemul Arch pentru actualizare sau pentru instalarea programelor.

Pentru început trebuie instalată aplicația: rate-mirrors

`paru -S rate-mirrors`

După care trebiue rulată comanda următoare:

`rate-mirrors --disable-comments-in-file --entry-country=RO --protocol=https arch --max-delay 7200 | sudo tee /etc/pacman.d/mirrorlist`

Această comandă ar trebui să actualizeze lista `mirrorlist` cu cele mai rapide oglinzi din regiune.