% Monitorizarea activități Ethernet cu Arpwatch
% ThinkRoot99

Arpwatch este un [program informatic open source](https://en.wikipedia.org/wiki/Arpwatch) care vă ajută să monitorizați activitatea de trafic Ethernet (cum ar fi schimbarea adreselor IP și MAC) în rețeaua dumneavoastră și menține o bază de date cu perechile de adrese `ethernet/ip`.

Acesta generează un jurnal al asocierii înregistrate a informațiilor privind adresele IP și MAC, împreună cu un marcaj temporal, astfel încât să puteți urmări cu atenție momentul în care activitatea de asociere a apărut în rețea.

De asemenea, are opțiunea de a trimite rapoarte prin e-mail unui administrator de rețea atunci când o asociere este adăugată sau modificată.

Instrumentul Arpwatch este deosebit de util pentru administratorii de rețea care doresc să supravegheze activitatea ARP pentru a detecta falsificarea ARP sau modificări neașteptate ale adreselor IP/MAC.

Acesta generează un jurnal al asocierii înregistrate a informațiilor privind adresele IP și MAC, împreună cu un marcaj temporal, astfel încât să puteți urmări cu atenție momentul în care activitatea de asociere a apărut în rețea.

De asemenea, are opțiunea de a trimite rapoarte prin e-mail unui administrator de rețea atunci când o asociere este adăugată sau modificată.

Instrumentul **Arpwatch** este deosebit de util pentru administratorii de rețea care doresc să supravegheze activitatea ARP pentru a detecta falsificarea ARP sau modificări neașteptate ale adreselor IP/MAC.

# Instalarea Arpwatch

Instrumentul Arpwatch nu este instalat implicit. Pentru al instala pe Arch Linux rulați comanda următoare>

    $ sudo pacman -S arpwatch

Odată instalat, puteți vizualiza cele mai importante fișiere - locațiile fișierelor sunt ușor diferite în funcție de sistemul de operare.

- **/usr/lib/systemd/system/arpwatch** – Serviciul pentru pornirea sau oprirea daemon-ului.
- **/etc/sysconfig/arpwatch** – Acesta este fișierul de configurare.
- **/usr/sbin/arpwatch** – Comanda binară pentru pornirea și oprirea instrumentului prin intermediul terminalului.
- **/var/lib/arpwatch/arp.dat** – Acesta este fișierul principal al bazei de date în care sunt înregistrate adresele IP/MAC.
- **/var/log/messages** – Fișierul jurnal, în care scrie orice modificare sau activitate neobișnuită la IP/MAC.

Acum rulați următoarele comenzi pentru a porni serviciul arpwatch.

    $ sudo systemctl enable arpwatch
    $ sudo systemctl start arpwatch
    $ sudo systemctl status arpwatch

# Cum se utilizează Arpwatch

Pentru a supraveghea o anumită interfață, tastați următoarea comandă cu indicatorul `-i` și numele dispozitivului.

    $ sudo arpwatch -i eth0

Astfel, de fiecare dată când un nou MAC este conectat sau când un anumit IP își schimbă adresa MAC în rețea, veți observa intrări syslog în fișierul `/var/log/syslog` sau `/var/log/message` folosind comanda `tail`.

    $ sudo tail -f /var/log/messages

Exemplu de rezultat

    Apr 15 12:45:17 thinkroot99 arpwatch: new station 172.16.16.64 d0:67:e5:c:9:67
    Apr 15 12:45:19 thinkroot99 arpwatch: new station 172.16.25.86 0:d0:b7:23:72:45
    Apr 15 12:45:19 thinkroot99 arpwatch: new station 172.16.25.86 0:d0:b7:23:72:45
    Apr 15 12:45:19 thinkroot99 arpwatch: new station 172.16.25.86 0:d0:b7:23:72:45
    Apr 15 12:45:19 thinkroot99 arpwatch: new station 172.16.25.86 0:d0:b7:23:72:45

Rezultatul de mai sus afișează o nouă stație de lucru. În cazul în care se efectuează modificări, veți obține următorul rezultat.

    Apr 15 12:45:17 thinkroot99 arpwatch: changed station 172.16.16.64 0:f0:b8:26:82:56 (d0:67:e5:c:9:67)
    Apr 15 12:45:19 thinkroot99 arpwatch: changed station 172.16.25.86 0:f0:b8:26:82:56 (0:d0:b7:23:72:45)
    Apr 15 12:45:19 thinkroot99 arpwatch: changed station 172.16.25.86 0:f0:b8:26:82:56 (0:d0:b7:23:72:45)
    Apr 15 12:45:19 thinkroot99 arpwatch: changed station 172.16.25.86 0:f0:b8:26:82:56 (0:d0:b7:23:72:45)
    Apr 15 12:45:19 thinkroot99 arpwatch: changed station 172.16.25.86 0:f0:b8:26:82:56 (0:d0:b7:23:72:45)

De asemenea, puteți verifica tabelul ARP curent, utilizând următoarea comand:

    $ sudo arp -a

Exemplu de rezultat

    crism.ro (172.16.16.94) at 00:14:5e:67:26:1d [ether] on eth0
    ? (172.16.25.125) at b8:ac:6f:2e:57:b3 [ether] on eth0

Dacă doriți să trimiteți alerte la adresa dvs. de e-mail, deschideți fișierul principal de configurare `/etc/sysconfig/arpwatch` și adăugați adresa de e-mail așa cum se arată mai jos.

    OPTIONS="-u arpwatch -e thinkroot99@crism.ro -s 'root (Arpwatch)'"
    [ce reprezintă opțiunile de mai sus]
    -u <username> : stabilește cu ce utilizator trebuie să ruleze arpwatch.
    -e <email> : definește adresa de e-mail la care trebuie să trimită rapoartele.
    -s <from> : adresa de la care se trimite rapoartele.

Iată un exemplu de raport prin e-mail, atunci când este conectat un nou MAC.

    hostname: archlinux
    ip address: 172.16.16.25
    interface: eth0
    ethernet address: 00:24:1d:76:e4:1d
    ethernet vendor: GIGA-BYTE TECHNOLOGY CO.,LTD.
    timestamp: Monday, April 15, 2022 15:32:29

Iată un exemplu de raport prin e-mail, atunci când un IP își schimbă adresa MAC.

    hostname: archlinux
    ip address: 172.16.16.25
    interface: eth0
    ethernet address: 00:56:1d:36:e6:fd
    ethernet vendor: GIGA-BYTE TECHNOLOGY CO.,LTD.
    old ethernet address: 00:24:1d:76:e4:1d
    timestamp: Monday, April 15, 2022 15:43:45
    previous timestamp: Monday, April 15, 2022 15:32:29
    delta: 9 minutes

După cum puteți vedea mai sus, acesta înregistrează numele gazdei, adresa IP, adresa MAC, numele furnizorului și marcajele de timp.

Pentru mai multe informații, consultați pagina de manual rulând comanda următoare în terminal.

    $ man arpwatch
