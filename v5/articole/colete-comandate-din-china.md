# Colete comandate din China

Am citit articol-ul lui [Revo](https://www.revoblog.ro/china-livrare-posta-2024/) cum că Chinezii nu mai livrează coletele prin Poșta Română.

Nici nu mai țin minte când am ridicat ultimul colet de la Poșta Română și nu au trecut câteva luni, au trecut câțiva ani.

Era un site foarte cunoscut de pe care cumpăram din China (la un moment dat s-a închis) care trimitea pachetele prin Poșta Română și dura câteva luni până primeam comanda.

Și Banggood trimitea coletele prin Poșta Română, chiar și Wish, și de multe ori trebuia să mă duc până la Cluj-Napoca la Oficiul Poștal 16 - Vama de la Poșta Română ca să ridic coletele primite.

Țin bine minte că după ce s-a închis acel magazin de pe care cumpăra toată lumea (din păcate i-am uitat numele), nu prea am mai cumpărat produse din China. Cumpăram foarte rar și cumpăram de pe Wish care a început să trimită coletele prin curier.

După câțiva ani mi-am făcut cont pe AliExpress și am început să fac comenzi din China, mai nou fac comenzi și de pe Temu. Spre surprinderea mea, termenii de livrare al pachetelor a scăzut de la câteva luni la câteva săptămâni.

Când mi-am făcut contul pe AliExpress primea coletele după 4/5 săptămâni, dar după un timp am început să primesc coletele după 2/3 săptămâni.

Din ce am înțeles, contul de pe AliExpress este asemănător cu un cont de joc. Dacă lași recenzii primești puncte, dacă pui și poze cu produsele primite primești puncte, la 100 de comenzi primești iar puncte și tot așa. Și ca răsplată scade timpul de primire al comenzilor și pot spune că chiar a scăzut timpul de primire al comenzilor.

Ultima comandă pe care am făcut-o a fost de pe Temu și din nou spre surprinderea mea, comanda am primit-o prin Poșta Română - dar partea cea mai plăcută este că am primit comanda destul de repede, după 3 săptămâni am primit comanda.

Nu îmi dau seama cum de am primit comanda prin Poșta Română și cum de a venit așa de repede prin serviciul Poștei Române.

Concluzia este că aceste magazine Chinezești încă se mai folosesc de Poșta Română, este adevărat că nu așa de des ca în urmă cu câțiva ani, dar se folosesc.
