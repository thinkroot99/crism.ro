# De la screenFetch la pfetch

Când am început să folosesc GNU/Linux și terminal-ul, am dat de aplicația [screeFetch](https://github.com/KittyKatt/screenFetch). 

screenFetch este o unealtă Bash pentru a afișa în terminal informații despre sistem.

Această unealtă/aplicație am folosit-o destul de mult timp. Dar cu timpul am dat peste, popularul, [Neofetch](https://github.com/dylanaraps/neofetch) - program oprit din dezvoltare pe date de 26 aprilie 2024.

Neofetch l-am folosit cel mai des pentru a vizualiza informațiile despre sistem în terminal. A fost o aplicație foarte bună care a mers fără probleme.

În ultimi doi, trei ani am dat peste aplicația [pfetch](https://github.com/dylanaraps/pfetch), scrisă de același autor care a scris și Neofetch - din păcate și la aceasta sa oprit dezvoltarea.

Înlocuitor pentru Neofetch sunt foarte multe, printre care și [Fastfetch](https://github.com/fastfetch-cli/fastfetch). Dar nu despre această aplicație este vorba în acest articol.

## pfetch

Cum îmi place `pfetch` am căutat un înlocuitor pentru această aplicație simplă.

![](https://user-images.githubusercontent.com/6799467/65944518-68834d80-e421-11e9-9b14-6ca26a16108a.png "pfetch")

Nu am căutat orice înlocuitor. Am vrut ca noua aplicație să fie scrisă în limbajul de programare `Rust`. Și astfel am dat peste aplicația [pfetch-rs](https://crates.io/crates/pfetch).

![](https://user-images.githubusercontent.com/50576978/219375863-579c495d-8db8-4aa9-a4a6-348ecb2c849f.png "pfetch-rs")

## Instalare

Pentru a instala această aplicație folosiți una din comenzile de mai jos:

`cargo install pfetch` sau `brew install pfetch-rs`