% Care este diferența dintre Internet și World Wide Web
% ThinkRoot99

Chiar dacă o folosim în fiecare zi terminologia internetului poate fi dificil de reținut. Există mulți termeni pe care este posibili să nu-i înțelegeți pe deplin, precum și cuvinte pe care le folosim în mod interschimbabil, dar care nu sunt de fapt identici.

Aceasta este cazul internetului și al World Wide Web. Ce înseamnă de fapt acești doi termeni și care este diferența dintre ei? Haideți să aflăm despre aceste sisteme esențiale și despre modul în care acestea lucrează împreună pentru a oferi experiența online modernă.

# Ce este Internetul?

Internetul este o rețea globală de rețele de calculatoare. Este tehnologia de bază care permite calculatoarelor, telefoanelor, consolelor de jocuri, echipamentelor casnice inteligente, serverelor și altor dispozitive capabile să comunice între ele, indiferent de locul în care se află în lume.

Ca atare, internetul include toată infrastructura fizică necesară pentru ca aceste rețele să funcționeze împreună. Fie că este vorba de cabluri locale ale furnizorilor de servicii de internet sau de cabluri submarine uriașe care leagă continentele, toate acestea sunt esențiale pentru ca internetul să funcționeze așa cum funcționează. Datorită naturii sale, [nimeni nu este cu adevărat „*proprietarul*” internetului](https://www.webopedia.com/insights/who-owns-internet/).

Dar internetul nu este doar un concept fizic. Multe protocoale și standarde sunt integrate în internet pentru a crea ceea ce cunoaștem astăzi.

De exemplu, adresele de Internet Protocol (adrese IP) reprezintă o parte esențială a modului de funcționare a sistemului. Fiecare dispozitiv are o [adresă IP](https://www.networkworld.com/article/3588315/what-is-an-ip-address-and-what-is-your-ip-address.html), la fel cum fiecare clădire fizică are o adresă. Fără Internet Protocol, dispozitivele care utilizează internetul nu ar putea trimite informații către destinațiile corecte.

# Ascensiunea internetului

Înainte de existența internetului, entități precum guvernele și universitățile aveau rețele locale care permiteau calculatoarelor lor să comunice între ele. Dar nu exista o rețea globală, așa cum avem astăzi. În anii `60 și `70 au avut loc numeroase încercetări și dezvoltări preliminare pentru a determina ce va alimenta internetul.

În anii 1980, guvernul SUA a investit mulți bani, timp și cercetare în dezvoltarea a ceea ce a devenit internetul modern, care a început să se dezvolte în curând în întreaga lume.

> [News report about internet in 80's](https://www.youtube.com/watch?v=L02DnLfJDBo)

Datorită comercializării, internetul a devenit mai răspândit la sfârșitul anilor 1990 și începutul anilor 2000. De la un instrument utilizat doar în medii profesionale, cum ar fi școlile, a devenit o oportunitate larg răspândită pentru toată lumea. Comunicarea, comerțul, cercetarea și multe altele erau acum posibile la o scară nemaiîntâlnită până atunci.

# Ce este World Wide Web?

După cum am menționat, World Wide Web și internetul nu sunt același lucru. World Wide Web (de obicei, prescurtat doar web) este un sistem de organizare a informațiilor care este accesibil prin utilizarea internetului.

Sir Tim Berners-Lee a inventat web-ul în 1989, iar în 1991 a devenit accesibil publicului. El nu și-a brevetat niciodată ideea, făcând-o deschisă și accesibilă tuturor.

> [A brief history of the World Wide Web](https://www.youtube.com/watch?v=k0gvAyCubGQ)

Se numește „*web*” datorită naturii sale interconectate; designul său facilitează accesul la diverse resurse de oriunde v-ați afla. Luați în considerare modul în care puteți face clic în jurul diferitelor articole pentru a naviga fără a fi nevoie să tastați adrese specifice în navigator.

Aproape tot ceea ce facem într-un navigator web face parte din World Wide Web. HTTP, care înseamnă Hypertext Transfer Protocol, este principala metodă de comunicare pe web. Atunci când deschideți navigatorul și accesați www.crism.ro, navigatorul utilizează HTTP pentru a solicita informații de la serverul web al site-ului, apoi le afișează în navigatorul vostru într-o formă lizibilă.

# Alte protocoale web

HTML, sau Hypertext Markup Language, este stilul de formatare de bază utilizat pe web. Pe lângă textul de bază și formatarea de bază, cum ar fi bold (îngroșat) sau italic (oblic), HTML poate include și legături către imagini, videoclipuri și alte medii. O parte vitală a web-ului este hyperlink-ul, care vă permite să creați un text pe care se poate da clic și care duce la alte pagini.

Resursele de pe web sunt identificate printr-un identificator uniform de resurse (Uniform Resource Identifier – URI); Uniform Resource Locator (URL) este cel mai comun timp de URI pe web în prezent. Un alt nume comun pentru un URL este adresa web.

După cum le spune și numele, acestea sunt referințe la o pagină web – dacă aveți adresa URL a unei pagini, o puteți deschide în navigator pentru a o accesa.

# Care sunt diferențele dintre Web și Internet?

Poate că cel mai simplu este să ilustrăm diferențele dintre aceste două sisteme prin demonstarea a ceea ce fiecare dintre ele este capabil să facă fără celălalt.

Puteți utiliza internetul fără a folosi web-ul, deoarece există multe forme de comunicare care nu se bazează pe World Wide Web. E-mail-ul este un exemplu obișnuit. Trimiterea unui e-mail nu necesită utilizarea web-ului, deoarece utilizează SMTP (Simple Mail Transfer Protocol) și funcționează fără un navigator web.

În prezent, majoritatea oamenilor accesează un client de webmail în navigatorul lor pentru a utiliza e-mail-ul, care utilizează web-ul. Includerea de link-uri către resurse web este, de asemenea, o parte obișnuită a e-mail-ului, dar nu este necesară pentru ca serviciul să funcționeze cu mesaje simple.

> [How to send an 'E mail' | Database | Retro Computers | Early E mail | 1980s Technology | 1984](https://www.youtube.com/watch?v=szdbKz5CyhA)

VoIP, sau Voice over Internet Protocol, utilizează, de asemenea, internetul, dar nu și web-ul. Atunci când efectuați un apel folosind un serviciu de telefonie prin internet, nu accesați pagini web sau încărcați informații de pe internet. Pur și simplu efectuați un apel în care informațiile trec prin infrastructura internetului în loc de liniile telefonice.

Un ultim exemplu sunt interacțiunle care utilizează un alt protocol de internet, cum ar fi FTP (File Transfer Protocol). FTP vă permite să mutați fișiere de pe o mașină pe alta, dar nu necesită un navigator (browser) web sau vreaun protocol de organizare web. Atât timp cât aveți un client FTP și ceva la care să vă conectați, puteți utiliza FTP fără a naviga pe web.

# Se poate utiliza Web-ul fără Internet?

Reversul nu este valabil; nu este cu adevărat posibil să navighezi pe web fără să folosești internetul. Pentru a accesa o resursă web (cum ar fi un site) care se află pe un alt server, trebui să utilizați internetul pentru a vă conecta la acesta. În caz contrar, dispozitivul vostru nu are nici o legătură cu rețeaua în care se află celălalt dispozitiv.

Cu toate acestea, puteți utiliza în continuare un navigator pentru a accesa resursele web din rețeaua locală. De exemplu, este posibil ca firma voastră să aibă un site web intern pe care îl puteți accesa numai dacă sunetți conectat la rețeaua sa (numit „*intranet*”).

> [What is an Intranet?](https://www.youtube.com/watch?v=dIA1KweJoRY)

Utilizarea navigatorului web vă permite să deschideți și să răsfoiți aceste informații, dar nu vă aflați de fapt pe internet, deoarece serverul există în rețeaua locală. Dacă ați merge în alt oraș și ați încerca să accesați aceste pagini, nu ar funcționa. Astfel, beneficiați de configurația organizațională familiară a World Wide Web atunci când accesați resurse locale, dar în acests caz nu sunteți pe internet.

# Web și Internet: Chei pentru lumea de astăzi

După cum am văzut, internetul și web-ul sunt importante unul pentru celălalt, dar nu sunt același lucru. Internetul este infrastructura care conectează calculatoarele între ele, în timp ce World Wide Web este un sistem de organizare a informațiilor accesibile prin intermediul internetului. Nu tot ceea ce este disponibil pe internet are nevoie de web.
