% Ce este G'MIC pentru GIMP
% ThinkRoot99

Am folosit **GNU Image Manipulation Project** (GIMP) de multe ori. Cu această aplicație am creat câteva imagini, am modificat imagini și de cele mai multe ori am redimensionat sau am schimbat formatul imaginilor.

GIMP este echivalentul open source al Photoshop, este o aplicație puternică chiar dacă multă lume spune contrariul.

Dar un lucru care lipsește din GIMP este un set de module care ajută la combinarea diferitelor filtre în așa fel încăt să vă fie ușor să adăugați toate tipurile de efecte la proiectele voastre.

Se pot folosi filtrele implicite pentru a vă modifca imaginile, dar uneori combinația potrivită de filtre poate fi un joc de ghici. Aici intervine [G'MIC](https://gmic.eu/).

G'MIC este acronimul de la **GREYC's Magic for Image Computing** și este un modul GIMP pentru procesarea digitală a imaginilor. G'MIC oferă o interfață GUI pentru GIMP care permite utilizatorilor să convertească, să proceseze și să vizualizeze imagini cu diferite combinații de filtre.

Cu acest plugin, puteți selecta din sute de combinații de filtre diferite, fiecare dintre acestea având un efect dramatic de diferit asupra imaginilor.

G'MIC include atât opțiuni pentru linia de comandă, cât și pentru GUI.

# Cum se instalează G'MIC

Puteți instala G'MIC pentru GIMP atât pe GNU/Linux, cât și pe Windows. Pentru Windows [descărcați](https://gmic.eu/download.html) aplicația .exe (**GIMP trebuie să fie instalat pentru a putea instla G'MIC**). Iar pe GNU/Linux trebuie să vă folosiți de un terminal.

1. Deschideți un terminal.
2. Închideți GIMP, dacă este deschis.
3. Instalați G'MIC folosind comanda următoare:

       $ sudo pacman -S gimp-plugin-gmic 'Arch Linux'
       $ sudo apt install gimp-gmic 'Debian/Ubuntu'
       $ sudo dnf install gmic-gimp 'Fedora'
       $ sudo zypper install gmic

După finalizarea instalării puteți închide terminalul.

# Cum se utiliează G'MIC

După ce GIMP se deschide, trebuie să creați sau să deschideți o imagine nouă înainte de a avea acces la instrumentul G'MIC.

## Selectați stratul care urmează să fie manipulat

Mai departe, trebuie să selectați stratul din imaginea pe care doriți să o utilizați cu G'MIC. Dacă imaginea voastră are doar un singur layer, nu trebuie să vă faceți griji cu privire la acest pas.

Dacă imaginea voastră are mai multe straturi, puteți selecta stratul din colțul dreapta jos al ferestrei GIMP.

> ![GIMP Layer](img/gimp-layer.webp)

## Deschideți G'MIC

După ce ați selectat stratul cu care doriți să lucrați, faceți clic pe meniul `Filtres` și apoi selectați G'MIC-Qt.

> ![G'MIC](img/gmic.webp)

## Selectați filtrul

Când se deschide G'MIC, veți vedea trei panouri.

- Panoul din stânga prezintă stratul de imagine pe care urmează să îl manipulați. Acest panou arată cum va arăta stratul atunci când selectați filtrul.
- Panoul din miljoc este lista de filtre. Puteți extinde fiecare intrare pentru a dezvălui diferitele filtre care vor fi utilizate.
- Panoul din dreapta va afișa opțiunile pentru fiecare filtru pe măsură ce îl selectați.

Unele filtre au o mulțime de opțiuni de modificare, în timp ce altele au mai puține. Nu vă lăsați intimidat de opțiuni, deoarece nimic nu este aplicat pe stratul de imagine până când nu faceți efectiv clic pe `Apply`.

Puteți de asemenea să vă jucați cu diapozitivele și drop-down-urile după bunul plac până când stratul de imagine este exact cum doriți.

> ![G'MIC Filters](img/gmic-filters.webp)

Un lucru este de reținut - unele filtre durează mai mult timp pentru a fi aplicate. Acest proces durează mai mult deoarece unele filtre sunt de fapt o combinație de filtre care trebuie aplicate pe rând.

Viteza va depinde, de asemenea, de cât de mare este imaginea. Dacă imaginea este mare și ați selectat un filtru care necesită mai multe treceri pentru a fi finalizat, procesul poate dura ceva timp.


