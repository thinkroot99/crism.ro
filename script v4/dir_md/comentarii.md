# Cum se poate lăsa un comentariu

Problema celor care urmăresc blog-uri statice este că nu pot comentat pentru că nu există o secțiune de comentarii. O astfel de secțiune se poate implmenta printr-un scipt PHP, nu știu dacă se poate și cu JavaScript. Mai există tot felul de servicii care oferă API-uri și script-uri gata făcute pentru a implementa o secțiune de comentari pe un blog static.

Dar unde mai este frumusețea unui astfel de blog, dacă este umplut cu tot felul de scripturi?

În cazul meu comentariile la articole se pot lăsa pe [forum](https://forum.crism.ro), pe [mastodon](https://linux.social/@thinkroot99) sau pe [e-mail](/contact.html).


