% De ce folosesc Arch Linux și Sway
% ThinkRoot99

Pâna nu de mult am folosit multe distribuții GNU/Linux - de la Ubuntu, Linux Mint la Debian, Fedora și altele mai puțin cunoscute ca Redcore Linux sau Calculate Linux cu diverse medii de lucru, ca Cinnamon, Plasma sau Xfce.
Dar nu m-am simțit confortabil cu nici una dintre acest distirbuții sau medii de lucru. Cel mai pe placul meu a fost Fedora cu GNOME.

Fedora Workstaton îmi place și dacă va fi nevoie să renunț la Arch Linux, Fedora o să folosesc în schimb. Dar să trecem la subiectul principal și anume de ce folosesc Arch Linux cu Sway.

În primul rând pentru că Arch Linux îl pot configura de la început exact așa cum doresc să arate o distribuție GNU/Linux pentru mine. Plus că în combinație cu AUR și Chaotic-AUR pot instala orice aplicație am nevoie. Arch Linux pe cât de complicat pare, pe atât de ușor este. Chiar și utilizatori novici de GNU/Linux, dacă sunt un pic atenți pot folosi această distribuție.


În timp ce folosesc Arch Linux și Sway învăț să folosesc terminal-ul tot mai bine. Îmi place că pot instala cele mai noi versiuni ale aplicațiior pe care le folosesc. Am trecut la Arch pentru simplitatea pe care o are, pentru AUR, pentru tot ține de Arch.

Ca mediu grafic folosesc managerul de ferestre Sway care rulează cu Wayland. Am mai folosit i3 care rulează cu X11. Dar Wayland este tot mai bun pe zi ce trece și împreună cu Sway am un sistem care rulează plăcut. Experiența cu X11 și i3 nu era așa plăcut cum este acum cu Sway și Wayland. Ținând cont că folosesc un laptop foarte vechi și care ar trebui să meargă mai bine cu X11.

Și pentru ce am nevoie Sway este perfect. Scriu articolele cu [Neovim](https://neovim.io/), citesc sit-urile șî blogurile cu cititorul de RSS Newsboat - chiar și la canalele de YouTube preferate mă uit cu [Newsboat](https://newsboat.org). Ca manager de fișiere folosesc o combinație între PCManFM și ranger - cel puțin până învăț [ranger](http://nongnu.org/ranger/) mai bine.

În prezent folosesc un Lenovo ThinkPad T440p cu 16 GB RAM. Arch împreună cu Sway merge foarte bine pe acest laptop și îmi oferă tot cea ce am nevoie.
