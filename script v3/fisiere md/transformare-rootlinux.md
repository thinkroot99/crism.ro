% Transformarea blog-ului / Linux este mai lungă

Transformarea blog-ului de știri Linux și Open Source este mai lungă decât îmi doresc. Nu este un blog mare, dar pentru mine este destul de mare mai ales că am și alte proiecte în derulare.

Cum am mai scris în alt articol. Prima transformare sau modificare a fost să scot plugin-ul de SEO și încât câteva pentru a face blog-ul cât mai simplu. Dar cum este bazat pe WordPress, nu poți se poate face așa de simplu - ca interfață grafică se poate. Dar eu vreau mult mai mult.

## Prima transformare

Prima transformare care vreau să o fac este a transforma blog-ul într-un blog static (HTML/CSS) și cât mai puți cod JavaScript și PHP. De PHP voi avea nevoie pentru a implementa categoria de comentarii, iar de JavaScript o să am nevoie de diverse funcții care nu se pot implementa prin HTML/CSS.

O parte din această treabă am rezolvat-o. Cum am făcut acest lucru?

Am creat un script pentru a descărca toate articolele și să le transforme direct în fișiere markdown. Am descărcat articolele (dar de atunci am mai scris pe rootlinux.ro), dar există o mică problemă - pe lângă artcolul propriuzis scriptul a mai colectat și alte texte care apar pe pagina articolului (poate că se poate face în această priviță, dar deocamdată nu știu cum) și astfel trebuie să le editez manual toate fișierele markdown și să elimin tot textul care este în plus.

## A doua transformare

Pentru partea a doua am nevoie de un desgine/șablone - și pe acesta îl am pentru că l-am construit în timpul când am făcut acest sit. Cred că am mai multe astfel de designe-uri :). Pe lângă acest șablon/design mai am nevoie de secțiunea de comentarii care trebuie scrisa în PHP și acest fișier nu îl am și nici nu știu când o să am timp să mă ocup.

De JavaScript deocamdată nu am nevoie pentru că încă nu știu dacă voi avea nevoie de ceva mai speciale care nu poate fi creat cu HTML/CSS - deci acestă parte nu este urgentă, așa cum este partea de comentarii.

Pe lângă acest lucru mai am nevoie de un script care să convertească fișierele markdown în fișiere html și să implementeze șablonul pentru noul blog. Script pe care îl am când am creat acest sit, de fapt am două astfel de scripturi; unul scris în Python și altul scris în Bash.

Deci și cu patea aceasta am terminat. Și nu cred că mai am nevoie de ceva, dar văd pe parcurs.

## Ultima parte

Ultima parte este cea mai simplă. Pun la un loc fișierele markdown (după ce le termin de editat) cu scriptul și șablonul HTML/CSS/PHP. Rulezz scriptul pentru crearea blog-ului și după ce totul se termină, mai rămâne doar să urc toate fișierele pe internet în loc la WordPress.

Și cam asta este tot. Dar până atunci mai am ceva de lucru și din păcate durează.

## Ce se întâmplă cu echipa actuală

Actualul blog [rootlinux.ro](https://rootlinux.ro) are o mică echipă sau pot spune că a avut o mică echipă, dar se pare că am rămas singur.

Când am început proiectul am fost trei oameni. Andrei Cojocaru care a scris două articole, Alexandru Bratu care a scris 15 articole și eu, am scris majoritatea articolelor. Andrei s-a retras din proiect pentru că nu a mai scris nimic, iar Alexandru nu s-a retras oficial, dar nici nu a mai scris nimic.

Deci ce se întâmplă mai departe? Este simplu, dacă Andrei și Alexandru vor să mai scris artciole pentru rootlinux.ro tot ce trebuie să facă este să scrie articolul în markdown sau text simplu și mi-l trimit prin e-mail sau altă metodă, după care eu creez fișierul markdown și rulezi scriptul de creare a fișierului html, urc noul fișier pe internet și asta este tot.

Dar Andrei nu mai face parte din proiect așa că ca trebuie să purtăm o discuție dacă mai vrea să revină. Cu Alexandru trebuie să discut pentru a vedea dacă mai vrea să facă parte din proiect sau nu.

## De final

În concluzie mai am mult de lucru pentru a transforma blog-ul rootlinux.ro în ceva mai frumos și un blog mai simplu și mai curat.

PS: La reclame care sunt acum pe blog încă mă mai gândesc dacă să renunț sau nu.
