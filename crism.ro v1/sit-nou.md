% De ce un sit nou?
% ThinkRoot99

# De ce un sit nou

De mult timp am vrut un sit într-un format cât mai simplu. La un moment dat am învâțat HTML și CSS la nivel de începător și am construit câteva sit-uri doar pentru mine.

Și tot timpul m-am întors la WordPress pentru că era mai ușor de publicat articole decât pe un sit construit în HTML/CSS, unde totul trebuie făcut manual - de la scrisul articolelor până la codul HTML.

În urmă cu câteva săptpmâni (maxim o lună) am dat întâmplător peste instrumentul `pandoc`, care este un convertor universal de documente. O unealtă simplă și simplu de utilizat care se poate folosi doar în linia de comandă indiferent de sistemul de operare folosit.

# De ce am renunțat la WordPress

WordPress este un CMS extraordinar, este simplu de utilizat dar complicat de administrat. Pentru a funcționa cum trebuie are nevoie de câteva cerințe, iar pentru a arăta cât de cât bine are nevoie de plugin-uri șî teme. Pe zi ce trece este tot mai greu de administrat și cere tot mai mult pentru un simplu sit sau un blog. Iar pentru un simplu sit sau un blog nu se merită folosirea WordPress-ului.

M-am săturat să întrețin ceva ce este complicat și de care nu am nevoie. Și resursele cerute de WordPress parcă sunt tot mai mari cu noile versiuni ce apar.

# Cum este construit acest sit

Tot site-ul este scris în `markdown` într-un editor de text (îmi place să folosesc vim) și convertit în html cu pandoc printr-o simplă comandă rulată în terminal.

Am creat un `makefile` unde am scris mai multe comenzi pentru a face sit-ul să arate așa cum este. Dar în terminal folosesc doar o singură comandă foarte scurtă.


# Despre ce voi scrie

Nu voi mai publica articole așă de des cum o făceam pe vechiul blog. O să scriu articol când am ceva de scris sau când citesc despre ceva interesant și consider că informația merită scrisă pe acest sit.

Mai mult o să scriu despre ceea ce folosesc, despre ce învăț pentru ca toată lumea care îmi urmărește sit-ul să poată să învețe mult mai ușor din experiența acumulată de mine.

# Concluzie

Am ajuns să folosesc unelte simple care par complicate :) și astfel am învățat și învăț mai bine să folosesc linia de comandă din GNU/Linux.

Sit-ul acum arată pe placul meu. Folosind WordPress nu am găsit niciodată o temă care să mă mulțumească și nici nu sunt nevoit să pornesc navigatorul de fiecare dată când vreau să scriu un articol.
