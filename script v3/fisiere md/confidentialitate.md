% Confidențialitate

Personal, am luat o serie de pași pentru a încerca să-mi păstrez propria intimitate acolo unde este posibil. Cred că aș fi neglijent dacă nu aș încerca să fac același lucru pentru cei care decid să citească conținutul pe care l-am publicat pe acest sit.

Pe lângă asta mai este și [legea UE](https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/) care se aplică și în România.

Având în vedere toate cele de mai sus, vreau să fac tot ce pot pentru a păstra confidențialitatea vizitatorilor acestui sit ori de câte ori este posibil.

Deci, ce fac pentru a vă asigura că confidențialitatea voastră este menținută?

1. Analytics 

Pe acest sit nu există analize

Nu îmi pasă câți oameni citesc conținutul acestui sit, îmi pasă doar de implicare - aș prefera ca o singură persoană să îmi trimită e-mail, decât să am 1 milion de vizitatori care nu se implică niciodată.

1.1 Fără cookie-uri

Nu vor fi introduse cookie-uri de urmărire pe calculatorul vostru de pe acest sit, ceea ce înseamnă că nu există un ID unic pe care să-l pot folosi pentru a vă identifica și urmări.

Puteți utiliza un instrument online, cum ar fi [Cookie Metrix](https://www.cookiemetrix.com/), pentru a vedea ce cookie-uri sunt stocate pe calculator atunci când vizitați sit-ul meu.

2. Condiții de utilizare

Nu trebuie să fiți de acord cu niciun termen al serviciului pentru a accesa, vizualiza, citi sau copia oricare dintre datele de pe acest sit.

Aproape tot conținutul de pe acest sit este publicat sub licența [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ro), dacă nu se specifică altfel.

3. Informații despre licență

Conținutul de pe paginile Acasă, Despre și Contact se află sub [legea dreptului de autor](https://www.cdep.ro/pls/legis/legis_pck.htp_act_text?idt=10396).

Conținutul de pe celelalte pagini, care nu sunt menționate mai sus, sunt produse sub licență [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ro) (dacă nu se specifică altfel).

- Sunteți liberi să partajați, să copiați, să copiați și să redistribuiți materialul în orice mediu sau format.
- De asemenea, sunteți liber să adaptați, să remixați, să transformați și șă construiți pe baza materialului pentru orice scop, chiar și comercial.
- Trebuie sa oferiți atribute autorului (eu) cu o legătură înapoi către acest sit. Un exemplu de atribuire este mai jos.
- Dacă remixați, transformați sau construiți pe baza materialului, trebuie să vă distribuiți contributiile sub aceeași licență ca și originalul.
- Nu puteți aplica termeni legali sau măsuri tehnologice care restricționează din punct de vedere legal pe ceilalți să facă orice permite licență.
 
3.1 Exemplu de atribuire

O atribuire acceptabilă care respectă licența ar fi ceva similar cu următoare:

`<p>Credit către <a href="https://crism.ro">Cristian Moldovan</a> pentru lucrarea originală.</p>`
