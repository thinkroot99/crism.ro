# Aproape o zi întreagă la Euro Truck Simulator 2

Ieri cum nu aveam ce face am pornit calculatorul copilului cel mare și am văzut că are un nou joc instalat.

Am pornit jocul, Euro Truck Simulator 2, în jurul orei 13:00 și m-am mai ridicat de la calculator în jurul orei 20:00. M-a prins așa de tare acest joc încât am uitat de mine acolo :)

Și asta nu este tot. După aproximativ 30 de minute m-am dus la calculatorul copilului cel mic și m-am mai jucat acolo până pe la ora 00:30 sau ceva de genul.

Euro Truck Simulator 2 este un joc frumos, bine făcut și chiar dacă cursele lungi de la 6, 8 ore (în joc, nu ore reale) în sus au fost un pic plictisitoare, tot nu m-au făcut să închid jocul.

Trebuie să mă pun mai rar la acest joc ca să nu pierd toată ziua jucând acest joc minunat.