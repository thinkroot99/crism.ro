% Ce este Arch User Repository (AUR)?
% ThinkRoot99

Utilizatorii Arch sunt răsfățați când vine vorba de a descărca programe. Puteți fie să vă obțineți pachetele din depozitul oficial Arch, Snap Store, Flathub, fie eliminați complet necesitatea de a instala programe prin simpla descărcare a AppImage-urilor.

Apoi, există o altă opțiune – descărcarea programelor din Arch User Repository (AUR). Dar nu toți utilizatori Arch sunt familiarizați cu aceasta, în special cei nou veniți. Deci, ce este AUR și cum puteți descărca pachete din acest depozit special? 

# Ce este AUR?

Arch User Repository este un depozit de pachete condus de comunitate și dezvoltat de utilizatorii Arch pentru utilizatorii Arch. Dacă nu găsiți un pachet în depozitul oficial, există șanse mari să fie în AUR.

Totuși, AUR nu conține pachete pre-compilate. În schimb, găzduiește build-uri de pachete `PKGBUILD`, care sunt scripturi pe care le puteți rula cu `makepkg` pentru a construi un pachet. Arhiva generată ar conține apoi toate binarele și instrucțiunile necesare pentru a instala pachetul pe sistemul vostru.

Dacă v-ați întrebat vreodată cum ajunge un pachet în depozitul oficial Arch. Majoritateea pachetelor își încep călătoria în AUR, unde utilizatorii le pot vota pentru a-și exprima interesul.

> ![AUR Official Website](img/aur-official-website.webp)

Și, după, cum este evident, un pachet cu un anumit număr de voturi și un ambalaj bun este testat și inclus în depozitul oficial de „*comunitate*” Arch (a nu fi confundat cu AUR). Numărul minim de voturi pentru ca pachetul să fie eligibil pentru includerea este de 10.

Dar aceasta nu este singura condiție prealabilă, un utilizator de încredere ar trebui să se ofere voluntar pentru a menține pachetul atunci când este mutat în depozitul oficial. Dacă un pachet are sute de voturi și nici o persoană care să-l mențină, nu va fi mutat în depozitul oficial.

De asemenea, spre deosebire de AUR, depozitul Arch conține pachete binare pe care utilizatorii le pot instala cu `pacman` și nu cu `PKGBUILD`.

# Ar trebui să descărcați pachete din AUR?

Dacă cineva își poate încărca și trimite fișierele `PKGBUILD` la AUR, asta nu compromite aspectul de securitate? Răspunsul este da, dar parțial. AUR are utilizatori care mențin pachetele, cunoscuți sub numele de „**utilizatori de încredere**” care fac parte din proiect de multă vreme.

Utilizatorii de încredere verifică în mod regulat ceea ce este încărcat în depozit și sunt atenți la orice lucru care pare suspect. Deși reglementările și verificările ajută la eliminarea încărcărilor rău intenționate, există momente în care aceste pachete ajung în depozit.

Prin urmare, un utilizator Arch avansat, conștient de securitate, verifică întotdeauna compilațiile pachetului înainte de a rula `makepkg` pentru a construi arhiva (și ar trebui să o faci și tu).

# Cum descărcați pachetele

Deoarece Arch User Repository și depozitul oficial nu sunt același, uneltele pentru descărcarea pachetelor din ambele depozite sunt diferite. Pentru depozitul oficial, puteți folosi pacman. Dar pentru AUR, aveți două opțiuni.

Fie puteți clona și crea pachetul manual, fie puteți automatiza procesul folosind un AUR Helper.

## Descărcarea manuală a unui pachet

Pentru a descărca un pachet, va trebui să instalați câteva utilitare necesare pe sistemul vostru. Tastați următoarea comandă în terminal pentru a instala pachetele:

    sudo pacman -S base-devel git

Începeți prin a accesa [aur.archlinux.org](https://aur.archlinux.org/) și căutați pachetul pe care doriți să îl descărcați. Accesați pagina pachetului și copiați adresa **Git Clone URL**.

> ![Get clone URL from the AUR](img/get-clone-url-from-the-aur.webp)

Apoi, deschideți terminalul și tastați următoarea comandă:

    $ git clone „link-copiat”

Navigați la directorul descărcat folosind comanda `cd` și rulați `makepkg` după cum este mai jos:

    $ cd „nume-pachet”/
    $ makepkg -si

De exemplu, descărcăm navigatorul Brave:

    $ git clone https://aur.archlinux.org/brave.git
    $ cd brave/
    $ makepkg -si

Nu trebuie să rulați pacman pentru a instala arhiva pachetului generat. Comanda `makepkg` va invoca automat pacman, care va instala apoi pachetul împreună cu dependințele necesare.

## Descărcare pachete utilizând AUR Helper

Un AUR Helper automatizează pur și smplu procesul de descărcare afișierelor `PKGBUILD` din AUR, generarea arhivei și instalarea pachetului folosind pacman.

Un astfel de AUR Helper este [yay](https://github.com/Jguer/yay), care este ușor de descărcat. Rulați următoarele comenzi una câte una pentru a instala yay pe sistemul vostru:

    $ git clone https://aur.archlinux.org/yay-git.git
    $ cd yay-git
    $ makepkg -si

Sintaxa de bază a lui yay este foarte asemănătoare cu cea a lui pacman:

    $ yay -S „nume-pachet”

Descărcarea navigatorului Brave folosind yay:

    $ yay -S brave

Așa de ușor este să instalezi pachete din AUR folosind yay.

# Cum se trimit pachete

Una dintre preocupările principale ale dezvoltatorilor este trimiterea pachetelor la AUR. Primul lucru pe care îl puteț face este să citiți [instrucțiunile de trimitere a pachetelor](https://wiki.archlinux.org/title/AUR_submission_guidelines) de pe sit-ul oficial. Rețineți că acest ghid presupune deja că știți [cum să creați un pachet pentru AUR](https://www.youtube.com/watch?v=ls_hpopfsQU).

Pentru a începe, trebuie să vă găzduiți pachetul pe o platformă precum GitHub sau Gitlab. Apoi, clonați depozitul în sistemul vostru și navigați la acel director folosind linia de comandă.

Următorul pas este să creați un `PKGBUILD` pe care îl veți adăuga la AUR. Pentru a face asta, replicați prototipul implicit de construire a pachetului disponibil la `/usr/share/pacman`:

    $ cp /usr/share/pacman/PKGBUILD.proto PKGBUILD

Deschideți fișierul de compilare a pachetului folosind la alegere orice editor de text. Apoi, în interiorul `PKGBUILD`, specificați informațiile necesare, cum ar fi numele pachetului, adresa URL a depozitului (GitHub sau GitLab), dependențele necesare etc.

> ![Demo PKGBUILD for AUR packages](img/demo-pkgbuild-for-aur-packages.webp)

Iată un tabel care acoperă variabilele importante în detaliu:

| Variabilă  | Utilizare                                             |
| ---------  | ---------                                             |
| pkgname	 | Numele pachetului                                     |
| pkgver	 | Versiunea actuală a pachetului                        |
| pkgdesc	 | O scurtă descriere a pachetului                       |
| arch	     | Arhitectura necesară a sistemului țintă               |
| url	     | Adresa URL a depozitului Git pentru a accesa pachetul |
| license	 | Licența pe care doriți să o utilizați                 |
| depends	 | Dependențe cerute de pachet                           |
| conflicts	 | Pachetele cu care intră în conflict                   |
| provides	 | Pachetul oferit de depozitul vostru AUR               |
| source	 | Sursa fișierelor necesare pentru a construi pachetul  |

Rețineți că trebuie să completați și detaliile întreținerii specificate la începutul fișierului. În plus, editați metodele de compilare, verificare, împachetare și pregătire la sfârșitul fișierului pentru a se potrivi nevoilor voastre.

Când ați terminat, construiți pachetul folosind fișierul `PKGBUILD` pe care tocmai l-ați creat.

    $ makepkg -si

Dacă totul merge bine, generați fișierul `SRCINFO` tastând:

    $ makepkg --printsrcinfo > .SRCINFO

Mutați `PKGBUILD` și `.SRCINFO într-un director separat.

Pentru a trimite compilarea pachetului, creați și clonați un depozit AUR gol folosind următoarea sintaxă a comenzii:

    $ git clone ssh://aur@aur.archlinux.org/packagename.git

…unde `packagename` este numele pachetului vostru.

Copiați compilarea pachetului și fișierului `SRCINFO` în directorul de depozit nou creat folosind comanda `cp`. În cele din urmă, tot ce trebui să faci este să comiți (commit), să adaugi (add) și să împingi (push) modificările în depozitul AUR de la distanță folosind git.

> [![Youtube Video](img/creating-packages-for-the-arch-user-repository.png)](https://www.youtube.com/watch?v=iUz28vbWgVw)

# Arch User Repository este sigur?

AUR este una dintre cele mai proeminente metode de distribuție a pachetelor pe Arch Linux, iar comunitatea se străduiește să o ducă la nivelul următor. Mulțumită tuturor utilizatorilor care participă la întreținerea acestui depozit condus de comunitate, găzduiește acum mii de programe dezvoltate de utilizatori, pline de funcții interesante.

Pentru a rezuma, nu există nici un rău real în descărcarea programelor din Arch User Repository, dar numai dacă verificați buid-urile pachetului și fișierele `INSTALL` înainte de instalare. Nerespectarea acestui lucru s-ar putea să nu aibă complicații grave de fiecare dată, deoarece **Trusted User** fac o treabă grozavă îndepărtând pachetele dăunătoare din întreaga bază de date. Dar întotdeauna este mai bine să fii în siguranță decât să-ți pară rău.

Dacă sunteți nou în Arch Linux, luați în considerare mai întâi să învățați cum să descărcați și să instalați pachete din depozitul oficial, apoi, când vă simțiți confortabil cu procesul, continuați cu AUR.
