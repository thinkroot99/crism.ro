<footer>
    [Acasă](index.html) &bull; [Blog](postari.html) &bull; [Despre](despre.html) &bull; [Contact](contact.html) &bull; [Blogroll](blogroll.html) &bull; [Glosar](glosar.html)

    2023, 2024 - crism.ro, Cristian Moldovan, este titularul drepturilor de autor asupra acestei lucrări.
    
    Textul și imaginile din paginile index (acasă), despre și contact sunt sub legea drepturilor de autor - Copyright
    
    Textul și imaginile din restul paginilor sunt libere [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ro).
    Oricine este liber să folosească, să copieze, să modifice și să (re)distribue aceaste informații în condițiile licențelor de mai sus.
    Sit creat cu [pandoc](https://pandoc.org/).
</footer>
