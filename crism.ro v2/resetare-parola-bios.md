% Cum se poate reseta parola la BIOS
% ThinkRoot99

Dacă ești preocupat de securitatea și confidențialitatea datelor tale, cel mai probabil calculatorul tău este protejat de multe parole.

Aceste metode de protecție variază de la protejarea cu parolă a fișierelor și a directoarelor la criptarea fișierelor, directoarelor sau a unităților de stocare.

Dacă doriți o protecție la un alt nivel, puteți folosi o parolă pusă pe tot calculatorul și/sau o parolă pentru a bloca BIOS-ul.

Problema cu parolele este că avem tendința să le uităm și de aceea recomand un carnețel unde să fie trecute parolele cele mai importante.

Acest lucru este perfect normal, mai ales dacă nu ați accesat BIOS-ul destul de des sau dacă folosiți o parolă diferită pentru fiecare cont.

Mai jos vă prezint trei metode despre cum puteți reseta parola la BIOS în cazul în care ați uitat această parolă.

# Resetați BIOS-ul îndepărtând bateria CMOS

1. Deconectați calculatorul de la orice sursă de alimentare.
   - Dacă utilizați un laptop, îndepărtați bateria.
2. Îndepărtați capacul calculatorului și localizați bateria CMOS.
3. Scoateți bateria.
4. Apăsați butonul de alimentare timp de aproximativ 10 secunde.
5. Puneți bateria CMOS la loc.
6. Puneți capacul înapoi sau reasamblați laptopul.
7. Porniți calculaotrul.

> ![Bateria pentru BIOS](img/bios-battery.png)

Veți primi un avertisment care vă spune că data BIOS-ului nu este setată, cea ce este un semn clar că a fost resetat și că nu mai există nici o parolă.

# Eliminați parola folosind o comandă MS-DOS

Trebuie să puneți calculatorul în modul MS-DOS

1. Reporniți / Porniți calculatorul.
2. Accesați meniul `Boot`.
3. Alegeți `Safe Mode with Command Prompt`.

În modul MS-DOS va fi afișată o casetă de dialog neagră cu un promt cmd:

    C:WINDOWS> prompt

1. Tastați `Debug`.
2. Apăsați tasta Enter.
3. Introduceți următoarea comandă de mai jos și apăsați tasta Enter.
    - debug
    - o70 2E
    - o71 FF
    - quit
4. Tastați `Exit` și apăsați tasta Enter.
5. Reporniți calculatorul.

Data viitoare când veți încerca să accesați setările BIOS, nu vi se va mai solicita parola.

# Resetați parola de BIOS cu o parolă din spate

Dacă din anumite motive, bateria CMOS nu poate fi înlăturată sau nu prea ești sigur de tine folosind linia de comandă, singura alegere este să utilizați o parolă din spate (backdoor password).

Această parolă este încorporată în special de producătorul calculatorului și în general nu este cunoscută de majoritatea.

1. Porniți calculatorul
2. Încercați să accesați meniul `Boot` și să introduceți intenționat parola de BIOS greșită de 3 ori.
    - Acest lucru va forța calculatorul să intre într-un mod de blocare împreună cu un mesaj de dezactivare a sistemului asociat unui cod.
3. Notați codul undeva.
4. Accesați de pe telefon sau de pe alt calculator sit-ul: [bios-pw.org](https://bios-pw.org/) și introduceți codul.
5. Apăsați butonul `Get Password`.
6. Sit-ul va genera parole similare cu cele pe care le-ați folosit.

> ![Recuperarea parolei](img/bios-pw.org.png)

Dacă nu vă oferă parola exactă, cele generat vor fi suficient de aproape încât să vă amintiți parola pusă.
