% Proiecte
% ThinkRoot99

Am creat câteva scripturi pentru propriile nevoie, dar pot fi folosi de oricine dorește - acestea se găsesc pe [GitHub](https://github.com/thinkroot99).

Este posibil ca unele scripturi să nu funcționeze cum trebuie pe sistemele voastre. Pentru a funcționa pe sistemele voastre, acestea trebuie ajustate.

> Le folosiți pe propria răspundere, eu nu sunt responsabil pentru problemele apărute atunci când folosiți scripturile.

Sunt scripturi care merg doar pe Arch Linux, sunt scripturi care merg doar pe Ubuntu, sunt scripturi care merg doar pe Fedora și mai sunt scripturi care merg pe orice distribuție Linux.

Când descărcați un script, citiți cu atenție descrierea și informațiile de utilizare a scriptului. Ajustați la nevoie scriptul pentru propriile voastre nevoi.

> **Atenție**: Nu răspund pentru problemele pe care le puteți avea. Le folosiți pe propria răspundere.

| Nume Script                 | Distribuție         | Descriere                                                                            |
| ---------                   | ------              | ---------                                                                            |
| Conversie săptămâni în zile | Site                | Un site web simplu și util pentru conversia rapidă între săptămâni și zile.          |
| DNF-Commands                | Fedora              | Configurează comenzi pentru gestionarea pachetelor în Fedora.                        |
| Generator de parole         | Site                | Generator de parole online gratuit.                                                  |
| Minifetch                   | Toate distribuțiile | MiniFetch este un script Bash care furnizează informații despre sistemul de operare. |
| PassCraft                   | Toate distribuțiile | Script care permite generarea de parole aleatorii.                                   |
| ThinkAUR                    | Arch Linux          | Script pentru instalarea mai ușoară a unui AUR Helper.                               |
| ThinkBackup                 | Toate distribuțiile | Script pentru a face backup sistemului Linux.                                        |
| ThinkCleanA                 | Arch Linux          | Script pentru curățarea sistemului Arch Linux.                                       |
| ThinkCleanB                 | Ubuntu              | Script pentru curățarea sistemului Ubuntu.                                           |
| ThinkCleanF                 | Fedora              | Script pentru curățarea sistemului Fedora.                                           |
| ThinkFTP                    | Toate distribuțiile | Script pentru urcarea fișierelor pe un server FTP.                                   |
| ThinkInstall                | Arch Linux          | Script pentru instalarea pachetelor în Arch Linux.                                   |
| ThinkInstallF               | Fedora              | Script pentru instalarea pachetelor în Fedora.                                       |
| ThinkNetwork                | Arch Linux          | Script pentru schimbarea NetworkManager cu systemd-network.                          |
| ThinkPlayer                 | Toate distribuțiile | Script pentru ascultarea melodiilor în Linux.                                        |
| ThinkRecorder               | Ubuntu              | Script pentru captură video/audio în Linux.                                          |
| ThinkRestore                | Toate distribuțiile | Script pentru restaurarea backup-ului pe sistemele Linux.                            |
| ThinkRSS                    | Toate distribuțiile | Script pentru citirea Feed-urilor RSS în Linux.                                      |
| ThinkStorage                | Toate distribuțiile | Script pentru monitorizarea spațiului de stocare în Linux.                           |
| ThinkTemp                   | Toate distribuțiile | Script pentru monitorizarea temperaturi în Linux.                                    |
| ThinkUpdate                 | Arch Linux          | Script pentru actualizare sistemului Arch Linux.                                     |
| ThinkUpdateDNF              | Fedora              | Script pentru schimbarea informațiilor din dnf.conf.                                 |
| ThinkUpdateU                | Ubuntu              | Script pentru actualizare sistemului Ubuntu.                                         |
