# Am schimat din nou distribuțiile

Am avut Fedora Rawhide GNOME și Arch Linux i3. După multe articole interesante pe care le-am citit despre BSD-uri, SystemD și un pic de Red Hat, am ajuns la concluzia că vreau FreeBSD, OpenBSD sau Artix Linux.

Cum nu am chef să învăț ceva nou, plus că anumite programe proprietărești nu funcționează pe BSD-uri, am zis că instalez Artix Linux.

Zis și făcut. Am descărcat ce am avut nevoie. Am refăcut memoria USB cu Ventoy și am copiat imaginile ISO acolo. Am repornit laptopul și am intrat pe memoria USB de unde am pornit Artix Linux.

Bineînțeles că nu știu toate comenzile de instalare și am apelat la un tutorial exact pentru ce am avut nevoie. Tutorialul a mers până la un anume punct, unde lucrurile au devenit un pic ciudate pentru că în tutorial la o anumită comanda apărea un rezultat și în situația mea, exact la aceași comandă apărea alt rezultat, și de acolo nu a mai mers nimic.

Fiind noapte și destul de târziu încăt să reiau instalarea, am trecut rapid la instalarea Arch-ului cu al său `archinstall`. Am instalat foarte repede Arch-ul, dar am făcut o instalare minimă - deci nu aveam nimic în afară de TTY și câteva pachete de bază care să facă Arch-ul funcțional.

Am lăsat laptopul așa fiind destul de târziu și am revenit a doua zi la el. Am trecut la instalarea lui i3 și povestea continuă în articol-ul următor :)

PS: Nu mă las bătut de Artix Linux. Mai am un laptop și reiau instalarea acolo :)
