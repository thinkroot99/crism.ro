#!/bin/bash

# Directorul sursă pentru fișierele Markdown
source_dir="dir_md"

# Directorul destinație pentru fișierele HTML
dest_dir="dir_html"

# Template HTML
template_html="template.html"

# Verificăm dacă directorul sursă există
if [ ! -d "$source_dir" ]; then
    echo "Source directory not found: $source_dir"
    exit 1
fi

# Verificăm dacă directorul destinație există sau îl cream dacă nu există
if [ ! -d "$dest_dir" ]; then
    mkdir -p "$dest_dir"
fi

# Verificăm dacă template-ul există
if [ ! -f "$template_html" ]; then
    echo "Template file not found: $template_html"
    exit 1
fi

# Funcție pentru conversia unui fișier Markdown în HTML
markdown2html() {
    local md_file="$1"
    local html_file="$dest_dir/$(basename "$md_file" .md).html"
    
    # Convertim fișierul Markdown în HTML folosind Pandoc
    pandoc --from=markdown --to=html "$md_file" -o "$html_file" || { echo "Error converting $md_file to HTML."; exit 1; }

    echo "HTML file generated: $html_file"
}

# Parcurgem toate fișierele Markdown din directorul sursă
for md_file in "$source_dir"/*.md; do
    # Verificăm dacă fișierul este un fișier Markdown
    if [ -f "$md_file" ]; then
        # Apelăm funcția pentru conversia fișierului
        markdown2html "$md_file"
    fi
done
