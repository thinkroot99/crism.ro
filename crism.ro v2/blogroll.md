% Blogroll

# Ce este blogroll?

Un blogroll este o listă de linkuri către alte bloguri sau site-uri web pe care un autor le recomandă sau le urmărește și le include pe propria sa pagină web sau blog. Aceasta poate fi o modalitate de a promova alte surse de conținut și de a conecta cititorii cu alte resurse relevante sau de interes pentru subiectele abordate în blogul respectiv.

# Site-uri intersante pentru mine

Am un cont pe [The Old Reader](https://theoldreader.com/profile/crismblog) unde sunt abonat la mai multe site-uri și blog-uri. Majoritatea sunt din domeniu IT&C.

În lista de mai jos sunt trecute site-uri și bloguri care sunt interesante și merită a fi promovate.

- [The Web Is Fucked](https://thewebisfucked.com/)
- [Start a Fucking Blog](https://startafuckingblog.com/)
- [Motherfucking Website](https://motherfuckingwebsite.com/)
- [smolweb](https://smolweb.org/index.html)
- [bt](https://btxx.org/)
- [Trag Date](http://tragdate.ninja/)
- [Lennart Poettering](https://0pointer.de/lennart/)
- [David Heinemeier Hansson](https://world.hey.com/dhh)

