% 7 aplicații pentru telefon interesante, ilegale și ciudate 
% ThinkRoot99

Mai jos vă prezint o listă cu 7 aplicații pentru telefon care sunt interesante, ciudate, ilegale și personal la unele nu le văd rostul lor.

Aceste aplicații sunt:

# 1. Randonautica

Aceasta aplicație vă oferă coordonate complet aleatorii pe o rază stabilită. Puteți alege dacă doriți să explorați locația sau nu.

# 2. AndroDumpper

AndroDumpper este o aplicație pentru a verifica nivelul de securitate al rețelei Wi-Fi.

# 3. RIP VIP

RIP VIP este cea mai bună aplicație pentru a fi la curent cu recentele decese ale unor persoane celebre, celebrităti și VIP-uri din întreaga lume.

# 4. Secret SMS Replicator

Această aplicație redirecționează automat fiecare mesaj, pe care îl primește persoana care are aplicația pe telefon, spre altă/alt persoană/telefon, poate trimite și notificările.

> Notă: Nu folosiți o astfel de aplicație pentru că puteți avea probleme foarte mari.

# 5. Girls Around Me

Această aplicație vă arată fetele care se află în jurul vostru. Aplicația obține datele de localizare din imaginile postate pe rețelele de socializare.

`Descrierea oficială este: Girls Around Me este o aplicație revoluționară de scanare a orașului. Poți vedea unde se întâlnesc fetele și băieți în zona ta.`

> Notă pentru fete: Țineți GPS-ul oprit atunci când nu aveți nevoie de el.

# 6.  Spirit Story Box

Spirit Story Box este un instrument care captează frecvențe electromagnetice pentru a detecta fantomele și spiritele.

Dacă un spirit se află în preajma ta, aceasta va afișa un mesaj aleatoriu pe ecranul telefonului.

# 7. Situationist

Situationist este o aplicație care te conectează cu un străin oarecare din jurul tău și poți comunica cu acesta.

Aplicația va împărtăși locația și imaginile tale cu cealaltă persoană și tu vei primi locația și pozele persoanei cu care comunici.

Apoi, această aplicație te provoacă să te întâlnești cu acel străin și nu numai.

> Notă: Nu folosiți această aplicație pentru că nu se știe peste ce fel de persoane puteți da.

# De final

Acestea sunt cele 7 aplicații pentru telefon, unele sunt interesante, altele sunt ilegale și puteți avea probleme mari dacă le folosiți și mai sunt câteva la care chiar nu le văd rostul.

Atenți mare la folosirea acestor aplicații și le folosiți pe propria voastră răspundere.

Eu doar le-am prezentat și nu răspund pentru nimeni, de aceea nu am adăugat legături spre aplicații.


