% Actualizare GRUB în Fedora

De câteva ori pe an reinstalez Fedora din diferite motive și la scrierea acestui articol am instalat Fedora Rawhide. Și dintr-un anume motive am fost nevoit să actualizez GRUB-ul.

Fiind utilizator de Linux de ceva ani, în mod normal, ar trebui să știu comanda de actualizare a GRUB-ului în Fedora, dar nu o știu :).

Și astfel am ajuns să fac acest mic articol unde să trec comanda și să nu mai fiu nevoit să o caut pe internet.

Dar cum treaba asta nu este așa de simplă, actualizarea GRUB-lui are două metode pentru Fedora - și acesstea sunt:

# Metoda 1

Se rulează comanda:

`sudo grub2-mkconfig -o /etc/grub2.cfg`

Urmată de comanda următoare:

`sudo grub2-mkconfig -o /etc/grub2-efi.cfg`

După exacutarea acestor două comenzi de mai sus se repornește sistemul.

# Metoda 2

În mod alternativ se poate folosi comanda următoare:

`sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg`

Iar după executarea comenzi, ca de obicei, se repornește sistemul.

# Care metoda este mai bună?

Ambele metode îndeplinesc același obiectiv de actualizare a GRUB.
Cu toate aceste, diferență consta în locul în care este salvat fișierul de configurare.

- Metoda 1 actualizează atât configuratiile BIOS, cât și cele UEFI.
- Metoda 2 actualizează în mod specific configurațiile UEFI.

Alegerea dintre cele două metode depinde de configurarea sistemului tău și de preferințele personale. Dacă nu ști ce să alegi atunci încearcă ambele metode și astfel poți observa care funcționează mai bine pentru sistemul tău.

