% Toată postările
% ThinkRoot99

Cauți ceva anume? Utilizează funcția de căutare încorporată în navigator!

##### Diverse

- De ce [am renunțat](am-renuntat-la-arch.html) la Arch Linux
- De ce folosesc [Arch și Sway](de-arch-sway.html)
- De ce un sit [nou](sit-nou.html)?

##### Arch Linux

- Cum se [remediază](remediaza-pacman.html) „Pacman is curently in use, please wait”
- Instalarea [Jekyll](jekyll.html) pe Arch Linux
- Creați o [listă a pachetelor](salvare-lista-pachete.html) instalate și instalați-le mai târziu în Arch Linux
- [paru](paru.html) AUR Helper: Ce este și cum se instalează
- [yay](yay.html) AUR Helper: Ce este și cum se instalează
- Ce este Arch User Repository [(AUR)](aur.html)?
- Cum se folosește [pacman](pacman-in-arch.html) în Arch Linux
- Ce este [Arch Linux](arch-linux.html)?
- Ce trebuie să știți [despre](despre-arch-linux.html) Arch Linux înainte de al instala

##### Tutoriale GNU/Linux

- Cum se șterg [aplicațiile](chrome-app.html) din Chrome
- De ce scurtătura [Alt+F2](alt-f2.html) este importantă în GNU/Linux
- Monitorizarea activități [Ethernet](monitorizare-ethernet.html) cu Arpwatch
- Sfaturi și trucuri pentru [liniei de comandă](linia-de-comanda.html)
- Lucruri de făcut dupa instalarea [i3wm](i3wm.html)
- Afișarea sistemului de fișiere într-un mod mai plăcut cu [lfs](lfs.html)
- Clonați și restaurați cu [comanda dd](comanda-dd.html)
- Cum să [reporniți și să închideți](repornie-si-inchidere-sistem.html) un sistem
- Comanda [htop](comanda-htop.html)
- Convertirea unui PDF cu [GIMP](pdf-cu-gimp.html)
- Convertirea unui [PDF](pdf-în-terminal.html) în terminal
- Spotify pentru [terminal](spotify-pentru-terminal.html)

##### Ubuntu, Fedora etc.

- [CentOS Stream](centos-stream.html): Tot ce trebui să știți
- Ce este [Fedora](fedora.html)? Tot ce trebui să știți
- Resetare [parolă de root](resetare-parola-root.html) în Ubuntu - metoda 2
- Resetare [parolă](resetare-parola-de-root.html) de root în Ubuntu - metoda 1
- Dezactivare [Snaps](dezactivare-snaps-2.html) în Ubuntu 21.10
- Dezactivare [Snaps](dezactivare-snaps.html) în Ubuntu 20.04

##### Explicații

- [Extender Wi-Fi vs. rețea Mesh](extender-wi-fi-vs-retea-mesh.html): care este diferența?
- Ce este [G'MIC](gmic-si-gimp.html) pentru GIMP
- Care este [diferența](internet-vs-web.html) dintre Internet și World Wide Web
- [Web1 vs Web2 vs Web3](web1-web2-web3.html): Care este diferența?
- Ce este [Fediverse](fediverse.html)
- [5 motive](motive-linia-de-comanda.html) pentru care GNU/Linux folosește atât de mult linia de comandă
- [Concepte](concepte-vechi.html) care sunt mai vechi decât crezi
- 5 canale de unde poți învăța [Kdenlive](invata-kdenlive.html)
- Ce este o distribuție [Rolling Release](rolling-release.html)?
- Care este diferența dintre [distribuții](distributii-gnu-linux.html) daca toate sunt GNU/Linux
- Ce este [GUI](gui.html)?
- De ce există atât de multe [distribuții](distributii.html)?
- Ce este [TTY](tty.html)?
- [Comanda](nu-trebuie-rulata.html) care nu trebuie rulată niciodată
- Merge [GNU/Linux](fara-mediu-de-lucru.html) fără un mediu de lucru?
- Tipuri de [distribuții](tipuri-de-distributii.html) GNU/Linux explicate
- [Desktop Environemtn si Window Manager](de-wm.html)
- [Dosar sau Director?](folder-sau-directory.html)

##### Windows

- Cum să [ocoliți cerințele](cerinte-windows-11.html) de instalare pentru Windows 11
- [Criptarea](criptare-fisiere-windows.html) fișierelor și a directoarelor în Windows
- Cum se poate reseta parola la [BIOS](resetare-parola-bios.html)
- [Autentificare automat](autentificare-automata.html) în Windows 10
- Conectare la un server FTP cu [File Explorer](conectare-ftp-cu-file-explorer.html) în Windows 10
