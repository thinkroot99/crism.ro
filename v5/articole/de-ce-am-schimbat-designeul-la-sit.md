# De ce am schimbat designe-ul la sit

Vechiul designe sau versiunea 3 a sit-ului nu a fost o versiunea rea și urâtă, mi-a plăcut mult cum a arătat sit-ul. Dar de când am aflat de conceptul „*small web*”, pe care îl promovez și în România prin intermediul sit-ului [netmic.ro](https://netmic.ro), am vrut să fac sit-ul cât mai mic posibil.

Nu știu cât de mare a fost sit-ul în celelalte versiuni [v1 și v2](de-ce-un-designe-nou.html), dar versiunea 3 a avut mărimea de 124.94 KB conform instrucțiunilor oferite de [512kb.club](https://512kb.club/) și acest lucru se poate vedea și în imaginea următoare:

![https://radar.cloudflare.com/scan/9fe7f23e-fd82-41af-8dbc-0424bfe03237/summary](/img/v3.webp)

Cu noul designe am adoptat conceptul „*small web*” și am trecut la un cod HTML și CSS cât mai simplu și cât mai mic posibil conform instrucțiunilor [netmic.ro](https://netmic.ro) și acum mărimea sit-ului este de 11.29 KB așa cum se poate vedea în imaginea de mai jos.

![https://radar.cloudflare.com/scan/ebc4ceb8-f666-4bbb-a00b-12061e66ff63/summary](/img/v4.webp)

Acesta este motivul pentru care am tot schimbat designe-ul sit-ului așa de des în ultima lună. Dar o dată cu simplificarea codului am schimbat și paleta de culori; am trecut de la [Nord Theme](https://www.nordtheme.com/) la [Gruvbox Dark mode](https://github.com/morhetz/gruvbox) pentru că paleta de culori din urmă mi se pare mult mai plăcută vizual și nici nu deranjează așa mult la ochi.

Pe scurt:

- am simplificat codul HTML și CSS.
- am scăzut mărimea sit-ului.
- am schimbat paleta de culori. 
- am eliminat butonul de radio pentru că conținea JavaScript și încărca sit-ul prea mult.
- am modificat mărimea imaginilor și am trecut de la formatul PNG la WEBP.

Un sit mai mic și mai plăcut. Și astfel încurajez pe toată lumea care deține un sit sau blog să facă trecerea la un sit static, simplu și frumos.
