# România nu are nimic autentic

România nu are nimic autentic, nu are un cuvânt autentic, nu are o mâncare autentică, nu are absolut nimic. Orice cuvânt se trage de la Romani, Greci, Slavi sau alte popoare. Mâncarea la fel este, este împrumutată de la toți vecini care sunt popoare diferite de Români.

Pe teritoriul României, Bulgariei, Ungariei și alte întinderi, în urmă cu ceva ani au trăit Daci, Geto-Traci și alte popoare care nu au legătura cu Slavi, Maghiari, Greci sau Romani. Și ce este curios, este faptul că poporul Dac și cele care au existat înaintea Dacilor nu au plecat din această zonă mare.

Chiar dacă au venit popoare ca Slavi și Maghiari care nu au mai plecat, poporul Dac nu a plecat din zona și asta o spune toate studiile și istoria, nu o spun eu. Singuri care au plecat au fost Romani - cel puțin din cunoștințele mele - care și așa nu au stat prea mult pe aici.

Deci, partea interesanta este așa cum am scris mai sus. Poporul Dac nu a plecat din zonă și poporul Român s-a format din unirea popoarelor Dacice, Romane și alte popoare. Dar „*sursa*” poporului Român este și a fost poporul Dac, care a avut un port, o limbă, un obicei, etc.

Interesant este ca Românii nu au moștenit nimic de la Daci - nici limba, nici mâncarea, nici portul, absolut nimic.

Personal nu cred aceste minciuni pentru că un popor nu avea cum să dispară după 100 și ceva de ani, cât au stat Romani pe aici. Iar Englezii nu și-au pierdut istoria, limba, portul, etc - mai ales că Romani au stat mult mai mult acolo decât în Dacia. Să nu mai vorbesc de Franța, Spania, Grecia, etc.

Personal cred că totul este o minciună mare începută cu ceva ani în urmă, ca poporul Român să își piardă identitatea și istoria.