% Creați o listă a pachetelor instalate și instalați-le mai târziu în Arch Linux
% ThinRoot99

În acest articol vă voi arăta cum să creați o listă cu pachetele instalate în Arch Linux și să le instalați mai târziu într-un sistem nou.

Această metodă poate fi utilă pentru raportarea defectelor sau pentru a discuta despre pachetele instalate.

Mai important, dacă doriți un set similar de pachete într-un grup de sisteme Arch Linux, aceasta este o astfel de modalitate de a realiza acest lucru.

# Creați o listă a pachetelor instalate în Arch Linux

Generați lista pachetelor instalate în mod explicit cu ajutorul comenzii:

    $ sudo pacman -Qqe > pkglist.txt

Această comandă va crea o listă a pachetelor instalate în mod explicit în ordine alfabetică și le va salva într-un fișier text numit `pkglist.txt`.

Ce înseamnă comanda:

- `Q` – Interogează baza de date a pachetului. Această opțiune vă permite să vizualizați pachetele instalate și fișierele acestora, alte meta-informații utile despre pachetele individuale (dependențe, conflicte, data instalării, data construirii, dimensiune).
- `q` – Afișează mai puține informații pentru anumite operațiuni de interogare. Acest lucru este util atunci când ieșirea lui pacman este procesată într-un script.
- `e` – Listează pachetele instalate în mod explicit care nu sunt solicitate de nici un alt pachet.
- `pkglist.txt` – Este fișierul de ieșire în care se stochează lista de fișiere instalate.

Salvați fișierul `pkglist.txt` pe o unitate USB sau într-un loc sigur.

# Instalați pachetele din listă în Arch Linux

După reinstalarea / instalarea sistemului, copiați fișierul `pkglist.txt` în sistemul nou instalat și rulați următoarea comandă penru a instala pachetele din lista de rezeră.

    $ sudo pacman -S - < pkglist.txt

În cazul în care lista de rezervă include pachete străine, cum ar fi pachetele din AUR, ar trebui să le eliminați-le mai întâi pe acestea și apoi sa instalați restul pachetelor folosind comanda:

    $ sudo pacman -S $(comm -12 <(pacman -Slq | sort) <(sort pkglist.txt))

Comanda de mai sus va elimina pachetele străine. Tastați `y` și apăsați `ENTER` pentru a le elimina. În cele din urmă, tastați `y` pentru a instala restul pachetelor din listă.

> ![Install Packages in Arch Linux](img/Install-packages-in-Arch-Linux.webp)

Nu este nevoie să instalați toate pachetele unul câte unul. Pacman va citi lista și va instala pachetele listate în ea.

Pentru a elimina toate pachetele de pe sistemul nou instalat care nu sunt menționate în lista de rezervă, rulați comanda:

    $ sudo pacman -Rsu $(comm -23 <(pacman -Qq | sort) <(sort pkglist.txt))

Pentru mai multe detalii, consultați paginile din manual.

    $ man pacman

Această metodă utilă vă va face viața mai ușoară atunci când doriți să reinstalați distribuția Arch Linux sau derivatele sale.

Nu vă derajați să instalați pachetele unul câte unul. Pur și simplu exportați lista de pachete instalate într-un fișier și salvați-o într-un loc sigur.

Puteți utiliza aceeași lista pentru orice sistem Arch Linux nou instalat în casă sau birou. Astfel veți economisi mult timp de la căutarea și instalarea pachetelor.
