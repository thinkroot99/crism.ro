# Autentificarea automată în TTY

De când am [instalat](am-schimbat-din-nou-distributiile.html) ultima dată Arch Linux a trecut două săptămâni și până astăzi nu am avut setată autentificarea automată în sistem. La început nu am avut nici o problemă să tastez numele de utilizator și parola, dar am început să mă satur :)

Astăzi am făcut setările necesare pentru autentificarea automată în TTY pentru că nu am un manager de afișare (display manager) instalat.

Pentru început a trebuit să instalez pachetul `util-linux` rulând comanda:

```bash
sudo pacman -S util-linux
```

Pe urmă a trebuie să creez directorul `getty@tty1.service.d` pentru că nu a existat și am rulat comanda de mai jos:

```bash
sudo mkdir /etc/systemd/system/getty@tty1.service.d/
```

Am intrat în director folosind comanda `cd /etc/systemd/system/getty@tty1.service.d/`, unde am creat fișierul `autologin.conf`.

```bash
sudo touch autologin.conf
sudo nvim autologin.conf
```

În acest fișier nou creat am trecut următoarele linii:

```cod
[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -f -- \\u' --noclear --autologin username %I $TERM
```

Am înlocuit `username` cu numele meu de utilizator și am salvat fișierul.

După care am rulat următoarele comenzi:

```bash
systemctl daemon-reload
systemctl restart getty@tty1
```

Asta a fost tot ce am făcut pentru ca sistemul să se autentifice automat în TTY în Arch Linux.