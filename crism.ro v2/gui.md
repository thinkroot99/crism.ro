% Ce este GUI?
% ThinkRoot99

GUI-urile sunt omniprezente, dar este posibil să nu știi nici măcar ce sunt și aproape sigur că le iei de la sine înțeles. Totuși, termenul este atât de integral în calcul încât merită o explicație și un context.

Deci, ce este un GUI?

# Ce înseamnă GUI?

În funcție de modul în care definiți calculatorul, acesta există într-o formă sau alta de mult timp.

De exemplu, mesageria instant de la distanță a fost realizată pentru prima dată cu telegraful în 1984. „*A-machine*” al lui Alan Turning a fost conceput în 1936 și a fost în esență un calculator timpuriu.

Cu toate acestea, niciunul dintre acestea nu seamănă prea mult cu calculatoarele de astăzi, în mare parte pentru că le lipsește o interfață grafică.

GUI înseamnă „*graphic user interface / interfață grafică cu utilizatorul*” sau „*graphical user interface*”. Mulți identifică prima interfață grafică ca fiind cea afișată de Doug Engelbart în „*Mother of All Demos / Mama tuturor demonstrațiilor*” din 1968, care acoperea mouse-ul, editarea textului de la distanță, legăturile și comenzile rapide.

> [![YouTube Video](img/1968-Mother-of-All-Demos-by-Doug-Engelbart-and-Team.webp)](https://www.youtube.com/watch?v=B6rKUf9DWRI)

Aceasta este o întrebare în afara drumului, dar ce este o interfață grafică cu utilizatorul? Pentru a înțelege asta, trebuie să înțelegem termenul mai larg „*user interface / interfața cu utilizatorul*”.

# GUI vs UI

Dacă ne uităm la orice mașină, aceasta va avea o modalitate prin care utilizatorii pot interacționa cu ea. Această parte a mașini este „*interfața cu utilizatorul*” sau „*UI*”. O interfață grafică cu utilizatorul este componenta de afișare electronică (dacă există una) a interfeței de utilizare mai mare a unei mașini (dacă există una) care permite utilizatorilor să selecteze pictograme și imagini, mai degrabă decât numere sau text.

Un cronomentru sau un cronometru de bucătărie are o interfață cu utilizatorul, dar probabil că nu încorporează o componentă de afișare grafică. Un touchpad pentru confirmarea unei tranzacții la magazin poate consta doar dintr-o interfață grafică, deoarece, în ceea ce vă privește, nu interacționați cu nicio parte a aparatului, în afară de afișaj.

Deși există aceste exemple de dispozitive fără GUI și dispozitive cu GUI, majoritatea dispozitivelor încorporează atât elemente GUI, cât și elemnte UI.

Luați în considerare dispozitivul de pe care citiți acest articol: acțiunile voastre sunt afișate în timp real pe GUI, dar utilizați și o tastatură sau butoane pentru a naviga prin alte comenzi și funcții.

Aproape fiecare dispozitiv cu care majoritatea oamenilor interacționează în prezent încorporează o interfață grafică, adesea pentru a confirma sau afișa acțiuni, deoarece folosim dispozitive din ce în ce mai complexe. În ultimele decenii, din ce în ce mai multe dintre aceste dispozitive pe care le folosim în fiecare zi au încorporate o interfață grafică.

# Ce este GUI în istoria calculatoarelor?

Mother of All Demos a introdus lumea în interfața grafică cu utilizatorul în 1968, dar probabil primul exemplu de GUI a funcționat cu șapte ani mai devreme. Sistemul informatic SAGE al IBM a fost utilizat cu un „*pistol ușor*” îndreptat către o hartă afișată. În uz până în 1983, este posibil ca Engelbart să nu fi știu sau să nu poată vorbi despre SAGE.

Mother of All Demos a introdus în primul rând procesarea de text, inclusiv colaboararea la distanță. În timp ce navigarea bazată pe text este de obicei exclusă din definițiile GUI, demonstrația a inclus și legături și fișiere. Ambele sunt exemple timpurii de arhitectură de calculator mai creativă care încorporează structuri precum ferestre și file.

A spune că SAGE l-a învins pe Engelbart cu câțiva ani nu înseamnă că trebuie eliminat din ecuație. Engelbart era încă cu mult înaintea timpului său. [Experimentele IBM](https://www.ibm.com/ibm/history/exhibits/pc/pc_1.html) cu primele calculatoare personale au creat SCAMP în 1973. În același an, [Xerox Alto](http://interface-experience.org/objects/xerox-alto/) a devenit primul calculator disponibil pe scară largă care a integrat scheme GUI moderne.

De acolo, puteți scrie o carte care să urmărească evoluțiile GUI de la un sistem de operare la alt sistem de operare. Între 1973 și prezent, interacțiunea cu calculatorul a devenit din ce în ce mai mult bazată pe grafică, până în punctul în care multe operațiuni pot fi finalizate fără nici un text.


> ![Același gen de evoluție a avut loc și la telefoane](img/Windows-3.1-GUI-Example.png)

# Exemple de GUI la dispozitive mobile

Ca și în cazul calculatoarelor, afișajele digitale existau pe telefoane înainte de ceea ce numim „*interfață grafică cu utilizatorul*”. Aceste afișaje digitale timpurii au arătat utilizatorului ce numere au fost introduse și, mai târziu, ce număr de telefon suna. Cu toate acestea, nu permiteau interacțiunea sau prezentau elemente grafice precum prictogramele aplicațiilor utilizate astăzi.

În 1999. Blackberry 850 a devenit unul dintre primele dispozitive mobile conectate la internet. Deși includea elementele de bază ale unui GUI, afișajul era monocrom și se naviga cu o tastatură convețională.

În 2003, Palm Treo 600 a combinat Asistentul Digital Personal cu un telefon mobil pentru a crea un precursor al telefoanelor inteligente moderne. PDA-urile se întorc cu aproape zece ani în urmă, iar telefoanele mobile cu zece mai departe, dar ambele prezentau meniuri text sau numerice mai degrabă decât plăcile grafice introduse de Palm Treo.

Primul telefon inteligent cu ecran tactil, iPhone, a fost anunțat în 2007. Meniurile grafice care stau la baza GUI erau deja o caracteristică în dispozitivele mobile, la fel și ecranele tactile, deși se foloseau cu un stylus.

Descoperirea iPhone-ului a făcut din GUI aproape întreagă suprafață a ecranului o interfață grafică cu utilizatorul pentru dispozitive.

> [![YouTube Video](img/Steve-Jobs-introduces-iPhone-in-2007.webp)](https://www.youtube.com/watch?v=MnrJzXM7a6o)

GUI cu ecran tactil care a debutat cu iPhone-ul a deschis calea pentru omniprezența acestui tip de interfață care și-a găsit locul de atunci în orice, de la automobile la frigidere.

# Probleme cu GUI

La prima vedere, o bară de instrumente GUI ar putea părea mai ordonată decât o bară de instrumente text, dar ce se întâmplă dacă un utilizator nu știe ce înseamnă grafica? Pentru ca un GUI să fie eficient, pictogramele afișate trebuie să transmită eficient acțiunile pe care le efectuează.

Uneori, dezvoltatorii GUI și designerii rezolvă această problemă în moduri aproape comice învechite.

Pictograma „*Save / Salvare*” din multe aplicații este încă o imagine a unei dischete, un artefact necunoscut pentru mulți utilizatori mai tineri de calculatoare. În mod similar, când ați văzut ultima dată un coș de gunoi de modă veche cu capac metalic în stil „*Delete / Ștergeți*”?

În plus, GUI-urile sunt mai pretențioase în ceea ce privește calculul și puterea decât modelele de interfață mai simple. Un exemplu excelent în acest sens este boot-ul fără GUI. Boot-ul fără GUI elimină bara de încărcare la pornirea aplicației.

Fără a alimenta și interfața grafică, procesul de pornire merge mai rapid, dar nici nu vă puteți da seama dacă sistemul are probleme.

> [![YouTube Video](img/Which-is-faster-No-GUI-Boot-or-GUI-boot.webp)](https://www.youtube.com/watch?v=YTfw3kWUTR8)

GUI-urile creează, de asemenea, încă un pas în procesul de proiectare, dar soluții precum [Qt GUI](https://doc.qt.io/qt-5/qtgui-index.html) facilitează pentru designeri să încorporeze un GUI în produsele și aplicațile lor.

# Care este GUI-ul tău preferat?

Interfața grafică cu utilizatorul ne-a schimbat treptat viața de zeci de ani. Formatul înlocuiește navigarea prin meniul text sau numeric cu selectarea pictogramelor ilustrative, făcând dispozitivele mai personalizabile și mai ușor de utilizat.
