% Care este diferența dintre distribuții daca toate sunt GNU/Linux
% ThinkRoot99

Când sunteți în căutarea unei noi distribuții GNU/Linux pe care să o instalați, observați două lucruri: numele și mediul de lucru.

O navigare rapidă arată diferențe evidente între Ubuntu, Fedora, Linux Mint, Debian, openSUSE și multe alte variante de GNU/Linux.

# 5 diferențe cheie între distribuțiile GNU/Linux

Căutați o nouă distribuție GNU/Linux? La un moment dat vă veți întreba de ce există atât de multe distribuții diferite, mai ales dacă toate sunt GNU/Linux.

Poate știți că Windows 10 are mai multe ediții, dar acestea nu sunt comercializate ca sisteme de operare complet separate. Între timp, macOS are o singură variantă (cel puțin pentru calculatoare). Așadar, de ce există atât de multe distribuții GNU/Linux diferite?

Dezvoltarea distribuțiilor GNU/Linux se datorează unor grupuri diferite, dar disparate, care colaborează între ele. De-a lungul anilor, de la prima lansare a nucleului Linux, această abordare a dus la crearea de diferite distribuții.

La bază, este GNU/Linux. Dar veți observa unele diferențe între versiunile GNU/Linux, în special:

- Mediile de lucru (Desktop Environments)
- Managerii de pachete (Package managers)
- Servere de afișare (Display servers)
- Obiective și scopuri
- Filozofia sursei deschise

Dar cât de mult contează cu adevărat aceste diferențe?

## 1. Mediile de lucru

> ![GNOME Desktop Environment](img/GNOME-Desktop-Environment.png)

Cele mai multe distribuții par să difere pur și simplu în funcție de mediul de lucru pe care îl foloses.

De exemplu, Ubuntu oferă mai multe medii de lucru în funcție de varianta pe care o alegeți. Puteți avea:

- **Ubuntu** – versiunea principală include mediul de lucru **GNOME**
- **Kubuntu** – include mediul de lucru **Plasma** (KDE sau KDE Plasma)
- **Lubuntu** – include mediul de lucru The Lightweight Qt Desktop Environment (**LXQt**)
- **Ubuntu Budgie** – include mediul de lucru Budgie Desktop
- **Ubuntu MATE** – include mediul de lucru MATE Desktop Environment (**MATE**)
- **Xubuntu** – include mediul de lucru Xfce Desktop Environment (**Xfce** sau XFCE)

Cu toate acestea, alte distribuții au la dispoziție o selecție modestă de medii de lucru, adesea oferite sub formă de „*spins*” care conține diferite medii de lucru. Un exemplu de distribuție care face acest lucru este Fedora. Între timp, veți mai găsi și mediu de lucru Panteon de pe Elementary OS.

## 2. Managerii de pachete și alte tehnologii

Persoanele care se află în spatele fiecărei distribuții GNU/Linux pot alege ce programe includ, cum ar fi managerii de fișiere (file managers) și managerii de pachete.

Liderii echipelor de dezvoltare au aceste opțiuni deoarece fiecare categorie de programe pot avea mai multe aplicații.

De exemplu, pentru GNU/Linux sunt disponibile mai multe managere de fișiere, cum ar fi **Nautilus** și **Konqueror**, fiecare dintre ele oferind o modalitate diferită de a naviga prin fișiere.

Un alt exemplu este reprezentat de managerii de pachete. Diferite metode de instalare a programelor sunt incluse în fiecare distribuție, dar toate au un manager de pachete la bază.

Pe distribuțiile bazate pe Debian, cum ar fi Ubuntu și Linux Mint, se alege `dpkg`, accesat prin intermediul `apt`. Pentru CentOS, `RPM` este managerul de pachete, supus comenzilor `dnf`.

## 3. Diferite servere de afișare

Sub capota GNU/Linux veți găsi o selecție de instrumente, aplicații, procese și servere care determină modul de funcționare.

Un exemplu cheie în acest sens este serverul de afișare. Acest program coordonează datele între hardware și afișaj, permițând utilizatorului să interacționeze cu interfața grafică cu utilizatorul (GUI).

Din punct de vedere istoric, serverul `X.Org` a fost cel mai des utilizat. Cu toate acestea sunt disponibile diverse alternative, cum ar fi `Mir` și `SurfaceFlinger`, care este utilizat pe Android (care utilizează nucleul Linux). Serverul de afișare `Wayland` este văzut ca fiind viitorul pentru GNU/Linux, majoritatea distribuțiilor populare adoptându-l.

## 4. Obiective și scopuri

> [![YouTube Video](img/How-To-Play-Windows-Games-On-Linux.webp)](https://www.youtube.com/watch?v=vXTPyrKICkA)

Unele distribuții există pentru că le plac anumite aspecte ale unei distribuții existente, dar doresc să înlocuiască anumite programe. Între timp, distribuțiile GNU/Linux pot diferi în ceea ce privește obiectivele lor.

De exemplu, Linux Mint se bazează pe Ubuntu, dar conține diferite instrumente de sistem, mediu de lucru și o temă verde-mentă. Scopul său principal este de a oferi un punct de pornire simplu și complet pentru ca utilizatorii de Windows și macOS să înceapă să folosească GNU/Linux.

În mod similar, Debian își propune să ofere o distribuție extrem de stabilă (și, prin urmare, conține programe vechi).

Dincolo de domeniul distribuțiilor universale, unele proiecte au scopuri specifice. De exemplu, distribuțiile pentru jocuri, cum ar fi **Steam OS**, sau distribuțiile multimedia, cum ar fi **Fedora Designe Suite**.

## 5. Filozofia Open Source vs. Filozofia proprietară

Deși **GNU/Linux** este probabil cel mai cunoscut proiect open source, nu toate distribuțiile sunt 100% open source.

Liderii de proiecte au poziții diferite cu privire la sursele deschise, ceea ce poate fi un factor decisiv pentru puriștii surselor deschise.

De exemplu, Ubuntu nu are nici o problemă în a include programe proprietare în depozitele sale. Veți descoperi că clientul de jocuri Steam este disponibil cu ușurință, în timp ce driverele grafice de la AMD și Nvidia pot fi instalat. Fedora, dimpotrivă, are o politică puternică de sursă deschisă care o împiedică să includă orice program proprietar în depozitele sale.

Bineînțeles, la sfârșitul zilei, puteți face orice doriți cu distribuția GNU/Linux pe care ați ales-o. Indiferent de politicile proiectului, nu există nici un blocaj cu privire la ceea ce instalați.

Pe scurt, în timp ce multe distribuții GNU/Linux ar putea avea obiective nobile de conformitate cu sursele deschise, nu toate au surse deschise.

# Ce au în comun toate distribuțiile

În ciuda acestor diferențe, toate distribuțiile GNU/Linux sunt considerate a fi GNU/Linux: dar de ce?

Toate au cel puțin un lucru în comun: nucleul Linux. Această bucată de program reprezintă nucleul sistemului de operare, făcând legătura între programul cu care interacționați (de exemplu, navigatorul web) și hardware-ul de bază care face toată treaba. De asemenea, include numeroase drivere de dispozitiv pentru a oferi suport pentru orice hardware pe care îl aveți la dispoziție.

De aceea, este important să păstrați nucleul actualizat sau să compilați singur nucleul dacă aveți cerințe speciale. Dezvoltatorii din întreaga lume contribuie la nucleu, alături de creatorul său, [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds).

**Folosiți diferențele GNU/Linux pentru a alege distribuția potrivită pentru voi**

Nu toate distribuțiile sunt destinate tuturor, așa că alegeți-o pe cea care se potrivește cel mai bine pentru tine și cu preferințele voastre.

Nu este absolut nimic rău în a încerca orice distribuție pentru a vă face o idee bună despre ce este vorba.
