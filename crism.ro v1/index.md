% Bine ai venit
% ThinkRoot99

# Despre

![Fotografia lui Cristian](img/Cristian.jpg)

Salut, numele meu este Cristian Moldovan, sunt utilizator de GNU/Linux, Fedora Linux mai exact.

Probabil că ești interesat de unele dintre articolele mele despre GNU/Linux sau poate de [proiectele](proiecte.html) mele.

Mai scriu și pe [Mastodon](https://mstdn.ro/@crismblog), [X](https://www.x.com/@thinkroot99) și [Threads](https://www.threads.net/@thinkroot99) despre GNU/Linux, Open Source și diverse subiecte.

Mai fac și ceva clipuri despre GNU/Linux,Open Source și diverse subiecte pe platformele: [YouTube/YouTube Shorts](https://www.youtube.com/@thinkroot99), [TikTok](https://www.tiktok.com/@thinkroot99) și [Instagram Reels](https://www.instagram.com/thinkroot99/).

# Articole

- De ce [am renunțat](am-renuntat-la-arch.html) la Arch Linux
- [Extender Wi-Fi vs. rețea Mesh](extender-wi-fi-vs-retea-mesh.html): care este diferența?
- Ce este [G'MIC](gmic-si-gimp.html) pentru GIMP
- Cum se [remediază](remediaza-pacman.html) „Pacman is curently in use, please wait”
- Cum se șterg [aplicațiile](chrome-app.html) din Chrome
- De ce folosesc [Arch și Sway](de-arch-sway.html)

[Toate articolele](postari.html)

[**Ghidul debutantului pentru Linux - Începeți să învățați Linux în câteva minute**](ghid/ghid-pentru-linux.html)

# Contact

Mă puteți contacta la <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#115;&#111;&#99;&#105;&#97;&#108;&#64;&#99;&#114;&#105;&#115;&#109;&#46;&#114;&#111;" class="email">&#115;&#111;&#99;&#105;&#97;&#108;&#64;&#99;&#114;&#105;&#115;&#109;&#46;&#114;&#111;</a>
