# Fedora vs Arch: Ce folosesc mai des?

Am laptopul ThinkPad T440p la care i-am pus două SSD-uri; unul de 120 GB și unul de 1 TB. Până în urmă cu câteva zile foloseam când Fedora când Arch Linux, și dacă tot îmi plac acestea două distribuții Linux, am zis să le instalez pe amândouă ca să nu tot schimb sistemul de operare o data la câteva luni.

Nu există o comparație între cele două sisteme pentru că sunt construite în moduri diferite și au o anumită destinație fiecare dintre cele două.

Arch Linux este o distribuție pentru utilizatorii avansați de Linux unde totul trebuie făcut manual. Fedora este o distribuție pentru utilizatorul mediu - dar poate fi folosită și de un începător - cu un ciclu de viață de câteva luni, după care trebuie actualizată la următoarea versiune.

În Fedora multe lucruri/setări se pot face din mediul grafic, dar în Arch Linux fără terminal și cunoașterea comenzilor nu se poate face nimic. Plus că Fedora Linux este mai mult o versiune Alpha (părerea mea personală) pentru Red Hat Linux Enterprise, chiar dacă este o distribuție finală, foarte stabilă.

## De ce am ales să instalez amândouă distribuții pe laptop?

În primul rând pentru că îmi plac amândouă distribuții chiar dacă nu sunt asemănătoare. Fiecare distribuție are farmecul ei.

Fedora este axată pe grafică și pe folosirea ultimelor tehnologi și a ultimelor versiuni de programe, pe când Arch Linux este orientat spre a face totul manual - cea ce nu poți face cu Fedora (dacă ești un utilizator avansat, poți face în Fedora tot ce faci în Arch Linux).

Am instalat Fedora pentru acele momente când vreau ca totul să fie setat și configura, pornesc laptopul și îmi văd de treabă. Arch Linux l-am instalat pentru acele momente când vreau să mai modific ceva, când vreau ca sistemul se miște repede.

Fedora se mișcă mai greu decât Arch Linux pentru că Fedora vine cu mediul de lucru GNOME, cu multe aplicații și pachete instalate, multe servicii pornite. Și cum am scris mai sus, nu mai trebuie să fac aproape nimic, și de aceea Fedora consumă multe resurse - dar nu ajunge la nivelul lui Windows :).

Arch Linux este mult mai rapid pentru că în această distribuție am instalat doar cea ce am avut nevoie, mediul de lucru este un manager de ferestre, i3 WM - care consumă foarte puține resurse în comparație cu GNOME, serviciile care pornesc automat sunt mult mai puține față de cele din Fedora.

## Ce folosesc cel mai des?

După ce le-am instalat în urmă cu câteva zile, cel mai des am folosit Fedora pentru că nu a trebuit să fac mare lucru. Pe când Arch Linux era doar un ecran negru unde tot ce puteam face era să taste comenzii și nu am avut chef să lucrez la Arch - așa că mai mult am stat pe Fedora.

Acum că și Arch Linux este configurat și mai am de făcut foarte puține lucruri, mai mult folosesc această distribuție pentru că totul se mișcă mult mai repede față de Fedora. Și acest articol este scris de pe Arch Linux cu aplicația de text Vim. Pe când în Fedora folosesc Apostrophe în combinație cu Vim.

## De final

În concluzie cel mai des folosesc Arch Linux, dar în momentele când nu am chef să mă folosesc de tastatură așa mult și doar să dau clic-uri folosesc Fedora pentru că este mult mai comod cu mouse-ul, chiar dacă tot ce fac este mult mai lent decât pe Arch.