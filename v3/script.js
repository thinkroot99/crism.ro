let startIndex = 0; // Indicele de la care începe încărcarea următoarelor postări
const postsPerPage = 20; // Numărul de postări pe pagină

function loadPosts() {
    // Faceți o cerere AJAX către scriptul PHP pentru a obține primele postări
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `get_articles.php?start=${startIndex}&count=${postsPerPage}`, true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            const responseData = JSON.parse(xhr.responseText);
            const postContainer = document.getElementById('postContainer');
            postContainer.innerHTML += responseData.html; // Adăugați postările la container

            // Verificați dacă nu mai sunt postări disponibile
            if (!responseData.morePostsAvailable) {
                document.getElementById('loadMoreBtn').style.display = 'none'; // Ascundeți butonul dacă nu mai sunt postări
            } else {
                startIndex += postsPerPage; // Incrementați indicele pentru următoarele postări
            }
        }
    };
    xhr.send();
}

function loadMorePosts() {
    loadPosts(); // Încărcați mai multe postări atunci când utilizatorul apasă butonul
}

// Încărcați primele postări când pagina este încărcată
window.onload = loadPosts;
