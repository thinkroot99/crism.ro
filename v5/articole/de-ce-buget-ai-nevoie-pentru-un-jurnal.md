# De ce buget ai nevoie pentru un jurnal?

Am citit câteva articole interesante despre cum nu ai nevoie de un buget mare pentru un blog. Era frumos dacă puneam aici legăturile spre acele articole, dar nu am obiceiul să salvez tot ce citesc.

În timp ce citeam acele articole, m-am uitat și în curtea mea și excluzând faptul că am o colecție mică de laptopuri [ThinkPad](colectia-de-laptopuri-thinkpad.html), nu pot spune că am cheltuit o sumă mare pe IT în ultima vreme.

Laptopul principal este un Lenovo ThinkPad T440p (vechi de 11 ani) pe care l-am cumpărat în 2023 cu 900 de lei de pe OLX, dar mai am un astfel de model cu un procesor mai slab pe care l-am cumpărat cu 450 de le în 2022.

Am un mouse Lenovo nou, cumpărat în 2024 tot de pe OLX la 35 de lei. Am o imprimantă HP LaserJet M110w cumpărată de nouă cu 500 de lei în 2024 de la Altex și boxe wireless Logi Z207 Withe cumpărate cu 150 de lei (dacă îmi aduc bine aminte, dar nu mai știu anul și de unde le-am cumpărat).

Dar tot ce am cumpărat nou se găsește și la mâna a doua la prețuri foarte bune. Nu este nevoie de sume mari pentru a investi în tehnologie, dar acolo unde este necesar nu trebuie evitate produsele noi. De exemplu nu aș cumpăra o imprimantă la mâna a doua - chiar dacă se găsesc produse bune.

Pentru crearea unui blog nu este nevoie de sume mari, deoarece domeniul .ro nu este scump și un spațiu de găzduire a blogului este ieftin. Eu plătesc pentru găzduire 36 de lei pe lună, iar pentru un domeniu .ro plătesc 51 de lei pe an.

## Rezumat

Acum să fac un rezumat pentru ce unelte și servicii avem nevoie pentru un blog. Să zicem că nu avem nimic și începem totul de la zero.

- În primul rând este nevoie de un laptop - sunt laptopuri mai ieftine ca al meu. Un laptop bun din seria [ThinkPad](https://www.olx.ro/oferte/q-thinkpad/) la mâna a doua costă în jur de 500 de lei.
- Sistemul de operare este gratuit pentru că se poate folosi o distribuție Linux - iar pentru începători recomand [Linux Mint](https://www.linuxmint.com/), [MX Linux](https://mxlinux.org/) sau [Zorin OS](https://zorin.com/).
- Este nevoie de un domeniu .ro care costă 51 de lei pe an la Hosterion ([legătură afiliată](https://hosterion.ro/client/aff.php?aff=881), [legătură neafiliată](https://hosterion.ro/domenii/ro#preturi)).
- Găzduirea la Hosterion ([legătură afiliată](https://hosterion.ro/client/aff.php?aff=881), [legătură neafiliată](https://hosterion.ro/domenii/ro#preturi)) costă 36 de lei pe lună.
- Crearea unui blog se poate face gratuit cu ajutorul unui CMS cum este [WordPress](https://wordpress.org/download/). Pentru a crea un blog puteți vizualiza tutoriale video sau să citiți tutoriale text - sunt foarte multe pe internet. Sau puteți face un blog static, cea ce recomand, cu [HTML și CSS](https://www.w3schools.com/howto/howto_css_blog_layout.asp) - și aici sunt multe tutoriale, plus că există și AI-urile.

Acum tot ce mai trebuie să faceți este să publicați articole.

## Total de plată

Un total pentru aceste costuri este:

- 500 de lei laptop-ul.
- Sistem de operare gratuit.
- 51 de lei domeniul .ro.
- 36 de lei găzduirea.
- WordPress sau HTML/CSS gratuit.

**587 de lei total** în prima lună. După care **în fiecare lună** veți plăti doar **36 de lei** și **o dată pe an** veți plăti doar **87 de lei**. Costuri foarte mici pentru un blog și astfel nu veți mai fi nevoiți să publicați pe rețelele de socializare nimic pentru că aici se pierde informația și nici nu puteți publica cea ce vreți.