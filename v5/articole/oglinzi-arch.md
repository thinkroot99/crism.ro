# Oglinzi rapid pentru Arch Linux

Citeam diverse sit-uri mici, scrise doar în HTML, așa cum erau cu ceva ani în urmă. Și cum treceam de la un sit la sit, de la un articol la alt articol, am dat peste un articol interesant cu subiect pentru Arch Linux.

Cum să detectezi ce oglindă este cea mai rapidă și la acele oglinzi să se conecteze sistemul Arch pentru actualizare sau pentru instalarea programelor.

Pentru început trebuie instalată aplicația: `rate-mirrors`.

```bash
paru -S rate-mirrors
```

După care trebuie rulată comanda următoare:

```bash
rate-mirrors --disable-comments-in-file --entry-country=RO --protocol=https arch --max-delay 7200 | sudo tee /etc/pacman.d/mirrorlist
```

Această comandă ar trebui să actualizeze lista `mirrorlist` cu cele mai rapide oglinzi din regiune.