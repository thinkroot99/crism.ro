# Am actualizat sit-ul din nou

În doar două luni am schimbat design-ul de 5 ori la acest sit. De ce am făcut acest lucru?

În primul rând pentru că vreau un sit cu un designe cât mai plăcut pentru mine și sit-ul să fie cât mai mic. Cu ultimul designe (v4) am obținut un sit mic, dar nu mi-a plăcut cum arată. Cel mai mult mi-a plăcut versiunea 3, dar sit-ul era destul de mare.

Cât de mic a devenit sit-ul? Acest lucru se poate vedea accesând legăturile următoare: [URL Scanner by Cloudflare Radar](https://radar.cloudflare.com/scan/702637d9-3bd9-4e5a-945d-924b105a0ee5/summary) și [Yellow Lab Tools](https://yellowlab.tools/result/gygn3bqj4x).

Dar de această dată, designe-ul nu l-am mai făcut de la zero și în schimb l-am luat de la [Karl Bartel](https://www.karl.berlin/), am schimbat culorile și am adăugat un cod care schimbă temă deschisă în temă închisă în funcție de tema sistemului de operare.

Acum pot spune că acest sit este mult mai mic decât înainte și îmi place cum arată.

Am mai scos din pagini și din informații pentru că nu consider că mai este nevoie de toate acele informații care erau înainte. Am lăsat doar informațiile pe care le consider cele mai esențiale pentru acest sit.

PS: Codul sursă este liber pentru descărcare și modificare, o legătură spre codul sursă se găsește la pagina [despre](despre.html).