% Afișarea sistemului de fișiere într-un mod mai plăcut cu lfs
% ThinkRoot99

Pe GNU/Linux, uneori trebuie să lucrați cu hard-uri și/sau partiții direct din linia de comandă. Adesea, doriți să efectuați acțiuni asupra sistemelor de fișier, dar faceți acest lucru specificând partițiile în care sunt stocate.

Există multe instrumente disponibile pentru a găsi lista sistemelor de fișiere disponibile în GNU/Linux, dintre care cel mai frecvent instrument este `df`.

Din păcate, pe sistemele cu multe hard-uri, partiții și unităti USB, poate fi dificil să indentifici numele dispozitivului atribuit fiecărui dintre ele. Și aici `lfs` apare pe scenă.

Comanda `lfs` este folosită pentru a arăta cantitatea de spațiu liber disponibilă pe hard și alte sisteme asemănătoare Unix și pentru a înțelege sistemele de fișiere care au fost montate. Toate acestea sunt prezente într-o formă tabelară clară și frumoasă.

Utilizarea comenzii `lfs` este mai mult decât ușoară. Tot ce trebuie să faceți este să introduceți `lfs` în terminal.

    $ lfs

> ![lfs](img/lfs.png)

În mod implicit, lfs listează numai sistemele de fișiere susținute de dispozitive bloc care arată ca hard-uri reale. Pe celelalte le puteți vedea cu idicatorul `-a`.

    $ lfs -a

> ![lfs -a](img/lsa-a.png)

După cum puteți vedea, majoritatea informațiilor oferite de `lfs` sunt deja frunizate de `df`. Cu toate acestea, există câteva îmbunătățiri care merită menționate.

- Vă ajută să recunoașteți discurile etichetându-le `rem` (detașabile), HDD sau SSD.
- `lfs` folosește doar **SI** (sistemul internațional de unități). Astfel încât nu trebuie să deschideți ajutorul și să verificați argumentul potrivit pentru unități.
- Afișează tipul de sistem de fișiere.
- Sortează sistemele de fișiere după dimensiune.

Pentru mai multe informații, puteți consulta site-ul [proiectului](https://dystroy.org/lfs/).

# Cum se instalează

Puteți descărca pachetul precompilat de pe pagina de [GitHub](https://github.com/Canop/lfs/releases).

    $ wget https://github.com/Canop/lfs/releases/download/v2.0.1/lfs_2.0.1.zip

Dezarhivați fișierul zip folosind următoarea comandă:

    $ unzip lfs_2.0.1.zip

În continuare, va trebui să vă asigurați că shell-ul poate găsi fișierul executabil al comenzii `lfs`. O soluție ușoară este să copiați fișierul la `/usr/local/bin` și să îl setați executabil – acest lucru se face cu contul de administrators sau puteți folosi comenzile următoare:

    $ sudo mv build/x86_64-linux/lfs /usr/local/bin/
    $ sudo chmod +x /usr/local/bin/lfs

În afară de Arch Linux, `lfs` se poate instala așă cum am explicat mai sus. În `Arch` lfs se poate instala folosind comanda:

    $ sudo pacman -S lfs

Acum `lfs` ar trebuie să fie instalat pe sistemul vostru.
