% Merge GNU/Linux fără un mediu de lucru?
% ThinkRoot99

În timp ce sistemele Linux moderne au interfețe grafice atractive, s-ar putea să vă întrebați dacă puteți utiliza Linux fără ele. Răspunsul simplu este **da**.

# Ce este un mediu de lucru (desktop environment)?

În timp ce mediile de lucru pe Windows și macOS sunt strâns integrate în sistem, pe GNU/Linux mediile de lucru precum GNOME, Plasma și Xfce sunt doar colecții de programe pe care le puteți instala lângă sistemul de operare de bază.

> ![Xfce Desktop Environment](img/Xfce-Desktop-Environment.jpg)

Un mediu de lucru constă dintr-un manager de ferestre, un set de instrumente care definește aspectul și diverse aplicații. Puteți schimba oricare dintre ele și acesta este un efect al modularității pe care dezvoltatorii Unix și GNU/Linux o prețuiesc.

# Înlocuirea DE cu un manager de ferestre

Este posibil să rulați un manager de ferestre în loc de un mediu de lucru complet în GNU/Linux și mulți utilizatori o fac deja, inclusiv eu.

S-ar putea să doriți să utilizați Fluxbox în loc de unul dintre mediile de lucru livrate cu distribuția voastră. Tot ce trebuie să faceți este să instalați managerul de ferestre pe care doriți să îl folosiți folosind managerul de pachete.

> ![Select Windows Manager](img/Select-Windows-Manager.png)

Când doriti să comutați, puteți selecta noul manager de ferestre din meniul de conectare al mangerului de afișare. Acest lucru este util dacă doriți să schimbați mediile mai des.

# GNU/Linux fără interfață grafică

De asemenea, este posibil să rulați GNU/Linux fără o interfață grafică completă. Multe servere o fac deja, fiind administrate prin linia de comandă și SSH, fără tastaturi și monitoare conectate la ele.

Acest lucru este cunoscut sub numele de alergare „*headless*”. Acest lucru se datorează faptului că GNU/Linux este asemănător cu Unix, iar Unix a fost dezvoltat într-o perioadă în care nu exista mediile de lucru grafice.

> ![Arch Linux Console](img/Arch-Console.png)

Puteți rula sistemul fără un `desktop environment` sau `window manager` și puteți rula aplicații bazate pe text dacă doriți. Puteți rula aceleași comenzi pe care le-ți face într-un terminal, precum lansarea unui editor de text și chiar a unui navigator web.

Unele distribuții mai avansate, cum ar fi Arch Linux, vine fără interfață grafică în mod implicit. Singura limită practică poate fi aceea că sit-urile moderne se așteaptă ca utilizatorii să ruleze distribuții grafice și ar putea refuza să-și afișeze conținutul într-un navigator de text precum Lynx.

Dacă doriți să dezactivați complet interfața grafica, puteți dezactiva managerul de afișare. Dacă rulați `LightDM`, puteți utiliza comanda următoare:

    $ sudo systemctl disable lightdm

După ce reporniți sistemul, vă veți găsi în consola de text. Pentru a alege managerul de ferestre sau mediul desktop preferat, trebuie să adăugați următoarea linie în fișierul `.xinitrc`.

    exec windowmanager
    ex:
    exec fluxbox

…unde `windowmanager` este managerul vostru preferat de ferestre. Apoi, tastați `startx` pentru a lansa interfața grafică.

# GNU/Linux vă oferă posibilitatea de a alege

Lucrul grozav este că aveți de ales în ceea ce privește modul în care doriți să interacționați cu sistemul. Puteți avea o interfață desktop tradițională sau puteți explora mai adânc linia de comandă.

Cea mai buna alegere a unui mediu de lucru în GNU/Linux depinde de gusturile voastre personale.
