% Comanda htop
% ThinkRoot99

# Comanda htop cu exemple

**Comanda htop** în sistemul GNU/Linux este un utilitar pentru linia de comandă care permite utilizatorului să monitorizeze interactiv resursele vitale ale sistemului sau procesele serverului în timp real.

[htop](https://htop.dev/) este un program mai nou în comparație cu [top](https://en.wikipedia.org/wiki/Top_%28software%29) și oferă multe îmbunătățiri față de comanda top.

Aplicația **htop** are suport pentru mouse, oferă informațiile în culori și oferă indicații vizuale despre utilizarera procesorului, memoriei RAM și Swap. **htop** oferă informații complete pentru procese și permite derularea informațiilor atât pe verticală, cât și pe orizontală.

**Sintaxa:**

    htop [-dChusv]

> ![htop](img/htop.jpg)

**Opțiuni:**

- `-d, –delay`: este folosit pentru a afișa întârzierea dintre actualizări, în zecimi de secunde.
- `-C, –no-color`: porniți htop în modul monocrom.
- `-h, –help`: este folosit pentru a afișa mesajul de ajutor.
- `-u, –user=[USERNAME]`: este folosit pentru a afișa numai procesele unui anumit utilizator.

        $ top -u thinkroot99

> ![htop -u username](img/htop-u.jpg)

- `-p, –pid=PID[,PID,PID…]`: este folosit pentru a afișa numai PID-urile date.
- `-s, –short-key=COLUMN`: sortați după această coloană (utilizați ajutorul –short-key pentru o listă de coloane).
- `-v, –version`: afișare informații despre aplicație.

> ![htop version](img/htop-v.jpg)

**Meniul htop:**

> ![htop menu](img/htop-interactive-command.jpg)

- Săgeți (arrow), Pagină sus (page up), Pagină jos (page down), Acasă (home), Sfârșit (end): derulați lista de procese.
- Spațiu (space): selectează sau deselectează un proces.
- **U**: nulați selectarea tuturor proceselor.
- **s**: urmăriți apelurile de sistem de proces.
- **F1**: se deschide pagina de ajutor.
- **F2**: se deschide pagina cu setările aplicației.
- **F3**: se deschide bare de căutare.
- **F4**: filtre – se poate introduce o parte a unei linii de comandă a procesului și vor fi afișate numai procesele ale căror nume se potrivesc.
- **F5**: vedere arborescentă aproceselor.

> ![htop three list](img/htop-tree.jpg)

- **F6**: sortarea informațiilor.
- **F7**: mărește prioritatea procesului selectat. Acest lucru poate fi făcut numai dintr-un cont de administrator.
- **F8**: reducerea priorități procesului selectat.
- **F9**: oprirea procesului.
- **F10**: oprirea htop.

Pentru mai multe informații puteți consula site-ul oficial (îl găsiți la începutul articolului) sau puteți rula comanda următoare în terminal.

    $ man htop
