% Ce este o distribuție Rolling Release?
% ThinkRoot99

În distribuția Rolling Release, instalați sistemul de operare o singură dată și nu vă mai obosiți să reinstalați și să vă restaurați datele.

Distribuțiile GNU/Linux de tip rolling release (lansare continuă) există de mult timp, dar nu toată lumea știe ce oferă. În zilele noastre, distribuțiile GNU/Linux cu lansare continuă devin mult mai populare.

Deci, ce este o distribuție GNU/Linux de tip lansare continu[? Răspunsul este în rândurile de mai jos, dar înainte de asta, să clarificăm ceva important.

# Ce este o distribuție cu versiune fixă sau lansare fixă?

În cadrul dezvoltării de versiuni fixe, se creează versiuni de programe care trebui reinstalate peste versiunea anterioară. O distribuție cu lansare fixă este o distribuție care este lansată în mod regulat la câteva luni.

Aceasta conține pachete de programe puternic testat și verificate, asigurându-se că totul este stabil și că totul funcționează.

Sunt șanse mari să folosiți în acest moment un sistem de operare cu versiune fixă. De exemplu, gândiți-vă la Ubuntu 21.10 și Ubuntu 22.04. Acestea sunt două versiuni ale unui sistem de operare care suportă hardware și tehnologii diferite și care vă solicită să instalați noua versiune pentru a utiliza cele mai recente caracteristici.

În plus, este imperativ să știți că va exista un `End of Life (EOL)` pentru fiecare versiune din acest model de dezvoltare, după care echipa de dezvoltare nu va mai lansa actualizări.

Cu alte cuvinte, nu mai există actualizări care să implice găuri de securitate deschise și probleme de compatibilitate cu cel mai nou hardware.

# Ce este o distribuție GNU/Linux Rolling Release?

Multe distribuții GNU/Linux optează pentru ceea ce este cunoscut sub numele de model de lansare continuă. Un sistem de operare bazat pe modelul lansare continuă (cunoscut și sub numele de [Continuous Delivery](https://en.wikipedia.org/wiki/Rolling_release)) are două caracteristici principale.

- În primul rând, instalați sistemul de operare doar o singură dată și apoi niciodată din nou.
- În al doilea rând, sistemul de operare este actualizat în permanență.

Printre distribuțiile GNU/Linux populare cu lansare continuă se numără Arch Linux, Manjaro Linux, openSUSE Tumbleweed, Void, Gentoo Linux, Solus și EndeavourOS.

Totul pe GNU/Linux este împărțit în pachete de programe, ceea ce face din GNU/Linux un sistem de operare modular. Fiecare pachet, inclusiv nucleele și driverele, pot fi actualizate cu ajutorul unui manager de pachete. Acesta este motivul pentru care un model de lansare continuă este aplicabil pentru distribuțiile GNU/Linux.

O distribuție GNU/Linux cu lansare continuă actualizează continuu pachetele de programe individuale și le pune la dispoziția utilizatorilor săi imediat ce sunt publicate.

În calitate de utilizator al distribuției, acest lucru înseamnă că aveți întotdeauna instalată cea mai nouă versiune de program. Cu alte cuvinte, vă bucurați de noile caracteristici imediat ce sunt lansate.

Distribuțiile `Rolling Release` nu au versiuni ale sistemului de operare în același mod ca o distribuție ca Ubuntu. În schimb, acestea sunt actualizate în mod continuu ori de câte ori este disponibil un nou program.

În comparație cu distribuțiile conservatoare cu versiune stabilă, de exemplu Debian, unde utilizatorii pot aștepta ani de zile pentru noi versiuni de programe, distribuțiile cu lansare continuă fac ca noul program să fie disponibil foarte repede.

## Pro

Se instalează o dată și se actualizează pentru totdeauna. Nu trebui să distrugeți totul și să reinstalați la fiecare 6-12 luni. Procesul de actualizare este, în general, nedureros și incremental.

În plus, în distribuția GNU/Linux cu lansare continuă, veți obține mai repede suport pentru hardware mai nou. Instalarea celui mai recent nucleu pe măsură ce devine disponibil în amonte este unul dintre avantajele unui rolling release.

Nu există solicitări pentru actualizări de versiune. În cazul distribuțiilor cu lansare contunuă, acest lucru nu se întâmplă niciodată.

Nu în ultimul rând, vă puteți lua adio de la pachetele realizate pentru o anumită versiune de sistem de operare. În distribuțiile cu lansare continuă nu există versiuni care să vă dicteze ce pachete puteți instala.

## Contra

Desigur, există dezavantaje în cazul distribuțiilor GNU/Linux cu lansare continuă. Principalul este că acestea nu ar trebui să fie folosite ca platformă de server.

Acest lucru se datorează faptului că programele lor este actualizat prea frecvent, ceea ce implică faptul că nu a avut timp suficient pentru a fi testat în profunzime pentru a depista eventualele probleme.

În plus, deși rar, este posibil ca următoarea actualizare să întrerupă funcționarea sistemului. Din nou, acest lucru este legat de instalarea celor mai recente versiuni insuficient testate ale pachetelor de programe sau de incompatibilități între acestea.

# De final

Așadar, principalul beneficiu al unui model de lansare continuă este capacitatea utilizatorului final de a avea întotdeauna instalată cea mai nouă versiune de program. Scopul este de a oferi utilizatorilor actualizări cât mai repede posibil.

Așadar, dacă sunteți unul dintre acei utilizatori care dorește cele mai recente caracteristici și servicii direct din producție, atunci distribuțiile de tip rolling release sunt cea mai bună afacere pentru voi.

Dar dacă stabilitatea, securitatea și predictibilitatea sunt importante, atunci o distribuție conservatoare precum Debian este probabil cea mai bună alegere.
