Fedora vs Arch: Ce folosesc mai des?

Am laptopul ThinkPad T440p la care i-am pus două SSD-uri; unul de 120 GB și unul de 1 TB. Până în urmă cu câteva zile foloseam când Fedora când Arch Linux, și dacă tot îmi plac acestea două distribuții Linux, am zis să le instalez pe amândouă ca să nu tot schimb sistmeul de operare o data la câteva luni.

Nu există o comparație între cele două sisteme pentru că sunt construit în moduri diferite și au o anumită destinație fiecare dintre cele dou. Arch Linux este o distribuție pentru utilizatorii avansați de Linux unde totul trebuie să faci manual și făra temrinal nu prea poți face nimic. Fedora este o distribuție pentru utilizatorul mediu - dar poate fi folosită și de un începător - cu un ciclu de viață de câteva luni, după care trebuie actualziată la următoarea versiune.

În Fedora multe lucuri/setări se pot face din mediul grafic, dar în Arch Linux fără terminal și cunoașterea comenzilor nu ai ce face. Plus că Fedora Linux este mai mult o fersiune Alpha pentru Red Hat Linux Enterprise, chiar dacă este o distribuție foarte stabilă.

# De ce am ales să instalez amândouă distribuții pe laptop?

În primul rând pentru că îmi plac amândouă distribuții chiar dacă nu sunt asemănătoare. Fiecare distribuție are farmecul ei. Fedora este exată pe grafică și pe folosirea ultimelor tehnologi și a ultimelor versiuni de programe, pe când Arch Linux este orientat spre a face totul manual și faci sistemul pe placul tău - cea ce nu poți face cu Fedora (dacă ești un utilizator avansat, poți face în Fedora tot ce faci în Arch Linux).

Am instalat Fedora pentru acele momente când vreau ca totul să fie setat și configurat, fără a mai face nimic. Pornesc laptopul și îmi văd de treabă. Arch Linux l-am instalat pentru acele momente când vreau să mai fac ceva, când vreau ca sistemul se miște repede.

Fedora se mișcă mai greu decât Arch Linux pentru că Fedora vine cu mediul de lucru GNOME, cu multe aplicății și pachete instalate, multe servicii pornite. Și cum am scris mai sus, nu mai trebuie să faci aproape nimic, și astfel Fedora consumă multe resurse - dar nu ajunge la nivelul lui Windows :).

Arch Linux este mult mai rapid pentru că în această distribuție am instalat doar cea ce am avut nevoie, mediul de lucru este un manager de ferestre, i3 WM - care consumă foarte muține resurse în comparăție cu GNOME, serviciile care pornesc automat sunt mult mai puține față de cel din Fedora.

# Pe care o folosesc cel mai des?

După ce le-am instalat în urmă cu câteva zile, cel mai des am folosit Fedora pentru că nu a trebuie să fac mare lucru. Pe când Arch Linux este doar un ecran negru unde tot ce putea face era sa taste comenzii și nu am avut chef să lucrez la Arch - așa că mai mult am stat pe Fedora.

Acum că și Arch Linxu este configurat și mai am de făcut foarte puține lucruri, mai mult folosesc această distribuție pentru că totul se mișcă mult mai repde față de Fedora. Și acest articol este scris de pe Arch Linux cu aplicația de text Vim. Pe când în Fedora folosesc Apostrophe în compinație cu Vim.

# De final

În concluzie cel mai des folosesc Arch Linux, dar în momentele când nu am chef să mă folosesc de tastatură așa mult și doar să dau clic-uri folosesc Fedora pentru că este mult mai comod cu mouse-ul, chiar dacă tot ce fac este mult mai lent decât pe Arch.
 
