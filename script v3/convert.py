import os
import markdown
import datetime

def read_template_html(template_file_path):
    with open(template_file_path, 'r', encoding='utf-8') as template_file:
        return template_file.read()

def read_css_file(css_file_path):
    with open(css_file_path, 'r', encoding='utf-8') as css_file:
        return css_file.read()

def convert_md_to_html(md_file_path):
    with open(md_file_path, 'r', encoding='utf-8') as md_file:
        markdown_content = md_file.read()

    lines = markdown_content.split("\n")

    # Extract title and author
    title = None
    author = None
    if lines[0].startswith("%"):
        title = lines[0].strip("%").strip()
        lines = lines[1:]
    if lines[0].startswith("%"):
        author = lines[0].strip("%").strip()
        lines = lines[1:]

    # Convert the remaining markdown content to HTML
    markdown_content = "\n".join(lines)
    html_content = markdown.markdown(markdown_content, extensions=['markdown.extensions.tables', 'markdown.extensions.fenced_code', 'markdown.extensions.codehilite', 'markdown.extensions.footnotes', 'markdown.extensions.toc', 'markdown.extensions.attr_list', 'markdown.extensions.def_list', 'markdown.extensions.abbr', 'markdown.extensions.meta'])

    # Generate the menu
    menu_html = generate_menu_from_markdown(markdown_content)

    # Place the menu above the first h2, h3, etc.
    html_content_with_menu = insert_menu_above_first_heading(html_content, menu_html)

    return title, author, html_content_with_menu

def generate_menu_from_markdown(markdown_content):
    def sanitize_title_for_id(title):
        # Replace special characters with their counterparts or remove them
        title = title.lower().replace(' ', '-').replace('&', 'and').replace('/', '-').replace('ă', 'a').replace('î', 'i').replace('ș', 's').replace('ț', 't').replace('â', 'a')
        title = ''.join(e for e in title if e.isalnum() or e in ['-', '_'])
        return title

    menu = "<ul>"
    lines = markdown_content.split("\n")
    previous_level = 0
    for line in lines:
        if line.startswith("#"):
            level = line.count("#")
            title = line.strip("#").strip()
            sanitized_title = sanitize_title_for_id(title)
            if level > previous_level:
                menu += "<ul>"
            elif level < previous_level:
                menu += "</li></ul>" * (previous_level - level)
            menu += f"<li><a href='#{sanitized_title}'>{title}</a></li>"
            previous_level = level
    menu += "</ul>" * previous_level
    return menu


def insert_menu_above_first_heading(html_content, menu_html):
    # Find the first heading tag and place the menu above it
    heading_index = html_content.find("<h1>")

    if heading_index != -1:
        html_content = html_content[:heading_index] + f"<div id='menu'>{menu_html}</div>" + html_content[heading_index:]
    else:
        # If no heading tag exists, add the menu at the beginning of the content
        html_content = menu_html + html_content

    return html_content

def write_html_to_file(title, author, html_content, md_file_path, template_file_path, css_file_path, output_dir):
    md_file_name = os.path.splitext(os.path.basename(md_file_path))[0]
    html_file_name = md_file_name + '.html'  # Numele fișierului HTML va fi același cu cel al fișierului Markdown
    output_file_path = os.path.join(output_dir, html_file_name)
    template_html = read_template_html(template_file_path)
    with open(output_file_path, 'w', encoding='utf-8') as html_file:
        try:
            # Replace the placeholders in the template with the actual content
            updated_template = template_html.replace('{{ title }}', title if title else "Untitled")
            updated_template = updated_template.replace('{{ author }}', author if author else "Unknown Author")
            # Add current date dynamically
            current_date = datetime.datetime.now().strftime('%d-%m-%Y')
            updated_template = updated_template.replace('{{ date }}', current_date)
            updated_template = updated_template.replace('{{ content }}', html_content)
            # Add a link to the external CSS file
            updated_template_with_css = updated_template.replace('{{ styles }}', f'<link rel="stylesheet" type="text/css" href="{css_file_path}">')
            html_file.write(updated_template_with_css)
        finally:
            html_file.close()

if __name__ == "__main__":
    script_dir = os.path.dirname(os.path.abspath(__file__))
    output_dir = os.path.join(script_dir, 'output')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    template_file_path = os.path.join(script_dir, 'template.html')  
    css_file_path = os.path.join(script_dir, 'style.css')  

    md_files = [f for f in os.listdir(script_dir) if os.path.isfile(os.path.join(script_dir, f)) and f.endswith('.md')]

    for md_file in md_files:
        title, author, html_content = convert_md_to_html(os.path.join(script_dir, md_file))
        write_html_to_file(title, author, html_content, md_file, template_file_path, css_file_path, output_dir)