% Resetare parolă de root în Ubuntu - metoda 2
% ThinkRoot99

Statisticile spun că aproape 25% dintre oameni obișnuiesc să uite parola calculatorului în termen de 14 zile de la instalarea unui sistem de operare. Așadar, nu intrați în panică dacă nu vă puteți aminti parola de root la Ubuntu.

Resetarea parolei root uitate nu este lipsită de etică atunci când trebuie să intrați în sistem. Dar, desigur, asta nu vă dă dreptul să spargeți fiecare calculator și să vă uitați în interiorul sistemului. Puteți reseta parola root a unui sistem cu Ubuntu fără a cunoaște parola curentă

# Resetarea parolei în Ubuntu

În Ubuntu, parola de administrator a sistemului este scrisă în interiorul directorului rădăcină al sistemului de fișiere GNU/Linux. Parola de root este esențială, deoarece nu vă puteți conecta la contul de administrator fără parolă.

Chiar, nu puteți atribui sarcini administrative fără a cunoaște parola root. În acest articol, vă voi arăta cum se resetează parola root la distribuția Ubuntu.

# Pasul 1: Intrați în Recovery Mode

Pentru a reseta parola root, trebui să reporniți calculatorul și să accesați meniul GNU GRand Unified Bootloader (GRUB). Este posibil să fie nevoie să găsiți tasta de acces rapid a meniului de boot pentru calculatorul vostru.

Odată deschis meniul de boot, selectați `Advanced options for Ubuntu (Opțiuni avansate pentru Ubuntu)` și apăsați tasta `Enter`. Dacă nu găsiți meniul de pornire de la calculatorul vostru, asigurați-vă că opțiunea corespunzătoare bootloader-ului este activată din setările BIOS.

> ![Reset Root Password in Ubuntu](img/Reset-Root-Password-in-Ubuntu-Linux.webp)

În acest pas, se va deschide o fereastră nouă și veți vedea un meniu derulant pentru opțiunea de recuperare. În cazul din acest exemplu, se selectează `Ubuntu, with Linux 5.4.0-40-generic (recovery mode)`. Este posibil să observați că există două opțiuni pentru a ajunge la modul de recuperare.

Puteți alege doar o singură opțiune în funcție de nucleul Linux și de versiunea dispozitivului de stocare cu acces direct (DASD).

> ![Reset Root Password in Ubuntu](img/Reset-Root-Password-in-Ubuntu-Linux-2.webp)

# Pasul 2: Accesați directorul rădăcină al sistemului de fișiere

După intrarea în modul de recuperare, se vor efectua unele verificări ale sistemului. Apoi, veți putea vedea meniul de recuperare al Ubuntu. Ar trebui să vă reamintesc că în modul de recuperare, veți obține permisiunea numai de citire a sistemului de fișiere.

Dar nu vă faceți griji, mai târziu vom vedea cum să activăm ambele persmisiuni „*read-write (citire-scriere)*”.

Acum, pentru că vom reseta parola root la Ubuntu, trebui să selectăm meniul root din meniul derulant. Folosiți tastele săgeată pentru a ajunge la meniul root.

Selectarea meniului root ne va permite să folosim linia de comandă. Vom folosi comenzi pentru a reseta parola sistemului.

> ![Reset Root Password in Ubuntu](img/Reset-Root-Password-in-Ubuntu-Linux-3.jpg)

# Pasul 3: Recuperarea parolei root

Deoarece am obținut acces la TTY, acum putem reseta parola root prin intermediul liniei de comandă. Apăsați tasta `Enter` pentru a continua procesul de resetare a parolei. Acum trebuie să montăm, să remontăm și să **activăm permisiunea de citire-scriere pentru sistemul de fișiere root**.

După ce am montat sistemul de fișiere root, vom tasta comanda de recuperare a parolei. Apoi vom primi o opțiune pentru a introduce o nouă parolă. După introducerea noii parole, tastați din nou parola în scopuri de securitate.

Dacă totul se face cu succes, veți putea vedea un mesajul: `Password update successfuly (Parola a fost actualizată cu succes)`. Procesul de resetare a parolei de root a Ubuntu s-a finalizat. Acum reporniți calculatorul pentru a continua.

**Comenzile necesare pentru resetarea parolei sunt:**

    # mount -n -o remount,rw / 
    # passwd
    New password:
    Retype new password:
    passwd: password updated successfully
    # reboot

Atunci când uitați parola root vă gândiți să ștergeți sistemul de operare de pe calculator și să îl reinstalați. Dar de ce ați face acest lucru când cunoașteți metoda de resetare a parolei de root.

În această postare, am acoperit conceptul de unde este stocată parola root și cum să resetați parola root prin modul de recuperare.

> Notă:
> Acest tutorial de resetare a parolei root se potrivește la majoritatea distribuțiilor GNU/Linux.
