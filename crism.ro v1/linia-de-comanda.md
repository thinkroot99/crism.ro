% Sfaturi și trucuri pentru linia de comandă
% ThinkRoot99

Pentru utilizatorii GNU/Linux, linia de comandă este un instrument esențial și ultra-puternic. În timp ce sistemele de operare GNU/Linux mai ușor de utilizat oferă o mulțime de funcționalități fără a fi nevoie de a intra în terminal, este un element necesar al sistemului de operare. Spre deosebire de opinia populară, linia de comandă poate chiar simplifica anumite acțiuni.

Indiferent dacă sunteți nou în lumea GNU/Linux sau un veteran experimentat, linia de comandă oferă o mulțime de utilizări. Încercați aceste sfaturi pentru a stăpâni linia de comandă.

# Fișiere și directoare

Deși puteți crea, muta și naviga cu ușurință între directoare într-o interfață grafică cu utilizatorul (GUI), linia de comandă este perfect capabilă să gestioneze fișiere și directoare.

## Schimbați directorul

Schimbarea directoarelor este destul de simplă. Într-un terminal, introduceți comanda `cd`.

    cd

De exemplu, pentru a naviga într-un director specific, cum ar fi directorul `Descărcări/Downloads`, introduceți doar calea către directorul dorit:

    $ cd /home/nume_utilizator/Downloads

Schimbarea directoarelor este incredibil de benefică atunci când instalați programe prin linia de comandă. Pentru a rula un program de instalare folosind terminalul, va trebui mai întâi să vă mutați în directorul în care se află acel program de instalare.

## Creați un director

Pe lângă schimbarea directoarelor, linia de comandă permite crearea de directoare. Puteți crea un director rulând comanda:

    mkdir

Prin urmare, pentru a crea un director numit „*Aplicații*”, ar trebui să introduceți comanda:

    $ mkdir Aplicatii

Dar directorul creat se face în directorul curent. Dacă doriți să specificați unde să fie creat un director, va trebui fie să schimbați directorul, fie să introduceți calea completă:

    $ mkdir /home/nume_utilizator/Documente/Aplicatii

Dacă nu există directoarea pentru calea completă, rularea acestei comenzi creează directoarele necesare pentru toate numele trecute în cale.

## Copiere

O comandă des folosită atunci când se manipulează fișiere și directoare este copierea.

    cp

Pentru a copia un fișier într-un alt fișier, rulați:

    $ cp [nume fișier 1] [nume fișier 2]

Alternativ, puteți copia fișiere în directoare folosind această comandă:

    $ cp [nume fișier] [nume director]

## Mutare

Ca și copierea fișierelor și directoarelor, puteți muta elemente cu ajutorul terminalului. Aceasta este comanda pentru mutare:

    mv

Când mutați conținutul unui fișier în altul, rulați comanda:

    $ mv [nume fișier 1] [nume fișier 2]

Cu toate acestea, dacă al doilea fișier nu există, primul fișier este redenumit folosind numele celui de al doilea fișier trecut în comandă. Dar dacă al doilea fișier există, atunci conținutul său este înlocuit cu cel al primului fișier. De asemenea, puteți utiliza comanda mutare și la directoare.

    $ mv [nume director 1] [nume director 2]

Similar cu modul în care comanda de mutare gestionează fișierele, dacă al doilea director nu există, primul director este pur și simplu redenumit. Totuși, dacă al doilea director există, conținutul primului director este mutat în al doilea director.

## Elimină / Șterge

Dacă doriți să eliminați fișiere sau directoare, trebuie să folosiți următoarea comandă:

    rm

Când ștergeți un fișier, comanda arată astfel:

    $ rm [nume fișier]

Sau dacă ștergeți un director:

    $ rm [nume director]

În plus, puteți elimina mai multe fișiere și directoare simultan:

    $ rm [nume fișier 1] [nume fișier 2]

## Caractere speciale

Ocazional, fișierele și directoarele cu caractere speciale sau spații prezintă o problemă. În aceste cazuri, utilizați ghilimele. De exemplu:

> Notă: Trebuie folosite ghilimelele din alfabetul englezesc: `""`. Ghilimelele fin alfabetul românesc nu funcționează în linia de comanmdă: `„”`

    $ cd /calea/către/director/"Documentele mele"

Rularea comenzii fără ghilimele nu se va efectua și astfel nu vei putea merge la acel director.

# Istorie

Când utilizați o distribuție GNU/Linux, este necesar să folosiți și linia de comandă. Uneori, va trebui să cunoașteți istoricul comenzilor rulate în terminal. Vizualizarea comenzilor executate recent este la fel de simplă ca și introducerea lor.

    $ history

Comanda `history` produce o listă care arată numărul comenzii și comanda bash corspunzătoare. Ocazional, acest lucru nu va fi suficient și veți avea nevoie de un istoric marcat de timp. În acest caz, rulați comanda:

    $ histtimeformat

Apoi, veți vedea o listă a istoricului comenzilor cu date și ore. Uneori poate doriți să căutați o comandă. Acest lucru este total fezabil cu linia de comandă. Doar folosiți combinația de taste `CTRL+R` – după care veți vedea un mesaj în care scrie:

    $ reverse-i-search

Acum puteți începe să căutați comenzi.

# Comenzi multimple

Deși puteți introduce comenzile în terminal separate, acestea pot fi rulate și împreună - mai multe comenzi odată. Acest lucru este util în special atunci când instalați sau actualizați programe. Astfel, puteți efectua ambele acțiuni simultan.

    $ sudo apt update && sudo apt upgrade

Pe lângă ampersand dublu, puteți rula comenzi împreună în linia de comandă cu punct și virgulă.

    $ sudo apt update ; sudo apt upgrade

Aceasta efectuează aceași acțiune. Comanda `apt` se folosește în Debian, Ubuntu și derivate. Dar combinarea de mai multe comenzi în linia de terminal se poate face în orice distribuție GNU/Linux.

# Adăugați un PPA (depozit terț)

> Notă: Acest sfat se aplică doar pentru Ubuntu și derivate

Un PPA este o arhivă personală de pachete sau un depozit de programe care nu este inclus în instalarea implicită a sistemului de operare Ubuntu.

Pentru a instala un program care nu se află în depozitul oficial, va trebui să adăugați un PPA. La fel ca multe acțiuni în GNU/Linux, aceasta este de obicei efectuată cu linia de comandă.

    $ sudo add-apt-repository [numele depozitului]

Una dintre opțiunile mele preferate este să instalez ultima versiune a programului GIMP, care îl folosesc pentru a edita imagini. Ultima versiune de GIMP necesită o adăugare de PPA înainte de a fi complet instalat.

    $ sudo add-apt-repository ppa:ubuntuhandbook1/gimp

# Reluare comandă

Una dintre cele mai utile comenzi pentru a vă ajuta cu adevărat să stăpâniți linia de comandă este capacitatea de a repeta comenzi. O metodă comună este tastarea:

    $ !!

Rulați comanda încă o dată adăugând de data aceasta permisiuni de super utilizator. Vă scutește de a fi nevoie să scrieți totul din nou.

    $ sudo !!

Dar aceasta nu este singura metodă de a repeta comenzile. De asemenea, puteți utiliza săgeata în sus pentru a vedea comenzile introduse anterior și puteți apăsa pe `Enter` pentru a le executa din nou.

În mod similar, puteți introduce:

    $ !-1

Alternativ, apăsând combinația de taste `Ctrl + P` se afișează comanda executată anterior. Apoi, apăsați pe `Enter` pentru a o executa.

# Listă

O comandă de bază, dar incredibil de utilă este funcția listă. Aceasta prezintă o listă în terminal care arată directoarele majore dintr-un anumit sistem de fișiere.

    $ ls
    
    $ ls /apps

Comanda de mai sus afișează o listă cu toate directoarele din directorul `/apps`.

Linia de comandă GNU/Linux poate părea descurajantă la început. Dar nu este atât de complicat pe cât ar putea părea. Gestionarea fișierelor și directoarelor, vizualizarea istoricului comenzilor și înșiruirea comenzilor se numără printre cele mai comune utilizări ale terminalului.
